# Master (will become release 2.11)

- ...

# 2.10 Release

- The user interface of `Dune::Fufem::Forms` is now fully documented
  in doxygen. The doxygen documentation was slightly improved to make
  this easier to discover.

- Dune-fufem now requires the dune-vtk module.

- Dune-fufem no longer supports dune version 2.8.

- `QuadratureRuleKey` now uses order `k` for Qk-like polynomials.
  Since all quadrature rules exploit the tensor structure,
  an order `k` quadrature rule in fact integrates tensor
  polynomials with order `k` in each factor exactly.
  This in many cases reduces the deduced quadrature order
  while still being exact on the desired polynomial space.
  Especially in 3d, the reduction of quadrature points
  may improve performance significantly.

- `Dune::Fufem::markBoundaryPatchDofs()` was added and supersedes the deprecated
  `::constructBoundaryDofs()`. The new version only supports dune-functions
  bases.

- The various `DuneFunctionsOperatorAssembler::assemble*()` methods as well as
  `assembleGlobalBasisTransferMatrix()` now allow to pass a `MatrixBackend`
  or a plain ISTL matrix. In the latter case it is internally wrapped using
  `Dune::Functions::toMatrixBackend()`.

- The new utility functions `Dune::Fufem::toMatrixBackend<RowBasis, ColBasis>(m)`
  and `Dune::Fufem::toConstMatrixBackend<RowBasis, ColBasis>(m)`
  wrap a given matrix object `m` into a `Dune::Fufem::ISTLMatrixBackend`
  suitable for multi-index access from given basis types,
  if it does not satisfy the corresponding concept yet.

- The new utility function `Dune::Fufem::makeAdolCFunction(signatureTag, f)` turns
  a given function `f` into a once or twice differentiable function providing the corresponding
  dune-functions interface. Derivatives are implemented using AdolC's automated differentiation.

- Support for multi-threaded assembly of operators and functionals was added. To use it
  one first has to compute a coloring using `Dune::Fufem::coloredGridViewPartition(gridView)`.
  The obtained partition can then be passed to the global assembler to enable multi-threaded
  assembling. Notice that one has to make sure that the local assembler is thread safe.

- Support code for thread parallel grid algorithms was added. E.g. the new
  `Dune::Fufem::coloredGridViewPartition(gridView)` function computes colorings
  of the elements in a grid view. Such colorings can be used in grid based
  algorithms using `Dune::Fufem::parallelAlgorithm(threadCount, alg)`.

- `Dune::Fufem::MassAssembler` and `Dune::Fufem::LaplaceAssembler` no longer zero-initialize
  the local matrix, because the global assembler already does this. This may have an impact
  on user code that uses these assemblers otherwise then directly handing it to the global
  assembler, because one now needs to manually zero-initialize before calling the assembler.

- The error-measuring routines in the file `discretizationerror.hh` now take
  `dune-functions`-style functions.  The methods have been moved into the namespace `Dune::Fufem`.
  The method `computeL2Error` has been replaced by `computeL2DifferenceSquared`,
  which returns the *square* of what the old method returned.

- The method `computeH1HalfNormDifferenceSquared` from the file `discretizationerror.hh`
  has been removed completely.  Please replace calls to that method by
  `computeL2DifferenceSquared(derivative(f1), derivative(f2), quadKey);`.

## Deprecations

- `::constructBoundaryDofs()` (for old dune-fufem and new dune-functions bases)
  in global scope is deprecated. Please use `Dune::markBoundaryPatchDofs()` instead.

- Objects of type `VirtualFunction` and `DifferentiableVirtualFunction` cannot be created
  from embedded Python anymore.  Both have been deprecated since before the release of
  `dune-common` 2.7.

- The following implementations of the deprecated `Dune::VirtualFunction` interface
  have been removed: `ConstantFunction`, `SumFunction`, `SumGridFunction`,
  `ComposedFunction`, `ComposedGridFunction`, `Polynomial`.

- The header `dune/fufem/concept.hh` has been deprecated. Please use the concept utilities
  from `dune/common/concept.hh` instead.

- The header `dune/fufem/assemblers/istlbackend.hh` is deprecated. Please include
  `dune/fufem/backends/istlmatrixbackend.hh` instead.




## Removals

- The method `assembleBasisInterpolationMatrix` has been removed.  It only worked
  for old `dune-fufem`-style function space bases.

- The class `AmiraMeshBasisWriter` has been removed.  It relied on AmiraMesh support
  in `dune-grid`, which has been removed before the 2.9 release.

- The class `VTKBasisWriter` has been removed. It only worked
  for old `dune-fufem`-style function space bases.

- The deprecated file `ulis_tools.hh` has been removed.  It contained a short list of
  basic linear algebra methods that are now covered by the `dune-matrix-vector` module.

- The deprecated method `Dune::Fufem::istlVectorBackend` has been removed.
  Please use `Dune::Functions::istlVectorBackend` instead!

- The deprecated class `MassAssembler` (from the global namespace) has been removed.
  It still relied on the old `dune-fufem` function space basis interface.
  Please use `Dune::Fufem::MassAssembler` instead!

- The class `NamedFunctionMap` based on `Dune::VirtualFunction` has been removed.
  Use `std::map<:std::string, std::function<Range(Domain)>>` as a replacement.

- The bases `DgPQKBasis` and `P3NodalBasis` have been removed.

- The deprecated class `::LaplaceAssembler` working with old dune-fufem bases has been removed.

- The deprecated `::FractionalMarkingStrategy::mark()` method taking a vector of non-`shared_ptr`s has been removed.

- The deprecated constructor `LumpedMassAssembler(int)` has been removed.

- The deprecated `Q1NodalBasis` has been removed.

- The deprecated function call interface of `Python::Reference` has been removed.

- The deprecated method `QuadratureRuleKey::refinement(int)` has been removed.

- The deprecated function `readBoundaryPatch<Grid>` templated by the grid type has been removed.

- The file `utilities/dirichletbcassembler.hh` that relied on the no longer existing AmiraMesh-support
  in dune-grid.

- All support code for Amira and the AmiraMesh format has been removed. This includes:
  - The header `readbitfield.hh` providing `readBitField()` and `readBoundaryPatch()`.
  - The header `boundarywriter.hh` providing `writeBoundary()`, `writeBoundaryField()` and `writeLineSet()`.
  - Support for `amiraPatchFile` in `BoundaryPatchFactory`.
  - Remove support for import and export of `ColorMap` in Amira formats.
  - The `bin/bin/matlab2amiramesh.pl` script.

- The following old-style local assembler implementations have been removed:
  - `assemblers/localassemblers/adolchessianassembler.hh`
  - `assemblers/localassemblers/adolclinearizationassembler.hh`
  - `assemblers/localassemblers/adolclocalenergy.hh`
  - `assemblers/localassemblers/generalizedboundarymassassembler.hh`
  - `assemblers/localassemblers/generalizedlaplaceassembler.hh`
  - `assemblers/localassemblers/geomexactstvenantkirchhofffunctionalassembler.hh`
  - `assemblers/localassemblers/geomexactstvenantkirchhoffoperatorassembler.hh`
  - `assemblers/localassemblers/h1functionalassembler.hh`
  - `assemblers/localassemblers/l2functionalassembler.hh`
  - `assemblers/localassemblers/lumpedmassassembler.hh`
  - `assemblers/localassemblers/neumannboundaryassembler.hh`
  - `assemblers/localassemblers/secondorderoperatorassembler.hh`
  - `assemblers/localassemblers/strainproductassembler.hh`
  - `assemblers/localassemblers/stvenantkirchhoffassembler.hh`
  - `assemblers/localassemblers/subgridgeneralizedlaplaceassembler.hh`
  - `assemblers/localassemblers/subgridh1functionalassembler.hh`
  - `assemblers/localassemblers/subgridl2functionalassembler.hh`
  - `assemblers/localassemblers/viscosityassembler.hh`
  - `assemblers/localassemblers/vvlaplaceassembler.hh`
  - `assemblers/localassemblers/vvmassassembler.hh`
  - `assemblers/localassemblers/weightedmassassembler.hh`
  - `assemblers/localassemblers/gradientassembler.hh`
  - `assemblers/localassemblers/normalstressboundaryassembler.hh`
  - `assemblers/localassemblers/vonmisesstressassembler.hh`
  - `assemblers/localassemblers/scaledsumfunctionalassembler.hh`
  - `assemblers/localassemblers/scaledsumoperatorassembler.hh`
  - `assemblers/localassemblers/dunefunctionsl2functionalassembler.hh`
  - `assemblers/localassemblers/convolutionassembler.hh`
  - `mechanics/newpfeassembler.hh`

- The local `InteriorPenaltyDGAssembler` has lost its template parameters
  for the `LocalFiniteElement` type and no longer implements the old
  local assembler interface but is still usable with the new global assemblers.

- The old global and local assembler framework has been removed. This includes:
  - The local assembler base classes `LocalAssembler`, `LocalOperatorAssembler`,
    `LocalFunctionalAssembler`, and `LocalBoundaryAssembler`.
  - The global assembler classes `Assembler`, `BoundaryFunctionalAssembler`,
    `BoundaryOperatorAssembler`, `FunctionalAssembler`, `IntegralOperatorAssembler`,
    `IntersectionOperatorAssembler`, `OperatorAssembler`, and `PreconditionedDefectAssembler`.
  - The global assembler-like classes `DifferenceNormSquared`, `SurfMassMatrix`,
    `HerarchicalEstimator`, and `HierarchicEstimatorBase`


# 2.9 Release

- Various improvements to the `MappedMatrix` class
    - `MappedMatrix` objects can now be printed using `printmatrix` (from `dune-istl`).
    - Iteration over rows is now implemented.  This implies that range-based `for`-loops
      over rows also work.


# 2.8 Release

- constructBoundaryDofs:
    - Small interface change in the template parameters: drop `blocksize` and replace it by `BitSetVector`
    - The method can now handle generic `dune-functions` basis types, as long as we have consistency in the data types

- assembleGlobalBasisTransferMatrix:
    - Support for all bases in `dune-functions` compatible form added

- `istlMatrixBackend` can now be used with `MultiTypeBlockMatrix`

- The class `DuneFunctionsLocalMassAssembler` has been renamed to `MassAssembler`,
  moved into the namespace `Dune::Fufem`, and to the file `massassembler.hh`.
  The old class is still there, but it is deprecated now.
