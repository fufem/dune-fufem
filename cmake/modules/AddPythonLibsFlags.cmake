#
# Module providing convenience methods for compile binaries with python support.
#
# Provides the following functions:
#
# add_dune_python_flags(target1 target2 ...)
#
# adds python flags to the targets for compilation and linking
#

function(add_dune_pythonlibs_flags _targets)
    foreach(_target ${_targets})
        if(Python3_FOUND)
            target_link_libraries(${_target} PUBLIC ${Python3_LIBRARIES})
            set_property(TARGET ${_target} APPEND PROPERTY INCLUDE_DIRECTORIES ${Python3_INCLUDE_DIRS})
            set_property(TARGET ${_target} APPEND PROPERTY COMPILE_DEFINITIONS "HAVE_PYTHON")
            set_property(TARGET ${_target} APPEND PROPERTY COMPILE_OPTIONS "-fno-strict-aliasing")
        endif()
    endforeach(_target ${_targets})
endfunction(add_dune_pythonlibs_flags)
