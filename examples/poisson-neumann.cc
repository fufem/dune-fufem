// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>
#include <cmath>

#include <dune/common/bitsetvector.hh>
#include <dune/common/version.hh>
#include <dune/common/stringutility.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/datacollectors/lagrangedatacollector.hh>
#else
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#endif

#include <dune/fufem/logger.hh>
#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/forms/forms.hh>

#include "utilities.hh"

using namespace Dune;



// Create a callback that applies the L^2 projection into the function
// with zero mean.
template<class Vector, class Basis, class... Args>
auto makeZeroMeanProjection(const Basis& basis, Args&&... args)
{
  using namespace ::Dune::Fufem::Forms;

  Vector massVector;

  auto globalAssembler = GlobalAssembler(basis, basis, 0, std::forward<Args>(args)...);
  globalAssembler.assembleFunctional(massVector, integrate(testFunction(basis)));

  double area = 0;
  for(const auto& mi: massVector)
    area += mi;
  std::cout << area << std::endl;
  massVector /= area;

  return [massVector](auto& x) {
    auto correction = -(massVector*x);
    for(auto& x_i : x)
      x_i += correction;
  };
}



// A preconditioner that composes another preconditioner with
// a projection operator
template<class V, class Preconditioner, class Projection>
class ProjectedPreconditioner : public Dune::Preconditioner<V,V> {

public:

  ProjectedPreconditioner (const V& x, Preconditioner pre, Projection proj) :
    preconditioner_(std::move(pre)),
    projection_(std::move(proj))
  {}

  void pre (V& x, V& b) override
  {
    using namespace Dune::Indices;
    preconditioner_.pre(x,b);
  }

  void apply (V& v, const V& d) override
  {
    using namespace Dune::Indices;
    preconditioner_.apply(v, d);
    projection_(v);
  }

  void post ([[maybe_unused]] V& x) override
  {}

  Dune::SolverCategory::Category category() const override
  {
    return Dune::SolverCategory::sequential;
  }

private:
  Preconditioner preconditioner_;
  Projection projection_;
};



template<int dim>
auto createUniformCubeGrid()
{
  using Grid = Dune::YaspGrid<dim>;
  Dune::FieldVector<double,dim> l(1.0);
  std::array<int,dim> elements = {{2, 2}};
  return std::make_unique<Grid>(l, elements);
}

#if HAVE_DUNE_UGGRID
auto createMixedGrid()
{
  using Grid = Dune::UGGrid<2>;
  Dune::GridFactory<Grid> factory;
  for(unsigned int k : Dune::range(9))
    factory.insertVertex({0.5*(k%3), 0.5*(k/3)});
  factory.insertElement(Dune::GeometryTypes::cube(2), {0, 1, 3, 4});
  factory.insertElement(Dune::GeometryTypes::cube(2), {1, 2, 4, 5});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {3, 4, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 7, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 5, 7});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {5, 8, 7});
  return std::unique_ptr<Grid>{factory.createGrid()};
}
#endif



int main (int argc, char *argv[]) try
{

  auto log = Dune::Fufem::makeLogger(std::cout, "[% 8.3f] [% 8.3f] ");

  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  std::size_t refinements = 4;
  if (argc>2)
    refinements = std::stoul(std::string(argv[2]));

  log("Command line processed");

  MPIHelper::instance(argc, argv);

  log("MPI initialized");

  ///////////////////////////////////
  //   Generate the grid
  ///////////////////////////////////

#if HAVE_DUNE_UGGRID
  auto grid = createMixedGrid();
#else
  auto grid = createUniformCubeGrid<2>();
#endif

  log("Grid created");

  grid->globalRefine(refinements);

  log(Dune::formatString("Grid refined %d times", refinements));

  auto gridView = grid->leafGridView();
  using GridView = decltype(gridView);

  /////////////////////////////////////////////////////////
  //   Choose a finite element space
  /////////////////////////////////////////////////////////

  using Basis = Dune::Functions::LagrangeBasis<GridView,2>;
  Basis basis(gridView);

  log(Dune::formatString("Basis created with dimension %d", basis.dimension()));
  
  /////////////////////////////////////////////////////////
  //   Stiffness matrix and right hand side vector
  /////////////////////////////////////////////////////////

  using Vector = Dune::BlockVector<double>;
  using BitVector = std::vector<bool>;
  using Matrix = Dune::BCRSMatrix<double>;

  Vector rhs;
  Vector sol;
  BitVector isConstrained;
  Matrix stiffnessMatrix;

  /////////////////////////////////////////////////////////
  //  Assemble the system
  /////////////////////////////////////////////////////////

  // The right hand side satisfies the zero-mean compatibility
  // condition of a pure Neumann problem
  auto rightHandSide = [] (auto x) {
    x -= .5;
    return ((x.infinity_norm() < .25) - .25)*20;
  };

// Disable parallel assembly for UGGrid before 2.10
#if DUNE_VERSION_GT(DUNE_FUNCTIONS, 2, 9)
  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(gridView);
  log("Grid coloring computed");

  auto globalAssembler = GlobalAssembler(basis, basis, 0, std::cref(gridViewPartition), threadCount);
#else
  auto globalAssembler = GlobalAssembler(basis, basis, 0);
#endif

  {
    using namespace ::Dune::Fufem::Forms;
    namespace F = ::Dune::Fufem::Forms;

    auto v = testFunction(basis);
    auto u = trialFunction(basis);
    auto f = Coefficient(Functions::makeGridViewFunction(rightHandSide, gridView));

    auto a = integrate(dot(grad(u), grad(v)));
    auto b = integrate(f*v);

    log("Assembler set up");
    globalAssembler.assembleOperator(stiffnessMatrix, a);
    log("Matrix assembled");
    globalAssembler.assembleFunctional(rhs, b);
    log("RHS assembled");
  }

  // *********************************************
  // Initialize solution vector
  // *********************************************

  Dune::Functions::istlVectorBackend(sol).resize(basis);
  Dune::Functions::istlVectorBackend(sol) = 0;

  ////////////////////////////
  //   Compute solution
  ////////////////////////////

  // Technicality:  turn the matrix into a linear operator
  MatrixAdapter<Matrix,Vector,Vector> op(stiffnessMatrix);

#if DUNE_VERSION_GT(DUNE_FUNCTIONS, 2, 9)
  auto zeroMeanProjection = makeZeroMeanProjection<Vector>(basis, std::cref(gridViewPartition), threadCount);
#else
  auto zeroMeanProjection = makeZeroMeanProjection<Vector>(basis);
#endif

// FastAMG is not working for non-blocked matrices in 2.10
#if DUNE_VERSION_GTE(DUNE_ISTL, 2, 11)
  auto preconditioner = ProjectedPreconditioner(sol, makeAMGPreconditioner<Vector>(stiffnessMatrix), zeroMeanProjection);
#else
  // Sequential incomplete LU decomposition as the preconditioner
  auto preconditioner = SeqILDL<Matrix,Vector,Vector>(stiffnessMatrix,1.0);
#endif

  log("Preconditioner created");

  // Preconditioned conjugate-gradient solver
  CGSolver<Vector> cg(op,
                          preconditioner, // preconditioner
                          1e-4, // desired residual reduction factor
                          1000,   // maximum number of iterations
                          2);   // verbosity of the solver

  log("Solver created");

  // Object storing some statistics about the solving process
  InverseOperatorResult statistics;

  // Solve!
  cg.apply(sol, rhs, statistics);

  log("Linear system solved");

  zeroMeanProjection(sol);

  log("Solution projected");

  ////////////////////////////////////////////////////////////////////////////
  //  Make a discrete function from the FE basis and the coefficient vector
  ////////////////////////////////////////////////////////////////////////////

  auto solFunction = Functions::makeDiscreteGlobalBasisFunction<double>(basis, sol);

  //////////////////////////////////////////////////////////////////////////////////////////////
  //  Write result to VTK file
  //////////////////////////////////////////////////////////////////////////////////////////////

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
  using Dune::Vtk::LagrangeDataCollector;
  using Dune::Vtk::UnstructuredGridWriter;
  auto vtkWriter = UnstructuredGridWriter(LagrangeDataCollector(basis.gridView(), 2));
  vtkWriter.addPointData(solFunction, VTK::FieldInfo("sol", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("poisson-neumann");
#else
  // We need to use the SubsamplingVTKWriter, because dune-vtk's LagrangeDataCollector
  // does not work with mixed grids in 2.9.
  SubsamplingVTKWriter<GridView> vtkWriter(gridView, Dune::refinementLevels(2));
  vtkWriter.addVertexData(solFunction, VTK::FieldInfo("sol", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("poisson-neumann");
#endif
  log("Solution written to vtk file");
 }
// Error handling
 catch (Exception& e) {
    std::cout << e.what() << std::endl;
 }
