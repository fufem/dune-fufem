// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>
#include <cmath>

#include <dune/common/bitsetvector.hh>
#include <dune/common/version.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/cholmod.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/datacollectors/discontinuouslagrangedatacollector.hh>
#else
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#endif

#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>
#include <dune/fufem/forms/forms.hh>

#include "utilities.hh"



int main (int argc, char *argv[]) try
{
  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  // Set up MPI, if available
  Dune::MPIHelper::instance(argc, argv);

  // *********************************************
  // Problem parameters
  // *********************************************

  // We simulate a steel beam clamped on one end
  // and deformed by gravity.
  // The beam has a length 1m and a quadratic profile
  // of thickness 1cm.

  // Spatial dimension
  const int dim = 3;

  // Vector type for spatial vector (e.g. coordinates)
  using LocalVector = Dune::FieldVector<double,dim>;

  // Standard acceleration of gravity
  double g_n = 9.80665;  // in m/s^2 = N/kg

  // Density, Young's modulus, and Poisson ratio of steel
  double rho = 7.85e3;   // in kg/m^3 = 10^3 g/cm^3
  double E = 200e9;      // in Pa = N/m^2 = 10^9 GPa
  double nu = 0.3;

  // Gravitational force in y-direction
  auto g = LocalVector(0.);
  g[1] = -g_n*rho;

  // Lamé parameters for St.Venant-Kirchhoff
  double lambda = E*nu/((1.-2.*nu) * (1.+nu));
  double mu = E/(2*(1+nu));

  // Domain length and width
  double l = 1;
  double w = .01;

  // Indicator function for Dirichlet boundary
  auto dirichletIndicatorFunction = [](auto x) { return x[0] <= 1e-10; };

  // Dirichlet values (zero for clamped boundary condition)
  auto dirichletValues = [](const auto& x){ return LocalVector(0.0); };

  // *********************************************
  // Discretization parameters
  // *********************************************

  // Ansatz order
  const int order = 2;

  // Elements per direction
  unsigned int elements_w = 1;
  unsigned int elements_l = l/w * elements_w;

  // Number of grid refinements
  std::size_t refinements = 1;
  if (argc>2)
    refinements = std::stoul(std::string(argv[2]));


  // *********************************************
  // Setup grid and finite element basis
  // *********************************************

  using Grid = Dune::YaspGrid<dim>;
  auto lowerCorner = LocalVector(0);
  auto upperCorner = LocalVector(w);
  upperCorner[0] = l;
  auto elements = std::array<unsigned int, dim>();
  std::fill(elements.begin(), elements.end(), elements_w);
  elements[0] = elements_l;

  auto grid = Dune::StructuredGridFactory<Grid>::createCubeGrid(lowerCorner, upperCorner, elements);

  grid->globalRefine(refinements);

  using namespace Dune::Functions::BasisFactory;

  auto basis = makeBasis(grid->leafGridView(), power<dim>(lagrange<order>()));

  std::cout << "Dimension of FE space is " << basis.dimension() << std::endl;

  // *********************************************
  // Define matrix and vector types and objects
  // *********************************************

  using Vector = Dune::BlockVector<LocalVector>;
  using BitVector = std::vector<std::array<char,dim>>;
  using LocalMatrix = Dune::FieldMatrix<double,dim,dim>;
  using Matrix = Dune::BCRSMatrix<LocalMatrix>;

  Vector rhs;
  Vector sol;
  BitVector isConstrained;
  Matrix stiffnessMatrix;

  // *********************************************
  // Assemble the system
  // *********************************************

  auto dirichletPatch = BoundaryPatch(basis.gridView());
  dirichletPatch.insertFacesByProperty([&](auto&& intersection) { return dirichletIndicatorFunction(intersection.geometry().center()); });

  auto rightHandSide = [g](const auto& x) { return g; };

  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(basis.gridView());
  auto globalAssembler = GlobalAssembler(basis, basis, 1, std::cref(gridViewPartition), threadCount);

  {
    using namespace Dune::Fufem::Forms;

    // Identity matrix
    auto Id = Dune::ScaledIdentityMatrix<double,dim>{1.0};

    // Symmetric gradient operator
    auto E = [&](const auto& v) {
      return symmetrize(grad(v));
    };

    auto u = trialFunction(basis);
    auto v = testFunction(basis);

    auto f = Coefficient(Dune::Functions::makeGridViewFunction(rightHandSide, basis.gridView()));

    // Use St. Venant-Kirchhoff material
    auto sigma = 2*mu*E(u) + lambda*Id*trace(E(u));

    auto a = integrate(dot(sigma, E(v)));

    // Alternatively we could use (and this is even a bit faster)
    // auto C = RangeOperator([&](const auto& e) {
    //   return 2*mu*e + lambda*LocalMatrix(Id)*trace(e);
    // });
    // auto a = integrate(dot(C(E(u)), E(v)));

    auto b = integrate(dot(f,v));

    globalAssembler.assembleOperator(stiffnessMatrix, a);
    globalAssembler.assembleFunctional(rhs, b);
  }

  // *********************************************
  // Initialize solution vector
  // *********************************************

  Dune::Functions::istlVectorBackend(sol).resize(basis);
  Dune::Functions::istlVectorBackend(sol) = 0;

  // *********************************************
  // Incorporate Dirichlet boundary conditions
  // *********************************************

  Dune::Functions::istlVectorBackend(isConstrained).resize(basis);
  Dune::Functions::istlVectorBackend(isConstrained) = 0;
  Dune::Fufem::markBoundaryPatchDofs(dirichletPatch, basis, isConstrained);

  interpolate(basis, sol, dirichletValues, isConstrained);

  incorporateEssentialConstraints(stiffnessMatrix, rhs, isConstrained, sol);


  // *********************************************
  // Solve linear system
  // *********************************************

  {
#if HAVE_SUITESPARSE_CHOLMOD
    // If available use pparse Cholesky using Cholmod
    auto solver = Dune::Cholmod<Vector>();
    solver.setMatrix(stiffnessMatrix);
#else
    // Otherwise use preconditioned conjugate-gradient solver with ILDL preconditioner
    Dune::MatrixAdapter<Matrix,Vector,Vector> op(stiffnessMatrix);
    Dune::SeqILDL<Matrix,Vector,Vector> ildl(stiffnessMatrix,1.0);
    auto solver = Dune::CGSolver<Vector>(
                            op,   // operator
                            ildl, // preconditioner
                            1e-4, // desired residual reduction factor
                            1000, // maximum number of iterations
                            2);   // verbosity of the solver
#endif

    // Object storing some statistics about the solving process
    Dune::InverseOperatorResult statistics;

    // Solve!
    solver.apply(sol, rhs, statistics);
  }

  // *********************************************
  // Write solution as VTK
  // *********************************************

  {
    using namespace Dune::Fufem::Forms;

    auto u = bindToCoefficients(trialFunction(basis), sol);

    auto Id = LocalMatrix(Dune::ScaledIdentityMatrix<double,dim>{1.0});

    auto E = [&](const auto& v) {
      return symmetrize(grad(v));
    };

    auto sqrt = RangeOperator([](const auto& z) { return std::sqrt(z); });

    auto sigma = 2*mu*E(u) + lambda*Id*trace(E(u));

    auto sigma_vol = 1./3. * trace(sigma);

    auto sigma_dev = sigma - sigma_vol*Id;

    auto sigma_von_Mises = sqrt(3./2.*Dune::Fufem::Forms::dot(sigma_dev, sigma_dev));

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
    using Dune::Vtk::DiscontinuousLagrangeDataCollector;
    using Dune::Vtk::UnstructuredGridWriter;
    auto vtkWriter = UnstructuredGridWriter(DiscontinuousLagrangeDataCollector(basis.gridView(), order));
    vtkWriter.addPointData(u, Dune::VTK::FieldInfo("u", Dune::VTK::FieldInfo::Type::vector, dim));
    vtkWriter.addPointData(E(u), Dune::VTK::FieldInfo("Eu", Dune::VTK::FieldInfo::Type::tensor, dim*dim));
    vtkWriter.addPointData(sigma, Dune::VTK::FieldInfo("sigma", Dune::VTK::FieldInfo::Type::tensor, dim*dim));
    vtkWriter.addPointData(sigma_vol, Dune::VTK::FieldInfo("sigma_vol", Dune::VTK::FieldInfo::Type::scalar, 1));
    vtkWriter.addPointData(sigma_von_Mises, Dune::VTK::FieldInfo("sigma_von_Mises", Dune::VTK::FieldInfo::Type::scalar, 1));
    vtkWriter.write("linear-elasticity");
#else
    auto vtkWriter = Dune::SubsamplingVTKWriter(basis.gridView(), Dune::refinementLevels(0));
    vtkWriter.addVertexData(u, Dune::VTK::FieldInfo("sol", Dune::VTK::FieldInfo::Type::vector, dim));
    vtkWriter.addVertexData(sigma_vol, Dune::VTK::FieldInfo("sigma_vol", Dune::VTK::FieldInfo::Type::scalar, 1));
    vtkWriter.addVertexData(sigma_von_Mises, Dune::VTK::FieldInfo("sigma_von_Mises", Dune::VTK::FieldInfo::Type::scalar, 1));
    vtkWriter.write("linear-elasticity");
#endif

  }

}
catch (Dune::Exception& e) {
  std::cout << e.what() << std::endl;
}
