// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>
#include <cmath>

#include <dune/common/bitsetvector.hh>
#include <dune/common/stringutility.hh>
#include <dune/common/version.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/umfpack.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>


#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/datacollectors/lagrangedatacollector.hh>
#include <dune/vtk/datacollectors/discontinuouslagrangedatacollector.hh>
#else
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#endif

#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>
#include <dune/fufem/forms/forms.hh>

#include "utilities.hh"

// This thin wrapper provides operator* for callables.
// This is helpful for the Dune::Fufem::Forms framework
// which is based on products of multilinear operators.
// If the range space of one of these operators consists
// e.g. of linear maps A between matrix spaces (i.e. 4th
// order tensors), this wrapper allows to still use
// the A*X syntax which allows to hook into the product
// framework.
template<class Base>
class LinearOperatorView : public Base
{
public:
  LinearOperatorView(Base&& base) :
    Base(std::move(base))
  {}

  template<class X,
    class Dummy = std::void_t<decltype(std::declval<Base>()(std::declval<X>()))>>
  friend auto operator*(const LinearOperatorView& L, const X& x)
  {
    return L(x);
  }

};

template<class Base>
LinearOperatorView(Base&&) -> LinearOperatorView<Base>;


// *********************************************
// Solve linear system
// *********************************************

template<class Matrix, class Vector>
void solve(const Matrix& matrix, Vector& rhs, Vector& sol)
{
#if HAVE_UMFPACK
  auto solver = Dune::UMFPack<Matrix>();
  solver.setMatrix(matrix);
#else
  Dune::MatrixAdapter<Matrix,Vector,Vector> stiffnessOperator(matrix);
  Dune::Richardson<Vector,Vector> preconditioner(1.0);
  Dune::RestartedGMResSolver<Vector> solver(
          stiffnessOperator,  // operator to invert
          preconditioner,     // preconditioner for iteration
          1e-10,              // desired residual reduction factor
          500,                // number of iterations between restarts
          500,                // maximum number of iterations
          2);                 // verbosity of the solver
#endif

  Dune::InverseOperatorResult statistics;
  solver.apply(sol, rhs, statistics);
}



// *********************************************
// Write solution as VTK
// *********************************************

template<class Basis, class Vector>
void write(const Basis& basis, const Vector& sol, std::string fileName)
{
  using namespace Dune::Fufem::Forms;
  const int dim = Basis::GridView::dimension;

  auto Nabla = [](auto&& op) { return jacobian(op); };
  auto Id = Dune::FieldMatrix<double,dim,dim>(Dune::ScaledIdentityMatrix<double,dim>{1.0});
  auto det = RangeOperator([](auto&& A) { return ::det(A); });
  auto cof = RangeOperator([](auto&& A) { return ::Ddet(A); });
  auto transpose = [](auto&& A) { return Dune::Fufem::Forms::transpose(A); };

  auto u = bindToCoefficients(trialFunction(basis), sol);

  auto F = compose([&](const auto& Grad_u) { return Grad_u + Id; }, Nabla(u));
  auto C = transpose(F)*F;

  auto I1 = trace(C);
  auto I2 = trace(cof(C));
  auto I3 = det(C);

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
  using Dune::Vtk::DiscontinuousLagrangeDataCollector;
  using Dune::Vtk::UnstructuredGridWriter;
  auto vtkWriter = UnstructuredGridWriter(DiscontinuousLagrangeDataCollector(basis.gridView(), 2));

  vtkWriter.addPointData(u, Dune::VTK::FieldInfo("u", Dune::VTK::FieldInfo::Type::vector, dim));
  vtkWriter.addPointData(I1, Dune::VTK::FieldInfo("I1", Dune::VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.addPointData(I2, Dune::VTK::FieldInfo("I2", Dune::VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.addPointData(I3, Dune::VTK::FieldInfo("I3", Dune::VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.addPointData(F, Dune::VTK::FieldInfo("F", Dune::VTK::FieldInfo::Type::tensor, 9));
  vtkWriter.addPointData(C, Dune::VTK::FieldInfo("C", Dune::VTK::FieldInfo::Type::tensor, 9));
  vtkWriter.addPointData(jacobian(u), Dune::VTK::FieldInfo("Du", Dune::VTK::FieldInfo::Type::tensor, 9));

  vtkWriter.write(fileName);
#else
  auto vtkWriter = Dune::SubsamplingVTKWriter(basis.gridView(), Dune::refinementLevels(1));
  vtkWriter.addVertexData(u, Dune::VTK::FieldInfo("displacement", Dune::VTK::FieldInfo::Type::vector, dim));
  vtkWriter.addVertexData(I1, Dune::VTK::FieldInfo("I1", Dune::VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.addVertexData(I2, Dune::VTK::FieldInfo("I2", Dune::VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.addVertexData(I3, Dune::VTK::FieldInfo("I3", Dune::VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write(fileName);
#endif
}



int main (int argc, char *argv[]) try
{
  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  // Set up MPI, if available
  Dune::MPIHelper::instance(argc, argv);

  checkDet();

  // *********************************************
  // Problem parameters
  // *********************************************

  // We simulate a beam that is clamped on both ends
  // and twisted. To aid the solver we apply a homotopy
  // wrt the rotation angle.

  // Spatial dimension
  const int dim = 3;

  // Vector type for spatial vector (e.g. coordinates)
  using LocalVector = Dune::FieldVector<double,dim>;
  using LocalMatrix = Dune::FieldMatrix<double,dim,dim>;

  // Standard acceleration of gravity
  double g_n = 9.80665;  // in m/s^2 = N/kg

  // Density, Young's modulus, and Poisson ratio of steel
  double rho = 7.85e3;   // in kg/m^3 = 10^3 g/cm^3
  double E = 200e9;      // in Pa = N/m^2 = 10^-9 GPa
  double nu = 0.3;

  // Gravitational force in y-direction
  auto g = LocalVector(0.);
  g[1] = -g_n*rho;

  // Lamé parameters
  double lambda = E*nu/((1.-2.*nu) * (1.+nu));
  double mu = E/(2*(1+nu));

  // Domain length and width
  double l = .1;
  double w = .01;

  // Indicator function for Dirichlet boundary on both ends
  auto dirichletIndicatorFunction = [&](auto x) { return (x[0] <= 1e-10) or (x[0] >= l-1e-10); };

  // Dirichlet values (rotate by alpha on one end)
  double alpha = std::acos(-1)*.5;
  auto dirichletValues = [&](auto x){
    if (x[0] <= 1e-10)
      return LocalVector(0.0);
    else
    {
      auto c = LocalVector({x[0], w/2., w/2.});
      auto R = LocalMatrix({{1.,0.,0.}, {0., std::cos(alpha), -std::sin(alpha)}, {0., std::sin(alpha), std::cos(alpha)}});
//      auto u = R*(x-c)+c-x;
      auto u = LocalVector();
      R.mv(x-c,u);
      u += c-x;
      return u;
    }
  };

  auto rightHandSide = [&](const auto& x) { return 0*g; };

  // *********************************************
  // Density of Neo-Hook material
  // *********************************************

  // Dependence on F = det(F) = det(Id+grad(u))
  auto gamma = [&](auto J) {
    return lambda/2. * (J-1)*(J-1) - mu*std::log(J);
  };

  auto Dgamma = [&](auto J) {
    return lambda * (J-1) - mu/J;
  };

  auto DDgamma = [&](auto J) {
    return lambda + mu/(J*J);
  };

  // Wrap density and its derivatives using RangeOperator.
  // That way it can directly be applied (pointwise) to
  // multilinear differential operators in the Dune::Fufem::Forms
  // framework instead of manually having to call compose(...).
  using namespace Dune::Fufem::Forms;

  // Density for deformation gradient F = Id+grad(u)
  [[maybe_unused]] auto W = RangeOperator([&](const auto& F) {
    return gamma(det(F)) + mu/2.*(dot(F,F) - dim);
  });

  // First derivative is a matrix
  auto DW = RangeOperator([&](const auto& F) {
    return Dgamma(det(F))*Ddet(F) + mu*F;
  });

  // The second derivative is a rank 4 tensor.
  // Since there's no dedicated class to represent this
  // we use a slim wrapper that allows to provide the
  // matrix-to-matrix linear maps by callable. The wrapper
  // then add operator* support.
  auto DDW = RangeOperator([&](const auto& F) {
    auto dot = Dune::Fufem::Forms::LocalOperators::DotOp();
    auto J = det(F);
    auto cof_F = Ddet(F);
    auto A = DDgamma(J)*cof_F;
    auto B = Dgamma(J)*DDdet(F);
    // Use wrapper to provide operator* for the resulting tensor
    // which represents the linear map mapping U to
    // DDgamma(J)*dot(cof_F,U)*cof_F + Dgamma(J)*DDdet(F)(U) + mu*U;
    return LinearOperatorView([=](const auto& U) {
      return A*dot(cof_F,U) + B*U + mu*U;
    });
  });

  // *********************************************
  // Discretization parameters
  // *********************************************

  // Ansatz order
  const int order = 2;

  // Elements per direction
  unsigned int elements_w = 1;
  unsigned int elements_l = l/w * elements_w;

  // Number of grid refinements
  std::size_t refinements = 1;
  if (argc>2)
    refinements = std::stoul(std::string(argv[2]));

  // *********************************************
  // Solver parameters
  // *********************************************

  // Define a homotopy callback function
  auto alpha_target = alpha;
  auto homotopy = [&](auto t) {
    alpha = alpha_target*t;
    std::cout << "Homotopy step: Using angle " << alpha << std::endl;
  };
  auto homotopySteps = 10;

  // Damping parameter
  double damping = 1.0;

  // Relative tolerance for fixed point iteration
  double relTol = 1.0e-10;

  // Relative tolerance for fixed point iteration
  std::size_t maxIter = 100;


  // *********************************************
  // Setup grid and finite element basis
  // *********************************************

  using Grid = Dune::YaspGrid<dim>;
  auto lowerCorner = LocalVector(0);
  auto upperCorner = LocalVector(w);
  upperCorner[0] = l;
  auto elements = std::array<unsigned int, dim>();
  std::fill(elements.begin(), elements.end(), elements_w);
  elements[0] = elements_l;

  auto grid = Dune::StructuredGridFactory<Grid>::createCubeGrid(lowerCorner, upperCorner, elements);

  grid->globalRefine(refinements);

  using namespace Dune::Functions::BasisFactory;

  auto basis = makeBasis(grid->leafGridView(), power<dim>(lagrange<order>()));

  std::cout << "Dimension of FE space is " << basis.dimension() << std::endl;

  // *********************************************
  // Define matrix and vector types and objects
  // *********************************************

  using Vector = Dune::BlockVector<LocalVector>;
  using BitVector = std::vector<std::array<char,dim>>;
  using Matrix = Dune::BCRSMatrix<LocalMatrix>;

  Vector rhs;
  Vector sol;
  BitVector isConstrained;
  Matrix stiffnessMatrix;

  // *********************************************
  // Initialize solution vector
  // *********************************************

  Dune::Functions::istlVectorBackend(sol).resize(basis);
  Dune::Functions::istlVectorBackend(sol) = 0;

  // *********************************************
  // Setup boundary information
  // *********************************************

  // Create a boundary patch for the Dirichlet boundary
  auto dirichletPatch = BoundaryPatch(basis.gridView());
  dirichletPatch.insertFacesByProperty([&](auto&& intersection) { return dirichletIndicatorFunction(intersection.geometry().center()); });

  // Mark velocity DOFs on Dirichlet boundary
  Dune::Functions::istlVectorBackend(isConstrained).resize(basis);
  Dune::Functions::istlVectorBackend(isConstrained) = 0;
  Dune::Fufem::markBoundaryPatchDofs(dirichletPatch, basis, isConstrained);

  // *********************************************
  // Assemble the system
  // *********************************************

  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(basis.gridView());
  auto globalAssembler = GlobalAssembler(basis, basis, 1, std::cref(gridViewPartition), threadCount);

  // Identity matrix
  auto Id = LocalMatrix(Dune::ScaledIdentityMatrix<double,dim>{1.0});

  auto u = trialFunction(basis);
  auto v = testFunction(basis);

  // Use geometrically linear St. Venant-Kirchhoff operator as norm
  Matrix energyMatrix;
  {
    auto Id = Dune::ScaledIdentityMatrix<double,dim>{1.0};
    auto E = [&](const auto& v) {
      return symmetrize(grad(v));
    };
    auto sigma = 2*mu*E(u) + lambda*Id*trace(E(u));
    globalAssembler.assembleOperator(energyMatrix, integrate(dot(sigma, E(v))));
  }
  auto norm = [&](const auto& v) {
    auto Mv = v;
    energyMatrix.mv(v, Mv);
    return std::sqrt(Mv*v);
  };

  for(auto homotopyStep : Dune::range(homotopySteps))
  {
    std::cout << "****************************************" << std::endl;
    std::cout << "Homotopy step      " << homotopyStep << std::endl;

    // Set homotopy value
    homotopy(homotopyStep/(homotopySteps-1.));

    // Interpolate into constrained DOFs
    interpolate(basis, sol, dirichletValues, isConstrained);

    auto f = Coefficient(Dune::Functions::makeGridViewFunction(rightHandSide, basis.gridView()));

    auto sol_old = sol;
    auto correction = sol;
    auto zero = sol;
    zero *=0;

    double relError = relTol+1;
    std::size_t k = 0;
    while ((relError > relTol) and (k<maxIter))
    {

      // Be careful with the notion of gradients:
      // In Dune::Fufem::Forms grad(..,) is the
      // transposed of the jacobian, where as
      // in mechanics, nabla often denotes the
      // jacobian itself. Hence we call the latter
      // Nabla() in the following.
      auto Nabla = [](auto&& op) { return jacobian(op); };

      // Previous iterate
      sol_old = sol;
      auto u_old = bindToCoefficients(u, sol_old);

      // Linearize around old deformation gradient
      auto F_old = compose([&](const auto& Grad_u) { return Grad_u + Id; }, Nabla(u_old));

      // Compute Newton correction
      {
        auto b = integrate(dot(f,v) - dot(DW(F_old), Nabla(v)));

        auto a = integrate(dot(DDW(F_old)*Nabla(u), Nabla(v)));

        globalAssembler.assembleOperator(stiffnessMatrix, a);
        globalAssembler.assembleFunctional(rhs, b);

        incorporateEssentialConstraints(stiffnessMatrix, rhs, isConstrained, zero);

        solve(stiffnessMatrix, rhs, correction);
      }

      // Apply damped update
      correction *= damping;
      sol = sol_old;
      sol += correction;

      // Write Newton iterate for homotopy step to VTK-file
//      write(basis, sol, Dune::formatString("hyperelasticity-%03d-%03d", homotopyStep, k));

      // Evaluate termination criterion
      relError = norm(correction)/norm(sol);
      std::cout
        << Dune::formatString("%02d", k)
        << Dune::formatString(" Relative norm of Newton damped correction: %010e", relError)
        << std::endl;

      ++k;
    }
    if ((relError > relTol) and (k==maxIter))
      std::cout << "Warning: Fixed point iteration terminated after " << maxIter << " iterations without matching tolerance." << std::endl;

    // Write homotopy step to VTK-file
    write(basis, sol, Dune::formatString("hyperelasticity-%03d", homotopyStep));
  }

  // Write solution to VTK-file
  write(basis, sol, "hyperelasticity");

}
catch (Dune::Exception& e) {
  std::cout << e.what() << std::endl;
}
