// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <array>
#include <vector>

#include <dune/common/bitsetvector.hh>
#include <dune/common/indices.hh>
#include <dune/common/stringutility.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/grid/utility/structuredgridfactory.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/umfpack.hh>
#include <dune/istl/preconditioners.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/subspacebasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>

#include <dune/functions/backends/istlvectorbackend.hh>

#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/dunefunctionsfunctionalassembler.hh>
#include <dune/fufem/backends/istlmatrixbackend.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>
#include <dune/fufem/forms/forms.hh>

#include "utilities.hh"



// Create a callable implementing an energy norm by assembling a bilinear form
template<class Matrix, class Form, class Partition>
auto makeEnergyNorm(Form form, const Partition& coloredGridViewPartition, int threadCount)
{
  const auto& [testBasis, trialBasis] = form.integrandOperator().basis();
  Matrix M;
  auto matrixBackend = Dune::Fufem::istlMatrixBackend(M);
  auto operatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler(testBasis, trialBasis);
  operatorAssembler.assembleBulk(matrixBackend, form, coloredGridViewPartition, threadCount);
  return [M = M](const auto& v) {
    auto Mv = v;
    M.mv(v, Mv);
    return std::sqrt(Mv*v);
  };
}



template<class Matrix, class Vector>
void solve(const Matrix& matrix, Vector& rhs, Vector& sol)
{
#if HAVE_UMFPACK
  auto solver = Dune::UMFPack<Matrix>();
  solver.setMatrix(matrix);
#else
  Dune::MatrixAdapter<Matrix,Vector,Vector> stiffnessOperator(matrix);
  Dune::Richardson<Vector,Vector> preconditioner(1.0);
  Dune::RestartedGMResSolver<Vector> solver(
          stiffnessOperator,  // operator to invert
          preconditioner,     // preconditioner for iteration
          1e-10,              // desired residual reduction factor
          500,                // number of iterations between restarts
          500,                // maximum number of iterations
          2);                 // verbosity of the solver
#endif

  Dune::InverseOperatorResult statistics;
  solver.apply(sol, rhs, statistics);
}



template<class Basis, class Vector>
void write(const Basis& basis, const Vector& sol, std::string fileName)
{
  using namespace Dune::VTK;
  using namespace Dune::Indices;
  using namespace Dune::Fufem::Forms;
  auto dim = Basis::GridView::dimension;

  auto u = trialFunction(basis, _0);
  auto p = trialFunction(basis, _1);

  auto vtkWriter = Dune::SubsamplingVTKWriter(basis.gridView(), Dune::refinementLevels(2));
  vtkWriter.addVertexData(bindToCoefficients(u, sol),       FieldInfo("velocity", FieldInfo::Type::vector, dim));
  vtkWriter.addVertexData(bindToCoefficients(p, sol),       FieldInfo("pressure", FieldInfo::Type::scalar, 1));
  vtkWriter.addVertexData(bindToCoefficients(div(u), sol),  FieldInfo("velocity_divergence", FieldInfo::Type::scalar, 1));
  vtkWriter.addVertexData(bindToCoefficients(grad(p), sol), FieldInfo("pressure_gradient", FieldInfo::Type::vector, dim));
  vtkWriter.write(fileName);
}



int main (int argc, char *argv[]) try
{

  // *********************************************
  // Setup program
  // *********************************************

  // Set up MPI, if available
  Dune::MPIHelper::instance(argc, argv);

  // Read number of threads from command line
  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  std::size_t refinements = 4;
  if (argc>2)
    refinements = std::stoul(std::string(argv[2]));


  // *********************************************
  // Problem data
  // *********************************************

  // Problem dimension
  const int dim = 2;

  // Define types for velocity and pressure values
  using Velocity = Dune::FieldVector<double,dim>;

  // Radius of tubular domain
  double radius = 0.5;

  // Kinematic viscosity
  double nu = .01;

  // Part of the boundary where we impose Dirichlet BCs
  auto dirichletIndicatorFunction = [](auto x) {
    return x[0] < 5;
  };

  // Velocity Dirichlet values
  auto velocityDirichletValues = [&](auto x){
    auto y = x[1]-radius;
    // Poiseuille flow
//    return Velocity{(radius*radius - y*y)/(2.*nu), 0};
    return Velocity{std::sin(y/radius* std::acos(-1)), 0};
  };

  // Velocity Volume forces
  auto velocityVolumeForces = [] (auto x) {
    return Velocity(0.);
  };



  // *********************************************
  // Setup grid
  // *********************************************

  // Generate and refine grid
  using Grid = Dune::YaspGrid<dim>;
  auto grid = Dune::StructuredGridFactory<Grid>::createCubeGrid({{0.,0.}}, {{5.,2*radius}}, {{5,1}});

  grid->globalRefine(refinements);

  auto gridView = grid->leafGridView();

  // Compute coloring for parallel computations
  auto coloredGridViewPartition = Dune::Fufem::coloredGridViewPartition(gridView);

  // *********************************************
  // Solver parameters
  // *********************************************

  // Type of nonlinear solver
  enum SolverType {fixedpoint, newton };
  auto solverType = newton;

  // Damping parameter
  double damping = 1.0;

  // Relative tolerance for fixed point iteration
  double relTol = 1.0e-10;

  // Relative tolerance for fixed point iteration
  std::size_t maxIter = 100;



  // *********************************************
  // Setup discretization
  // *********************************************

  // Create Taylor-Hood space
  using namespace Dune::Indices;
  using namespace Dune::Functions::BasisFactory;
  constexpr std::size_t K = 2;
  auto basis = makeBasis(
          gridView,
          composite(
            power<dim>(
              lagrange<K+1>(),
              flatInterleaved()
            ),
            lagrange<K>(),
            flatLexicographic()
          )
        );

  // For later reference denote the velocity and pressure subspaces
  auto velocityBasis = Dune::Functions::subspaceBasis(basis, _0);
  auto pressureBasis = Dune::Functions::subspaceBasis(basis, _1);

  // Define suitable matrix and vector types
  using Vector = Dune::BlockVector<double>;
  using BitVector = Dune::BlockVector<char>;
  using Matrix = Dune::BCRSMatrix<double>;



  // *********************************************
  // Setup boundary information
  // *********************************************

  // Create a boundary patch for the Dirichlet boundary
  auto dirichletPatch = BoundaryPatch(basis.gridView());
  dirichletPatch.insertFacesByProperty([&](auto&& intersection) { return dirichletIndicatorFunction(intersection.geometry().center()); });

  // Mark velocity DOFs on Dirichlet boundary
  BitVector isConstrained;
  Dune::Functions::istlVectorBackend(isConstrained).resize(basis);
  Dune::Functions::istlVectorBackend(isConstrained) = 0;
  Dune::Fufem::markBoundaryPatchDofs(dirichletPatch, velocityBasis, isConstrained);

  // Interpolate into constrained DOFs
  Vector constraintValues;
  Dune::Functions::istlVectorBackend(constraintValues).resize(basis);
  Dune::Functions::istlVectorBackend(constraintValues) = 0;
  Dune::Functions::interpolate(velocityBasis, constraintValues, velocityDirichletValues, isConstrained);



  // *********************************************
  // Matrices and vectors for dicretization
  // *********************************************

  // Initialize rhs and solution vector
  Vector rhs;
  Dune::Functions::istlVectorBackend(rhs).resize(basis);
  Dune::Functions::istlVectorBackend(rhs) = 0;

  Vector sol;
  Dune::Functions::istlVectorBackend(sol).resize(basis);
  Dune::Functions::istlVectorBackend(sol) = 0;

  // Create stiffness matrix
  Matrix stiffnessMatrix;



  // *********************************************
  // Solve stationare Navier-Stokes problem
  // *********************************************

  auto globalAssembler = GlobalAssembler(basis, basis, 0, std::cref(coloredGridViewPartition), threadCount);

  {
    using namespace Dune::Indices;
    using namespace Dune::Fufem::Forms;

    auto u = trialFunction(velocityBasis);
    auto p = trialFunction(pressureBasis);
    auto v = testFunction(velocityBasis);
    auto q = testFunction(pressureBasis);

    auto f = Coefficient(Dune::Functions::makeGridViewFunction(velocityVolumeForces, gridView));

    // Create a (H^1)^d x L^2 norm for coefficient vectors
    auto norm = makeEnergyNorm<Matrix>(integrate(dot(grad(u), grad(v)) + dot(u,v) + p*q), coloredGridViewPartition, threadCount);

    // Compute divergence free initial value satisfying the Dirichlet BCs
    {
      auto A = integrate( dot(grad(u), grad(v)) - div(u)*q - div(v)*p );
      auto b = integrate( dot(Velocity(0),v) );


      globalAssembler.assembleOperator(stiffnessMatrix, A);
      globalAssembler.assembleFunctional(rhs, b);
      incorporateEssentialConstraints(stiffnessMatrix, rhs, isConstrained, constraintValues);

      solve(stiffnessMatrix, rhs, sol);
    }

    // Auxiliary vector used during iterative solve
    auto sol_old = sol;
    auto correction = sol;
    auto zero = sol;
    zero *=0;

    double relError = relTol+1;
    std::size_t k = 0;
    while ((relError > relTol) and (k<maxIter))
    {

      // Previous iterate
      sol_old = sol;
      auto u_old = bindToCoefficients(u, sol_old);
      auto p_old = bindToCoefficients(p, sol_old);

      if (solverType == fixedpoint)
      {
        // Solve Oseen problem with given advection
        auto A = integrate( nu*dot(grad(u), grad(v)) + dot(dot(u_old, grad(u)),v) - div(u)*q - div(v)*p );
        auto b = integrate( dot(f,v) );

        globalAssembler.assembleOperator(stiffnessMatrix, A);
        globalAssembler.assembleFunctional(rhs, b);
        incorporateEssentialConstraints(stiffnessMatrix, rhs, isConstrained, constraintValues);

        solve(stiffnessMatrix, rhs, correction);
        correction -= sol;
      }
      else if (solverType == newton)
      {
        // Solve linearized Navier-Stokes problem
        auto F = integrate( dot(f,v) - (nu*dot(grad(u_old), grad(v)) + dot(jacobian(u_old)*u_old,v) - div(u_old)*q - div(v)*p_old ));
        auto DF = integrate( nu*dot(grad(u), grad(v)) + dot(dot(u,grad(u_old)) + dot(u_old,grad(u)),v) - div(u)*q - div(v)*p );

        globalAssembler.assembleOperator(stiffnessMatrix, DF);
        globalAssembler.assembleFunctional(rhs, F);
        incorporateEssentialConstraints(stiffnessMatrix, rhs, isConstrained, zero);

        solve(stiffnessMatrix, rhs, correction);
      }

      // Apply damped update
      correction *= damping;
      sol = sol_old;
      sol += correction;

      // Write iterate to VTK-file
      write(basis, sol, Dune::formatString("navier-stokes-result-0000-%03d", k));

      // Evaluate termination criterion
      relError = norm(correction)/norm(sol);
      std::cout
        << Dune::formatString("%02d", k)
        << Dune::formatString(" Relative norm of damped correction: %010e", relError)
        << std::endl;

      ++k;
    }
    if ((relError > relTol) and (k==maxIter))
      std::cout << "Warning: Fixed point iteration terminated after " << maxIter << " iterations without matching tolerance." << std::endl;
    write(basis, sol, Dune::formatString("navier-stokes-result-0000", k));
  }

}
catch (Dune::Exception& e)
{
  std::cout << e.what() << std::endl;
}
