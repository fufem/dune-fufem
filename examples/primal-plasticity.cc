// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <array>
#include <vector>

#include <dune/common/bitsetvector.hh>
#include <dune/common/indices.hh>
#include <dune/common/version.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/cholmod.hh>
#include <dune/istl/multitypeblockmatrix.hh>
#include <dune/istl/multitypeblockvector.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/backends/istlvectorbackend.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>

#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/datacollectors/discontinuousdatacollector.hh>
#else
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#endif

#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/dunefunctionsfunctionalassembler.hh>

#include <dune/fufem/forms/forms.hh>

#include "utilities.hh"



int main (int argc, char *argv[]) try
{
  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  std::size_t refinements = 0;
  if (argc>2)
    refinements = std::stoul(std::string(argv[2]));


  // Set up MPI, if available
  Dune::MPIHelper::instance(argc, argv);

  ///////////////////////////////////
  //   Generate the grid
  ///////////////////////////////////

  const int dim = 2;
  using Grid = Dune::YaspGrid<dim>;
  Dune::FieldVector<double,dim> upperRight = {1, 1};
  std::array<int,dim> elements = {{40, 40}};
//  std::array<int,dim> elements = {{4, 4}};
  Grid grid(upperRight,elements);
  grid.globalRefine(refinements);


  // Problem parameters
  double lambda = 10;
  double mu = 6.5;
  double k1 = 3;
  double k2 = 1; // ???

  /////////////////////////////////////////////////////////
  //   Choose a finite element space
  /////////////////////////////////////////////////////////

  using namespace Dune::Indices;
  using namespace Dune::Functions::BasisFactory;

  // Dimension of symmetric trace-free matrix space
  const int dp = dim*(dim+1)/2 - 1;

  auto basis = makeBasis(
    grid.leafGridView(),
    composite(
      power<dim>(lagrange<1>()),
      power<dp>(lagrange<0>()),
      lagrange<0>()
    ));

  /////////////////////////////////////////////////////////
  //   Stiffness matrix and right hand side vector
  /////////////////////////////////////////////////////////

  using M00 = Dune::BCRSMatrix<Dune::FieldMatrix<double,dim,dim>>;
  using M01 = Dune::BCRSMatrix<Dune::FieldMatrix<double,dim,dp>>;
  using M02 = Dune::BCRSMatrix<Dune::Fufem::SingleColumnMatrix<Dune::FieldMatrix<double,dim,1>>>;

  using M10 = Dune::BCRSMatrix<Dune::FieldMatrix<double,dp,dim>>;
  using M11 = Dune::BCRSMatrix<Dune::FieldMatrix<double,dp,dp>>;
  using M12 = Dune::BCRSMatrix<Dune::Fufem::SingleColumnMatrix<Dune::FieldMatrix<double,dp,1>>>;

  using M20 = Dune::BCRSMatrix<Dune::Fufem::SingleRowMatrix<Dune::FieldMatrix<double,1,dim>>>;
  using M21 = Dune::BCRSMatrix<Dune::Fufem::SingleRowMatrix<Dune::FieldMatrix<double,1,dp>>>;
  using M22 = Dune::BCRSMatrix<double>;

  using Matrix = Dune::MultiTypeBlockMatrix<
                  Dune::MultiTypeBlockVector<M00, M01, M02>,
                  Dune::MultiTypeBlockVector<M10, M11, M12>,
                  Dune::MultiTypeBlockVector<M20, M21, M22>
                  >;

  using V0 = Dune::BlockVector<Dune::FieldVector<double,dim> >;
  using V1 = Dune::BlockVector<Dune::FieldVector<double,dp> >;
  using V2 = Dune::BlockVector<double>;

  using Vector = Dune::MultiTypeBlockVector<V0, V1, V2>;

  using BV0 = std::vector<std::array<char,dim>>;
  using BV1 = std::vector<std::array<char,dp>>;
  using BV2 = std::vector<char>;

  using BitVector = Dune::TupleVector<BV0, BV1, BV2>;


  /////////////////////////////////////////////////////////
  // Some linear operators used in the problem definition
  /////////////////////////////////////////////////////////

  using Dune::Fufem::Forms::RangeOperator;
  using Dune::Fufem::Forms::trace;

  // Coordinate isometry for trace free symmetric matrices
  // This can be used to convert coefficient vector into matrices
  auto B = RangeOperator([](const auto& p) {
    if constexpr (dim==2)
      return 1.0/std::sqrt(2.0) * Dune::FieldMatrix<double,2,2>{{p[0], p[1]},{p[1], -p[0]}};
  });

  // Identity matrix (unfortunately, there's no operator+ for FieldMatrix and ScaledIdentityMatrix)
  auto Id = Dune::FieldMatrix<double,dim,dim>(0);
  for (auto k : Dune::range(dim))
    Id[k][k] = 1;

  // Elasticity tensor (for isotropic material)
  auto C = RangeOperator([&](const auto& e) {
    return 2*mu*e + lambda*Id*trace(e);
  });

  // Symmetric gradient
  auto E = [&](const auto& v) {
    return symmetrize(grad(v));
  };


  /////////////////////////////////////////////////////////
  //  Assemble the system
  /////////////////////////////////////////////////////////

  // Compute colored partition of the grid view for parallel assembly
  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(basis.gridView());

  Matrix matrix;

  // Assemble matrix
  {
    Dune::Timer timer;
    using namespace Dune::Indices;
    using namespace Dune::Fufem::Forms;

    // Some point-wise linear operators used in the problem definition


    // Ansatz functions
    auto u = trialFunction(basis, _0);
    auto p = trialFunction(basis, _1);
    auto eta = trialFunction(basis, _2);
    auto P = B(p);

    // Test functions
    auto v = testFunction(basis, _0);
    auto q = testFunction(basis, _1);
    auto nu = testFunction(basis, _2);
    auto Q = B(q);

    // Bilinear form for primal plasticity formulation with kinematic and isotropic hardening
    auto A = integrate(dot(C(E(u)-P),E(v)-Q) + k1*dot(P,Q) + k2*dot(eta,nu));

    // Alternatively we could e.g. use:
//     auto sigma = C(E(u)-P);
//     auto A = integrate(dot(sigma,E(v)-Q)  + k1*dot(P,Q) + k2*dot(eta,nu));

    auto operatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler{basis, basis};
    operatorAssembler.assembleBulk(matrix, A, gridViewPartition, threadCount);

    std::cout << "Assembling the problem took " << timer.elapsed() << "s" << std::endl;
  }



  Vector rhs;
  auto rhsBackend = Dune::Functions::istlVectorBackend(rhs);
  rhsBackend.resize(basis);
  rhs = 0;

  BitVector isDirichlet;
  auto isDirichletBackend = Dune::Functions::istlVectorBackend(isDirichlet);
  isDirichletBackend.resize(basis);
  isDirichletBackend = 0;

  {
    using namespace Dune::Indices;
    using namespace Dune::Functions;
    forEachBoundaryDOF(
      subspaceBasis(basis, _0),
      [&] (auto&& index) {
        isDirichletBackend[index] = true;
      });
  }

  Vector x;
  auto xBackend = Dune::Functions::istlVectorBackend(x);
  xBackend.resize(basis);
  xBackend = 0;

  {
    using namespace Dune::Indices;
    using namespace Dune::Functions;
    interpolate(
      subspaceBasis(basis, _0),
      x,
      [](auto x) {
        x[0] *= x[1];
        return x;
      },
      isDirichlet);
  }

  incorporateEssentialConstraints(matrix, rhs, isDirichlet, x);

  // Solve a linear problem
  // This is not an actual elasto-plastic problem, since we're missing
  // the nonlinearity. However, it helps to get some 'solution' that
  // we can use to demonstrate writing solutions.
  {
    Dune::Cholmod<Vector> solver;
    solver.setMatrix(matrix);
    Dune::InverseOperatorResult solverStatistics;
    solver.apply(x,rhs,solverStatistics);
  }

  {
    using namespace Dune::Indices;
    using namespace Dune::Fufem::Forms;

    auto u = bindToCoefficients(trialFunction(basis, _0), x);
    auto p = bindToCoefficients(trialFunction(basis, _1), x);
    auto P = B(p);

    auto sigma = C(E(u)-P);

    auto sigma_vol = 1./3. * trace(sigma);

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
    using Dune::Vtk::DiscontinuousDataCollector;
    using Dune::Vtk::UnstructuredGridWriter;
    auto vtkWriter = UnstructuredGridWriter(DiscontinuousDataCollector(basis.gridView()));
    vtkWriter.addPointData(u,         Dune::VTK::FieldInfo("u", Dune::VTK::FieldInfo::Type::vector, dim));
    vtkWriter.addPointData(E(u),      Dune::VTK::FieldInfo("Eu", Dune::VTK::FieldInfo::Type::tensor, dim*dim));
    vtkWriter.addPointData(sigma,     Dune::VTK::FieldInfo("sigma", Dune::VTK::FieldInfo::Type::tensor, dim*dim));
    vtkWriter.addPointData(P,         Dune::VTK::FieldInfo("plastic_stress", Dune::VTK::FieldInfo::Type::tensor, dim*dim));
    vtkWriter.addPointData(sigma_vol, Dune::VTK::FieldInfo("volumetric_stress", Dune::VTK::FieldInfo::Type::scalar, 1));
    vtkWriter.write("primal-plasticity-result");
#else
    using Basis = decltype(basis);
    Dune::SubsamplingVTKWriter<Basis::GridView> vtkWriter(basis.gridView(), Dune::refinementLevels(2));
    vtkWriter.addVertexData(u, Dune::VTK::FieldInfo("displacement", Dune::VTK::FieldInfo::Type::vector, dim));
    vtkWriter.addVertexData(sigma_vol, Dune::VTK::FieldInfo("volumetric_stress", Dune::VTK::FieldInfo::Type::scalar, 1));
    vtkWriter.write("primal-plasticity-result");
#endif

  }


}
catch (Dune::Exception& e) {
  std::cout << e.what() << std::endl;
}
