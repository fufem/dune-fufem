import numpy as np
import scipy.sparse.linalg

import dune.common
import dune.geometry
import dune.grid
import dune.functions

from dune.grid import DataType
from dune.functions import defaultGlobalBasis, Power, Composite, Lagrange

from fufemforms import *
from utilities import *

def isNear(a,b):
  return np.abs(a-b) <= 1e-10



ansatzOrder = 2

# Scalar right hand side
rhs = lambda x: 10

# Indicator function of Dirichlet boundary
isDirichlet = lambda x : isNear(x[0], 0) or isNear(x[0], 1) or isNear(x[1], 0) or isNear(x[1], 1)

# Dirichlet boundary values
dirichletValues = lambda x : np.sin(2*np.pi*x[0])



def globalAssembler(basis):

    N = len(basis)

    # Mark all Dirichlet DOFs
    isConstrained = np.zeros( N, dtype=bool )
    basis.interpolate(isConstrained,isDirichlet)

    # Interpolate the boundary values
    constraintDOFValues = np.zeros( N )
    basis.interpolate(constraintDOFValues, dirichletValues)

    # Trial and test functions for variational problem
    u = trialFunction(basis)
    v = testFunction(basis)

    # Scalar rhs coefficient function
    f = Coefficient(rhs, basis.gridView)

    # Bilinear form and rhs functional
    a = integrate(dot(grad(u),grad(v)))
    b = integrate(f*v)

    # Assemble into matrix and vector
    assembler = GlobalAssembler(basis)
    A = assembler.assembleOperator(a)
    B = assembler.assembleFunctional(b)

    # Build constraints into matrix
    incorporateEssentialConstraints(A, B, isConstrained, constraintDOFValues)

    return A, B

############################### START ########################################

# Number of grid elements (in one direction)
gridSize = 32

# Create a grid of the unit square
grid = dune.grid.structuredGrid([0,0],[1,1],[gridSize,gridSize])

# Create a nodal Lagrange FE basis
basis = defaultGlobalBasis(grid, Lagrange(order=ansatzOrder))

print("Dimension of FE space is "+str(len(basis)))

# Compute A and b
A,b = globalAssembler(basis)

# Solve linear system!
x = scipy.sparse.linalg.spsolve(A, b)

u = basis.asFunction(x)

vtk = grid.vtkWriter(2)
u.addToVTKWriter("sol", vtk, DataType.PointData)
vtk.write("poisson-pq2-py")

