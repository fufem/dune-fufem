from math import acosh, sqrt
import numpy as np
import scipy.sparse.linalg

import dune.common
import dune.geometry
import dune.grid
import dune.functions

from dune.grid import DataType
from dune.functions import defaultGlobalBasis, Power, Composite, Lagrange

from fufemforms import *
from utilities import *

def isNear(a,b):
  return np.abs(a-b) <= 1e-10


# Mark all DOFs associated to entities for which
# the boundary intersections center is marked
# by the given indicator functions.
def markBoundaryDOFsByIndicator(basis, vector, indicator):
    code="""
    #include<utility>
    #include<functional>
    #include<dune/common/fvector.hh>
    #include<dune/functions/functionspacebases/boundarydofs.hh>
    template<class Basis, class Vector, class Indicator>
    void run(const Basis& basis, Vector& vector, const Indicator& indicator)
    {
      auto vectorBackend = vector.mutable_unchecked();
      Dune::Functions::forEachBoundaryDOF(basis, [&] (auto&& localIndex, const auto& localView, const auto& intersection) {
        if (indicator(intersection.geometry().center()).template cast<bool>())
          vectorBackend[localView.index(localIndex)] = true;
      });
    }
    """
    dune.generator.algorithm.run("run",StringIO(code), basis, vector, indicator)



ansatzOrder = 2

# Indicator function of Dirichlet boundary
dirichletIndicatorFunction = lambda x : True

# Select Dirichlet values for a catenoid.
# This way the solution should be radially symmetric
# for the minimal surface equation in contrast to the
# linear Poisson-problem.
a = 0.5;
def dirichletValues(x):
  return a*acosh(max(x.two_norm/a, 1.0))



# Technicallity: Expose some C++ functions
def RangeOperator(cpp):
  return lambda expr : FunctionCallExpression("RangeOperator("+cpp+")", expr)

psi = RangeOperator("""[](auto g) {
    return 1./std::sqrt(1. + g.frobenius_norm2()) * g;
  }""")

Dpsi = RangeOperator("""[](auto g) {
    auto I = Dune::FieldMatrix<double,2,2>{{1, 0}, {0, 1}};
    auto gNorm2 = g.frobenius_norm2();
    return 1./std::sqrt(1.+gNorm2) * I - std::pow(1 + gNorm2,-3./2.)*g*Dune::Fufem::Forms::transpose(g);
  }""")

def assembleF(U, basis, isConstrained):
  u = bindToCoefficients(trialFunction(basis), U)
  v = testFunction(basis)

  # Nonlinear variational form of the operator
  f = integrate(dot(psi(grad(u)), grad(v)))

  # Evaluation of the operator by assembling the nonlinear defect
  assembler = GlobalAssembler(basis, verbosity=0)
  F = assembler.assembleFunctional(f)

  # Constrain evaluation to subspace
  F -= F*isConstrained
  return F



def assembleDF(U, basis, isConstrained):
  u = bindToCoefficients(trialFunction(basis), U)
  w = trialFunction(basis)
  v = testFunction(basis)

  # Nonlinear variational form of the operator
  df = integrate(dot(Dpsi(grad(u))*grad(w), grad(v)))

  # Evaluation of the operator by assembling the nonlinear defect
  assembler = GlobalAssembler(basis, verbosity=0)
  DF = assembler.assembleOperator(df)

  # Constrain evaluation to subspace
  rows, cols = DF.nonzero()
  for i,j in zip(rows, cols):
    if isConstrained[i] or isConstrained[j]:
      DF[i,j] = 0
    if isConstrained[i] and (i==j):
      DF[i,j] = 1
  return DF



############################### START ########################################

# Number of grid elements (in one direction)
gridSize = 32

# Create a grid of the unit square
grid = dune.grid.structuredGrid([a/sqrt(2.0),a/sqrt(2.0)],[a*sqrt(2.0),a*sqrt(2.0)],[gridSize,gridSize])


# Create a nodal Lagrange FE basis
basis = defaultGlobalBasis(grid, Lagrange(order=ansatzOrder))
N = len(basis)

print("Dimension of FE space is "+str(N))



# Mark all Dirichlet DOFs
isConstrained = np.zeros(N)
markBoundaryDOFsByIndicator(basis, isConstrained, dirichletIndicatorFunction)

# Interpolate the boundary values into solution vector
U0 = np.zeros( N )
basis.interpolate(U0, dirichletValues)
U0 *= isConstrained

# Compute smooth initial value by solving Poisson equation
u = trialFunction(basis)
v = testFunction(basis)
a = integrate(dot(grad(u), grad(v)));
A = GlobalAssembler(basis, verbosity=0).assembleOperator(a)
b = np.zeros(N)
incorporateEssentialConstraints(A, b, isConstrained, U0)
U0 = scipy.sparse.linalg.spsolve(A, b)

# Solve nonlinear system
F = lambda U : assembleF(U, basis, isConstrained)
DF = lambda U : assembleDF(U, basis, isConstrained)
U = NewtonSolver(F, DF, U0, tolerance=1e-10, damping=True)

u = basis.asFunction(U)

vtk = grid.vtkWriter(2)
u.addToVTKWriter("sol", vtk, DataType.PointData)
vtk.write("minimalsurface-equation-py")

