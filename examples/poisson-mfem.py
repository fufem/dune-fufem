import numpy as np
import scipy.sparse.linalg

import dune.common
import dune.geometry
import dune.grid
import dune.functions

from dune.grid import DataType
from dune.functions import defaultGlobalBasis, Power, Composite, Lagrange, RaviartThomas, subspaceBasis

from fufemforms import *
from utilities import *

def isNear(a,b):
  return np.abs(a-b) <= 1e-10

# Mark all DOFs associated to entities for which
# the boundary intersections center is marked
# by the given indicator functions.
def markBoundaryDOFsByIndicator(basis, vector, indicator):
    code="""
    #include<utility>
    #include<functional>
    #include<dune/common/fvector.hh>
    #include<dune/functions/functionspacebases/boundarydofs.hh>
    template<class Basis, class Vector, class Indicator>
    void run(const Basis& basis, Vector& vector, const Indicator& indicator)
    {
      auto vectorBackend = vector.mutable_unchecked();
      Dune::Functions::forEachBoundaryDOF(basis, [&] (auto&& localIndex, const auto& localView, const auto& intersection) {
        if (indicator(intersection.geometry().center()).template cast<bool>())
          vectorBackend[localView.index(localIndex)] = true;
      });
    }
    """
    dune.generator.algorithm.run("run",StringIO(code), basis, vector, indicator)


ansatzOrder = 0

# Scalar right hand side
rhs = lambda x: 2

# Indicator function of Dirichlet boundary
isFluxBoundary = lambda x : False
isFluxBoundary = lambda x : 1.*(isNear(x[1], 1) or isNear(x[1], 0))

############################################################
# ToDo: We should provide binding for FaceNormalGridFunction
# and support for grid functions to the bindings of interpolate().
# This would allow to avoid having to define the normal field
# manually.
############################################################

# Dirichlet boundary values
normal = lambda x : np.array([0.,1.]) if isNear(x[1], 1) else np.array([0., -1.])
fluxBoundaryValues = lambda x : np.sin(2.*np.pi*x[0]) * normal(x);



def globalAssembler(basis):

    N = len(basis)

    fluxBasis = subspaceBasis(basis, 0)
    pressureBasis = subspaceBasis(basis, 1)

    # Mark all Dirichlet DOFs
    isConstrained = np.zeros( N );

    # Mark all DOFs located in a boundary intersection marked
    # by the fluxDirichletIndicator function. If the flux
    # ansatz space also contains tangential components, this
    # approach will fail, because those are also marked.
    # For Raviart-Thomas this does not happen.
    markBoundaryDOFsByIndicator(fluxBasis, isConstrained, isFluxBoundary);

    # Interpolate the boundary values
    constraintDOFValues = np.zeros( N )
    fluxBasis.interpolate(constraintDOFValues, fluxBoundaryValues)

    # Trial and test functions for variational problem
    sigma = trialFunction(fluxBasis, NonAffineFamiliy=True)
    u = trialFunction(pressureBasis)
    tau = testFunction(fluxBasis, NonAffineFamiliy=True)
    v = testFunction(pressureBasis)

    # Scalar rhs coefficient function
    f = Coefficient(rhs, basis.gridView)

    # Bilinear form and rhs functional
    a = integrate(dot(sigma, tau) + div(tau)*u + div(sigma)*v)
    b = integrate(-f*v)

    # Assemble into matrix and vector
    assembler = GlobalAssembler(basis)
    A = assembler.assembleOperator(a)
    B = assembler.assembleFunctional(b)

    # Build constraints into matrix
    incorporateEssentialConstraints(A, B, isConstrained, constraintDOFValues)

    return A, B

############################### START ########################################

# Number of grid elements (in one direction)
gridSize = 50

# Create a grid of the unit square
grid = dune.grid.structuredGrid([0,0],[1,1],[gridSize,gridSize])

# Create a nodal Lagrange FE basis
basis = defaultGlobalBasis(grid, Composite(RaviartThomas(order=ansatzOrder), Lagrange(order=ansatzOrder)))

print("Dimension of FE space is "+str(len(basis)))

# Compute A and b
A,b = globalAssembler(basis)

# Solve linear system!
x = scipy.sparse.linalg.spsolve(A, b)

fluxBasis = subspaceBasis(basis, 0)
pressureBasis = subspaceBasis(basis, 1)

fluxFunction = fluxBasis.asFunction(x)
pressureFunction = pressureBasis.asFunction(x)


vtk = grid.vtkWriter(0)
fluxFunction.addToVTKWriter("flux", vtk, dune.grid.DataType.PointVector)
pressureFunction.addToVTKWriter("pressure", vtk, dune.grid.DataType.PointData)
vtk.write("poisson-mfem-py")
