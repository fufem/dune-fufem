// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>
#include <cmath>
#include <ranges>
#include <tuple>

#include <dune/common/bitsetvector.hh>
#include <dune/common/stringutility.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/hierarchicallagrangebasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/composedgridfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/datacollectors/lagrangedatacollector.hh>

#include <dune/fufem/logger.hh>
#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>
#include <dune/fufem/forms/forms.hh>
#include <dune/fufem/constraints/continuityconstraints.hh>
#include <dune/fufem/constraints/boundaryconstraints.hh>

#include "utilities.hh"

using namespace Dune;

template<int order, int extensionOrder, class BV, class V, class MI, class C, class Basis>
auto computeHierarchicConstraints(Dune::Fufem::AffineConstraints<BV, V, MI, C>& constraints, const Basis& basis)
{
  static_assert((extensionOrder == 2*order) and (order<=2));
  constexpr auto dim = Basis::GridView::Grid::dimension;
  auto& isConstrained = constraints.isConstrained();
  auto localView = basis.localView();
  for(const auto& element : elements(basis.gridView()))
  {
    localView.bind(element);
    const auto& localCoefficients = localView.tree().finiteElement().localCoefficients();
    for(auto i : Dune::range(localCoefficients.size()))
    {
      // For P1->P2 constrain all vertex dofs
      // For P2->P4 constrain all vertex and odd edge DOFs
      if (localCoefficients.localKey(i).codim() == dim)
        isConstrained[localView.index(i)] = true;
      if (localCoefficients.localKey(i).codim() == dim-1)
      {
        if constexpr (order == 2)
          if ((localCoefficients.localKey(i).index() % 2) == 1)
            isConstrained[localView.index(i)] = true;
      }
    }
  }
}

template<class BilinearForm, class Residual, class ExtensionBasis, class Constraints, class Vector, class... Args>
auto hierarchicalErrorEstimator(BilinearForm b, Residual residual, const ExtensionBasis& extension, const Constraints& constraints, Vector& defect, Args... args)
{
  using namespace Dune::Fufem::Forms;

  auto d = trialFunction(extension);
  auto v = testFunction(extension);

  auto r = Vector();
  auto B = Dune::BCRSMatrix<double>();

  auto globalAssembler = GlobalAssembler(extension, extension, 0, std::forward<Args>(args)...);
  globalAssembler.assembleSystem(B, r, b(d,v), residual(v), constraints);

  const auto& isConstrained = constraints.isConstrained();
  auto eta = std::vector<double>(extension.size(), 0.0);
  double etaSum = 0;
  for(auto k : Dune::range(extension.size()))
  {
    if (not isConstrained[k])
      defect[k] = r[k]/B[k][k];
    eta[k] = defect[k]*defect[k]*B[k][k];
    etaSum += eta[k];
  }
  constraints.interpolate(defect);

  return std::make_tuple(std::sqrt(etaSum), eta);
}


// Guess a value of theta for Doerfler marking.
// Doerfler's marking strategy uses a parameter \theta from the interval (0,1)
// to control the locallity of refinement, where larger values lead to more local
// refinement. He proposes values from the interval [0.25, .5] to balance locallity
// per refinement step with the number of needed refinement steps. In practice
// these values all work well. However, if we are very close to matching the global
// tolerance, a more local refinement might be sufficient to match the tolerance
// in the next step with less unknowns. Given the current estimanted error, the target
// tolerance and the expected local error reduction this function computes a guess for
// the value theta that suffices to match the tolerance.
double guessTheta(double tol, double error, double expectedReduction, double theta_min = 0.25, double theta_max = 0.5)
{
  // The value beta is the fraction of the squared local error estimators that
  // we need to refine such that we match the tolerance if the local errors
  // reduce as expected after refinement.
  double beta = (1.-(tol/error)*(tol/error))/(1.-expectedReduction*expectedReduction);

  // For err>=tol the value beta should be non-negative.
  // But we want to avoid rounding issues if error almost matches tol.
  beta = std::max(beta,0.0);

  // The paper by Doerfer uses a parameter theta
  // with beta=(1-theta)^2
  double theta = 1. - std::sqrt(beta);

  // Project into desired range of values for theta
  return std::max(std::min(theta, theta_max), theta_min);
}


// Mark grid using the strategy proposed by Doerfler.
template<class Grid, class ExtensionBasis, class LocalIndicators>
std::size_t doerflerMarking(Grid& grid, const ExtensionBasis& extensionBasis, const LocalIndicators& eta, double error, double theta)
{
  auto etaSorted = eta;
  std::sort(etaSorted.begin(), etaSorted.end(), std::greater{});

  double etaRefinedTarget = (1-theta)*(1-theta)*error*error;
  double etaRefined = 0;
  double localTol = 0;
  double localThreshold = 0;
  for(auto eta_k : etaSorted)
  {
    etaRefined += eta_k;
    localTol = eta_k;
    if (etaRefined >= etaRefinedTarget)
      break;
  }

  // Cosmetic hack:
  // If the error is symmetric we may still get non-symmetric refinement due
  // to round-off errors. By decreasing the tolerance slightly (here by 1%),
  // it is more likely that we refine contributions that are the same up to
  // round-off errors.
  localTol *= 0.99;

  std::size_t marked = 0;
  auto localView = extensionBasis.localView();
  for(const auto& element : elements(extensionBasis.gridView()))
  {
    localView.bind(element);
    for(auto i : Dune::range(localView.size()))
    {
      auto k = localView.index(i);
      if (eta[k] >= localTol)
      {
        grid.mark(1, element);
        ++marked;
      }
    }
  }
  return marked;
}



auto createGrid()
{
  using Grid = Dune::UGGrid<2>;
  Dune::GridFactory<Grid> factory;
  for(unsigned int k : Dune::range(8))
    factory.insertVertex({0.5*(k%3), 0.5*(k/3)});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {0, 1, 3});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {1, 4, 3});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {1, 2, 4});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {2, 5, 4});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {3, 4, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 7, 6});
  return std::unique_ptr<Grid>{factory.createGrid()};
}

int main (int argc, char *argv[]) try
{

  auto log = Dune::Fufem::makeLogger(std::cout, "[% 8.3f] [% 8.3f] ");

  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  std::size_t refinements = 1;
  if (argc>2)
    refinements = std::stoul(std::string(argv[2]));

  std::size_t localRefinements = 1;
  if (argc>3)
    localRefinements = std::stoul(std::string(argv[3]));

  log("Command line processed");

  MPIHelper::instance(argc, argv);

  log("MPI initialized");

  ///////////////////////////////////
  //   Generate the grid
  ///////////////////////////////////

  auto gridPtr = createGrid();

  auto& grid = *gridPtr;

  using Grid = std::decay_t<decltype(grid)>;

  grid.setClosureType(Grid::ClosureType::NONE);

  log("Grid created");

  grid.globalRefine(refinements);

  log(Dune::formatString("Grid uniformly refined %d times", refinements));

  // *********************************************
  // Boundary and right hand side data
  // *********************************************

  // We use an example combining two features that have to be
  // detected by the error estimator:
  // * An L-shape domain with a reentrent corner.
  // * A boundary condition not visible on the coarsest mesh.

  auto dirichletIndicatorFunction = [](auto x) { return true; };

  auto rightHandSide = [] (const auto& x) { return 10;};

  auto dirichletValues = [](const auto& x){
    auto z = x[0]+x[1];
    return 1000*std::max(std::pow(1./9., 4.)-std::pow(1./9.-z, 4.), 0.0);
  };

  // *********************************************
  // Standard adaptive loop
  // *********************************************

  // Discretize -> solve -> estimate -> mark -> refine

  using Vector = Dune::BlockVector<double>;
  auto sol_initial = Vector();

  std::size_t refinementStep = 0;

  // Protocol errors and dimensions for estimating convergence order
  std::vector<std::size_t> dimensionProtocol;
  std::vector<double> errorProtocol;
  while(true)
  {

    auto gridView = grid.leafGridView();
    using GridView = decltype(gridView);

    // *********************************************
    // Choose a finite element space
    // *********************************************

    constexpr auto order = 2;
    constexpr auto extensionOrder = 2*order;

    auto basis = Dune::Functions::LagrangeBasis<GridView,order>(gridView);

    // Define suitable matrix and vector types
    using Matrix = Dune::BCRSMatrix<double>;
    using Vector = Dune::BlockVector<double>;
    using BitVector = std::vector<bool>;

    log(Dune::formatString("Basis created with dimension %d", basis.dimension()));

    // *********************************************
    // Compute hanging node and Dirichlet constraints
    // *********************************************

    auto dirichletPatch = BoundaryPatch(basis.gridView());
    dirichletPatch.insertFacesByProperty([&](auto&& intersection) { return dirichletIndicatorFunction(intersection.geometry().center()); });
    log("Boundary patch setup");

    auto constraints = Dune::Fufem::makeAffineConstraints<BitVector, Vector>(basis);

    computeContinuityConstraints(constraints, basis);
    log("Hanging node constraints computed");

    computeBoundaryConstraints(constraints, basis, dirichletValues, dirichletPatch);
    log("Boundary constraints computed");

    constraints.check();
    log("Constraints checked");

    // *********************************************
    // Stiffness matrix and right hand side vector
    // *********************************************

    Vector rhs;
    Vector sol;
    Matrix stiffnessMatrix;

    // *********************************************
    // Assemble the system
    // *********************************************

    // Disable parallel assembly for UGGrid before 2.10
    auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(basis.gridView());
    log("Grid coloring computed");

    auto globalAssembler = GlobalAssembler(basis, basis, 0, std::cref(gridViewPartition), threadCount);

    {
      using namespace ::Dune::Fufem::Forms;

      auto v = testFunction(basis);
      auto u = trialFunction(basis);
      auto f = Coefficient(Dune::Functions::makeGridViewFunction(rightHandSide, basis.gridView()));

      auto a = integrate(dot(grad(u), grad(v)));
      auto b = integrate(f*v);

      globalAssembler.assembleSystem(stiffnessMatrix, rhs, a, b, constraints);
      log("Linear problem assembled");
    }

    // *********************************************
    // Initialize solution vector
    // *********************************************

    // For simplicity use zero as initial value.
    // Here we could use an adapted solution from
    // the previous grid as initial value.
    Dune::Functions::istlVectorBackend(sol_initial).resize(basis);
    Dune::Functions::istlVectorBackend(sol_initial) = 0;

    sol = sol_initial;

    // *********************************************
    // Solve linear system
    // *********************************************

    {
      // Technicality:  turn the matrix into a linear operator
      MatrixAdapter<Matrix,Vector,Vector> op(stiffnessMatrix);

      // FastAMG is not working for non-blocked matrices in 2.10
  #if DUNE_VERSION_GTE(DUNE_ISTL, 2, 11)
      auto preconditioner = makeAMGPreconditioner<Vector>(stiffnessMatrix);
  #else
      // Sequential incomplete LU decomposition as the preconditioner
      auto preconditioner = SeqILDL<Matrix,Vector,Vector>(stiffnessMatrix,1.0);
  #endif

      log("Preconditioner created");

      // Preconditioned conjugate-gradient solver
      CGSolver<Vector> cg(op,
          preconditioner, // preconditioner
          1e-5, // desired residual reduction factor
          1000,   // maximum number of iterations
          2);   // verbosity of the solver

      log("Solver created");

      // Object storing some statistics about the solving process
      InverseOperatorResult statistics;

      // Solve!
      cg.apply(sol, rhs, statistics);

      log("Linear system solved");

      constraints.interpolate(sol);
      log("Constrained solution interpolated");
    }

    // *********************************************
    // Evaluate refinement indicator and adapt grid
    // *********************************************

    auto tol = 1e-2;
    {
      using namespace ::Dune::Fufem::Forms;

      auto u = bindToCoefficients(trialFunction(basis), sol);

      auto extensionBasis = Dune::Functions::LagrangeBasis<GridView,extensionOrder>(gridView);
      log("Extension basis created");

      auto extensionConstraints = Dune::Fufem::makeAffineConstraints<BitVector, Vector>(extensionBasis);

      computeContinuityConstraints(extensionConstraints, extensionBasis);
      log("Extension hanging node constraints computed");

      computeHierarchicConstraints<order, extensionOrder>(extensionConstraints, extensionBasis);
      log("Extension DOFs identified");

      auto defectDirichletValues = Dune::Functions::makeComposedGridFunction([&](auto u_d, auto u) {
          return u_d-u;
        },
        Dune::Functions::makeAnalyticGridViewFunction(dirichletValues, gridView), u);
      computeBoundaryConstraints(extensionConstraints, extensionBasis, defectDirichletValues, dirichletPatch);
      log("Extension boundary constraints computed");

      auto defect = Vector();
      Dune::Functions::istlVectorBackend(defect).resize(extensionBasis);
      Dune::Functions::istlVectorBackend(defect) = 0;

      // Define defect problem assemblers
      auto f = Coefficient(Dune::Functions::makeGridViewFunction(rightHandSide, basis.gridView()));
      auto a = [&](auto u, auto v) {
        return integrate(dot(grad(u),grad(v)));
      };
      auto residual = [&](auto v) {
        return integrate(f*v - dot(grad(u),grad(v)));
      };

      // Compute local error estimates
      auto [error, eta] = hierarchicalErrorEstimator(a, residual, extensionBasis, extensionConstraints, defect, std::cref(gridViewPartition), threadCount);

      log(Dune::formatString("Estimated total error : %11.5e", error));

      errorProtocol.push_back(error);
      dimensionProtocol.push_back(basis.dimension());
      if (refinementStep>0)
      {
        double err_old = errorProtocol[refinementStep-1];
        double err_new = errorProtocol[refinementStep];
        double h_old = std::pow((double)dimensionProtocol[refinementStep-1], -1./Grid::dimension);
        double h_new = std::pow((double)dimensionProtocol[refinementStep], -1./Grid::dimension);
        double eoc = std::log(err_new/err_old) / std::log(h_new/h_old);
        log(Dune::formatString("Estimated order (eoc) : %f", eoc));
      }

      // *********************************************
      // Write solution and solution of localized defect problem
      // *********************************************

      {
        auto u_initial = bindToCoefficients(trialFunction(basis), sol_initial);
        auto d = bindToCoefficients(trialFunction(extensionBasis), defect);
        using Dune::Vtk::LagrangeDataCollector;
        using Dune::Vtk::UnstructuredGridWriter;
        using Dune::VTK::FieldInfo;
        auto vtkWriter = UnstructuredGridWriter(LagrangeDataCollector(basis.gridView(), extensionOrder));
        vtkWriter.addPointData(u, FieldInfo("sol", FieldInfo::Type::scalar, 1));
        vtkWriter.addPointData(u_initial, FieldInfo("sol_initial", FieldInfo::Type::scalar, 1));
        vtkWriter.addPointData(d, FieldInfo("defect", FieldInfo::Type::scalar, 1));
        vtkWriter.write(Dune::formatString("poisson-adaptive-%03d", refinementStep));
        log("Solution written to vtk file");
      }

      if (error <= tol)
        break;

      double expectedReduction = .5;
      double theta = guessTheta(tol, error, expectedReduction);
      log(Dune::formatString("Computed guess for refinement parameter theta :    % 12.5e", theta));

      doerflerMarking(grid, extensionBasis, eta, error, theta);
      log("Grid marked for refinement");

      grid.preAdapt();
      grid.adapt();
      grid.postAdapt();
      log(Dune::formatString("Grid adapted. New grid has %d levels.", grid.maxLevel()));

      ++refinementStep;
    }
  }
}
// Error handling
catch (Exception& e) {
  std::cout << e.what() << std::endl;
}
