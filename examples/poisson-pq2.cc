// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>
#include <cmath>

#include <dune/common/bitsetvector.hh>
#include <dune/common/version.hh>
#include <dune/common/stringutility.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/datacollectors/lagrangedatacollector.hh>
#else
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#endif

#include <dune/fufem/logger.hh>
#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>
#include <dune/fufem/forms/forms.hh>

#include "utilities.hh"

using namespace Dune;



template<int dim>
auto createUniformCubeGrid()
{
  using Grid = Dune::YaspGrid<dim>;
  Dune::FieldVector<double,dim> l(1.0);
  std::array<int,dim> elements = {{2, 2}};
  return std::make_unique<Grid>(l, elements);
}

#if HAVE_DUNE_UGGRID
auto createMixedGrid()
{
  using Grid = Dune::UGGrid<2>;
  Dune::GridFactory<Grid> factory;
  for(unsigned int k : Dune::range(9))
    factory.insertVertex({0.5*(k%3), 0.5*(k/3)});
  factory.insertElement(Dune::GeometryTypes::cube(2), {0, 1, 3, 4});
  factory.insertElement(Dune::GeometryTypes::cube(2), {1, 2, 4, 5});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {3, 4, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 7, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 5, 7});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {5, 8, 7});
  return std::unique_ptr<Grid>{factory.createGrid()};
}
#endif

int main (int argc, char *argv[]) try
{

  auto log = Dune::Fufem::makeLogger(std::cout, "[% 8.3f] [% 8.3f] ");

  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  std::size_t refinements = 4;
  if (argc>2)
    refinements = std::stoul(std::string(argv[2]));

  log("Command line processed");

  MPIHelper::instance(argc, argv);

  log("MPI initialized");

  ///////////////////////////////////
  //   Generate the grid
  ///////////////////////////////////

#if HAVE_DUNE_UGGRID
  auto grid = createMixedGrid();
#else
  auto grid = createUniformCubeGrid<2>();
#endif

  log("Grid created");

  grid->globalRefine(refinements);

  log(Dune::formatString("Grid refined %d times", refinements));

  auto gridView = grid->leafGridView();
  using GridView = decltype(gridView);

  /////////////////////////////////////////////////////////
  //   Choose a finite element space
  /////////////////////////////////////////////////////////

  using Basis = Dune::Functions::LagrangeBasis<GridView,2>;
  Basis basis(gridView);

  log(Dune::formatString("Basis created with dimension %d", basis.dimension()));

  /////////////////////////////////////////////////////////
  //   Stiffness matrix and right hand side vector
  /////////////////////////////////////////////////////////

  using Vector = Dune::BlockVector<double>;
  using BitVector = std::vector<bool>;
  using Matrix = Dune::BCRSMatrix<double>;

  Vector rhs;
  Vector sol;
  BitVector isConstrained;
  Matrix stiffnessMatrix;

  /////////////////////////////////////////////////////////
  //  Assemble the system
  /////////////////////////////////////////////////////////

  auto dirichletIndicatorFunction = [](auto x) { return true; };

  auto dirichletPatch = BoundaryPatch(basis.gridView());
  dirichletPatch.insertFacesByProperty([&](auto&& intersection) { return dirichletIndicatorFunction(intersection.geometry().center()); });

  auto rightHandSide = [] (const auto& x) { return 10;};
  auto pi = std::acos(-1.0);
  auto dirichletValues = [pi](const auto& x){ return std::sin(2*pi*x[0]); };


// Disable parallel assembly for UGGrid before 2.10
#if DUNE_VERSION_GT(DUNE_FUNCTIONS, 2, 9)
  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(gridView);
  log("Grid coloring computed");

  auto globalAssembler = GlobalAssembler(basis, basis, 0, std::cref(gridViewPartition), threadCount);
#else
  auto globalAssembler = GlobalAssembler(basis, basis, 0);
#endif

  {
    using namespace ::Dune::Fufem::Forms;
    namespace F = ::Dune::Fufem::Forms;

    auto v = testFunction(basis);
    auto u = trialFunction(basis);
    auto f = Coefficient(Functions::makeGridViewFunction(rightHandSide, gridView));

    auto a = integrate(dot(grad(u), grad(v)));
    auto b = integrate(f*v);

    log("Assembler set up");
    globalAssembler.assembleOperator(stiffnessMatrix, a);
    log("Matrix assembled");
    globalAssembler.assembleFunctional(rhs, b);
    log("RHS assembled");
  }

  // *********************************************
  // Initialize solution vector
  // *********************************************

  Dune::Functions::istlVectorBackend(sol).resize(basis);
  Dune::Functions::istlVectorBackend(sol) = 0;

  // *********************************************
  // Incorporate Dirichlet boundary conditions
  // *********************************************

  Dune::Functions::istlVectorBackend(isConstrained).resize(basis);
  Dune::Functions::istlVectorBackend(isConstrained) = 0;
  Dune::Fufem::markBoundaryPatchDofs(dirichletPatch, basis, isConstrained);
  log("Boundary DOFs marked");

  interpolate(basis, sol, dirichletValues, isConstrained);
  log("Boundary DOFs interpolated");

  incorporateEssentialConstraints(stiffnessMatrix, rhs, isConstrained, sol);
  log("Boundary condition incorporated into system");

  ////////////////////////////
  //   Compute solution
  ////////////////////////////

  // Technicality:  turn the matrix into a linear operator
  MatrixAdapter<Matrix,Vector,Vector> op(stiffnessMatrix);

// FastAMG is not working for non-blocked matrices in 2.10
#if DUNE_VERSION_GTE(DUNE_ISTL, 2, 11)
  auto preconditioner = makeAMGPreconditioner<Vector>(stiffnessMatrix);
#else
  // Sequential incomplete LU decomposition as the preconditioner
  auto preconditioner = SeqILDL<Matrix,Vector,Vector>(stiffnessMatrix,1.0);
#endif

  log("Preconditioner created");

  // Preconditioned conjugate-gradient solver
  CGSolver<Vector> cg(op,
                          preconditioner, // preconditioner
                          1e-4, // desired residual reduction factor
                          1000,   // maximum number of iterations
                          2);   // verbosity of the solver

  log("Solver created");

  // Object storing some statistics about the solving process
  InverseOperatorResult statistics;

  // Solve!
  cg.apply(sol, rhs, statistics);

  log("Linear system solved");

  ////////////////////////////////////////////////////////////////////////////
  //  Make a discrete function from the FE basis and the coefficient vector
  ////////////////////////////////////////////////////////////////////////////

  auto solFunction = Functions::makeDiscreteGlobalBasisFunction<double>(basis, sol);

  //////////////////////////////////////////////////////////////////////////////////////////////
  //  Write result to VTK file
  //////////////////////////////////////////////////////////////////////////////////////////////

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
  using Dune::Vtk::LagrangeDataCollector;
  using Dune::Vtk::UnstructuredGridWriter;
  auto vtkWriter = UnstructuredGridWriter(LagrangeDataCollector(basis.gridView(), 2));
  vtkWriter.addPointData(solFunction, VTK::FieldInfo("sol", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("poisson-pq2");
#else
  // We need to use the SubsamplingVTKWriter, because dune-vtk's LagrangeDataCollector
  // does not work with mixed grids in 2.9.
  SubsamplingVTKWriter<GridView> vtkWriter(gridView, Dune::refinementLevels(2));
  vtkWriter.addVertexData(solFunction, VTK::FieldInfo("sol", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("poisson-pq2");
#endif
  log("Solution written to vtk file");
 }
// Error handling
 catch (Exception& e) {
    std::cout << e.what() << std::endl;
 }
