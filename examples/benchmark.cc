// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>
#include <cmath>

#include <dune/common/bitsetvector.hh>
#include <dune/common/transpose.hh>
#include <dune/common/stringutility.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/raviartthomasbasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/subspacebasis.hh>
#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/dunefunctionsfunctionalassembler.hh>
#include <dune/fufem/backends/istlmatrixbackend.hh>

#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>

#include <dune/fufem/forms/forms.hh>

using namespace Dune;

struct LocalLaplaceAssembler
{
  template<class Element, class LocalMatrix, class TestLocalView, class AnsatzLocalView>
  void operator()(const Element& element, LocalMatrix& localMatrix, const TestLocalView& testLocalView, const AnsatzLocalView& ansatzLocalView)
  {
    const int dim = Element::dimension;
    const auto& geometry = element.geometry();
    const auto& localFiniteElement = ansatzLocalView.tree().finiteElement();

    int order = localFiniteElement.localBasis().order();
    if (not element.type().isSimplex())
      order *= dim;
    order = 2*(order-1);
    const QuadratureRule<double, dim>& quad = QuadratureRules<double, dim>::rule(element.type(), order);
    for (size_t pt=0; pt < quad.size(); pt++) {
      const FieldVector<double,dim>& quadPos = quad[pt].position();
      const auto& jacobianInverse = geometry.jacobianInverse(quadPos);
      const double integrationElement = geometry.integrationElement(quadPos);

      std::vector<FieldMatrix<double,1,dim> > referenceJacobians;
      localFiniteElement.localBasis().evaluateJacobian(quadPos, referenceJacobians);

      std::vector<FieldMatrix<double,1,dim> > jacobians(referenceJacobians.size());
      auto z = quad[pt].weight() * integrationElement;
      for (size_t i=0; i<jacobians.size(); i++)
        jacobians[i] = referenceJacobians[i] * jacobianInverse;

      for (size_t i=0; i<localFiniteElement.localBasis().size(); i++)
        for (size_t j=0; j<localFiniteElement.localBasis().size(); j++ )
          localMatrix[i][j] += (jacobians[i] * transpose(jacobians[j])) * z;
    }
  }
};



struct LocalMixedLaplaceAssembler
{
  template<class Element, class LocalMatrix, class TestLocalView, class AnsatzLocalView>
  void operator()(const Element& element, LocalMatrix& localMatrix, const TestLocalView& testLocalView, const AnsatzLocalView& ansatzLocalView)
  {
    const int dim = Element::dimension;
    auto geometry = element.geometry();
    using namespace Dune::Indices;
    const auto& fluxLocalFiniteElement     = testLocalView.tree().child(_0).finiteElement();
    const auto& pressureLocalFiniteElement = testLocalView.tree().child(_1).finiteElement();

    int fluxOrder = dim*fluxLocalFiniteElement.localBasis().order();
    int pressureOrder = dim*pressureLocalFiniteElement.localBasis().order();
    int order = std::max(2*fluxOrder, (fluxOrder-1)*pressureOrder);
    const auto& quad = QuadratureRules<double, dim>::rule(element.type(), order);
    for (const auto& quadPoint : quad)
    {
      const auto quadPos = quadPoint.position();
      const auto jacInvTrans = geometry.jacobianInverseTransposed(quadPos);
      const auto integrationElement = geometry.integrationElement(quadPos);

      std::vector<FieldVector<double,dim> > fluxValues(fluxLocalFiniteElement.size());
      fluxLocalFiniteElement.localBasis().evaluateFunction(quadPos, fluxValues);

      std::vector<FieldMatrix<double,dim,dim> > fluxReferenceJacobians(fluxLocalFiniteElement.size());
      fluxLocalFiniteElement.localBasis().evaluateJacobian(quadPos, fluxReferenceJacobians);

      auto trace = [](const auto& matrix) {
        double r=0;
        for (size_t j=0; j<matrix.N(); j++)
          r += matrix[j][j];
        return r;
      };

      std::vector<double> fluxDivergence(fluxValues.size(), 0.0);
      for (size_t i=0; i<fluxReferenceJacobians.size(); i++)
        fluxDivergence[i] = trace(fluxReferenceJacobians[i] * transpose(jacInvTrans));

      std::vector<FieldVector<double,1> > pressureValues(pressureLocalFiniteElement.size());
      pressureLocalFiniteElement.localBasis().evaluateFunction(quadPos, pressureValues);

      for (size_t i=0; i<fluxLocalFiniteElement.size(); i++)
      {
        size_t row = testLocalView.tree().child(_0).localIndex(i);
        for (size_t j=0; j<fluxLocalFiniteElement.size(); j++)
        {
          size_t col = testLocalView.tree().child(_0).localIndex(j);
          localMatrix[row][col] += (fluxValues[i] * fluxValues[j]) * quadPoint.weight() * integrationElement;
        }
      }

      for (size_t i=0; i<fluxLocalFiniteElement.size(); i++)
      {
        size_t fluxIndex     = testLocalView.tree().child(_0).localIndex(i);
        for (size_t j=0; j<pressureLocalFiniteElement.size(); j++)
        {
          size_t pressureIndex = testLocalView.tree().child(_1).localIndex(j);

          double tmp = - (fluxDivergence[i] * pressureValues[j]) * quadPoint.weight() * integrationElement;

          localMatrix[fluxIndex][pressureIndex] += tmp;
          localMatrix[pressureIndex][fluxIndex] += tmp;
        }
      }
    }
  }
};



struct LocalStokesAssembler
{
  template<class Element, class LocalMatrix, class TestLocalView, class AnsatzLocalView>
  void operator()(const Element& element, LocalMatrix& localMatrix, const TestLocalView& testLocalView, const AnsatzLocalView& ansatzLocalView)
  {
    const int dim = Element::dimension;
    auto geometry = element.geometry();
    using namespace Indices;
    const auto& velocityLocalFiniteElement = testLocalView.tree().child(_0,0).finiteElement();
    const auto& pressureLocalFiniteElement = testLocalView.tree().child(_1).finiteElement();

    int order = 2*(dim*velocityLocalFiniteElement.localBasis().order()-1);
    const auto& quad = QuadratureRules<double, dim>::rule(element.type(), order);
    for (const auto& quadPoint : quad)
    {
      const auto jacobianInverseTransposed = geometry.jacobianInverseTransposed(quadPoint.position());
      const auto integrationElement = geometry.integrationElement(quadPoint.position());

      std::vector<FieldMatrix<double,1,dim> > referenceJacobians;
      velocityLocalFiniteElement.localBasis().evaluateJacobian( quadPoint.position(), referenceJacobians);
      
      std::vector<FieldMatrix<double,1,dim> > velocityJacobians(referenceJacobians.size());
      for (size_t i=0; i<velocityJacobians.size(); i++)
        velocityJacobians[i] = referenceJacobians[i] * transpose(jacobianInverseTransposed);

      for (size_t i=0; i<velocityLocalFiniteElement.size(); i++)
        for (size_t j=0; j<velocityLocalFiniteElement.size(); j++ )
          for (size_t k=0; k<dim; k++)
          {
            size_t row = testLocalView.tree().child(_0,k).localIndex(i);
            size_t col = testLocalView.tree().child(_0,k).localIndex(j);
            localMatrix[row][col] += (velocityJacobians[i] * transpose(velocityJacobians[j])) * quadPoint.weight() * integrationElement;
          }

      std::vector<FieldVector<double,1> > pressureValues;
      pressureLocalFiniteElement.localBasis().evaluateFunction( quadPoint.position(), pressureValues);

      for (size_t i=0; i<velocityLocalFiniteElement.size(); i++)
        for (size_t j=0; j<pressureLocalFiniteElement.size(); j++ )
          for (size_t k=0; k<dim; k++)
          {
            size_t vIndex = testLocalView.tree().child(_0,k).localIndex(i);
            size_t pIndex = testLocalView.tree().child(_1).localIndex(j);
            localMatrix[vIndex][pIndex] += velocityJacobians[i][0][k] * pressureValues[j] * quadPoint.weight() * integrationElement;
            localMatrix[pIndex][vIndex] += velocityJacobians[i][0][k] * pressureValues[j] * quadPoint.weight() * integrationElement;
          }
    }
  }
};



template<int dim>
auto createUniformCubeGrid()
{
  using Grid = Dune::YaspGrid<dim>;
  Dune::FieldVector<double,dim> l(1.0);
  std::array<int,dim> elements = {{2, 2}};
  return std::make_unique<Grid>(l, elements);
}



auto createMixedGrid2d()
{
  using Grid = Dune::UGGrid<2>;
  Dune::GridFactory<Grid> factory;
  for(unsigned int k : Dune::range(9))
    factory.insertVertex({0.5*(k%3), 0.5*(k/3)});
  factory.insertElement(Dune::GeometryTypes::cube(2), {0, 1, 3, 4});
  factory.insertElement(Dune::GeometryTypes::cube(2), {1, 2, 4, 5});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {3, 4, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 7, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 5, 7});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {5, 8, 7});
  return std::unique_ptr<Grid>{factory.createGrid()};
}


template<class Matrix, class Basis, class... LocalAssembler>
auto compareOperatorAssemblers(const Basis& basis, LocalAssembler... localAssemblers)
{
  auto matrix = Matrix{};
  auto matrixBackend = Dune::Fufem::istlMatrixBackend(matrix);
  auto globalOperatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler(basis, basis);
  double patternWCT;
  {
    Dune::Timer timer;
    auto patternBuilder = matrixBackend.patternBuilder();
    globalOperatorAssembler.assembleBulkPattern(patternBuilder);
    patternBuilder.setupMatrix();
    patternWCT = timer.elapsed();
    matrix = 0.0;
  }

  auto assemble = [&](auto localAssembler)
  {
    Dune::Timer timer;
    double wct = std::numeric_limits<double>::max();
    for([[maybe_unused]] auto i: Dune::range(3))
    {
      matrix = 0;
      timer.reset();
      globalOperatorAssembler.assembleBulkEntries(matrixBackend, localAssembler);
      wct = std::min(wct, timer.elapsed());
    }
    return wct;
  };

  return std::array{patternWCT, assemble(localAssemblers)...};
}



int main (int argc, char *argv[]) try
{
  MPIHelper::instance(argc, argv);

  using namespace Dune::Functions::BasisFactory;
  using namespace Dune::Fufem::Forms;
  using namespace Dune::Indices;

  std::cout << std::left << std::setw(25) << "Testcase" << std::flush;
  std::cout << std::right << std::setw(10) << "size" << std::flush;
  std::cout << std::right << std::setw(15) << "pattern" << std::flush;
  std::cout << std::right << std::setw(15) << "manual" << std::flush;
  std::cout << std::right << std::setw(15) << "form" << std::flush;
  std::cout << std::right << std::setw(15) << "..." << std::flush;
  std::cout << std::endl;
  std::cout << std::left << std::setw(95) << std::setfill('-') << "" << std::endl;
  std::cout << std::setfill(' ');

  {
//    constexpr int dim = 2;
    auto grid = createMixedGrid2d();
    grid->globalRefine(8);

    auto basis = makeBasis(grid->leafGridView(), lagrange<1>());
    using Matrix = Dune::BCRSMatrix<FieldMatrix<double,1,1>>;

    std::cout << std::left << std::setw(25) << "Laplace,dim=2,PQ1" << std::flush;
    std::cout << Dune::formatString("   %7d", basis.dimension()) << std::flush;

//    auto manualAssembler = LocalLaplaceAssembler{};
    auto manualAssembler = Dune::Fufem::LaplaceAssembler{};

    auto v = testFunction(basis);
    auto u = trialFunction(basis);
    auto formAssembler = integrate(dot(grad(v), grad(u)));

    auto results = compareOperatorAssemblers<Matrix>(basis, manualAssembler, formAssembler);

    for(double wct : results)
      std::cout << Dune::formatString("   %12.5e", wct);

    std::cout << std::endl;
  }

  {
//    constexpr int dim = 3;
    using Grid = Dune::UGGrid<3>;
    auto grid = Dune::StructuredGridFactory<Grid>::createCubeGrid({{0,0,0}}, {{1,1,1}}, {{40,40,40}});

    auto basis = makeBasis(grid->leafGridView(), lagrange<1>());
    using Matrix = Dune::BCRSMatrix<FieldMatrix<double,1,1>>;

    std::cout << std::left << std::setw(25) << "Laplace,dim=3,PQ1" << std::flush;
    std::cout << Dune::formatString("   %7d", basis.dimension()) << std::flush;

//    auto manualAssembler = LocalLaplaceAssembler{};
    auto manualAssembler = Dune::Fufem::LaplaceAssembler{};

    auto v = testFunction(basis);
    auto u = trialFunction(basis);
    auto formAssembler = integrate(dot(grad(v), grad(u)));

    auto results = compareOperatorAssemblers<Matrix>(basis, manualAssembler, formAssembler);

    for(double wct : results)
      std::cout << Dune::formatString("   %12.5e", wct);

    std::cout << std::endl;
  }

  {
//    constexpr int dim = 2;
    auto grid = createMixedGrid2d();
    grid->globalRefine(7);

    auto basis = makeBasis(grid->leafGridView(), lagrange<2>());
    using Matrix = Dune::BCRSMatrix<FieldMatrix<double,1,1>>;

    std::cout << std::left << std::setw(25) << "Laplace,dim=2,PQ2" << std::flush;
    std::cout << Dune::formatString("   %7d", basis.dimension()) << std::flush;

//    auto manualAssembler = LocalLaplaceAssembler{};
    auto manualAssembler = Dune::Fufem::LaplaceAssembler{};

    auto v = testFunction(basis);
    auto u = trialFunction(basis);
    auto formAssembler = integrate(dot(grad(v), grad(u)));

    auto results = compareOperatorAssemblers<Matrix>(basis, manualAssembler, formAssembler);

    for(double wct : results)
      std::cout << Dune::formatString("   %12.5e", wct);

    std::cout << std::endl;
  }

  {
//    constexpr int dim = 3;
    using Grid = Dune::UGGrid<3>;
    auto grid = Dune::StructuredGridFactory<Grid>::createCubeGrid({{0,0,0}}, {{1,1,1}}, {{20,20,20}});

    auto basis = makeBasis(grid->leafGridView(), lagrange<2>());
    using Matrix = Dune::BCRSMatrix<FieldMatrix<double,1,1>>;

    std::cout << std::left << std::setw(25) << "Laplace,dim=3,PQ2" << std::flush;
    std::cout << Dune::formatString("   %7d", basis.dimension()) << std::flush;

//    auto manualAssembler = LocalLaplaceAssembler{};
    auto manualAssembler = Dune::Fufem::LaplaceAssembler{};

    auto v = testFunction(basis);
    auto u = trialFunction(basis);
    auto formAssembler = integrate(dot(grad(v), grad(u)));

    auto results = compareOperatorAssemblers<Matrix>(basis, manualAssembler, formAssembler);

    for(double wct : results)
      std::cout << Dune::formatString("   %12.5e", wct);

    std::cout << std::endl;
  }

  {
//    constexpr int dim = 2;
    auto grid = createMixedGrid2d();
    grid->globalRefine(8);

    auto basis = makeBasis(grid->leafGridView(), power<2>(lagrange<1>()));
    using Matrix = Dune::BCRSMatrix<FieldMatrix<double,2,2>>;

    std::cout << std::left << std::setw(25) << "Laplace,dim=2,(PQ1)^2" << std::flush;
    std::cout << Dune::formatString("   %7d", basis.dimension()) << std::flush;

//    auto manualAssembler = LocalLaplaceAssembler{};
    auto manualAssembler = Dune::Fufem::LaplaceAssembler{};

    auto v = testFunction(basis);
    auto u = trialFunction(basis);
    auto formAssembler = integrate(dot(grad(v), grad(u)));

    auto results = compareOperatorAssemblers<Matrix>(basis, manualAssembler, formAssembler);

    for(double wct : results)
      std::cout << Dune::formatString("   %12.5e", wct);

    std::cout << std::endl;
  }

  {
//    constexpr int dim = 3;
    using Grid = Dune::UGGrid<3>;
    auto grid = Dune::StructuredGridFactory<Grid>::createCubeGrid({{0,0,0}}, {{1,1,1}}, {{40,40,40}});

    auto basis = makeBasis(grid->leafGridView(), power<3>(lagrange<1>()));
    using Matrix = Dune::BCRSMatrix<FieldMatrix<double,3,3>>;

    std::cout << std::left << std::setw(25) << "Laplace,dim=3,(PQ1)^3" << std::flush;
    std::cout << Dune::formatString("   %7d", basis.dimension()) << std::flush;

//    auto manualAssembler = LocalLaplaceAssembler{};
    auto manualAssembler = Dune::Fufem::LaplaceAssembler{};

    auto v = testFunction(basis);
    auto u = trialFunction(basis);
    auto formAssembler = integrate(dot(grad(v), grad(u)));

    auto results = compareOperatorAssemblers<Matrix>(basis, manualAssembler, formAssembler);

    for(double wct : results)
      std::cout << Dune::formatString("   %12.5e", wct);

    std::cout << std::endl;
  }

  {
    constexpr int dim = 2;
    constexpr int order = 1;

    using Grid = Dune::YaspGrid<dim>;
    auto grid = Dune::StructuredGridFactory<Grid>::createCubeGrid({{0,0}}, {{1,1}}, {{200,200}});

    auto basis = makeBasis(grid->leafGridView(),
        composite(
          raviartThomas<order>(),
          lagrange<order>()
          ));

    using Matrix = Dune::Matrix<Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1>>>;

    std::cout << std::left << std::setw(25) << "Laplace,dim=2,RT1/PQ1" << std::flush;
    std::cout << Dune::formatString("   %7d", basis.dimension()) << std::flush;

    auto manualAssembler = LocalMixedLaplaceAssembler{};

    auto fluxBasis = Dune::Functions::subspaceBasis(basis, _0);
    auto pressureBasis = Dune::Functions::subspaceBasis(basis, _1);

    auto sigma = trialFunction(fluxBasis, NonAffineFamiliy{});
    auto u = trialFunction(pressureBasis);
    auto tau = testFunction(fluxBasis, NonAffineFamiliy{});
    auto v = testFunction(pressureBasis);

    auto formAssembler = integrate(dot(sigma, tau) - div(tau)*u - div(sigma)*v);

    auto results = compareOperatorAssemblers<Matrix>(basis, manualAssembler, formAssembler);

    for(double wct : results)
      std::cout << Dune::formatString("   %12.5e", wct);

    std::cout << std::endl;
  }

  {
    constexpr int dim = 2;
    constexpr int order = 1;

    using Grid = Dune::YaspGrid<dim>;
    auto grid = Dune::StructuredGridFactory<Grid>::createCubeGrid({{0,0}}, {{1,1}}, {{200,200}});

    auto basis = makeBasis(grid->leafGridView(),
        composite(
          power<dim>(
            lagrange<order+1>(),
            blockedInterleaved()),
          lagrange<order>()
          ));

    using Matrix00 = Dune::BCRSMatrix<Dune::FieldMatrix<double,dim,dim>>;
    using Matrix01 = BCRSMatrix<Dune::Fufem::SingleColumnMatrix<FieldMatrix<double,dim,1>>>;
    using Matrix10 = BCRSMatrix<Dune::Fufem::SingleRowMatrix<FieldMatrix<double,1,dim>>>;
    using Matrix11 = BCRSMatrix<FieldMatrix<double,1,1>>;
    using Matrix0 = Dune::MultiTypeBlockVector<Matrix00, Matrix01>;
    using Matrix1 = Dune::MultiTypeBlockVector<Matrix10, Matrix11>;
    using Matrix = Dune::MultiTypeBlockMatrix<Matrix0,Matrix1>;

    std::cout << std::left << std::setw(25) << "Stokes,dim=2,TH2/1" << std::flush;
    std::cout << Dune::formatString("%10d", basis.dimension()) << std::flush;

    auto manualAssembler = LocalStokesAssembler{};

    auto velocityBasis = Dune::Functions::subspaceBasis(basis, _0);
    auto pressureBasis = Dune::Functions::subspaceBasis(basis, _1);
    auto u = trialFunction(velocityBasis);
    auto p = trialFunction(pressureBasis);
    auto q = testFunction(pressureBasis);
    auto v = testFunction(velocityBasis);
    auto formAssembler = integrate( dot(grad(u), grad(v)) + div(u)*q + div(v)*p );

    auto results = compareOperatorAssemblers<Matrix>(basis, manualAssembler, formAssembler);

    for(double wct : results)
      std::cout << Dune::formatString("   %12.5e", wct);

    std::cout << std::endl;
  }

  {
    constexpr int dim = 3;
    constexpr int order = 1;

    using Grid = Dune::YaspGrid<dim>;
    auto grid = Dune::StructuredGridFactory<Grid>::createCubeGrid({{0,0,0}}, {{1,1,1}}, {{10,10,10}});

    auto basis = makeBasis(grid->leafGridView(),
        composite(
          power<dim>(
            lagrange<order+1>(),
            blockedInterleaved()),
          lagrange<order>()
          ));

    using Matrix00 = Dune::BCRSMatrix<Dune::FieldMatrix<double,dim,dim>>;
    using Matrix01 = BCRSMatrix<Dune::Fufem::SingleColumnMatrix<FieldMatrix<double,dim,1>>>;
    using Matrix10 = BCRSMatrix<Dune::Fufem::SingleRowMatrix<FieldMatrix<double,1,dim>>>;
    using Matrix11 = BCRSMatrix<FieldMatrix<double,1,1>>;
    using Matrix0 = Dune::MultiTypeBlockVector<Matrix00, Matrix01>;
    using Matrix1 = Dune::MultiTypeBlockVector<Matrix10, Matrix11>;
    using Matrix = Dune::MultiTypeBlockMatrix<Matrix0,Matrix1>;

    std::cout << std::left << std::setw(25) << "Stokes,dim=3,TH2/1" << std::flush;
    std::cout << Dune::formatString("%10d", basis.dimension()) << std::flush;

    auto manualAssembler = LocalStokesAssembler{};

    auto velocityBasis = Dune::Functions::subspaceBasis(basis, _0);
    auto pressureBasis = Dune::Functions::subspaceBasis(basis, _1);
    auto u = trialFunction(velocityBasis);
    auto p = trialFunction(pressureBasis);
    auto q = testFunction(pressureBasis);
    auto v = testFunction(velocityBasis);
    auto formAssembler = integrate( dot(grad(u), grad(v)) + div(u)*q + div(v)*p );

    auto results = compareOperatorAssemblers<Matrix>(basis, manualAssembler, formAssembler);

    for(double wct : results)
      std::cout << Dune::formatString("   %12.5e", wct);

    std::cout << std::endl;
  }

  return 0;
}
catch (Exception& e) {
  std::cout << e.what() << std::endl;
}
