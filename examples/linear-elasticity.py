import numpy as np
import scipy.sparse.linalg

import dune.common
import dune.geometry
import dune.grid
import dune.functions

from dune.grid import DataType
from dune.functions import defaultGlobalBasis, Power, Composite, Lagrange

from fufemforms import *
from utilities import *

def isNear(a,b):
  return np.abs(a-b) <= 1e-10



dimension = 3
ansatzOrder = 2

# Standard acceleration of gravity
g_n = 9.80665         # in m/s^2 = N/kg

# Density, Young's modulus, and Poisson ratio of steel
rho = 7.85e3          # in kg/m^3 = 10^3 g/cm^3
E = 200e9             # in Pa = N/m^2 = 10^9 GPa
nu = 0.3

# Gravitational force in y-direction
g = np.zeros(dimension)
g[1] = -g_n*rho

# Lamé parameters for St.Venant-Kirchhoff
lambda_ = E*nu/((1.-2.*nu) * (1.+nu))
mu = E/(2*(1+nu))

# Right hand side
def rhs(x):
    return g

# Dirichlet boundarBC: BEam clamped on one end

# Indicator function of Dirichlet boundary
def isDirichlet(x):
    y = np.zeros(dimension, dtype=bool)
    y.fill(isNear(x[0], 0))
    return y

# Dirichlet boundary values
def dirichletValues(x):
    return np.zeros(dimension)



def globalAssembler(basis):

    N = len(basis)

    # Mark all Dirichlet DOFs
    isConstrained = np.zeros( N, dtype=bool )
    basis.interpolate(isConstrained,isDirichlet)

    # Interpolate the boundary values
    constraintDOFValues = np.zeros( N )
    basis.interpolate(constraintDOFValues,dirichletValues)

    # Identity matrix
    Id = dune.common.FieldMatrix_double_3_3([[1,0,0], [0,1,0], [0,0,1]])

    # Symmetric gradient
    E = lambda w: symmetrize(grad(w))

    # Trial and test functions for variational problem
    u = trialFunction(basis)
    v = testFunction(basis)

    # Vector valued rhs coefficient function
    f = Coefficient(rhs, basis.gridView, rangeType=VectorType(dimension))

    # Use St. Venant-Kirchhoff material
    sigma = 2*mu*E(u) + lambda_*(Id*trace(E(u)))

    # Bilinear form and rhs functional
    a = integrate(dot(sigma, E(v)))
    b = integrate(dot(f,v))

    # Assemble into matrix and vector
    assembler = GlobalAssembler(basis)
    A = assembler.assembleOperator(a)
    B = assembler.assembleFunctional(b)

    # Build constraints into matrix
    incorporateEssentialConstraints(A, B, isConstrained, constraintDOFValues)

    return A, B

############################### START ########################################

# Create grid on a 100x1x1 beam
grid = dune.grid.structuredGrid([0,0,0],[1,.01,.01],[200, 2, 2])

# Create a vector-valued nodal Lagrange FE basis
basis = defaultGlobalBasis(grid, Power(Lagrange(order=ansatzOrder),exponent=dimension,blocked=False,layout="interleaved"))

print("Dimension of FE space is "+str(len(basis)))

# Compute A and b
A,b = globalAssembler(basis)

# Solve linear system!
x = scipy.sparse.linalg.spsolve(A, b)

u = basis.asFunction(x)

vtk = grid.vtkWriter(0)
u.addToVTKWriter("sol", vtk, DataType.PointVector)
vtk.write("linear-elasticity-py")

