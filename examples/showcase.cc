// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>
#include <cmath>

#include <dune/common/bitsetvector.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/dunefunctionsfunctionalassembler.hh>
#include <dune/fufem/backends/istlmatrixbackend.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>

#include <dune/fufem/forms/forms.hh>

#include "utilities.hh"

using namespace Dune;



template<int dim>
auto createUniformCubeGrid()
{
  using Grid = Dune::YaspGrid<dim>;
  Dune::FieldVector<double,dim> l(1.0);
  std::array<int,dim> elements = {{2, 2}};
  return std::make_unique<Grid>(l, elements);
}

#if HAVE_DUNE_UGGRID
auto createMixedGrid()
{
  using Grid = Dune::UGGrid<2>;
  Dune::GridFactory<Grid> factory;
  for(unsigned int k : Dune::range(9))
    factory.insertVertex({0.5*(k%3), 0.5*(k/3)});
  factory.insertElement(Dune::GeometryTypes::cube(2), {0, 1, 3, 4});
  factory.insertElement(Dune::GeometryTypes::cube(2), {1, 2, 4, 5});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {3, 4, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 7, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 5, 7});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {5, 8, 7});
  return std::unique_ptr<Grid>{factory.createGrid()};
}
#endif

int main (int argc, char *argv[]) try
{
  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  // Set up MPI, if available
  MPIHelper::instance(argc, argv);

  ///////////////////////////////////
  //   Generate the grid
  ///////////////////////////////////

#if HAVE_DUNE_UGGRID
  auto grid = createMixedGrid();
#else
  auto grid = createUniformCubeGrid<2>();
#endif

  grid->globalRefine(4);

  auto gridView = grid->leafGridView();
  using GridView = decltype(gridView);

  /////////////////////////////////////////////////////////
  //   Choose a finite element space
  /////////////////////////////////////////////////////////

  using namespace Functions::BasisFactory;

  auto basis = makeBasis(gridView, lagrange<2>());

  std::cout << "Number of DOFs is " << basis.dimension() << std::endl;

  /////////////////////////////////////////////////////////
  //   Stiffness matrix and right hand side vector
  /////////////////////////////////////////////////////////

  using Vector = Dune::BlockVector<double>;
  using Matrix = Dune::BCRSMatrix<double>;

  auto rhs = Vector();
  auto stiffnessMatrix = Matrix();



  /////////////////////////////////////////////////////////
  //  Problem data
  /////////////////////////////////////////////////////////

  auto alpha = Dune::FieldMatrix<double,2,2>({{1e-1, 0}, {0, 1}});
  auto beta = 1.0;

  auto dirichletIndicatorFunction = [](auto x) { return x[0] < 1; };
  auto rightHandSide = [] (const auto& x) { return 0; };
  auto robinValues = [] (const auto& x) { return x[1]>0.5 ? 1.0 : -1.0; };
  auto dirichletValues = [](const auto& x){ return 0; };
//  auto robinValues = [&] (const auto& x) { return alpha[0][0]; };
//  auto dirichletValues = [](const auto& x){ return x[0]; };

  /////////////////////////////////////////////////////////
  //  Create boundary patches for boundary conditions handling
  /////////////////////////////////////////////////////////

  auto dirichletPatch = BoundaryPatch(basis.gridView());
  dirichletPatch.insertFacesByProperty([&](auto&& intersection) { return dirichletIndicatorFunction(intersection.geometry().center()); });

  auto robinPatch = BoundaryPatch(basis.gridView());
  robinPatch.insertFacesByProperty([&](auto&& intersection) { return not dirichletPatch.contains(intersection); });


  /////////////////////////////////////////////////////////
  //  Create a colored grid partition for thread parallel assembly.
  /////////////////////////////////////////////////////////

#if DUNE_VERSION_GT(DUNE_FUNCTIONS, 2, 9)
  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(basis.gridView());
  auto globalAssembler = GlobalAssembler(basis, basis, 1, std::cref(gridViewPartition), threadCount);
#else
  auto globalAssembler = GlobalAssembler(basis, basis, 1);
#endif

  /////////////////////////////////////////////////////////
  //  Assemble the system
  /////////////////////////////////////////////////////////

  {
    Dune::Timer timer;
    using namespace ::Dune::Fufem::Forms;
    namespace F = ::Dune::Fufem::Forms;

    // To create an assembler by successively plugging together
    // k-linear (k=0,1,2) operators on function spaces. In the
    // end we e.g. arrive at a linear or bilinear operator
    // B:V \to V or A:V \times V \to V. Integrating these over
    // the domain gives a linear or bilinear form.

    // We start from trial and test functions. Here u and v both
    // represent the identity operator on the corresponding
    // finite element space. By declaring them as test or trial
    // function, we decide that they will finally be used as
    // first (test) or second (trial) argument of the bilinear
    // operator. Each operation on k-linear operators results in
    // a new l-linear operator.

    // Construct trial and test function representation.
    // u and v are both 1-linear operators (mapping u to u)
    // and v to v.
    auto u = trialFunction(basis);
    auto v = testFunction(basis);

    // Construct some coefficient functions.
    // These f and g are 0-linear operators, because they don't depend on the function space,
    // but are just values in a function space.
    auto f = Coefficient(Functions::makeGridViewFunction(rightHandSide, gridView));
    auto g = Coefficient(Functions::makeGridViewFunction(robinValues, gridView));

    // We use a constant diffusivity matrix.
    // Alternatively, we could use an x-dependent function
    // by marking it as Coefficient (aka 0-linear operator).

    // By applying grad we transform the 1-linear operators
    // u and v to the 1-linear operators grad(u) and grad(v)
    // that map u and v to their gradients.
    //
    // Multiplication always happens pointwise. Such that
    // alpha*grad(u) is againg in a 1-linear operator that.
    // Finally the dot operator multiplies the two 1-linear
    // operators pointwise to get a 2-linear operator.
    // The pointwise multiplication in the function space range space
    // is done using the dot inner product.
    // Finally we integrate over the domain, to represent
    // a bilinear form.
    auto a = integrate(dot(alpha*grad(u), grad(v))) + integrate(beta*u*v, robinPatch);

    // Using the same principles we can also construct
    // a linear form by integrating a 1-linear functional.
    // Notice that a plain integrate(...) represents a bulk
    // integral over the whole domain, while integrate(..., BoundaryPatch)
    // is a surface integral only over this subset of the boundary.
    auto b = integrate(f*v) + integrate(g*v, robinPatch);

    globalAssembler.assembleOperator(stiffnessMatrix, a);
    globalAssembler.assembleFunctional(rhs, b);

    // Other possible operations:
    //
    // For a treepath, that corresponds to a power space one level below leaf
    // the corresponding vector field is obtained by:
    //
    //   auto u = trialFunction(Functions::subspaceBasis(basis, treePath));
    //
    // Compute the Jacobian
    //
    //   jacobian(u)
    //
    // Compute the gradient (transposed of the jacobian)
    //
    //   gradient(u)
    //
    // Divergence of (vector field)
    //
    //   div(u)
    // i-th partial derivative (i-th component of the gradient)
    //
    //   D(u,i)
    //
    // Pointwise transposed of matrix valued u
    //
    //   transpose(u)
    //
    // Apply operator F pointwise
    //
    //   compose(F, u)
    //
    // Create a custom transformation operator (that applies F pointwise)
    // and apply it to u:
    //
    //   auto FF = RangeOperator(F);
    //   FF(u)
    //
    // Example: Manually create an operator that computes the i-th partial derivative
    // (same as D(u,i))
    //
    //   auto partial = RangeOperator([](auto u, int i) { return u[i]; });
    //   partial(u, i)
    //
    // Example: Define and use symmetric gradient
    //
    //   auto E = [](const auto& v) {
    //     return symmetrize(grad(v));
    //   };
    //
    //   auto a = integrate(2*mu*dot(E(u),E(v)) + lambda*trace(u)*trace(v));
  }


  /////////////////////////////////////////////////
  //   Choose an initial iterate
  /////////////////////////////////////////////////
  auto x = Vector(basis.size());
  x = 0;

  // Determine Dirichlet DOFs and interpolate boundary condition
  std::vector<char> dirichletNodes;
  Dune::Fufem::markBoundaryPatchDofs(dirichletPatch, basis, dirichletNodes);
  interpolate(basis, x, dirichletValues, dirichletNodes);

  //////////////////////////////////////////////////////
  //   Incorporate dirichlet values in a symmetric way
  //////////////////////////////////////////////////////


  incorporateEssentialConstraints(stiffnessMatrix, rhs, dirichletNodes, x);

  ////////////////////////////
  //   Compute solution
  ////////////////////////////

  // Technicality:  turn the matrix into a linear operator
  MatrixAdapter<Matrix,Vector,Vector> op(stiffnessMatrix);

  // Sequential incomplete LU decomposition as the preconditioner
  SeqILDL<Matrix,Vector,Vector> ildl(stiffnessMatrix,1.0);

  // Preconditioned conjugate-gradient solver
  CGSolver<Vector> cg(op,
                          ildl, // preconditioner
                          1e-4, // desired residual reduction factor
                          100,   // maximum number of iterations
                          2);   // verbosity of the solver

  // Object storing some statistics about the solving process
  InverseOperatorResult statistics;

  // Solve!
  cg.apply(x, rhs, statistics);

  ////////////////////////////////////////////////////////////////////////////
  //  Make a discrete function from the FE basis and the coefficient vector
  ////////////////////////////////////////////////////////////////////////////

  auto xFunction = Functions::makeDiscreteGlobalBasisFunction<double>(basis, x);

  //////////////////////////////////////////////////////////////////////////////////////////////
  //  Write result to VTK file
  //  We need to subsample, because VTK cannot natively display real second-order functions
  //////////////////////////////////////////////////////////////////////////////////////////////
  SubsamplingVTKWriter<GridView> vtkWriter(gridView, Dune::refinementLevels(2));
  vtkWriter.addVertexData(xFunction, VTK::FieldInfo("x", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("poisson-pq2");

 }
// Error handling
 catch (Exception& e) {
    std::cout << e.what() << std::endl;
 }
