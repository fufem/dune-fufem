// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_EXAMPLES_UTILITIES_HH
#define DUNE_FUFEM_EXAMPLES_UTILITIES_HH

#include <dune/common/timer.hh>
#include <dune/common/referencehelper.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/rangeutilities.hh>
#include <dune/common/typetraits.hh>
#include <dune/common/concept.hh>

#if DUNE_VERSION_GTE(DUNE_ISTL, 2, 10)
#include <dune/istl/paamg/fastamg.hh>
#endif

#include <dune/grid/common/rangegenerators.hh>

#include <dune/functions/backends/concepts.hh>
#include <dune/functions/backends/istlvectorbackend.hh>

#include <dune/fufem/backends/istlmatrixbackend.hh>
#include <dune/fufem/backends/traversal.hh>
#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/dunefunctionsfunctionalassembler.hh>


template<class Basis, class Vector>
decltype(auto) toVectorBackend(Vector& vector) {
  if constexpr (Dune::models<Dune::Functions::Concept::VectorBackend<Basis>, Vector>())
    return vector;
  else
    return Dune::Functions::istlVectorBackend(vector);
}

/*
 * Convenience class for global assembly.
 * This encapsulates global operator and functional assembler
 * and additional optional arguments for parallel assembly.
 */
template <class TestBasis, class TrialBasis, class... Args>
class GlobalAssembler
{
  using GridView = typename TestBasis::GridView;

public:

  GlobalAssembler(const TestBasis& testBasis, const TrialBasis& trialBasis, std::size_t verbosity, const Args&... args) :
    testBasis_(testBasis),
    trialBasis_(trialBasis),
    verbosity_(verbosity),
    args_(args...)
  {}

  template<class Matrix, class LocalAssembler>
  void assembleOperator(Matrix& matrix, LocalAssembler&& localAssembler) const
  {
    auto operatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler{testBasis_, trialBasis_};
    Dune::Timer timer;
    std::apply([&](auto&&... args) {
      operatorAssembler.assembleBulk(matrix, localAssembler, Dune::resolveRef(args)...);
    }, args_);
    if (verbosity_>0)
      std::cout << "Assembling operator took " << timer.elapsed() << "s" << std::endl;
  }

  template<class Matrix, class LocalAssembler, class Constraints>
  void assembleOperator(Matrix& matrix, LocalAssembler&& localAssembler, const Constraints& constraints) const
  {
    auto operatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler{testBasis_, trialBasis_};
    Dune::Timer timer;
    std::apply([&](auto&&... args) {
      auto&& matrixBackend = Dune::Fufem::istlMatrixBackend(matrix);
      auto patternBackend = matrixBackend.patternBuilder();

      operatorAssembler.assembleBulkPattern(patternBackend, Dune::resolveRef(args)...);

      constraints.extendMatrixPattern(patternBackend, testBasis_);

      patternBackend.setupMatrix();

      matrixBackend.assign(0);
      operatorAssembler.assembleBulkEntries(matrixBackend, localAssembler, Dune::resolveRef(args)...);
      constraints.constrainMatrix(matrix);
    }, args_);
    if (verbosity_>0)
      std::cout << "Assembling operator took " << timer.elapsed() << "s" << std::endl;
  }

  template<class Vector, class LocalAssembler>
  void assembleFunctional(Vector& vector, LocalAssembler&& localAssembler) const
  {
    auto functionalAssembler = Dune::Fufem::DuneFunctionsFunctionalAssembler{testBasis_};

    Dune::Timer timer;
    std::apply([&](auto&&... args) {
      functionalAssembler.assembleBulk(vector, localAssembler, Dune::resolveRef(args)...);
    }, args_);
    if (verbosity_>0)
      std::cout << "Assembling functional took " << timer.elapsed() << "s" << std::endl;
  }

  template<class Vector, class LocalAssembler, class Constraints>
  void assembleFunctional(Vector& vector, LocalAssembler&& localAssembler, const Constraints& constraints) const
  {
    auto functionalAssembler = Dune::Fufem::DuneFunctionsFunctionalAssembler{testBasis_};

    Dune::Timer timer;
    std::apply([&](auto&&... args) {
      functionalAssembler.assembleBulk(vector, localAssembler, Dune::resolveRef(args)...);
      constraints.constrainVector(vector);
    }, args_);
    if (verbosity_>0)
      std::cout << "Assembling functional took " << timer.elapsed() << "s" << std::endl;
  }

  template<class Matrix, class Vector, class LocalOperatorAssembler, class LocalFunctionalAssembler, class Constraints>
  void assembleSystem(Matrix& matrix, Vector& vector, LocalOperatorAssembler&& localOperatorAssembler, LocalFunctionalAssembler&& localFunctionalAssembler, const Constraints& constraints) const
  {
    Dune::Timer timer;

    auto operatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler{testBasis_, trialBasis_};
    std::apply([&](auto&&... args) {
      auto&& matrixBackend = Dune::Fufem::istlMatrixBackend(matrix);
      auto patternBackend = matrixBackend.patternBuilder();

      operatorAssembler.assembleBulkPattern(patternBackend, Dune::resolveRef(args)...);

      constraints.extendMatrixPattern(patternBackend, testBasis_);

      patternBackend.setupMatrix();

      matrixBackend.assign(0);
      operatorAssembler.assembleBulkEntries(matrixBackend, localOperatorAssembler, Dune::resolveRef(args)...);
    }, args_);
    if (verbosity_>0)
      std::cout << "Assembling operator took " << timer.elapsed() << "s" << std::endl;

    auto functionalAssembler = Dune::Fufem::DuneFunctionsFunctionalAssembler{testBasis_};
    std::apply([&](auto&&... args) {
      functionalAssembler.assembleBulk(vector, localFunctionalAssembler, Dune::resolveRef(args)...);
    }, args_);
    if (verbosity_>0)
      std::cout << "Assembling functional took " << timer.elapsed() << "s" << std::endl;

    constraints.constrainLinearSystem(matrix, vector);
    if (verbosity_>0)
      std::cout << "Constraining linear system took " << timer.elapsed() << "s" << std::endl;
  }

  template<class Vector>
  void initializeVector(Vector& vector) const
  {
    auto functionalAssembler = Dune::Fufem::DuneFunctionsFunctionalAssembler{testBasis_};
    auto&& vectorBackend = functionalAssembler.toVectorBackend(vector);
    vectorBackend.resize(trialBasis_);
    vectorBackend = 0;
  }


private:
  const TestBasis& testBasis_;
  const TrialBasis& trialBasis_;
  std::size_t verbosity_;
  std::tuple<Args...> args_;
};



/*
 * Convenience function for incorporating essential constraints
 * into a matrix and right-hand-side vector.
 *
 *
 *
 */
template<class Matrix, class Vector, class BitVector>
void incorporateEssentialConstraints(Matrix& matrix, Vector& rhs, const BitVector& isConstrained, const Vector& constraintValues)
{
  auto matrixBackend = Dune::Fufem::istlMatrixBackend(matrix);
  auto rhsBackend = Dune::Functions::istlVectorBackend(rhs);
  auto constraintValuesBackend = Dune::Functions::istlVectorBackend(constraintValues);
  auto isConstrainedBackend = Dune::Functions::istlVectorBackend(isConstrained);
  auto equals = [](const auto& i, const auto& j) {
    if constexpr (std::is_same_v<decltype(i), decltype(j)>)
      return i==j;
    else
      return false;
  };
  Dune::Fufem::recursiveForEachMatrixEntry(matrix, [&](auto&& matrix_ij, const auto& i, const auto& j){
    if (isConstrainedBackend[i])
    {
      rhsBackend[i] = constraintValuesBackend[i];
      matrix_ij = equals(i, j) ? 1 : 0;
    }
    else
    {
      if (isConstrainedBackend[j])
      {
        rhsBackend[i] -= matrix_ij*constraintValuesBackend[j];
        matrix_ij = 0;
      }
    }
  });
}



#if DUNE_VERSION_GTE(DUNE_ISTL, 2, 10)
/**
 * \brief Create a FastAMG preconditioner
 * \tparam Matrix Type of the matrix the preconditioner should act on.
 * \tparam Vector Type of vectors the preconditioner should act on.
 * \param matrix The matrix used in the preconditioner
 *
 * This constructs a Dune::FastAMG preconditioner. With some
 * standard arguments that are not defaulted in dune-istl.
 */
template<class Vector, class Matrix>
auto makeAMGPreconditioner(const Matrix& matrix)
{
  using Operator = Dune::MatrixAdapter<Matrix,Vector,Vector>;
  using Criterion = Dune::Amg::SymmetricCriterion<Matrix, Dune::Amg::RowSum>;
  using AMG = Dune::Amg::FastAMG<Operator, Vector>;

  auto parameters = Dune::Amg::Parameters();
  parameters.setDebugLevel(0);
  parameters.setNoPreSmoothSteps(1);
  parameters.setNoPostSmoothSteps(1);

  auto criterion = Criterion();
  criterion.setDebugLevel(0);

  auto opPtr = std::make_shared<Operator>(matrix);
  return AMG(opPtr, criterion, parameters);
}

/**
 * \brief Create a FastAMG preconditioner
 * \tparam Matrix Type of the matrix the preconditioner should act on.
 * \tparam Vector Type of vectors the preconditioner should act on.
 * \param matrix The matrix used in the preconditioner
 * \param dummy A dummy vector argument to enable type deduction for the Vector type.
 *
 * This constructs a Dune::FastAMG preconditioner.
 */
template<class Vector, class Matrix>
auto makeAMGPreconditioner(const Matrix& matrix, const Vector& dummy)
{
  return makeAMGPreconditioner<Vector>(matrix);
}
#endif



// Compute the (i,j)-th cofactor of F
template<class K, int d>
constexpr K cofactor(const Dune::FieldMatrix<K,d,d>& F, std::size_t i, std::size_t j)
{
  int sign = ((i+j) % 2) ? -1:1;
  if constexpr (d==2)
    return sign * F[(i+1)%2][(j+1)%2];
  if constexpr (d==3)
  {
    // View on the submatrix with i-th row and i-th column removed
    auto F_ij = [&](std::size_t k, std::size_t l) {
      return F[k+(i<=k)][l+(j<=l)];
    };
    return sign * (F_ij(0,0)*F_ij(1,1) - F_ij(0,1)*F_ij(1,0));
  }
}



// Compute the determinant
template<class K, int d>
constexpr K det(const Dune::FieldMatrix<K,d,d>& F)
{
  if constexpr (d==1)
    return F;
  if constexpr (d==2)
    return F[0][0]*F[1][1] - F[0][1]*F[1][0];
  if constexpr (d==3)
    return F[0][0]*cofactor(F,0,0) + F[0][1]*cofactor(F,0,1) + F[0][2]*cofactor(F,0,2);
}

// Compute the first derivative of det(F) wrt F.
// This is a linear map from matrix space to R.
// The result is represented as a matrix and should
// be interpreted wrt the Frobenius inner product.
// The application of the linear functional to a
// (matrix) direction U is then obtained as dot(Ddet(F),U).
//
// In this representation it is just the
// cofactor matrix cof(F) of F.
template<class K, int d>
constexpr Dune::FieldMatrix<K,d,d> Ddet(const Dune::FieldMatrix<K,d,d>& F)
{
  Dune::FieldMatrix<K,d,d> D;
  if constexpr (d==1)
    D = 1;
  if constexpr ((d==2) or (d==3))
  {
    for(auto i : Dune::range(d))
      for(auto j : Dune::range(d))
        D[i][j] = cofactor(F, i, j);
  }
  return D;
}



template<class K, int d>
class DeterminantHessianTensor;

template<class K>
class DeterminantHessianTensor<K,1>
{
public:

  DeterminantHessianTensor() = default;

  DeterminantHessianTensor(const Dune::FieldMatrix<K,1,1>& F)
  {}

  template <class Scalar, std::enable_if_t<Dune::IsNumber<Scalar>::value, int> = 0>
  auto& operator*= (const Scalar& scalar)
  {
    return (*this);
  }

  auto operator()(const Dune::FieldMatrix<K,1,1>& U) const
  {
    // Determinant is linear for d=1, hence the hessian is constant 0 in F
    return Dune::FieldMatrix<K,1,1>(0);
  }

  template <class Scalar, std::enable_if_t<Dune::IsNumber<Scalar>::value, int> = 0>
  friend auto operator*(const Scalar&, const DeterminantHessianTensor& H)
  {
    return DeterminantHessianTensor();
  }

  friend auto operator*(const DeterminantHessianTensor& H, const Dune::FieldMatrix<K,1,1>& U)
  {
    return H(U);
  }

};

template<class K>
class DeterminantHessianTensor<K,2>
{
public:
  DeterminantHessianTensor(const Dune::FieldMatrix<K,2,2>& F)
  {}

  template <class Scalar, std::enable_if_t<Dune::IsNumber<Scalar>::value, int> = 0>
  auto& operator*= (const Scalar& scalar)
  {
    return (*this);
  }

  auto operator()(const Dune::FieldMatrix<K,2,2>& U) const
  {
    // Determinant is quadratic for d=2, hence the hessian is constant in F
    return Ddet(U);
  }

  template <class Scalar, std::enable_if_t<Dune::IsNumber<Scalar>::value, int> = 0>
  friend auto operator*(const Scalar&, const DeterminantHessianTensor& H)
  {
    return DeterminantHessianTensor();
  }

  friend auto operator*(const DeterminantHessianTensor& H, const Dune::FieldMatrix<K,2,2>& U)
  {
    return H(U);
  }

};

template<class K>
class DeterminantHessianTensor<K,3>
{
public:
  DeterminantHessianTensor(const Dune::FieldMatrix<K,3,3>& F) :
    F_(F)
  {}

  template <class Scalar, std::enable_if_t<Dune::IsNumber<Scalar>::value, int> = 0>
  auto& operator*= (const Scalar& scalar)
  {
    F_ *= scalar;
    return (*this);
  }

  auto operator()(const Dune::FieldMatrix<K,3,3>& U) const
  {
    Dune::FieldMatrix<K,3,3> HU(0);

    // The algorithm to compute the ij-th entry of the result HU is:
    //
    // Let U_ij the 2x2 submatrix of U with i-th row and j-th column removed.
    // Let F_ij the 2x2 submatrix of F with i-th row and j-th column removed.
    // Let cof(F_ij) the 2x2 cofactor matrix of F_ij. Then we have:
    //
    //   HU_ij = (-1)^(i+j) * (U_ij : cof(F_ij))
    //
    // Alternatively: Let H_ij_kl = Dij_Dkl_det(F) the factor in front of the kl-th
    // entry of U in the sum of HU_ij. Then we have
    //
    // H_ij_kl = det(F+Eij+Ekl) - det(F+Eij) - det(F+Ekl) + det(F) = det(F(ij,kl)+Eij+Ekl)
    //
    // where F(ij,kl) is obtained by setting the the i-th and k-th row
    // as well as the j-th and l-th column of A to zero. I.e. H_ij_kl
    // can be considered a higher order cofactor of F.
    //
    // This can be condensed to the following algorithm. But even if all loops
    // are hard wired, the compiler generates faster code, if the expressions
    // are listed explicitly (see below).
#if 0
    Dune::Hybrid::forEach(std::make_index_sequence<3>(), [&](auto i)
    {
      Dune::Hybrid::forEach(std::make_index_sequence<3>(), [&](auto j)
      {
        Dune::Hybrid::forEach(std::make_index_sequence<2>(), [&](auto local_k)
        {
          constexpr auto k = local_k+(i<=local_k);
          constexpr auto swap_k = not(local_k)+(i<=not(local_k));
          Dune::Hybrid::forEach(std::make_index_sequence<2>(), [&](auto local_l)
          {
            constexpr auto l = local_l+(j<=local_l);
            constexpr auto swap_l = not(local_l)+(j<=not(local_l));
            constexpr int sign = ((i+j+local_k+local_l) % 2) ? -1:1;
            HU[i][j] += sign*F_[swap_k][swap_l] * U[k][l];
          });
        });
      });
    });
#else
    HU[0][0] =  + F_[2][2]*U[1][1] - F_[2][1]*U[1][2] - F_[1][2]*U[2][1] + F_[1][1]*U[2][2];
    HU[0][1] =  - F_[2][2]*U[1][0] + F_[2][0]*U[1][2] + F_[1][2]*U[2][0] - F_[1][0]*U[2][2];
    HU[0][2] =  + F_[2][1]*U[1][0] - F_[2][0]*U[1][1] - F_[1][1]*U[2][0] + F_[1][0]*U[2][1];

    HU[1][0] =  - F_[2][2]*U[0][1] + F_[2][1]*U[0][2] + F_[0][2]*U[2][1] - F_[0][1]*U[2][2];
    HU[1][1] =  + F_[2][2]*U[0][0] - F_[2][0]*U[0][2] - F_[0][2]*U[2][0] + F_[0][0]*U[2][2];
    HU[1][2] =  - F_[2][1]*U[0][0] + F_[2][0]*U[0][1] + F_[0][1]*U[2][0] - F_[0][0]*U[2][1];

    HU[2][0] =  + F_[1][2]*U[0][1] - F_[1][1]*U[0][2] - F_[0][2]*U[1][1] + F_[0][1]*U[1][2];
    HU[2][1] =  - F_[1][2]*U[0][0] + F_[1][0]*U[0][2] + F_[0][2]*U[1][0] - F_[0][0]*U[1][2];
    HU[2][2] =  + F_[1][1]*U[0][0] - F_[1][0]*U[0][1] - F_[0][1]*U[1][0] + F_[0][0]*U[1][1];
#endif

    return HU;
  }

  template <class Scalar, std::enable_if_t<Dune::IsNumber<Scalar>::value, int> = 0>
  friend auto operator*(const Scalar& scalar, const DeterminantHessianTensor& H)
  {
    auto Copy = H;
    Copy *= scalar;
    return Copy;
  }

  friend auto operator*(const DeterminantHessianTensor& H, const Dune::FieldMatrix<K,3,3>& U)
  {
    return H(U);
  }

protected:
  Dune::FieldMatrix<K,3,3> F_;
};



// Compute the second derivative of det(F) wrt F.
// This can be interpreted in several ways:
// (a) As a bilinear map from R^{dxd} x R^{dxd} to R.
// (b) As a fourth order tensor in R^{dxdxdxd}.
// (c) As a linear map from R^{dxd} to R^{dxd}.
//
// We represent the tensor as a special type, that
// allows essentially one operation: Application
// of the linear map from (c) to a dxd matrix.
// Due to linearity, this operation is provided
// by operator() and operator*.
//
// The resulting dxd matrix has to be interpreted
// wrt the Frobenius inner product. Hence given
// two matrices U and V we obtain the second order
// directional derivative indirections U and V of
// det at F by dot(DDdet(F)*U,V).
template<class K, int d>
constexpr auto DDdet(const Dune::FieldMatrix<K,d,d>& F)
{
  return DeterminantHessianTensor<K,d>(F);
}



template<int d>
void checkDdet(const Dune::FieldMatrix<double,d,d>& F)
{
  // Since det is d-linear, it depends linearly on each entry F_ij.
  // Thus we can compute the exact (ij)-th first partial derivative
  // using a difference quotient with fixed h=1.
  double err = 0;
  auto D_detF = Ddet(F);
  for(auto i : Dune::range(d))
  {
    for(auto j : Dune::range(d))
    {
      Dune::FieldMatrix<double,d,d> Eij;
      Eij[i][j] = 1;
      auto Dij_detF = D_detF[i][j];
      auto Dij_detF_h = det(F+Eij) - det(F);
      err = std::max(err, std::fabs(Dij_detF - Dij_detF_h));
    }
  }
  std::cout << "Error for Ddet()  : " << err << std::endl;
}

template<int d>
void checkDDdet(const Dune::FieldMatrix<double,d,d>& F)
{
  // Since DDdet is (d-2)-linear, it depends linearly on each entry F_ij.
  // Thus we can compute the exact (ij)-th second partial derivative
  // using a difference quotient with fixed h=1.
  double err = 0;
  auto D_D_detF = DDdet(F);
  for(auto i : Dune::range(d))
  {
    for(auto j : Dune::range(d))
    {
      Dune::FieldMatrix<double,d,d> Eij;
      Eij[i][j] = 1;
      auto D_Dij_detF = D_D_detF*Eij;
      for(auto k : Dune::range(d))
      {
        for(auto l : Dune::range(d))
        {
          Dune::FieldMatrix<double,d,d> Ekl;
          Ekl[k][l] = 1;
          auto Dkl_Dij_detF = D_Dij_detF[k][l];
          auto Dkl_Dij_detF_h = (det(F+Ekl+Eij) - det(F+Ekl)) - (det(F+Eij) - det(F));
          err = std::max(err, std::fabs(Dkl_Dij_detF - Dkl_Dij_detF_h));
        }
      }
    }
  }
  std::cout << "Error for DDdet() : " << err << std::endl;
}

template<int d=0>
void checkDet()
{
  if constexpr (d == 0)
  {
    checkDet<1>();
    checkDet<2>();
    checkDet<3>();
  }
  else
  {
    std::cout << "Checking det and derivatives for d=" << d << std::endl;

    // Creating test matrix
    Dune::FieldMatrix<double,d,d> F;
    for(auto i : Dune::range(d))
      for(auto j : Dune::range(d))
        F[i][j] = i + j + std::sin(1+i)*std::cos(1+j);

    std::cout << "Error for det()   : " << std::fabs(det(F) - F.determinant()) << std::endl;
    checkDdet(F);
    checkDDdet(F);
  }
}





#endif // DUNE_FUFEM_EXAMPLES_UTILITIES_HH
