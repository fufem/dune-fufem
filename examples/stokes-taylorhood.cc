// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <array>
#include <vector>

#include <dune/common/bitsetvector.hh>
#include <dune/common/indices.hh>
#include <dune/common/version.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/utility/structuredgridfactory.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/multitypeblockmatrix.hh>
#include <dune/istl/multitypeblockvector.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/taylorhoodbasis.hh>
#include <dune/functions/backends/istlvectorbackend.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/subspacebasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/fufem/constraints/affineconstraints.hh>
#include <dune/fufem/constraints/boundaryconstraints.hh>

#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/datacollectors/lagrangedatacollector.hh>
#else
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#endif


#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/dunefunctionsfunctionalassembler.hh>
#include <dune/fufem/backends/istlmatrixbackend.hh>

#include <dune/fufem/forms/forms.hh>

#include "utilities.hh"


#define BLOCKEDBASIS 1

// { using_namespace_dune_begin }
using namespace Dune;
// { using_namespace_dune_end }



#if DUNE_VERSION_GTE(DUNE_ISTL, 2, 10)

template<class V, class... P>
class BlockDiagonalPreconditioner : public Preconditioner<V,V> {

public:

  BlockDiagonalPreconditioner (const V& x, const P&... p) :
    preconditioners_(p...)
  {}

  void pre (V& x, V& b) override
  {
    using namespace Dune::Indices;
    preconditioners_[_0].pre(x[_0], b[_0]);
    preconditioners_[_1].pre(x[_1], b[_1]);
  }

  void apply (V& v, const V& d) override
  {
    using namespace Dune::Indices;
    preconditioners_[_0].apply(v[_0], d[_0]);
    preconditioners_[_1].apply(v[_1], d[_1]);
  }

  void post ([[maybe_unused]] V& x) override
  {}

  Dune::SolverCategory::Category category() const override
  {
    return Dune::SolverCategory::sequential;
  }

private:
  Dune::TupleVector<P...> preconditioners_;
};

#endif



// { main_begin }
int main (int argc, char *argv[]) try
{
  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  std::size_t refinements = 0;
  if (argc>2)
    refinements = std::stoul(std::string(argv[2]));


  // Set up MPI, if available
  MPIHelper::instance(argc, argv);
  // { mpi_setup_end }

  ///////////////////////////////////
  //   Generate the grid
  ///////////////////////////////////

  // { grid_setup_begin }
  const int dim = 2;
  using Grid = Dune::YaspGrid<dim>;
  auto gridPtr = Dune::StructuredGridFactory<Grid>::createCubeGrid({{0,0}}, {{1,1}}, {{4,4}});
  auto& grid = *gridPtr;

  grid.globalRefine(refinements);

  using GridView = typename Grid::LeafGridView;
  GridView gridView = grid.leafGridView();
  // { grid_setup_end }

  /////////////////////////////////////////////////////////
  //   Choose a finite element space
  /////////////////////////////////////////////////////////

#if BLOCKEDBASIS
  // { function_space_basis_begin }
  using namespace Functions::BasisFactory;

  constexpr std::size_t K = 1; // pressure order for Taylor-Hood

  auto taylorHoodBasis = makeBasis(
          gridView,
          composite(
            power<dim>(
              lagrange<K+1>(),
              blockedInterleaved()),
            lagrange<K>()
          ));
  // { function_space_basis_end }
#else
  using namespace Functions::BasisFactory;

  static const std::size_t K = 1; // pressure order for Taylor-Hood
  auto taylorHoodBasis = makeBasis(
          gridView,
          composite(
            power<dim>(
              lagrange<K+1>(),
              flatInterleaved()),
            lagrange<K>()
          ));
#endif

  std::cout << "Dimension of Taylor-Hood ansatz space is " << taylorHoodBasis.dimension() << std::endl;

  /////////////////////////////////////////////////////////
  //   Stiffness matrix and right hand side vector
  /////////////////////////////////////////////////////////

#if BLOCKEDBASIS
  // { linear_algebra_setup_begin }
  using VelocityVector = BlockVector<FieldVector<double,dim>>;
  using PressureVector = BlockVector<FieldVector<double,1>>;
  using Vector = MultiTypeBlockVector<VelocityVector, PressureVector>;

  using VelocityBitVector = BlockVector<FieldVector<char,dim>>;
  using PressureBitVector = BlockVector<FieldVector<char,1>>;
  using BitVector = MultiTypeBlockVector<VelocityBitVector, PressureBitVector>;

  using Matrix00 = BCRSMatrix<FieldMatrix<double,dim,dim>>;
  using Matrix01 = BCRSMatrix<Dune::Fufem::SingleColumnMatrix<FieldMatrix<double,dim,1>>>;
  using Matrix10 = BCRSMatrix<Dune::Fufem::SingleRowMatrix<FieldMatrix<double,1,dim>>>;
  using Matrix11 = BCRSMatrix<FieldMatrix<double,1,1>>;   /*@\label{li:matrix_type_pressure_pressure}@*/
  using MatrixRow0 = MultiTypeBlockVector<Matrix00, Matrix01>;
  using MatrixRow1 = MultiTypeBlockVector<Matrix10, Matrix11>;
  using Matrix = MultiTypeBlockMatrix<MatrixRow0,MatrixRow1>;
  // { linear_algebra_setup_end }
#else
  using Vector = BlockVector<BlockVector<double> >;
  using BitVector = BlockVector<BlockVector<char> >;
  using Matrix = Dune::Matrix<BCRSMatrix<double> >;
#endif

  /////////////////////////////////////////////////////////
  //  Assemble the system
  /////////////////////////////////////////////////////////

  using namespace Indices;

  // { rhs_assembly_begin }
  Vector rhs;

  auto rhsBackend = Dune::Functions::istlVectorBackend(rhs);

  rhsBackend.resize(taylorHoodBasis);
  rhs = 0;                                 /*@\label{li:stokes_taylorhood_set_rhs_to_zero}@*/
  // { rhs_assembly_end }

  // { matrix_assembly_begin }
  Matrix stiffnessMatrix;

  auto coloredPartition = Dune::Fufem::coloredGridViewPartition(gridView);

  {
    Dune::Timer timer;
    using namespace ::Dune::Fufem::Forms;
    auto velocityBasis = Functions::subspaceBasis(taylorHoodBasis, _0);
    auto pressureBasis = Functions::subspaceBasis(taylorHoodBasis, _1);

    auto u = trialFunction(velocityBasis);
    auto p = trialFunction(pressureBasis);
    auto q = testFunction(pressureBasis);
    auto v = testFunction(velocityBasis);

    auto A = integrate( dot(grad(u), grad(v)) - div(u)*q - div(v)*p );

    auto operatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler{taylorHoodBasis, taylorHoodBasis};

    std::cout << "Setting up local assembler and matrix backend took " << timer.elapsed() << "s" << std::endl;
    timer.reset();

    {
      Dune::Timer timer;

      operatorAssembler.assembleBulk(stiffnessMatrix, A, coloredPartition, threadCount);

      std::cout << "assembleBulk took " << timer.elapsed() << "s." << std::endl;
    }

    std::cout << "Assembling the problem took " << timer.elapsed() << "s" << std::endl;
  }

  // { matrix_assembly_end }

  /////////////////////////////////////////////////////////
  // Set Dirichlet values.
  // Only velocity components have Dirichlet boundary values
  /////////////////////////////////////////////////////////

  using Coordinate = GridView::Codim<0> ::Geometry::GlobalCoordinate;
  using VelocityRange = FieldVector<double,dim>;
  auto&& velocityDirichletData = [](Coordinate x)
  {
    return VelocityRange{0.0, double(x[0] < 1e-8)};
  };

  auto constraints = Dune::Fufem::makeAffineConstraints<BitVector, Vector>(taylorHoodBasis);
  computeBoundaryConstraints(constraints, Functions::subspaceBasis(taylorHoodBasis, _0), velocityDirichletData);

  ////////////////////////////////////////////
  //   Modify Dirichlet rows
  ////////////////////////////////////////////

  // loop over the matrix rows
  // { set_dirichlet_matrix_begin }
  constraints.constrainLinearSystem(stiffnessMatrix, rhs);
  // { set_dirichlet_matrix_end }

  ////////////////////////////
  //   Compute solution
  ////////////////////////////
  // { stokes_solve_begin }
  // Start from the rhs vector; that way the Dirichlet entries are already correct
  Vector x = rhs;

  // Technicality:  turn the matrix into a linear operator
  MatrixAdapter<Matrix,Vector,Vector> stiffnessOperator(stiffnessMatrix);

#if DUNE_VERSION_GTE(DUNE_ISTL, 2, 10)
  // As iterative solver we use MinRes with a block-diagonal preconditioner
  // for the saddle point problem. An optimal preconditioner would be obtained,
  // if the (0,0)-velocity-block is exact, while the (1,1)-block is the Schur-
  // complement of the system. Since the discretization is inf-sup-stable,
  // the Schur-complement operator is spectrally equivalent to the L^2 inner
  // product. Hence an h-independent preconditioner of the Schur-complement
  // is given by the pressure-mass matrix. Since both diagonal blocks are sparse
  // and s.p.d. we invert both inexactly with one step an AMG-preconditioner.

  // Compute pressure-mass-matrix.
  using PressureMatrix = std::decay_t<decltype(stiffnessMatrix[_1][_1])>;
  auto pressureMassMatrix = PressureMatrix();
  {
    using namespace ::Dune::Fufem::Forms;
    auto pressureBasis = makeBasis(gridView, lagrange<K>());
    auto p = trialFunction(pressureBasis);
    auto q = testFunction(pressureBasis);
    auto M = integrate(p*q);
    auto operatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler{pressureBasis, pressureBasis};
    operatorAssembler.assembleBulk(pressureMassMatrix, M, coloredPartition, threadCount);
  }

  using namespace Dune::Indices;

  auto preconditioner = BlockDiagonalPreconditioner(x,
                          makeAMGPreconditioner(stiffnessMatrix[_0][_0], x[_0]),
                          makeAMGPreconditioner(pressureMassMatrix, x[_1]));

  // Construct the actual iterative solver
  MINRESSolver<Vector> solver(
          stiffnessOperator,  // operator to invert
          preconditioner,     // preconditioner for iteration
          1e-10,              // desired residual reduction factor
          500,                // maximum number of iterations
          2);                 // verbosity of the solver
#else

  // Fancy (but only) way to not have a preconditioner at all
  Richardson<Vector,Vector> preconditioner(1.0);

  // Construct the actual iterative solver
  RestartedGMResSolver<Vector> solver(
          stiffnessOperator,  // operator to invert
          preconditioner,     // preconditioner for iteration
          1e-10,              // desired residual reduction factor
          500,                // number of iterations between restarts
          500,                // maximum number of iterations
          2);                 // verbosity of the solver
#endif


  // Object storing some statistics about the solving process
  InverseOperatorResult statistics;

  // Solve!
  solver.apply(x, rhs, statistics);
  // { stokes_solve_end }

  ////////////////////////////////////////////////////////////////////////////
  //  Make a discrete function from the FE basis and the coefficient vector
  ////////////////////////////////////////////////////////////////////////////

  // { make_result_functions_begin }
//  using VelocityRange = FieldVector<double,dim>;
//  using PressureRange = double;

//  auto velocityFunction
//          = Functions::makeDiscreteGlobalBasisFunction<VelocityRange>(
//            Functions::subspaceBasis(taylorHoodBasis, _0), x);
//  auto pressureFunction
//          = Functions::makeDiscreteGlobalBasisFunction<PressureRange>(
//            Functions::subspaceBasis(taylorHoodBasis, _1), x);

  using namespace Dune::Fufem::Forms;
  auto u = bindToCoefficients(trialFunction(taylorHoodBasis, _0), x);
  auto p = bindToCoefficients(trialFunction(taylorHoodBasis, _1), x);

  // { make_result_functions_end }

  //////////////////////////////////////////////////////////////////////////////////////////////
  //  Write result to VTK file
  //////////////////////////////////////////////////////////////////////////////////////////////
  // { vtk_output_begin }
#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
  using Dune::Vtk::LagrangeDataCollector;
  using Dune::Vtk::UnstructuredGridWriter;
  auto vtkWriter = UnstructuredGridWriter(LagrangeDataCollector(gridView, K));
  vtkWriter.write("stokes-taylorhood-result");
  vtkWriter.addPointData(u, VTK::FieldInfo("velocity", VTK::FieldInfo::Type::vector, dim));
  vtkWriter.addPointData(p, VTK::FieldInfo("pressure", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.addPointData(grad(p), VTK::FieldInfo("pressure_gradient", VTK::FieldInfo::Type::vector, dim));
  vtkWriter.addPointData(div(u), VTK::FieldInfo("divergence", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("stokes-taylorhood-result");
#else
  //  We need to subsample, because VTK cannot natively display real second-order functions
  auto vtkWriter = SubsamplingVTKWriter(gridView, refinementLevels(2));
  vtkWriter.addVertexData(u, VTK::FieldInfo("velocity", VTK::FieldInfo::Type::vector, dim));
  vtkWriter.addVertexData(p, VTK::FieldInfo("pressure", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.addVertexData(grad(p), VTK::FieldInfo("pressure_gradient", VTK::FieldInfo::Type::vector, dim));
  vtkWriter.addVertexData(div(u), VTK::FieldInfo("divergence", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("stokes-taylorhood-result");
#endif
  // { vtk_output_end }

 }
// Error handling
 catch (Exception& e) {
    std::cout << e.what() << std::endl;
 }
