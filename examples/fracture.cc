// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <array>
#include <vector>

#include <dune/common/bitsetvector.hh>
#include <dune/common/indices.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/multitypeblockmatrix.hh>
#include <dune/istl/multitypeblockvector.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/taylorhoodbasis.hh>
#include <dune/functions/backends/istlvectorbackend.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/subspacebasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/functionspacebases/transformedindexbasis.hh>

#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/dunefunctionsfunctionalassembler.hh>
#include <dune/fufem/backends/istlmatrixbackend.hh>

#include <dune/fufem/forms/forms.hh>



int main (int argc, char *argv[]) try
{
  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  std::size_t refinements = 0;
  if (argc>2)
    refinements = std::stoul(std::string(argv[2]));


  // Set up MPI, if available
  Dune::MPIHelper::instance(argc, argv);

  ///////////////////////////////////
  //   Generate the grid
  ///////////////////////////////////

  const int dim = 2;
  using Grid = Dune::YaspGrid<dim>;
  Dune::FieldVector<double,dim> upperRight = {1, 1};
//  std::array<int,dim> elements = {{400, 400}};
  std::array<int,dim> elements = {{4, 4}};
  Grid grid(upperRight,elements);
  grid.globalRefine(refinements);

  /////////////////////////////////////////////////////////
  //   Choose a finite element space
  /////////////////////////////////////////////////////////

  using namespace Dune::Indices;
  using namespace Dune::Functions::BasisFactory;

  auto basis = makeBasis(
    grid.leafGridView(),
    composite(
      power<dim>(lagrange<1>()),
      lagrange<1>()
    ));

  /////////////////////////////////////////////////////////
  //   Stiffness matrix and right hand side vector
  /////////////////////////////////////////////////////////

  using M00 = Dune::BCRSMatrix<Dune::FieldMatrix<double,dim,dim>>;
  using M01 = Dune::BCRSMatrix<Dune::Fufem::SingleColumnMatrix<Dune::FieldMatrix<double,dim,1>>>;
  using M10 = Dune::BCRSMatrix<Dune::Fufem::SingleRowMatrix<Dune::FieldMatrix<double,1,dim>>>;
  using M11 = Dune::BCRSMatrix<double>;
  using Matrix = Dune::MultiTypeBlockMatrix<Dune::MultiTypeBlockVector<M00,M01>, Dune::MultiTypeBlockVector<M10, M11>>;

  using DisplacementVector = Dune::BlockVector<Dune::FieldVector<double,dim> >;
  using DamageVector = Dune::BlockVector<double>;
  using Vector = Dune::MultiTypeBlockVector<DisplacementVector, DamageVector>;

  /////////////////////////////////////////////////////////
  //  Assemble the system
  /////////////////////////////////////////////////////////

  Vector rhs;

  auto rhsBackend = Dune::Functions::istlVectorBackend(rhs);

  rhsBackend.resize(basis);
  rhs = 0;                                 /*@\label{li:stokes_taylorhood_set_rhs_to_zero}@*/

  Matrix matrix;

  double g_c = 1e-2;
  double c_w = 0.5;
  double l = 1e-3;
  double lambda = 121;
  double mu = 80;

  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(basis.gridView());

  {
    Dune::Timer timer;
    auto matrixBackend = Dune::Fufem::istlMatrixBackend(matrix);
    auto operatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler{basis, basis};
    auto patternBuilder = matrixBackend.patternBuilder();

    timer.reset();
    operatorAssembler.assembleBulkPattern(patternBuilder, gridViewPartition, threadCount);
    std::cout << "assembleBulkPattern took " << timer.elapsed() << "s." << std::endl;

    timer.reset();
    patternBuilder.setupMatrix();
    matrixBackend.assign(0);
    std::cout << "setupMatrix took " << timer.elapsed() << "s." << std::endl;
  }

  // Dummy to demonstrate how this could be assembled
  auto positiveElasticEnergyDensity = Dune::Functions::makeGridViewFunction([](auto x) { return 42.0; }, basis.gridView());
  {
    Dune::Timer timer;
    using namespace Dune::Indices;
    using namespace Dune::Fufem::Forms;

    [[maybe_unused]] auto displacementBasis = Dune::Functions::subspaceBasis(basis, _0);
    auto damageBasis = Dune::Functions::subspaceBasis(basis, _1);

    auto d = trialFunction(damageBasis);
    auto e = testFunction(damageBasis);

    auto psiPlus = Coefficient(positiveElasticEnergyDensity);

    auto alpha = 2.0 * g_c / (4.0*c_w);

    auto crackDensity = alpha * (1./l * d*e + l * dot(grad(d),grad(e)));

    auto localAssembler = integrate(crackDensity + 2.*psiPlus*d*e);

    auto operatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler{damageBasis, damageBasis};
    operatorAssembler.assembleBulkEntries(matrix, localAssembler, gridViewPartition, threadCount);

    std::cout << "Assembling the problem took " << timer.elapsed() << "s" << std::endl;
  }

  {
    Dune::Timer timer;
    using namespace Dune::Indices;
    using namespace Dune::Fufem::Forms;
    namespace F = Dune::Fufem::Forms;


    auto displacementBasis = Dune::Functions::subspaceBasis(basis, _0);
    auto damageBasis = Dune::Functions::subspaceBasis(basis, _1);

    auto Id = Dune::ScaledIdentityMatrix<double,dim>{1.0};

    auto u = trialFunction(displacementBasis);
    auto d = trialFunction(damageBasis);
    auto v = testFunction(displacementBasis);
    auto e = testFunction(damageBasis);

    auto E = [](const auto& v) {
      return 0.5*(grad(v)+F::transpose(grad(v)));
    };

    auto alpha = 2.0 * g_c / (4.0*c_w);

    auto crackDensity = alpha * (1./l * d*e + l * dot(grad(d),grad(e)));
//    auto stVenantKirchhoffDensity = 2*mu*F::dot(E(u), E(v)) + lambda*trace(E(u))*trace(E(v));
//    auto stVenantKirchhoffDensity = F::dot(2*mu*E(u) + lambda*Id*trace(E(u)), E(v));
    auto sigma = 2*mu*E(u) + lambda*Id*trace(E(u));
    auto stVenantKirchhoffDensity = F::dot(sigma, E(v));
//    auto stVenantKirchhoffDensity = dot(sigma, E(v));
    auto localAssembler = integrate(stVenantKirchhoffDensity + crackDensity);

    auto operatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler{basis, basis};
    operatorAssembler.assembleBulkEntries(matrix, localAssembler, gridViewPartition, threadCount);

    std::cout << "Assembling the problem took " << timer.elapsed() << "s" << std::endl;
  }


}
catch (Dune::Exception& e) {
  std::cout << e.what() << std::endl;
}
