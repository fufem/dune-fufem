// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>
#include <cmath>

#include <dune/common/bitsetvector.hh>
#include <dune/common/version.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/morleybasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/datacollectors/discontinuouslagrangedatacollector.hh>
#else
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#endif

#ifdef HAVE_ADOLC
#include <dune/fufem/functions/adolcfunction.hh>
#endif
#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>
#include <dune/fufem/forms/forms.hh>

#include "utilities.hh"

// A custom differential operator for evaluating the Hessian
// that simply uses an internal vector to cache the Hessian
// values at quadrature points.
template<class B, class TP, std::size_t argIndex>
class FEFunctionHessianOperator : public Dune::Fufem::Forms::FEOperatorBase<B, TP, argIndex>
{
  using Base = Dune::Fufem::Forms::FEOperatorBase<B, TP, argIndex>;
  using Node = typename Base::Node;
  using LeafLBTraits = typename Base::LeafNode::FiniteElement::Traits::LocalBasisType::Traits;

public:

  using Element = typename Base::Element;
  using Range = typename LeafLBTraits::HessianType;

  using Base::Base;

  class LocalOperator : public Base::LocalOperator
  {
    using Base::LocalOperator::quadratureRuleKey_;
    using Base::LocalOperator::leafNode;
    using Base::LocalOperator::leafNodeCache;
    using Base::LocalOperator::LocalOperator;

  public:

    using Range = typename FEFunctionHessianOperator::Range;

    void bind(const Element&)
    {
      quadratureRuleKey_ = QuadratureRuleKey(leafNode().finiteElement()).derivative().derivative();
    }

    template<class CacheManager>
    void bindToCaches(CacheManager& cacheManager)
    {
      const auto& rule = cacheManager.rule();
      const auto& localBasis = leafNode().finiteElement().localBasis();
      const auto& geometry = leafNode().element().geometry();
      const auto& center = leafNode().element().geometry().center();
      const auto& invJ = geometry.jacobianInverse(center);
      const auto& invJT = geometry.jacobianInverseTransposed(center);
      globalHessianCache_.resize(rule.size());
      for(auto i : Dune::range(rule.size()))
      {
        globalHessianCache_[i].resize(localBasis.size());
        localBasis.evaluateHessian(rule[i].position(), globalHessianCache_[i]);
        for(auto k : Dune::range(localBasis.size()))
        {
          auto localHessian = globalHessianCache_[i][k];
          globalHessianCache_[i][k] = invJT*localHessian*invJ;
        }
      }
    }

    template<class AugmentedLocalDomain>
    auto operator()(const AugmentedLocalDomain& x) const
    {
      using namespace Dune::Fufem::Forms::Impl::Tensor;
      const auto& globalHessians = globalHessianCache_[x.index()];
      return LazyTensorBlock(
        [&](const auto& i) {
            return globalHessians[i];
          },
          Extents(leafNode().size()),
          Offsets(leafNode().localIndex(0))
        );
    }

  private:
    std::vector<std::vector<Range>> globalHessianCache_;
  };

  friend LocalOperator localOperator(const FEFunctionHessianOperator& op)
  {
    return LocalOperator(op);
  }

};

template<class B, class TP, std::size_t argIndex>
auto hessian(const Dune::Fufem::Forms::FEFunctionOperator<B, TP, argIndex>& op)
{
  return FEFunctionHessianOperator<B, TP, argIndex>(std::get<0>(op.basis()), std::get<0>(op.treePath()), op.isAffine());
}

using namespace Dune;


int main (int argc, char *argv[]) try
{
  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  std::size_t refinements = 4;
  if (argc>2)
    refinements = std::stoul(std::string(argv[2]));


  // Set up MPI, if available
  MPIHelper::instance(argc, argv);

  ///////////////////////////////////
  //   Generate the grid
  ///////////////////////////////////

  auto grid = Dune::StructuredGridFactory<Dune::UGGrid<2>>::createSimplexGrid({0,0}, {1, 1}, {2, 2});

  grid->globalRefine(refinements);

  auto gridView = grid->leafGridView();

  /////////////////////////////////////////////////////////
  //   Choose a finite element space
  /////////////////////////////////////////////////////////

  using namespace Functions::BasisFactory;

  auto basis = makeBasis(gridView, morley());

  /////////////////////////////////////////////////////////
  //   Stiffness matrix and right hand side vector
  /////////////////////////////////////////////////////////

  using Vector = Dune::BlockVector<double>;
  using BitVector = std::vector<bool>;
  using Matrix = Dune::BCRSMatrix<double>;

  Vector rhs;
  Vector sol;
  BitVector isConstrained;
  Matrix stiffnessMatrix;

  /////////////////////////////////////////////////////////
  //  Assemble the system
  /////////////////////////////////////////////////////////

  std::cout << "Number of DOFs is " << basis.dimension() << std::endl;

  auto dirichletIndicatorFunction = [](auto x) { return true; };

  auto dirichletPatch = BoundaryPatch(basis.gridView());
  dirichletPatch.insertFacesByProperty([&](auto&& intersection) { return dirichletIndicatorFunction(intersection.geometry().center()); });

  auto pi = std::acos(-1.0);

  using Domain = Dune::FieldVector<double, 2>;
  using Range = double;
  using DerivativeRange = Dune::FieldVector<double, 2>;
  using SignatureTag = Dune::Functions::SignatureTag<Range(Domain)>;

//  auto exactSol = [pi](const auto& x) { return std::pow(std::sin(pi*x[0]), 2.)*std::pow(std::sin(pi*x[1]), 2.); };
//  auto rightHandSide = [exactSol,pi] (const auto& x) {
//    return std::pow(pi, 4.)*(8.-24.*std::pow(std::sin(pi*x[0]), 2.)-24.*std::pow(std::sin(pi*x[1]), 2.) + 64*exactSol(x));
//  };
//  auto dirichletFunctionValues = [pi](const auto& x){
//    return 0.0;
//  };
//  auto dirichletValues = Dune::Fufem::makeAdolCFunction(SignatureTag(), dirichletFunctionValues);

//  auto exactSol = [&](const auto& x) { return 16*x[0]*(x[0]-1)*x[1]*(x[1]-1); };
//  auto rightHandSide = [] (const auto& x) { return 8*16;};
//  auto dirichletFunctionValues = [&](const auto& x){
//    return exactSol(x);
//  };
//  auto dirichletValues = Dune::Fufem::makeAdolCFunction(SignatureTag(), dirichletFunctionValues);

//  auto rightHandSide = [] (const auto& x) { return 10;};
//  auto dirichletFunctionValues = [pi](const auto& x){
//    using std::sin;
//    return sin(2*pi*x[0]);
//  };
//  auto dirichletValues = Dune::Fufem::makeAdolCFunction(SignatureTag(), dirichletFunctionValues);

  auto rightHandSide = [] (const auto& x) {
    return 500;
  };

  auto dirichletFunctionValues = [](auto x) { return Range(); };
#ifdef HAVE_ADOLC
  auto dirichletValues = Dune::Fufem::makeAdolCFunction(SignatureTag(), dirichletFunctionValues);
#else
  auto dirichletDerivativeValues = [](auto x) { return DerivativeRange(); };
  auto dirichletValues = Dune::Functions::makeDifferentiableFunctionFromCallables(SignatureTag(),
    dirichletFunctionValues, dirichletDerivativeValues);
#endif

  // Disable parallel assembly for UGGrid before 2.10
#if DUNE_VERSION_GT(DUNE_FUNCTIONS, 2, 9)
  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(gridView);
  auto globalAssembler = GlobalAssembler(basis, basis, 1, std::cref(gridViewPartition), threadCount);
#else
  auto globalAssembler = GlobalAssembler(basis, basis, 1);
#endif

  {
    using namespace ::Dune::Fufem::Forms;
    namespace F = ::Dune::Fufem::Forms;

    auto v = testFunction(basis, NonAffineFamiliy{});
    auto u = trialFunction(basis, NonAffineFamiliy{});
    auto f = Coefficient(Functions::makeGridViewFunction(rightHandSide, gridView));

    auto a = integrate(dot(hessian(u), hessian(v)));
    auto b = integrate(f*v);

    globalAssembler.assembleOperator(stiffnessMatrix, a);
    globalAssembler.assembleFunctional(rhs, b);
  }

  // *********************************************
  // Initialize solution vector
  // *********************************************

  Dune::Functions::istlVectorBackend(sol).resize(basis);
  Dune::Functions::istlVectorBackend(sol) = 0;

  // *********************************************
  // Incorporate Dirichlet boundary conditions
  // *********************************************

  Dune::Functions::istlVectorBackend(isConstrained).resize(basis);
  Dune::Functions::istlVectorBackend(isConstrained) = 0;

  // Mark all boundary DOFs with derivative order one in normal direction
  {
    auto normalDerivativeOrder = [](const auto& functionalDescriptor, const auto& unitNormal) -> std::size_t
    {
      auto&& partialOrder = functionalDescriptor.partialDerivativeOrder();
      for(auto i : Dune::range(unitNormal.size()))
      {
        if ((1-std::fabs(unitNormal[i]) < 1e-10))
          return partialOrder[i] + functionalDescriptor.normalDerivativeOrder();
      }
      return functionalDescriptor.normalDerivativeOrder();
    };
    auto isConstrainedBackend = Dune::Functions::istlVectorBackend(isConstrained);
    auto localView = basis.localView();
    auto seDOFs = subEntityDOFs(basis);
    for(auto&& intersection : dirichletPatch)
    {
      localView.bind(intersection.inside());
      seDOFs.bind(localView, intersection);
      Dune::TypeTree::forEachLeafNode(localView.tree(), [&](auto&& node, auto&& treePath) {
        for(auto k : Dune::range(node.size()))
        {
          auto localIndex = node.localIndex(k);
          if (seDOFs.contains(localIndex))
          {
            auto functionalDescriptor = node.finiteElement().localInterpolation().functionalDescriptor(k);
            auto normal = intersection.centerUnitOuterNormal();
            if (normalDerivativeOrder(functionalDescriptor, normal)<=1)
              isConstrainedBackend[localView.index(localIndex)] = true;
          }
        }
      });
    }
  }

  interpolate(basis, sol, dirichletValues, isConstrained);

  incorporateEssentialConstraints(stiffnessMatrix, rhs, isConstrained, sol);

  ////////////////////////////
  //   Compute solution
  ////////////////////////////

  // Technicality:  turn the matrix into a linear operator
  MatrixAdapter<Matrix,Vector,Vector> op(stiffnessMatrix);

  // FastAMG is not working for non-blocked matrices in 2.10
#if DUNE_VERSION_GTE(DUNE_ISTL, 2, 11)
  auto preconditioner = makeAMGPreconditioner<Vector>(stiffnessMatrix);
#else
  // Sequential incomplete LU decomposition as the preconditioner
  auto preconditioner = SeqILDL<Matrix,Vector,Vector>(stiffnessMatrix,1.0);
#endif

  // Preconditioned conjugate-gradient solver
  CGSolver<Vector> cg(op,
                          preconditioner, // preconditioner
                          1e-4, // desired residual reduction factor
                          1000,   // maximum number of iterations
                          2);   // verbosity of the solver

  // Object storing some statistics about the solving process
  InverseOperatorResult statistics;

  // Solve!
  cg.apply(sol, rhs, statistics);

  ////////////////////////////////////////////////////////////////////////////
  //  Make a discrete function from the FE basis and the coefficient vector
  ////////////////////////////////////////////////////////////////////////////

  auto solFunction = Functions::makeDiscreteGlobalBasisFunction<double>(basis, sol);

  //////////////////////////////////////////////////////////////////////////////////////////////
  //  Write result to VTK file
  //////////////////////////////////////////////////////////////////////////////////////////////

  using namespace ::Dune::Fufem::Forms;
  auto u = bindToCoefficients(trialFunction(basis, NonAffineFamiliy{}), sol);

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
  using Dune::Vtk::DiscontinuousLagrangeDataCollector;
  using Dune::Vtk::UnstructuredGridWriter;
  auto vtkWriter = UnstructuredGridWriter(DiscontinuousLagrangeDataCollector(basis.gridView(), 3));
  vtkWriter.addPointData(u, VTK::FieldInfo("sol", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.addPointData(jacobian(u), VTK::FieldInfo("grad_sol", VTK::FieldInfo::Type::vector, 2));
  vtkWriter.write("biharmonic");
#else
  // We need to use the SubsamplingVTKWriter, because dune-vtk's LagrangeDataCollector
  // does not work with mixed grids in 2.9.
  using GridView = decltype(gridView);
  SubsamplingVTKWriter<GridView> vtkWriter(gridView, Dune::refinementLevels(2));
  vtkWriter.addVertexData(u, VTK::FieldInfo("sol", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.addVertexData(grad(u), VTK::FieldInfo("grad_sol", VTK::FieldInfo::Type::vector, 2));
  vtkWriter.write("biharmonic");
#endif

 }
// Error handling
 catch (Exception& e) {
    std::cout << e.what() << std::endl;
 }
