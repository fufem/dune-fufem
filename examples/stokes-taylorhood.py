import numpy as np
import scipy.sparse.linalg

import dune.common
import dune.geometry
import dune.grid
import dune.functions

from dune.grid import DataType
from dune.functions import defaultGlobalBasis, Power, Composite, Lagrange, subspaceBasis

from fufemforms import *
from utilities import *

def isNear(a,b):
  return np.abs(a-b) <= 1e-10



dimension = 2
ansatzOrder = 1

# Indicator function of Dirichlet boundary
def velocityIsDirichlet(x):
    y = np.zeros(dimension, dtype=bool)
    y.fill(isNear(x[0], 0) or isNear(x[0], 1) or isNear(x[1], 0) or isNear(x[1], 1))
    return y

# Dirichlet boundary values
def velocityDirichletValues(x):
    y = np.zeros(dimension)
    y[1] = isNear(x[0], 0)
    return y



def globalAssembler(basis):

    velocityBasis = subspaceBasis(basis, 0)

    N = len(basis)

    # Mark all Dirichlet DOFs
    isConstrained = np.zeros( N, dtype=bool )
    velocityBasis.interpolate(isConstrained, velocityIsDirichlet)

    # Interpolate the boundary values
    constraintDOFValues = np.zeros( N )
    velocityBasis.interpolate(constraintDOFValues, velocityDirichletValues)

    # Trial and test functions for variational problem
    u = trialFunction(basis, 0)
    p = trialFunction(basis, 1)
    v = testFunction(basis, 0)
    q = testFunction(basis, 1)

    a = integrate( dot(grad(u), grad(v)) - div(u)*q - div(v)*p )

    # Assemble into matrix and vector
    assembler = GlobalAssembler(basis)
    A = assembler.assembleOperator(a)
    B = np.zeros( N )

    # Build constraints into matrix
    incorporateEssentialConstraints(A, B, isConstrained, constraintDOFValues)

    return A, B

############################### START ########################################

# Number of grid elements (in one direction)
gridSize = 4

# Create a grid of the unit square
grid = dune.grid.structuredGrid([0,0],[1,1],[gridSize, gridSize])

# Create Taylor-Hood FE basis
basis = defaultGlobalBasis(grid, Composite(Power(Lagrange(order=ansatzOrder+1),exponent=dimension,layout="interleaved"),Lagrange(order=ansatzOrder)))

print("Dimension of FE space is "+str(len(basis)))

# Compute A and b
A,b = globalAssembler(basis)

# Solve linear system!
x = scipy.sparse.linalg.spsolve(A, b)

velocityBasis = subspaceBasis(basis, 0)
pressureBasis = subspaceBasis(basis, 1)

u = velocityBasis.asFunction(x)
p = pressureBasis.asFunction(x)

vtk = grid.vtkWriter(2)
u.addToVTKWriter("velocity", vtk, DataType.PointVector)
p.addToVTKWriter("pressure", vtk, DataType.PointData)
vtk.write("stokes-taylorhood-py")

