// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>
#include <cmath>

#include <dune/common/bitsetvector.hh>
#include <dune/common/version.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/cubichermitebasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/datacollectors/lagrangedatacollector.hh>
#else
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#endif

#include <dune/fufem/logger.hh>
#ifdef HAVE_ADOLC
#include <dune/fufem/functions/adolcfunction.hh>
#endif
#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>
#include <dune/fufem/forms/forms.hh>

#include "utilities.hh"

using namespace Dune;



int main (int argc, char *argv[]) try
{

  auto log = Dune::Fufem::makeLogger(std::cout, "[% 8.3f] [% 8.3f] ");

  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  std::size_t refinements = 4;
  if (argc>2)
    refinements = std::stoul(std::string(argv[2]));

  log("Command line processed");

  MPIHelper::instance(argc, argv);

  log("MPI initialized");

  ///////////////////////////////////
  //   Generate the grid
  ///////////////////////////////////

  constexpr auto dim = 2;
  auto grid = Dune::StructuredGridFactory<Dune::UGGrid<dim>>::createSimplexGrid({0,0}, {1, 1}, {2, 2});

  log("Grid created");

  grid->globalRefine(refinements);

  log(Dune::formatString("Grid refined %d times", refinements));

  auto gridView = grid->leafGridView();
  using GridView = decltype(gridView);

  /////////////////////////////////////////////////////////
  //   Choose a finite element space
  /////////////////////////////////////////////////////////

  using namespace Functions::BasisFactory;

  auto basis = makeBasis(gridView, cubicHermite());
//  auto basis = makeBasis(gridView, reducedCubicHermite());

  log(Dune::formatString("Basis created with dimension %d", basis.dimension()));

  /////////////////////////////////////////////////////////
  //   Stiffness matrix and right hand side vector
  /////////////////////////////////////////////////////////

  using Vector = Dune::BlockVector<double>;
  using BitVector = std::vector<bool>;
  using Matrix = Dune::BCRSMatrix<double>;

  Vector rhs;
  Vector sol;
  BitVector isConstrained;
  Matrix stiffnessMatrix;

  /////////////////////////////////////////////////////////
  //  Assemble the system
  /////////////////////////////////////////////////////////

  auto dirichletIndicatorFunction = [](auto x) { return true; };

  auto dirichletPatch = BoundaryPatch(basis.gridView());
  dirichletPatch.insertFacesByProperty([&](auto&& intersection) { return dirichletIndicatorFunction(intersection.geometry().center()); });

  using Domain = Dune::FieldVector<double, dim>;
  using Range = double;
  using DerivativeRange = Dune::FieldVector<double, dim>;
  using SignatureTag = Dune::Functions::SignatureTag<Range(Domain)>;

  auto rightHandSide = [] (const auto& x) { return 10;};
  auto pi = std::acos(-1.0);
  auto dirichletFunctionValues = [pi](const auto& x){
    using std::sin;
    return sin(2*pi*x[0]);
  };

  // Make Dirichlet data differentiable, since this is required by local interpolation
  // Notice that, depite the fact that we have essential constraints for function values
  // only, the derivative values do matter, because we also have to constrain tangential DOFs.
#ifdef HAVE_ADOLC
  auto dirichletValues = Dune::Fufem::makeAdolCFunction(SignatureTag(), dirichletFunctionValues);
#else
  auto dirichletDerivativeValues = [pi](auto x) {
    using std::cos;
    return DerivativeRange({2.*pi*cos(2.*pi*x[0]), 0.0});
  };
  auto dirichletValues = Dune::Functions::makeDifferentiableFunctionFromCallables(SignatureTag(),
    dirichletFunctionValues, dirichletDerivativeValues);
#endif

  // Disable parallel assembly for UGGrid before 2.10
#if DUNE_VERSION_GT(DUNE_FUNCTIONS, 2, 9)
  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(gridView);
  log("Grid coloring computed");

  auto globalAssembler = GlobalAssembler(basis, basis, 0, std::cref(gridViewPartition), threadCount);
#else
  auto globalAssembler = GlobalAssembler(basis, basis, 0);
#endif

  {
    using namespace ::Dune::Fufem::Forms;
    namespace F = ::Dune::Fufem::Forms;

    auto v = testFunction(basis, NonAffineFamiliy{});
    auto u = trialFunction(basis, NonAffineFamiliy{});
    auto f = Coefficient(Functions::makeGridViewFunction(rightHandSide, gridView));

    auto a = integrate(dot(grad(u), grad(v)));
    auto b = integrate(f*v);

    log("Assembler set up");
    globalAssembler.assembleOperator(stiffnessMatrix, a);
    log("Matrix assembled");
    globalAssembler.assembleFunctional(rhs, b);
    log("RHS assembled");
  }

  // *********************************************
  // Initialize solution vector
  // *********************************************

  Dune::Functions::istlVectorBackend(sol).resize(basis);
  Dune::Functions::istlVectorBackend(sol) = 0;

  // *********************************************
  // Incorporate Dirichlet boundary conditions
  // *********************************************

  Dune::Functions::istlVectorBackend(isConstrained).resize(basis);
  Dune::Functions::istlVectorBackend(isConstrained) = 0;

  // Mark all boundary DOFs with derivative order zero in normal direction
  {
    auto normalDerivativeOrder = [](const auto& functionalDescriptor, const auto& unitNormal) -> std::size_t
    {
      auto&& partialOrder = functionalDescriptor.partialDerivativeOrder();
      for(auto i : Dune::range(unitNormal.size()))
      {
        if ((1-std::fabs(unitNormal[i]) < 1e-10))
          return partialOrder[i] + functionalDescriptor.normalDerivativeOrder();
      }
      return functionalDescriptor.normalDerivativeOrder();
    };
    auto isConstrainedBackend = Dune::Functions::istlVectorBackend(isConstrained);
    auto localView = basis.localView();
    auto seDOFs = subEntityDOFs(basis);
    for(auto&& intersection : dirichletPatch)
    {
      localView.bind(intersection.inside());
      seDOFs.bind(localView, intersection);
      Dune::TypeTree::forEachLeafNode(localView.tree(), [&](auto&& node, auto&& treePath) {
        for(auto k : Dune::range(node.size()))
        {
          auto localIndex = node.localIndex(k);
          if (seDOFs.contains(localIndex))
          {
            auto functionalDescriptor = node.finiteElement().localInterpolation().functionalDescriptor(k);
            auto normal = intersection.centerUnitOuterNormal();
            if (normalDerivativeOrder(functionalDescriptor, normal)==0)
              isConstrainedBackend[localView.index(localIndex)] = true;
          }
        }
      });
    }
  }

  interpolate(basis, sol, dirichletValues, isConstrained);
  log("Boundary DOFs interpolated");

  incorporateEssentialConstraints(stiffnessMatrix, rhs, isConstrained, sol);
  log("Boundary condition incorporated into system");

  ////////////////////////////
  //   Compute solution
  ////////////////////////////

  // Technicality:  turn the matrix into a linear operator
  MatrixAdapter<Matrix,Vector,Vector> op(stiffnessMatrix);

  // FastAMG is not working for non-blocked matrices in 2.10
#if DUNE_VERSION_GTE(DUNE_ISTL, 2, 11)
  auto preconditioner = makeAMGPreconditioner<Vector>(stiffnessMatrix);
#else
  // Sequential incomplete LU decomposition as the preconditioner
  auto preconditioner = SeqILDL<Matrix,Vector,Vector>(stiffnessMatrix,1.0);
#endif

  log("Preconditioner created");

  // Preconditioned conjugate-gradient solver
  CGSolver<Vector> cg(op,
                          preconditioner, // preconditioner
                          1e-4, // desired residual reduction factor
                          100,   // maximum number of iterations
                          2);   // verbosity of the solver

  log("Solver created");

  // Object storing some statistics about the solving process
  InverseOperatorResult statistics;

  // Solve!
  cg.apply(sol, rhs, statistics);

  log("Linear system solved");

  ////////////////////////////////////////////////////////////////////////////
  //  Make a discrete function from the FE basis and the coefficient vector
  ////////////////////////////////////////////////////////////////////////////

  auto solFunction = Functions::makeDiscreteGlobalBasisFunction<double>(basis, sol);

  //////////////////////////////////////////////////////////////////////////////////////////////
  //  Write result to VTK file
  //////////////////////////////////////////////////////////////////////////////////////////////

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
  using Dune::Vtk::LagrangeDataCollector;
  using Dune::Vtk::UnstructuredGridWriter;
  auto vtkWriter = UnstructuredGridWriter(LagrangeDataCollector(basis.gridView(), 3));
  vtkWriter.addPointData(solFunction, VTK::FieldInfo("sol", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("poisson-hermite");
#else
  // We need to use the SubsamplingVTKWriter, because dune-vtk's LagrangeDataCollector
  // does not work with mixed grids in 2.9.
  SubsamplingVTKWriter<GridView> vtkWriter(gridView, Dune::refinementLevels(2));
  vtkWriter.addVertexData(solFunction, VTK::FieldInfo("sol", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("poisson-hermite");
#endif

 }
// Error handling
 catch (Exception& e) {
    std::cout << e.what() << std::endl;
 }
