// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>
#include <cmath>

#include <dune/common/bitsetvector.hh>
#include <dune/common/version.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/cubichermitebasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>
#include <dune/functions/gridfunctions/facenormalgridfunction.hh>
#include <dune/functions/gridfunctions/composedgridfunction.hh>

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/datacollectors/lagrangedatacollector.hh>
#else
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#endif

#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/dunefunctionsfunctionalassembler.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>
#include <dune/fufem/forms/forms.hh>

#include "utilities.hh"

using namespace Dune;


// A (rather strange) grid function that maps x to a tuple
// of element, local geometry, and local coordinate.
// By composition, this can be can use to implements element
// or geometry-based grid functions like, e.g. the local mesh
// or element index.
template<class GV>
class ElementGeometryGridFunction
{
public:
  using GridView = GV;
  using EntitySet = Dune::Functions::GridViewEntitySet<GridView, 0>;
  using Element = typename EntitySet::Element;

  using LocalDomain = typename EntitySet::LocalCoordinate;
  using Domain = typename EntitySet::GlobalCoordinate;
  using Range = std::tuple<const Element&, const typename Element::Geometry&, LocalDomain>;

private:

  using Traits = Dune::Functions::Imp::GridFunctionTraits<Range(Domain), EntitySet, Dune::Functions::DefaultDerivativeTraits, 16>;

  class LocalFunction
  {
    using Geometry = typename Element::Geometry;
    static const int dimension = GV::dimension;
  public:

    void bind(const Element& element)
    {
      element_ = element;
      geometry_.emplace(element_.geometry());
    }

    void unbind()
    {
      geometry_.reset();
    }

    /** \brief Return if the local function is bound to a grid element
     */
    bool bound() const
    {
      return static_cast<bool>(geometry_);
    }

    Range operator()(const LocalDomain& x) const
    {
      return {element_, *geometry_, x};
    }

    //! Return the bound element stored as copy in the \ref bind function.
    const Element& localContext() const
    {
      return element_;
    }

    //! Not implemented.
    friend typename Traits::LocalFunctionTraits::DerivativeInterface derivative(const LocalFunction& t)
    {
      DUNE_THROW(NotImplemented,"not implemented");
    }

  private:
    std::optional<Geometry> geometry_;
    Element element_;
  };

public:

  ElementGeometryGridFunction(const GridView& gridView) :
    entitySet_(gridView)
  {}

  Range operator()(const Domain& x) const
  {
    DUNE_THROW(NotImplemented,"not implemented");
  }

  friend typename Traits::DerivativeInterface derivative(const ElementGeometryGridFunction& t)
  {
    DUNE_THROW(NotImplemented,"not implemented");
  }

  friend LocalFunction localFunction(const ElementGeometryGridFunction& t)
  {
    return LocalFunction{};
  }

  const EntitySet& entitySet() const
  {
    return entitySet_;
  }

private:
  EntitySet entitySet_;
};



template<class GridView>
auto elementSizeGridFunction(const GridView& gridView)
{
  static const int dimension = GridView::dimension;

  auto meshSizeOfGeometry = [](auto arg)
  {
    const auto& [element, geometry, x] = arg;
    auto&& re = Dune::referenceElement(geometry);
    return std::pow(geometry.integrationElement(re.position(0, 0)), 1./dimension);
  };

  return Dune::Functions::makeComposedGridFunction(meshSizeOfGeometry, ElementGeometryGridFunction(gridView));
}



int main (int argc, char *argv[]) try
{
  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  std::size_t refinements = 4;
  if (argc>2)
    refinements = std::stoul(std::string(argv[2]));


  // Set up MPI, if available
  MPIHelper::instance(argc, argv);

  ///////////////////////////////////
  //   Generate the grid
  ///////////////////////////////////

  auto grid = Dune::StructuredGridFactory<Dune::UGGrid<2>>::createSimplexGrid({0,0}, {1, 1}, {2, 2});

  grid->globalRefine(refinements);

  auto gridView = grid->leafGridView();
  using GridView = decltype(gridView);

  /////////////////////////////////////////////////////////
  //   Choose a finite element space
  /////////////////////////////////////////////////////////

  using namespace Functions::BasisFactory;

  auto feBasis = makeBasis(gridView, cubicHermite());
//  auto feBasis = makeBasis(gridView, reducedCubicHermite());
  std::size_t order = 3;

  /////////////////////////////////////////////////////////
  //   Stiffness matrix and right hand side vector
  /////////////////////////////////////////////////////////

  using Vector = Dune::BlockVector<double>;
  using Matrix = Dune::BCRSMatrix<double>;

  Vector rhs;
  Matrix stiffnessMatrix;

  /////////////////////////////////////////////////////////
  //  Assemble the system
  /////////////////////////////////////////////////////////

  std::cout << "Number of DOFs is " << feBasis.dimension() << std::endl;

  auto boundary = BoundaryPatch(feBasis.gridView(), true);

  auto rightHandSide = [] (const auto& x) { return 10;};
  auto pi = std::acos(-1.0);
  auto dirichletValues = [pi](const auto& x){ return std::sin(2*pi*x[0]); };


  // Disable parallel assembly for UGGrid before 2.10
#if DUNE_VERSION_GT(DUNE_FUNCTIONS, 2, 9)
  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(gridView);
  auto globalAssembler = GlobalAssembler(feBasis, feBasis, 1, std::cref(gridViewPartition), threadCount);
#else
  auto globalAssembler = GlobalAssembler(feBasis, feBasis, 1);
#endif

  {
    using namespace ::Dune::Fufem::Forms;
    namespace F = ::Dune::Fufem::Forms;

    auto v = testFunction(feBasis, NonAffineFamiliy{});
    auto u = trialFunction(feBasis, NonAffineFamiliy{});
    auto f = Coefficient(Functions::makeGridViewFunction(rightHandSide, gridView), 2);

    auto nu = Coefficient(Dune::Functions::FaceNormalGridFunction(gridView));
    auto h = Coefficient(elementSizeGridFunction(gridView));

    auto u_D = Coefficient(Functions::makeGridViewFunction(dirichletValues, gridView), 2);

    auto D = [&](auto&& v, auto&& w) { return dot(grad(v), w); };
    auto gamma = 10;

    auto a = integrate(dot(grad(u), grad(v)));
    auto b = integrate(f*v);

    // Incorporate Dirichlet conditions weakly using Nitsche's method (Joachim Nitsche '71)
    auto a_h = a + integrate(gamma/h*u*v - D(u,nu)*v - u*D(v,nu), boundary);
    auto b_h = b + integrate(gamma/h*u_D*v - u_D*D(v,nu), boundary);

    globalAssembler.assembleOperator(stiffnessMatrix, a_h);
    globalAssembler.assembleFunctional(rhs, b_h);
  }


  /////////////////////////////////////////////////
  //   Choose an initial iterate
  /////////////////////////////////////////////////
  Vector x(feBasis.size());
  x = 0;

  ////////////////////////////
  //   Compute solution
  ////////////////////////////

  // Technicality:  turn the matrix into a linear operator
  MatrixAdapter<Matrix,Vector,Vector> op(stiffnessMatrix);

  // Sequential incomplete LU decomposition as the preconditioner
  SeqILDL<Matrix,Vector,Vector> ildl(stiffnessMatrix,1.0);

  // Preconditioned conjugate-gradient solver
  CGSolver<Vector> cg(op,
                          ildl, // preconditioner
                          1e-4, // desired residual reduction factor
                          500,   // maximum number of iterations
                          2);   // verbosity of the solver

  // Object storing some statistics about the solving process
  InverseOperatorResult statistics;

  // Solve!
  cg.apply(x, rhs, statistics);

  ////////////////////////////////////////////////////////////////////////////
  //  Make a discrete function from the FE basis and the coefficient vector
  ////////////////////////////////////////////////////////////////////////////

  auto xFunction = Functions::makeDiscreteGlobalBasisFunction<double>(feBasis, x);

  //////////////////////////////////////////////////////////////////////////////////////////////
  //  Write result to VTK file
  //////////////////////////////////////////////////////////////////////////////////////////////
#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
  using Dune::Vtk::LagrangeDataCollector;
  using Dune::Vtk::UnstructuredGridWriter;
  auto vtkWriter = UnstructuredGridWriter(LagrangeDataCollector(feBasis.gridView(), order));
  vtkWriter.addPointData(xFunction, VTK::FieldInfo("x", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("poisson-hermite-nitsche");
#else
  // We need to use the SubsamplingVTKWriter, because dune-vtk's LagrangeDataCollector
  // does not work with mixed grids in 2.9.
  SubsamplingVTKWriter<GridView> vtkWriter(gridView, Dune::refinementLevels(2));
  vtkWriter.addVertexData(xFunction, VTK::FieldInfo("x", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("poisson-hermite-nitsche");
#endif

 }
// Error handling
 catch (Exception& e) {
    std::cout << e.what() << std::endl;
 }
