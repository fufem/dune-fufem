// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <cmath>
#include <thread>

#include <dune/common/bitsetvector.hh>
#include <dune/common/version.hh>
#include <dune/common/stringutility.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/umfpack.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/datacollectors/lagrangedatacollector.hh>
#else
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#endif

#include <dune/fufem/makering.hh>
#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>
#include <dune/fufem/forms/forms.hh>

#include "utilities.hh"



template<class Matrix, class Vector>
void solve(const Matrix& matrix, Vector& rhs, Vector& sol)
{
#if HAVE_UMFPACK
  auto solver = Dune::UMFPack<Matrix>();
  solver.setMatrix(matrix);
#else
  Dune::MatrixAdapter<Matrix,Vector,Vector> op(matrix);
  Dune::SeqILDL<Matrix,Vector,Vector> ildl(matrix, 1.0);
  // Preconditioned conjugate-gradient solver
  Dune::CGSolver<Vector> solver(op,
                          ildl, // preconditioner
                          1e-10, // desired residual reduction factor
                          100,   // maximum number of iterations
                          2);   // verbosity of the solver
#endif
  Dune::InverseOperatorResult statistics;
  solver.apply(sol, rhs, statistics);
}



int main (int argc, char *argv[]) try
{
  std::size_t threadCount = std::thread::hardware_concurrency();
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  std::size_t refinements = 4;
  if (argc>2)
    refinements = std::stoul(std::string(argv[2]));

  // Set up MPI, if available
  Dune::MPIHelper::instance(argc, argv);



  ///////////////////////////////////
  // Generate the grid
  ///////////////////////////////////

  double a = 0.5;

#if HAVE_DUNE_UGGRID
  using Grid = Dune::UGGrid<2>;
  auto grid = makeRingSegment2D<Grid>({0., 0.}, a, a, 0.0, std::acos(-1.0)/2.0, 3, false);
#else
  using Grid = Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<double,2>>;
  auto grid = Dune::StructuredGridFactory<Grid>::createCubeGrid({a/std::sqrt(2.), a/std::sqrt(2.)}, {a*std::sqrt(2.), a*std::sqrt(2.)}, {1, 1});
#endif

  grid->globalRefine(refinements);

  auto gridView = grid->leafGridView();

  /////////////////////////////////////////////////////////
  // Choose a finite element space
  /////////////////////////////////////////////////////////

  using namespace Dune::Functions::BasisFactory;

  auto basis = makeBasis(gridView, lagrange<2>());

  std::cout << "Number of DOFs is " << basis.dimension() << std::endl;



  /////////////////////////////////////////////////////////
  // Stiffness matrix and right hand side vector
  /////////////////////////////////////////////////////////

  using Vector = Dune::BlockVector<double>;
  using BitVector = Dune::BlockVector<char>;
  using Matrix = Dune::BCRSMatrix<double>;

  Vector rhs;
  Vector sol;
  BitVector isConstrained;
  Matrix stiffnessMatrix;



  /////////////////////////////////////////////////////////
  // Setup problem data
  /////////////////////////////////////////////////////////

  using namespace ::Dune::Fufem::Forms;

  auto dirichletPatch = BoundaryPatch(basis.gridView(), true);

  // Select Dirichlet values for a catenoid.
  // This way the solution should be radially symmetric
  // for the minimal surface equation in contrast to the
  // linear Poisson-problem.
  auto dirichletValues = [&](const auto& x){
    double r = x.two_norm();
    return a*std::acosh(std::max(r/a, 1.0));
  };

  // Nonlinearity of the minimal surface equation
  // and it's derivative. By changing the nonlinearity
  // we can basically implement any quasi-linear problem,
  // e.g. the p-Laplacian, anisotropic diffusion, ... .
  auto psi = RangeOperator([](auto g) {
    return 1./std::sqrt(1. + g.frobenius_norm2()) * g;
  });

  auto Dpsi = RangeOperator([](auto g) {
    auto I = Dune::FieldMatrix<double,2,2>{{1, 0}, {0, 1}};
    auto gNorm2 = g.frobenius_norm2();
    return 1./std::sqrt(1.+gNorm2) * I - std::pow(1 + gNorm2,-3./2.)*g*Dune::Fufem::Forms::transpose(g);
  });



  /////////////////////////////////////////////////////////
  // Setup a global assembler
  /////////////////////////////////////////////////////////

  // Disable parallel assembly for UGGrid before 2.10
#if DUNE_VERSION_GT(DUNE_FUNCTIONS, 2, 9)
  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(gridView);
  auto assembler = GlobalAssembler(basis, basis, 1, std::cref(gridViewPartition), threadCount);
#else
  auto assembler = GlobalAssembler(basis, basis, 1);
#endif



  /////////////////////////////////////////////////////////
  // Mark Dirichlet DOFs
  /////////////////////////////////////////////////////////

  Dune::Functions::istlVectorBackend(isConstrained).resize(basis);
  Dune::Functions::istlVectorBackend(isConstrained) = 0;
  Dune::Fufem::markBoundaryPatchDofs(dirichletPatch, basis, isConstrained);



  /////////////////////////////////////////////////////////
  // Initialize solution vector
  /////////////////////////////////////////////////////////

  // Initialize solution vector with boundary values.
  Dune::Functions::istlVectorBackend(sol).resize(basis);
  Dune::Functions::istlVectorBackend(sol) = 0;
  interpolate(basis, sol, dirichletValues, isConstrained);

  // Later we solve correction problems with zero boundary values.
  auto zero = sol;
  zero *= 0;

  // Compute smooth initial value by solving Poisson equation
  {
    auto u = trialFunction(basis);
    auto v = testFunction(basis);
    auto a = integrate(dot(grad(u), grad(v)));
    assembler.assembleOperator(stiffnessMatrix, a);
    rhs = zero;
    incorporateEssentialConstraints(stiffnessMatrix, rhs, isConstrained, sol);
    solve(stiffnessMatrix, rhs, sol);
  }



  /////////////////////////////////////////////////////////
  // Newton solver parameters
  /////////////////////////////////////////////////////////

  // Use H^1-norm for the termination criterion of the Newton-solver
  auto h1NormMatrix = Matrix();
  {
    auto u = trialFunction(basis);
    auto v = testFunction(basis);
    assembler.assembleOperator(h1NormMatrix, integrate(dot(grad(u), grad(v)) + dot(u,v)));
  }
  auto norm = [&](const auto& v) {
    auto Mv = v;
    h1NormMatrix.mv(v, Mv);
    return std::sqrt(Mv*v);
  };

  // Relative tolerance for Newton point iteration
  double relTol = 1.0e-10;

  // Maximal iteration number for Newton iteraRelative tolerance for fixed point iteration
  std::size_t maxIter = 100;

  // Damping parameter
  double damping = 1.0;



  /////////////////////////////////////////////////////////
  // Newton solver loop
  /////////////////////////////////////////////////////////

  double relError = relTol+1;
  std::size_t k = 0;
  auto correction = zero;
  while ((relError > relTol) and (k<maxIter))
  {
    auto u = bindToCoefficients(trialFunction(basis), sol);
    auto w = trialFunction(basis);
    auto v = testFunction(basis);

    // Solve linearized quasi-linear problem
    auto F = integrate(dot(psi(grad(u)), grad(v)));
    auto DF = integrate(dot(Dpsi(grad(u))*grad(w), grad(v)));

    assembler.assembleOperator(stiffnessMatrix, DF);
    assembler.assembleFunctional(rhs, F);

    incorporateEssentialConstraints(stiffnessMatrix, rhs, isConstrained, zero);
    rhs *= -1;

    solve(stiffnessMatrix, rhs, correction);

    // Apply damped update
    correction *= damping;
    sol += correction;

    // Evaluate termination criterion
    relError = norm(correction)/norm(sol);
    std::cout
      << Dune::formatString("%02d", k)
      << Dune::formatString(" Relative norm of damped correction: %010e", relError)
      << std::endl;

    ++k;
  }
  if ((relError > relTol) and (k==maxIter))
    std::cout << "Warning: Fixed point iteration terminated after " << maxIter << " iterations without matching tolerance." << std::endl;



  ////////////////////////////////////////////////////////////////////////////
  // Make a discrete function from the FE basis and write it to VTK file
  ////////////////////////////////////////////////////////////////////////////

  auto solFunction = Dune::Functions::makeDiscreteGlobalBasisFunction<double>(basis, sol);

#if DUNE_VERSION_GT(DUNE_VTK, 2, 9)
  using Dune::Vtk::LagrangeDataCollector;
  using Dune::Vtk::UnstructuredGridWriter;
  auto vtkWriter = UnstructuredGridWriter(LagrangeDataCollector(basis.gridView(), 2));
  vtkWriter.addPointData(solFunction, Dune::VTK::FieldInfo("sol", Dune::VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("minimalsurface-equation");
#else
  // We need to use the SubsamplingVTKWriter, because dune-vtk's LagrangeDataCollector
  // does not work with mixed grids in 2.9.
  using GridView = decltype(gridView);

  Dune::SubsamplingVTKWriter<GridView> vtkWriter(gridView, Dune::refinementLevels(2));
  vtkWriter.addVertexData(solFunction, Dune::VTK::FieldInfo("sol", Dune::VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("minimalsurface-equation");
#endif

}
catch (Dune::Exception& e)
{
  std::cout << e.what() << std::endl;
}
