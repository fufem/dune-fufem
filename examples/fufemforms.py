import dune.functions

from io import StringIO
import dune.generator.algorithm



############################################################
# This module contains a very rudimentary version of
# bindings for Dune::Fufem::Forms.
############################################################



# Types for tagging return types of coefficient functions
def ScalarType():
  return "double"

def MatrixType(rows,cols):
  return "Dune::FieldMatrix<double, "+str(rows)+","+str(cols)+">"

def VectorType(size):
  return "Dune::FieldVector<double, "+str(size)+">"



# Base class for expressions.
#
# Each expression provides self.cppExpression string member
# providing the C++ representation of this expression.
#
# The arguments that this expression depends on, are encoded
# in three lists:
#
# self.argList is the list of actual argument objects
# self.templateArgList is the list of template argument names
# self.callArgList is the list of function argument names
#
# All template and argument names must be unique. This
# can be ensured by postfixing them with '#id' placeholder
# strings. For elementary expressions one may use the trivial
# placeholder '#'. This will be extended by the path in the
# expression tree when constructing combined expressions.
class Expression:
  def __init__(self):
    self.argList = []
    self.templateArgList = []
    self.callArgList = []
    self.cppExpression = ""

  # Hand out the C++ template argument list as string.
  # This will replace each '#id' placeholders by the corresponding  '_id'.
  def templateArgString(self):
    return ", ".join([s.replace("#", "_") for s in self.templateArgList])

  # Hand out the C++ function argument list as string.
  # This will replace each '#id' placeholders by the corresponding  '_id'.
  def callArgString(self):
    return ", ".join([s.replace("#", "_") for s in self.callArgList])

  # Hand out the C++ expression string.
  # This will replace each '#id' placeholders by the corresponding  '_id'.
  def expressionString(self):
    return self.cppExpression.replace("#", "_")

  # Readable string representation of the expression
  def __str__(self):
    return self.expressionString()

  # Arithmetic operations
  def __mul__(self, other):
    if not isinstance(other, Expression):
      other = Constant(other)
    return Mult(self, other)

  def __neg__(self):
    return negate(self)

  def __rmul__(self, other):
    if not isinstance(other, Expression):
      other = Constant(other)
    return Mult(other, self)

  def __add__(self, other):
    if not isinstance(other, Expression):
      other = Constant(other)
    return Sum(self, other)

  def __radd__(self, other):
    if not isinstance(other, Expression):
      other = Constant(other)
    return Sum(other, self)

  def __sub__(self, other):
    if not isinstance(other, Expression):
      other = Constant(other)
    return Sum(self, -other)

  def __rsub__(self, other):
    if not isinstance(other, Expression):
      other = Constant(other)
    return Sum(other, -self)



# An expression combining several subexpressions.
# If there are multiple subexpressions, the argument
# identifiers of subexpressions are prefixed
# by the subexpression's index to ensure uniqueness
# of template and function argument names
#
# The cppExpression member has to be constructed
# for the specific combination in a derived class.
class CombinedExpression(Expression):
  def __init__(self, *subExpressions):
    Expression.__init__(self)
    self.subExpressions = subExpressions
    if len(subExpressions) == 1:
      self.argList = subExpressions[0].argList
      self.templateArgList= subExpressions[0].templateArgList
      self.callArgList= subExpressions[0].callArgList
      self.cppSubExpressions = [subExpressions[0].cppExpression]
    else:
      def prefixArg(arg, digit):
        return arg.replace("#", "#"+str(digit))
      def prefixArgs(argList, digit):
        return [prefixArg(s, digit) for s in argList]
      self.cppSubExpressions = []
      for i in range(len(subExpressions)):
        self.argList.extend(subExpressions[i].argList)
        self.templateArgList.extend(prefixArgs(subExpressions[i].templateArgList, i))
        self.callArgList.extend(prefixArgs(subExpressions[i].callArgList, i))
        self.cppSubExpressions.append(prefixArg(subExpressions[i].cppExpression, i))

# Combined expression obtained by a named function call
class FunctionCallExpression(CombinedExpression):
  def __init__(self, functionName, *subExpressions):
    CombinedExpression.__init__(self, *subExpressions)
    self.cppExpression = functionName+"("+( ",".join(self.cppSubExpressions) )+")"



# Expression representing the sum of several subexpressions
class Sum(CombinedExpression):
  def __init__(self, *subExpressions):
    CombinedExpression.__init__(self, *subExpressions)
    self.cppExpression = "+".join(self.cppSubExpressions)
    self.cppExpression = "("+self.cppExpression+")"



# Expression representing the product of several subexpressions
class Mult(CombinedExpression):
  def __init__(self, *subExpressions):
    CombinedExpression.__init__(self, *subExpressions)
    self.cppExpression = "*".join(self.cppSubExpressions)



# Expression representing a constant value
class Constant(Expression):
  def __init__(self, constant, **kwargs):
    Expression.__init__(self)
    self.argList = [constant]
    self.templateArgList = ["class Constant#"]
    self.callArgList = ["Constant#&& constant#"]
    rangeType = ""
    for key, value in kwargs.items():
      if key=="type":
        rangeType = value
    if rangeType == "":
      self.cppExpression = "constant#"
    else:
      self.cppExpression = "constant#.template cast<"+rangeType+">()"



# Expression representing a fixed grid function
class Coefficient(Expression):
  def __init__(self, f, gridView, **kwargs):
    Expression.__init__(self)
    self.argList = [f, gridView]
    self.templateArgList = ["class F#", "class GridView#"]
    self.callArgList = ["F#&& f#", "const GridView#& gridView#"]
    rangeType = ScalarType()
    for key, value in kwargs.items():
      if key=="rangeDim":
        rangeType = VectorType(value)
      if key=="rangeType":
        rangeType = value
    if rangeType == "":
      self.cppExpression = "Coefficient(Dune::Functions::makeGridViewFunction(f#, gridView#))"
    else:
      self.cppExpression = "Coefficient(Dune::Functions::makeGridViewFunction([=](auto&& x) { return f#(x).template cast<"+rangeType+">(); }, gridView#))"



# Expression representing a test function
class TestFunction(Expression):
  def __init__(self, basis, *treePathArgs, NonAffineFamiliy=False):
    Expression.__init__(self)
    self.argList = [basis]
    self.templateArgList = ["class TestBasis#"]
    self.callArgList = ["const TestBasis#& testBasis#"]
    argExpressions = ["testBasis#", *["_"+str(arg) for arg in treePathArgs]]
    if NonAffineFamiliy:
      argExpressions.append("NonAffineFamiliy{}")
    self.cppExpression = "testFunction("+ ", ".join(argExpressions) + ")"

# Low case version to mimic C++ interface
testFunction = TestFunction



# Expression representing a trial function
class TrialFunction(Expression):
  def __init__(self, basis, *treePathArgs, NonAffineFamiliy=False):
    Expression.__init__(self)
    self.argList = [basis]
    self.templateArgList = ["class TrialBasis#"]
    self.callArgList = ["const TrialBasis#& trialBasis#"]
    argExpressions = ["trialBasis#", *["_"+str(arg) for arg in treePathArgs]]
    if NonAffineFamiliy:
      argExpressions.append("NonAffineFamiliy{}")
    self.cppExpression = "trialFunction("+ ", ".join(argExpressions) + ")"

# Low case version to mimic C++ interface
trialFunction = TrialFunction



# Short cut functions for several named function calls

def jacobian(expr):
  return FunctionCallExpression("jacobian", expr)

def grad(expr):
  return FunctionCallExpression("grad", expr)

def div(expr):
  return FunctionCallExpression("div", expr)

def negate(expr):
  return FunctionCallExpression("-", expr)

def trace(expr):
  return FunctionCallExpression("trace", expr)

def transpose(expr):
  return FunctionCallExpression("Dune::Fufem::Forms::transpose", expr)

def symmetrize(expr):
  return FunctionCallExpression("symmetrize", expr)

def integrate(expr):
  return FunctionCallExpression("integrate", expr)

def dot(expr1, expr2):
  return FunctionCallExpression("Dune::Fufem::Forms::dot", expr1, expr2)

def bindToCoefficients(form, coefficients):
  return FunctionCallExpression("bindToCoefficients", form, Constant(coefficients, type="std::vector<double>"))



class LocalBilinearFormAssembler:
  def __init__(self, expr, localView):
    self.expr = expr
    self.localView = localView
    self.code="""
    #include <dune/functions/gridfunctions/gridviewfunction.hh>
    #include <dune/fufem/forms/forms.hh>

    template <class LocalView, ###TEMPLATEARGS###>
    auto run(const LocalView& localView, ###CALLARGS###) {

      using namespace Dune::Fufem::Forms;
      using namespace Dune::Indices;

      auto FORM = ###EXPRESSION### ;

      FORM.preprocess(localView, localView);

      using Element = typename LocalView::Element;
      using LocalContainer = typename Dune::DynamicMatrix<double>;
      using Signature = void(const Element&, LocalContainer&, const LocalView&, const LocalView&);

      return std::function<Signature>(FORM);
    }
    """
    self.code = self.code.replace("###TEMPLATEARGS###", expr.templateArgString())
    self.code = self.code.replace("###CALLARGS###", expr.callArgString())
    self.code = self.code.replace("###EXPRESSION###", expr.expressionString())
    factory = dune.generator.algorithm.load("run", StringIO(self.code), self.localView, *expr.argList, pythonName = "Local bilinear form assembler")
    self.generated = factory(self.localView, *expr.argList)

  def __call__(self, element, localContainer):
    return self.generated(element, localContainer, self.localView, self.localView)



class LocalLinearFormAssembler:
  def __init__(self, expr, localView):
    self.expr = expr
    self.localView = localView
    self.code="""
    #include <dune/functions/gridfunctions/gridviewfunction.hh>
    #include <dune/fufem/forms/forms.hh>

    template <class LocalView, ###TEMPLATEARGS###>
    auto run(const LocalView& localView, ###CALLARGS###) {

      using namespace Dune::Fufem::Forms;
      using namespace Dune::Indices;

      auto FORM = ###EXPRESSION### ;

      FORM.preprocess(localView);

      using Element = typename LocalView::Element;
      using LocalContainer = typename Dune::DynamicVector<double>;
      using Signature = void(const Element&, LocalContainer&, const LocalView&);

      return std::function<Signature>(FORM);
    }
    """
    self.code = self.code.replace("###TEMPLATEARGS###", expr.templateArgString())
    self.code = self.code.replace("###CALLARGS###", expr.callArgString())
    self.code = self.code.replace("###EXPRESSION###", expr.expressionString())
    factory = dune.generator.algorithm.load("run", StringIO(self.code), self.localView, *expr.argList, pythonName = "Local linear form assembler")
    self.generated = factory(self.localView, *expr.argList)

  def __call__(self, element, localContainer):
    return self.generated(element, localContainer, self.localView)


def localAssembler(expr, localView):
  isBilinear=False
  for t in expr.templateArgList:
    if t.count("class TrialBasis"):
      isBilinear = True
  if isBilinear:
    return LocalBilinearFormAssembler(expr, localView)
  else:
    return LocalLinearFormAssembler(expr, localView)
