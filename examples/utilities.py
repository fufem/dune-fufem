from time import perf_counter
from io import StringIO

import numpy as np
from scipy.sparse import lil_matrix, csr_matrix
import scipy.sparse.linalg

import dune.geometry
import dune.grid
import dune.functions

from dune.grid import DataType
from dune.common import DynamicMatrix, DynamicVector

import dune.generator.algorithm

from fufemforms import *

class GlobalAssembler:
  def __init__(self, basis, useCPP=True, verbosity=1):
    self.basis = basis
    self.useCPP = useCPP
    self.verbosity = verbosity

  def assembleOperator(self, form, threads=1):
    if self.useCPP:
      return self.assembleOperatorCPP(form, threads=threads)
    else:
      assert(threads==1)
      return self.assembleOperatorPython(form)

  def assembleOperatorPython(self, form):
    N = len(self.basis)
    A = lil_matrix( (N,N) )
    localView = self.basis.localView()
    grid = self.basis.gridView
    localAssembler = LocalBilinearFormAssembler(form, localView)
    zeros = [[0]*localView.maxSize()]*localView.maxSize()
    localA = DynamicMatrix(zeros)
    t = perf_counter()
    for element in grid.elements:
        localView.bind(element)
        localN = len(localView)
        localA *= 0
        localAssembler(element, localA)
        for i in range(localN):
            global_i = localView.index(i)[0]
            for j in range(localN):
                global_j = localView.index(j)[0]
                A[global_i, global_j] += localA[i][j]
    if (self.verbosity>0):
      print("Assembling the bilinear form took {} s".format(perf_counter() - t))
    return A.tocsr()

  def assembleOperatorCPP(self, form, threads=1):
    code="""
    #include <dune/functions/gridfunctions/gridviewfunction.hh>
    #include <dune/fufem/forms/forms.hh>
    #include <dune/fufem/backends/numpybackend.hh>
    #include <dune/fufem/parallel/elementcoloring.hh>

    #include <examples/utilities.hh>

    template <class Basis, ###TEMPLATEARGS###>
    auto run(const Basis& basis, ###CALLARGS###, std::size_t threads) {

      using namespace Dune::Fufem::Forms;
      using namespace Dune::Indices;

      auto FORM = ###EXPRESSION### ;

      auto matrix = Dune::Fufem::NumPyCSRMatrix<double>();
      auto backend = Dune::Fufem::NumPyCSRMatrixBackend(matrix);

      if (threads == 1)
      {
        auto globalAssembler = GlobalAssembler(basis, basis, 0);
        globalAssembler.assembleOperator(backend, FORM);
      }
      else
      {
        auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(basis.gridView());
        auto globalAssembler = GlobalAssembler(basis, basis, 0, std::cref(gridViewPartition), threads);
        globalAssembler.assembleOperator(backend, FORM);
      }

      return matrix.asTuple();
    }
    """
    code = code.replace("###TEMPLATEARGS###", form.templateArgString())
    code = code.replace("###CALLARGS###", form.callArgString())
    code = code.replace("###EXPRESSION###", form.expressionString())
    cppAssembler = dune.generator.algorithm.load("run", StringIO(code), self.basis, *form.argList, threads, pythonName="Global bilinear form assembler")
    t = perf_counter()
    rawCSRMatrix = cppAssembler(self.basis, *form.argList, threads)
    if (self.verbosity>0):
      print("Assembling the bilinear form took {} s".format(perf_counter() - t))
    return csr_matrix(rawCSRMatrix[0], shape=rawCSRMatrix[1])

  def assembleFunctional(self, form, threads=1):
    if self.useCPP:
      return self.assembleFunctionalCPP(form, threads=threads)
    else:
      assert(threads==1)
      return self.assembleFunctionalPython(form)

  def assembleFunctionalPython(self, form):
    N = len(self.basis)
    B = np.zeros( N )
    localView = self.basis.localView()
    grid = self.basis.gridView
    localAssembler = LocalLinearFormAssembler(form, localView)
    zeros = [0]*localView.maxSize()
    localB = DynamicVector(zeros)
    t = perf_counter()
    for element in grid.elements:
        localView.bind(element)
        localN = len(localView)
        localB *= 0
        localAssembler(element, localB)
        for i in range(localN):
            global_i = localView.index(i)[0]
            B[global_i] += localB[i]
    if (self.verbosity>0):
      print("Assembling the linear form took {} s".format(perf_counter() - t))
    return B

  def assembleFunctionalCPP(self, form, threads=1):
    code="""
    #include <dune/functions/gridfunctions/gridviewfunction.hh>
    #include <dune/fufem/forms/forms.hh>
    #include <dune/fufem/backends/numpybackend.hh>
    #include <dune/fufem/parallel/elementcoloring.hh>

    #include <examples/utilities.hh>

    template <class Basis, ###TEMPLATEARGS###>
    auto run(const Basis& basis, ###CALLARGS###, std::size_t threads) {

      using namespace Dune::Fufem::Forms;
      using namespace Dune::Indices;

      auto FORM = ###EXPRESSION### ;

      auto vector = pybind11::array_t<double>();
      auto backend = Dune::Fufem::NumPyVectorBackend(vector);

      if (threads == 1)
      {
        auto globalAssembler = GlobalAssembler(basis, basis, 0);
        globalAssembler.assembleFunctional(backend, FORM);
      }
      else
      {
        auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(basis.gridView());
        auto globalAssembler = GlobalAssembler(basis, basis, 0, std::cref(gridViewPartition), threads);
        globalAssembler.assembleFunctional(backend, FORM);
      }

      return vector;
    }
    """
    code = code.replace("###TEMPLATEARGS###", form.templateArgString())
    code = code.replace("###CALLARGS###", form.callArgString())
    code = code.replace("###EXPRESSION###", form.expressionString())
    cppAssembler = dune.generator.algorithm.load("run", StringIO(code), self.basis, *form.argList, threads, pythonName="Global linear form assembler")
    t = perf_counter()
    vector = cppAssembler(self.basis, *form.argList, threads)
    if (self.verbosity>0):
      print("Assembling the linear form took {} s".format(perf_counter() - t))
    return vector;

def incorporateEssentialConstraints(A, b, isConstrained, x):
    b -= A*(x*isConstrained)
    N = len(b)
    rows, cols = A.nonzero()
    for i,j in zip(rows, cols):
        if isConstrained[i] or isConstrained[j]:
          A[i,j] = 0
    for i in range(N):
        if isConstrained[i]:
            A[i,i] = 1
            b[i] = x[i]



def NewtonSolver(F, DF, U0, tolerance=1e-10, damping=False):
  U = U0.copy()
  norm = np.linalg.norm
  solve = scipy.sparse.linalg.spsolve
  err = tolerance+1
  k = 0
  rho = 1
  while err > tolerance:
    k = k+1

    F_U = F(U)
    DF_U = DF(U)

    DF_U_inverse = scipy.sparse.linalg.factorized(DF_U.tocsc())
    V = DF_U_inverse(-F_U)

    err = norm(V)

    #Damping by monotonicity test
    if damping:
      rho = 1
      while norm(DF_U_inverse(F(U+rho*V))) > (1-rho/2)*err:
        rho *= 1./2
      U += rho*V
    else:
      U += V

    print("Norm of Newton correction ", str(err))

  return U
