// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>
#include <cmath>

#include <dune/common/bitsetvector.hh>
#include <dune/common/indices.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/raviartthomasbasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/backends/istlvectorbackend.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/subspacebasis.hh>
#include <dune/functions/gridfunctions/analyticgridviewfunction.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>
#include <dune/functions/gridfunctions/facenormalgridfunction.hh>
#include <dune/functions/gridfunctions/composedgridfunction.hh>

#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>

#include <dune/fufem/forms/forms.hh>

#include "utilities.hh"

#define DIM2 // Use a two-dimensional test, otherwise three-dimensional

using namespace Dune;



int main (int argc, char *argv[])
{
  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  std::size_t refinements = 0;
  if (argc>2)
    refinements = std::stoul(std::string(argv[2]));


  // Set up MPI, if available
  MPIHelper::instance(argc, argv);

  ///////////////////////////////////
  //   Generate the grid
  ///////////////////////////////////

#ifdef DIM2
  const int dim = 2;
  std::array<int,dim> elements = {{50, 50}};
#else
  const int dim = 3;
  std::array<int,dim> elements = {{10, 10, 10}};
#endif
  using Grid = Dune::YaspGrid<dim>;
  FieldVector<double,dim> l(1);
  Grid grid(l,elements);
  grid.globalRefine(refinements);

  auto gridView = grid.leafGridView();

  /////////////////////////////////////////////////////////
  //   Choose a finite element space
  /////////////////////////////////////////////////////////

  using namespace Functions::BasisFactory;

  const int k = 0;

  auto basis = makeBasis(
    gridView,
    composite(
      raviartThomas<k>(),
      lagrange<k>()
    ));

  using namespace Indices;
  auto fluxBasis = Functions::subspaceBasis(basis, _0);
  auto pressureBasis = Functions::subspaceBasis(basis, _1);


  /////////////////////////////////////////////////////////
  //   Stiffness matrix and right hand side vector
  /////////////////////////////////////////////////////////

  using Matrix = Dune::Matrix<Dune::BCRSMatrix<double> >;
  using Vector = Dune::BlockVector<Dune::BlockVector<double> >;
  using BitVector = Dune::BlockVector<Dune::BlockVector<char> >;

  using Functions::istlVectorBackend;

  Matrix stiffnessMatrix;
  Vector rhs;
  Vector sol;

  /////////////////////////////////////////////////////////
  //  Assemble the system
  /////////////////////////////////////////////////////////

  using Domain = Grid::template Codim<0>::Geometry::GlobalCoordinate;

  auto rightHandSide = [] (const Domain& x) { return 10; };

  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(gridView);
  auto globalAssembler = GlobalAssembler(basis, basis, 1, std::cref(gridViewPartition), threadCount);

  {
    using namespace Indices;
    using namespace ::Dune::Fufem::Forms;

    auto f = Coefficient(Functions::makeGridViewFunction(rightHandSide, gridView));

    auto sigma = trialFunction(fluxBasis, NonAffineFamiliy{});
    auto u = trialFunction(pressureBasis);
    auto tau = testFunction(fluxBasis, NonAffineFamiliy{});
    auto v = testFunction(pressureBasis);

    auto A = integrate(dot(sigma, tau) - div(tau)*u - div(sigma)*v);
    auto b = integrate(-f*v);

    globalAssembler.assembleOperator(stiffnessMatrix, A);
    globalAssembler.assembleFunctional(rhs, b);
  }

  // *********************************************
  // Initialize solution vector
  // *********************************************

  Dune::Functions::istlVectorBackend(sol).resize(basis);
  Dune::Functions::istlVectorBackend(sol) = 0;

  // *********************************************
  // Incorporate Dirichlet boundary conditions
  // *********************************************

  // This marks the top and bottom boundary of the domain
  auto fluxDirichletIndicator = [&l] (const auto& x) {
    return ((x[dim-1] > l[dim-1] - 1e-8) or (x[dim-1] < 1e-8));
  };

  auto coordinate = Dune::Functions::makeAnalyticGridViewFunction([](const auto& x) { return x; }, gridView);
  auto normal = Dune::Functions::FaceNormalGridFunction(gridView);
  auto fluxDirichletValues = Dune::Functions::makeComposedGridFunction(
    [pi = std::acos(-1.0)](const auto& x, const auto& normal) {
      return (-0.05 * (1. - x[0]) * std::sin(2.*pi*x[0])) * normal;
    },
    coordinate,
    normal);

  // Mark all DOFs located in a boundary intersection marked
  // by the fluxDirichletIndicator function. If the flux
  // ansatz space also contains tangential components, this
  // approach will fail, because those are also marked.
  // For Raviart-Thomas this does not happen.
  auto fluxDirichletPatch = BoundaryPatch(gridView);
  fluxDirichletPatch.insertFacesByProperty([&](auto&& intersection) { return fluxDirichletIndicator(intersection.geometry().center()); });

  auto isConstrained = BitVector();
  Dune::Fufem::markBoundaryPatchDofs(fluxDirichletPatch, fluxBasis, isConstrained);

  // Interpolate flux Dirichlet values
  interpolate(fluxBasis, sol, fluxDirichletValues, isConstrained);

  // Modify matrix and rhs according to incorporate boundary values
  incorporateEssentialConstraints(stiffnessMatrix, rhs, isConstrained, sol);

  ////////////////////////////
  //   Compute solution
  ////////////////////////////

  // Technicality:  turn the matrix into a linear operator
  MatrixAdapter<Matrix,Vector,Vector> op(stiffnessMatrix);

  // Fancy (but only) way to not have a preconditioner at all
  Richardson<Vector,Vector> preconditioner(1.0);

  // Preconditioned GMRES / BiCGSTAB solver
  //RestartedGMResSolver<Vector> solver (op, preconditioner, 1e-6, 1000, 10000, 2);
  BiCGSTABSolver<Vector> solver(op, preconditioner, 1e-6, 4000, 2);

  // Object storing some statistics about the solving process
  InverseOperatorResult statistics;

  // Solve!
  solver.apply(sol, rhs, statistics);

  ////////////////////////////////////////////////////////////////////////////
  //  Make a discrete function from the FE basis and the coefficient vector
  ////////////////////////////////////////////////////////////////////////////

  using FluxRange = FieldVector<double,dim>;
  using PressureRange = double;

  auto fluxFunction = Functions::makeDiscreteGlobalBasisFunction<FluxRange>(fluxBasis, sol);
  auto pressureFunction = Functions::makeDiscreteGlobalBasisFunction<PressureRange>(pressureBasis, sol);

  //////////////////////////////////////////////////////////////////////////////////////////////
  //  Write result to VTK file
  //////////////////////////////////////////////////////////////////////////////////////////////
  auto vtkWriter = SubsamplingVTKWriter(gridView, Dune::refinementLevels(0));
  vtkWriter.addVertexData(fluxFunction, VTK::FieldInfo("flux", VTK::FieldInfo::Type::vector, dim));
  vtkWriter.addVertexData(pressureFunction, VTK::FieldInfo("pressure", VTK::FieldInfo::Type::scalar, 1));
  vtkWriter.write("poisson-mfem-result");
}
