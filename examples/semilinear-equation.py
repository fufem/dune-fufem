import numpy as np
import scipy.sparse.linalg

import dune.common
import dune.geometry
import dune.grid
import dune.functions

from dune.grid import DataType
from dune.functions import defaultGlobalBasis, Power, Composite, Lagrange

from fufemforms import *
from utilities import *

def isNear(a,b):
  return np.abs(a-b) <= 1e-10



ansatzOrder = 2

# Scalar right hand side
rhs = lambda x: 10

# Indicator function of Dirichlet boundary
isDirichlet = lambda x : isNear(x[0], 0) or isNear(x[0], 1) or isNear(x[1], 0) or isNear(x[1], 1)

# Dirichlet boundary values
dirichletValues = lambda x : 0



# Technicallity: Expose some C++ functions
def power(expr, p):
  cpp = "RangeOperator([](auto x, auto p) { return std::pow(x, p);})"
  return FunctionCallExpression(cpp, expr, Constant(p))

def liftOrder(basis, k):
  return FunctionCallExpression("[&](const auto& basis, auto k) { return Coefficient(Dune::Functions::makeGridViewFunction([](auto){ return 1; }, basis.gridView()), k); }", Constant(basis), Constant(k))



# Nonlinearity and derivative
p = 5
psi = lambda u : power(u, p)
Dpsi = lambda u : p*power(u, p-1)



def assembleF(U, basis, isConstrained):
  u = bindToCoefficients(trialFunction(basis), U)
  v = testFunction(basis)

  # Nonlinear variational form of the operator
  b = Coefficient(rhs, basis.gridView)
  f = integrate(dot(grad(u),grad(v)) - b*v + psi(u)*v)

  # Evaluation of the operator by assembling the nonlinear defect
  assembler = GlobalAssembler(basis, verbosity=0)
  F = assembler.assembleFunctional(f)

  # Constrain evaluation to subspace
  F -= F*isConstrained
  return F



def assembleDF(U, basis, isConstrained):
  u = bindToCoefficients(trialFunction(basis), U)
  w = trialFunction(basis)
  v = testFunction(basis)

  # Nonlinear variational form of the operator
  df = integrate(dot(grad(w),grad(v)) + Dpsi(u)*w*v)

  # Evaluation of the operator by assembling the nonlinear defect
  assembler = GlobalAssembler(basis, verbosity=0)
  DF = assembler.assembleOperator(df)

  # Constrain evaluation to subspace
  rows, cols = DF.nonzero()
  for i,j in zip(rows, cols):
    if isConstrained[i] or isConstrained[j]:
      DF[i,j] = 0
    if isConstrained[i] and (i==j):
      DF[i,j] = 1
  return DF



############################### START ########################################

# Number of grid elements (in one direction)
gridSize = 32

# Create a grid of the unit square
grid = dune.grid.structuredGrid([0,0],[1,1],[gridSize,gridSize])

# Create a nodal Lagrange FE basis
basis = defaultGlobalBasis(grid, Lagrange(order=ansatzOrder))
N = len(basis)

print("Dimension of FE space is "+str(N))



# Mark all Dirichlet DOFs
isConstrained = np.zeros( N, dtype=bool )
basis.interpolate(isConstrained,isDirichlet)

# Interpolate the boundary values into solution vector
U0 = np.zeros( N )
basis.interpolate(U0, dirichletValues)

# Solve nonlinear system
F = lambda U : assembleF(U, basis, isConstrained)
DF = lambda U : assembleDF(U, basis, isConstrained)
U = NewtonSolver(F, DF, U0, tolerance=1e-10, damping=True)

u = basis.asFunction(U)

vtk = grid.vtkWriter(2)
u.addToVTKWriter("sol", vtk, DataType.PointData)
vtk.write("semilinear-equation-py")

