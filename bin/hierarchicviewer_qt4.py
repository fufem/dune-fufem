import threading
import sys
import time
from PyQt4 import Qt
import getopt
import itertools

from regexptools import *
from iteratortools import *
from treetools import *
        
class MyItemModel(Qt.QStandardItemModel):
  
    def __init__(self, *args):
        apply(Qt.QStandardItemModel.__init__, (self,) + args)
	self.parentItem = self.invisibleRootItem()
	self.lastItemLevel = 0
	self.lastItem = self.parentItem
                
    def queueItem(self,item):
        thisItem = Qt.QStandardItem(item[0].rstrip())
        thisItemLevel = item[1]
        
        levelDiff = self.lastItemLevel - thisItemLevel
        
        if (levelDiff == 0):
	    parent = self.lastItem.parent()
	    if (parent):
		parent.appendRow(thisItem)
	    else:
		self.parentItem.appendRow(thisItem)
		
	if (levelDiff > 0):
	    for _ in itertools.repeat(None, levelDiff-1):
	        self.lastItem = self.lastItem.parent()
		if not self.lastItem:
		    print "ERROR: BAD LEVEL PARENT"
	    parent = self.lastItem.parent()
	    parent = parent.parent()
	    if (parent):
		parent.appendRow(thisItem)
		
	    else:
		self.parentItem.appendRow(thisItem)
	    self.lastItemLevel = thisItemLevel

	if (levelDiff < 0):
	    if (levelDiff < -1):
                for _ in itertools.repeat(None, -levelDiff-1):
                    dummyItem = Qt.QStandardItem(item[0].rstrip() + '-Dummy')
                    self.lastItem.appendRow(dummyItem)
                    self.lastItem = dummyItem
	    self.lastItem.appendRow(thisItem)
	    self.lastItemLevel = thisItemLevel
	
	self.lastItem = thisItem
	
        #def saveModel(self, file): TODO
        #def saveItem(self, file, item): TODO


class MainWindow(Qt.QMainWindow):

    #def __init__(self, app, hit, timeout, autoAppend, follow, *args):
    def __init__(self, app, hit, timeout, follow, *args):
        apply(Qt.QMainWindow.__init__, (self,) + args)
        self.app = app
        self.hit = hit
        self.timeout = timeout # deprecated

        self.content = Qt.QFrame(self)
        self.setCentralWidget(self.content)
        self.contentlayout = Qt.QVBoxLayout(self.content)
        self.controlBoxLayout = Qt.QHBoxLayout()
        self.controlBoxLayout.setSpacing(20)
        self.controlBoxLayout.setMargin(10)
        self.contentlayout.addLayout(self.controlBoxLayout)

        self.maxLevelControl = Qt.QSpinBox()
        self.maxLevelControl.setRange(0,10)
        self.maxLevelControl.setSingleStep(1)

        self.itemModel = MyItemModel()
        self.treeview = Qt.QTreeView(self)
        self.treeview.setModel(self.itemModel)
        self.treeview.setRootIsDecorated(True)
        self.treeview.setHeaderHidden(True)

        self.controlBoxLayout.addWidget(self.maxLevelControl)
        self.contentlayout.addWidget(self.treeview)
        self.controlBoxLayout.insertStretch(-1)

        self.connect(self.maxLevelControl, Qt.SIGNAL('valueChanged(int)'), self.expandToDepth2)

        self.readerThread = ItemReaderThread(self.hit,self.itemModel.queueItem)
        self.readerThread.setDaemon(True)
        self.readerThread.start()

    def expandToDepth2(self, depth):
	if (depth == 0):
	    self.treeview.collapseAll()
	else:
	    self.treeview.expandToDepth(depth-1)

class ItemReaderThread (threading.Thread):
    def __init__(self,it,action):
        self.it = it
        self.action = action
        threading.Thread.__init__ (self)

    def run(self):
        try:
            for item in self.it:
                self.action(item)
        except:
#            print "Unexpected error:", sys.exc_info()[0]
            pass

def usage():
    print 'SYNOPSIS'
    print '    python hierarchicviewer.py [OPTIONS] FILE'
    print '    python hierarchicviewer.py [OPTIONS] -'
    print ''
    print 'DESCRIPTION'
    print '    If FILE was given view content of file in a hierarchic view.'
    print '    If - is given view standard input in a hierarchic view.'
    print '    Input should contain UP and DOWN indicators that indicate'
    print '    to switch the level in the output hierarchy correspondingly.'
    print ''
    print '    --help'
    print '        View this help text.'
    print ''
    print '    -u REGEX , --up=REGEX'
    print '        Lines matching REGEX are considered as indicator to switch'
    print '        up in the hierarchy. (default: \'^ *\\}\')'
    print ''
    print '    -d REGEX , --down=REGEX'
    print '        Lines matching REGEX are considered as indicator to switch'
    print '        down in the hierarchy. (default: \'^ *\\{\')'
    print ''
    print '    -h, --hideupdown'
    print '        Hide lines that contain UP or DOWN indicators in the output.'
    print ''
    print '    -n, --nofollow [IGNORED]' 
    print '        If supplied the view does not follow if new lines are appended.'
    print ''
    print '    -t TIMEOUT, --timeout=TIMEOUT [IGNORED]'
    print '        The input buffer it cleared periodically and added to the'
    print '        hierarchic view after TIMEOUT seconds. (default: 0.1)'


def main(args):

    try:
        (opts, args) = getopt.getopt(sys.argv[1:], "t:u:d:hn", ["timeout=", "up=", "down=", "hideupdown", "nofollow", "noappend", "help"])
    except getopt.GetoptError, err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        print ''
        usage()
        sys.exit(2)

    for (o, a) in opts:
        if o in ("--help",):
            usage()
            return

    # timeout to clear buffer and view items (default: 0.1s)
    timeout = 2.0    #timeout = 0.1

    # reexp to switch level up (default: {)
    up = regexpMatchFunction('^ *\{')

    # reexp to switch level down (default: })
    down = regexpMatchFunction('^ *\}')

    # switch to hide lines matching up and down (default: False)
    hideupdown = False

    # switch to determine if new lines are auto appended (default: True)
    #autoAppend = True

    # switch to determine if view follows appended lines (default: True)
    follow = True

    if (len(args)==1):
        if (args[0]=='-'):
            infile = sys.stdin
        else:
            try:
                infile = file(args[0], 'r')
            except IOError:
                print 'Could not open file:', args[0]
                print ''
                usage()
                sys.exit(2)
    else:
        print 'You did not supply a file or the - argument.'
        print ''
        usage()
        sys.exit(2)

    for (o, a) in opts:
        if o in ("-t", "--timeout"):
            timeout = float(a)
            print '-t/--timeout option is [IGNORED]'
        elif o in ("-u", "--up"):
            up = regexpMatchFunction(a)
        elif o in ("-d", "--down"):
            up = regexpMatchFunction(a)
        elif o in ("-h", "--hideupdown"):
            hideupdown = True
        elif o in ("-n", "--nofollow"):
            print '-n/--nofollow option is [IGNORED]'
            follow = False
        #elif o in ("--noappend"):
            #autoAppend = False

    it = FileIterator(infile)
    hit = HierarchyIterator(it, up, down, hideupdown)

    app=Qt.QApplication(args)
    #win=MainWindow(app, hit, timeout, autoAppend, follow)
    win=MainWindow(app, hit, timeout, follow)
    win.resize(1000,600)
    win.show()
    app.connect(app, Qt.SIGNAL("lastWindowClosed()"),
                app, Qt.SLOT("quit()"))

    app.exec_()

    infile.close()


if __name__=="__main__":
    main(sys.argv)
