// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_FUNCTIONTOOLS_BOUNDARYDOFS_HH
#define DUNE_FUFEM_FUNCTIONTOOLS_BOUNDARYDOFS_HH

#include <dune/common/concept.hh>

#include <dune/fufem/boundarypatch.hh>

#include <dune/functions/backends/concepts.hh>
#include <dune/functions/backends/istlvectorbackend.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>


namespace Dune::Fufem {

/**
 * \brief For a given basis and boundary patch, determine all degrees
 *        of freedom on the patch.
 *
 * Set each entry of the vector to true, that corresponds to a DOF
 * associated to an intersection (or a subentity) contained in the
 * given BoundaryPatch. If the passed vector does not implement
 * the VectorBackend concept, it is automatically wrapped in a
 * Dune::Functions::ISTLVectorBackend.
 *
 * \param boundaryPatch Only mark boundary DOFs associated to intersections in this patch.
 * \param basis Mark DOFS of this basis
 * \param vector A bit vector corresponding to the basis DOFs
 */
template <class GridView, class Basis, class Vector>
void markBoundaryPatchDofs(const BoundaryPatch<GridView>& boundaryPatch,
                           const Basis& basis,
                           Vector& vector)
{
    auto&& toVectorBackend = [&](auto& v) -> decltype(auto) {
        if constexpr (Dune::models<Dune::Functions::Concept::VectorBackend<Basis>, decltype(v)>()) {
            return v;
        } else {
            return Dune::Functions::istlVectorBackend(v);
        }
    };

    auto&& vectorBackend = toVectorBackend(vector);
    vectorBackend.resize(basis);
    vectorBackend = false;
    Dune::Functions::forEachBoundaryDOF(basis, [&] (auto&& localIndex, const auto& localView, const auto& intersection) {
        if (boundaryPatch.contains(intersection))
            vectorBackend[localView.index(localIndex)] = true;
    });
}

/**
 * \brief For a given basis determine all degrees of freedom on the boundary.
 *
 * Set each entry of the vector to true, that corresponds to a DOF
 * associated to an intersection (or a subentity) contained in the
 * boundary. If the passed vector does not implement
 * the VectorBackend concept, it is automatically wrapped in a
 * Dune::Functions::ISTLVectorBackend.
 *
 * \param basis Mark DOFS of this basis
 * \param vector A bit vector corresponding to the basis DOFs
 */
template <class Basis, class Vector>
void markBoundaryDofs(const Basis& basis,
                           Vector& vector)
{
    auto&& toVectorBackend = [&](auto& v) -> decltype(auto) {
        if constexpr (Dune::models<Dune::Functions::Concept::VectorBackend<Basis>, decltype(v)>()) {
            return v;
        } else {
            return Dune::Functions::istlVectorBackend(v);
        }
    };

    auto&& vectorBackend = toVectorBackend(vector);
    vectorBackend.resize(basis);
    vectorBackend = false;
    Dune::Functions::forEachBoundaryDOF(basis, [&] (auto&& index) {
        vectorBackend[index] = true;
    });
}


} // namespace Dune::Fufem



#endif   // DUNE_FUFEM_FUNCTIONTOOLS_BOUNDARYDOFS_HH
