#ifndef DUNE_FUFEM_INSERTIONINDEXPERMUTATION_HH
#define DUNE_FUFEM_INSERTIONINDEXPERMUTATION_HH

#include <cstddef>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>

#include <dune/common/iteratorrange.hh>
#include <dune/common/rangeutilities.hh>

#include <dune/fufem/boundaryiterator.hh>



namespace Dune::Fufem {



/**
 * \brief Apply inplace permutation to container
 *
 * This permutes the entries of the data container according
 * to the given permutation using subsequent calls to std::swap
 * such that data[k] contains the former entry of data[permutation[k]].
 * I.e. the permutation is assumed to store for each new index the
 * corresponding old index.
 *
 * \param permutation Index permutation
 * \param data Data container that should be permuted
 */
template<class Permutation, class DataContainer>
void permuteInplace(const Permutation& permutation, DataContainer& data)
{
  auto processed = std::vector<bool>(data.size(), false);
  for(auto k : Dune::IntegralRange(data.size()))
  {
    if (not processed[k])
    {
      auto pos = k;
      while (std::size_t(permutation[pos]) != k)
      {
        std::swap(data[pos], data[permutation[pos]]);
        processed[pos] = true;
        pos = permutation[pos];
      }
      processed[pos] = true;
    }
  }
}



/**
 * \brief Compute permutation of boundary segments
 *
 * This computes the permutation from boundary segment index as provided by the grid
 * to insertion index as provided by the gridFactory. The result is returned
 * as vector of insertion indices ordered by boundary segment indices.
 *
 * \param gridFactory The factory the grid was created with
 * \param grid The grid
 * \returns A std::vector<std::size_t> mapping boundarySegmentIndex to insertionIndex
 */
template<class GridFactory, class Grid>
std::vector<std::size_t> boundarySegmentInsertionIndices(const GridFactory& gridFactory, const Grid& grid)
{
  auto gridView = grid.leafGridView();
  auto defaultValue = std::numeric_limits<std::size_t>::max();
  auto indices = std::vector<std::size_t>(grid.numBoundarySegments(), defaultValue);

  using GridView = decltype(gridView);
  using It = BoundaryIterator<GridView>;

  auto boundaryIntersections = Dune::IteratorRange(It(gridView, It::PositionFlag::begin), It(gridView, It::PositionFlag::end));
  for(auto&& intersection : boundaryIntersections)
    if (gridFactory.wasInserted(intersection))
      indices[intersection.boundarySegmentIndex()] = gridFactory.insertionIndex(intersection);
    else
      indices[intersection.boundarySegmentIndex()] = defaultValue;

  return indices;
}



/**
 * \brief Permute container according to boundary segment permutation
 *
 * This will permute the entries of a container index by insertion indices,
 * to the ordering corresponidng to boundary segment indices. The permutation
 * is applied in-place using std::swap.
 *
 * Notice that this first computes the permutation, then applies it to the
 * container, and finally discards it. Hence, if you want to permute several
 * containers, it is more efficient to compute and store the permutation
 * while applying the in-place permutation manually using Dune::Fufem::permuteInplace().
 *
 * \param gridFactory The factory the grid was created with
 * \param grid The grid
 * \param data The container to permute. Must support std::swap.
 */
template<class GridFactory, class Grid, class Container>
void permuteFromBoundarySegmentInsertionIndices(const GridFactory& gridFactory, const Grid& grid, Container& data)
{
  auto permutation = boundarySegmentInsertionIndices(gridFactory, grid);
  Dune::Fufem::permuteInplace(permutation, data);
}



}

#endif // DUNE_FUFEM_INSERTIONINDEXPERMUTATION_HH
