// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_BACKENDS_SINGLEROWCOLMATRIX_HH
#define DUNE_FUFEM_BACKENDS_SINGLEROWCOLMATRIX_HH


#include <type_traits>
#include <utility>

#include <dune/common/ftraits.hh>

namespace Dune::Fufem {



/**
 * \brief Tag matrix as single row matrix
 *
 * \tparam Base The base matrix type that should be tagged.
 *
 * This class helps to solve the problem, that the distribution
 * of digits from non-uniform row- and column-multi-indices to the
 * different levels of a nested matrix cannot be done generically.
 *
 * The reason for this problem is, that all matrices in Dune need
 * a row and a column index to access their entries. Hence, for a
 * nested matrix the row and column multi-index always need to have
 * the same length. However, there are natural applications of multi-indices
 * with non-uniform length (e.g. for the Stokes problem). In this
 * case it is implicitly assumed that for certain matrices in the
 * nested matrix hierarchy only a column or row index is provided.
 * In practice this means, you need to insert a zero-digit at the
 * correct position in the complementary multi-index.
 * Unfortunately, this cannot be done automatically, because there
 * are many candidates for this position and it's in fact a user
 * choice, which one is correct.
 *
 * To solve this, one can tag a matrix type Base as single-row by
 * using SingleRowMatrix<Base> instead. Then the algorithm for
 * dispatching multi-indices knows, that it has to insert a
 * zero digit into the row multi-index here.
 *
 * The class derives from Base and inherits all its members without
 * overwriting any and can thus be used as a replacement for Base.
 */
template<class Base>
class SingleRowMatrix :
  public Base
{
public:
  using Base::Base;
};

/**
 * \brief Tag matrix as single column matrix
 *
 * \tparam Base The base matrix type that should be tagged.
 *
 * This class helps to solve the problem, that the distribution
 * of digits from non-uniform row- and column-multi-indices to the
 * different levels of a nested matrix cannot be done generically.
 *
 * The reason for this problem is, that all matrices in Dune need
 * a row and a column index to access their entries. Hence, for a
 * nested matrix the row and column multi-index always need to have
 * the same length. However, there are natural applications of multi-indices
 * with non-uniform length (e.g. for the Stokes problem). In this
 * case it is implicitly assumed that for certain matrices in the
 * nested matrix hierarchy only a column or row index is provided.
 * In practice this means, you need to insert a zero-digit at the
 * correct position in the complementary multi-index.
 * Unfortunately, this cannot be done automatically, because there
 * are many candidates for this position and it's in fact a user
 * choice, which one is correct.
 *
 * To solve this, one can tag a matrix type M as single-column by
 * using SingleColumnMatrix<M> instead. Then the algorithm for
 * dispatching multi-indices knows, that it has to insert a
 * zero digit into the column multi-index here.
 *
 * The class derives from Base and inherits all its members without
 * overwriting any and can thus be used as a replacement for Base.
 */
template<class Base>
class SingleColumnMatrix :
  public Base
{
public:
  using Base::Base;
};



namespace Imp {

constexpr std::false_type isSingleColumnMatrix(const void*)
{ return {}; }

template<class T>
constexpr std::true_type isSingleColumnMatrix(const SingleColumnMatrix<T>*)
{ return {}; }

constexpr std::false_type isSingleRowMatrix(const void*)
{ return {}; }

template<class T>
constexpr std::true_type isSingleRowMatrix(const SingleRowMatrix<T>*)
{ return {}; }

} // namespace Imp



/**
 * \brief Traits class to check if matrix is marked as single column matrix
 */
template<class T>
struct IsSingleColumnMatrix : public decltype(Imp::isSingleColumnMatrix( std::declval<std::decay_t<T>*>()))
{};

/**
 * \brief Traits class to check if matrix is marked as single row matrix
 */
template<class T>
struct IsSingleRowMatrix : public decltype(Imp::isSingleRowMatrix( std::declval<std::decay_t<T>*>()))
{};



} // namespace Dune::Fufem



namespace Dune {

/**
 * \brief Specialization of FieldTraits for SingleRowMatrix
 */
template <class Base>
struct FieldTraits<Dune::Fufem::SingleRowMatrix<Base>>
  : public FieldTraits<Base>
{};

/**
 * \brief Specialization of FieldTraits for SingleColumnMatrix
 */
template <class Base>
struct FieldTraits<Dune::Fufem::SingleColumnMatrix<Base>>
  : public FieldTraits<Base>
{};

} // namespace Dune



#endif // DUNE_FUFEM_BACKENDS_SINGLEROWCOLMATRIX_HH
