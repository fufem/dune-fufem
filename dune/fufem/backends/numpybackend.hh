// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_BACKENDS_NUMPYBACKEND_HH
#define DUNE_FUFEM_BACKENDS_NUMPYBACKEND_HH

#include <type_traits>
#include <utility>
#include <algorithm>
#include <array>

#include <dune/fufem/backends/matrixbuilder.hh>
#include <dune/fufem/backends/istlmatrixbackend.hh>

#include <dune/python/pybind11/pybind11.h>
#include <dune/python/pybind11/stl.h>
#include <dune/python/pybind11/numpy.h>



namespace Dune::Fufem {

/**
 * \brief Implementation of the VectorBackend concept for numpy vectors
 *
 * This implements VectorBackend concept from dune-functions
 * for flat numpy arrays represented by pybind11::array_t.
 */
template<class T>
class NumPyVectorBackend
{

public:

  using Vector = pybind11::array_t<T>;

  NumPyVectorBackend(Vector& vector) :
    vector_(&vector)
  {}

  template<class SizeInfo>
  void resize(const SizeInfo& sizeInfo)
  {
    *vector_ = pybind11::array_t<T>(sizeInfo.size());
  }

  template<class MultiIndex>
  decltype(auto) operator[](const MultiIndex& index) const
  {
    auto vector_u = vector_->unchecked();
    return vector_u[index];
  }

  template<class MultiIndex>
  decltype(auto) operator[](const MultiIndex& index)
  {
    auto vector_mu = vector_->mutable_unchecked();
    return vector_mu[index];
  }

  template<typename Value>
  void operator= (const Value& value)
  {
    auto size = vector_->shape(0);
    auto vector_mu = vector_->mutable_unchecked();
    for(auto&& i : Dune::range(size))
      vector_mu(i) = value;
  }

  void operator= (const NumPyVectorBackend<T>& other)
  {
    vector() = other.vector();
  }

  const Vector& vector() const
  {
    return *vector_;
  }

  Vector& vector()
  {
    return *vector_;
  }

private:
  Vector* vector_;
};



/**
 * \brief Struct providing raw storage for a flat CSR matrix using numpy arrays
 *
 * Since there's no C/C++ API for scipy.sparse.csr_matrix, we cannot directly
 * implement a MatrixBackend for this. However, the csr_matrix uses a simple
 * triple of numpy arrays as storage and can be constructed from one.
 * Thus we can almost rely on automatic pybind11 conversion to create
 * a csr_matrix. To this end we let pybind11 auto-convert asTuple() to Python
 * which results in a nested tuple of numpy arrays. The result can be
 * used to construct a csr_matrix in Python using csr_matrix(data[0], shape=data[1]).
 */
template<class T=double>
struct NumPyCSRMatrix
{
  using size_type = std::size_t;

  pybind11::array_t<T> entries;
  pybind11::array_t<size_type> colIndices;
  pybind11::array_t<size_type> rowPtrs;

  std::array<size_type, 2> shape;

  auto asTuple()
  {
    return std::tuple(std::tuple(entries, colIndices, rowPtrs), shape);
  }

  auto asTuple() const
  {
    return std::tuple(std::tuple(entries, colIndices, rowPtrs), shape);
  }

};



/**
 * \brief Implementation of the MatrixBuilder for NumPyCSRMatrix
 *
 * This implements the unified pattern builder interface for
 * NumPyCSRMatrix.
 */
template<class T>
class MatrixBuilder<NumPyCSRMatrix<T>>
{
public:

  using Matrix = NumPyCSRMatrix<T>;
  using size_type = typename Matrix::size_type;

  MatrixBuilder(Matrix& matrix) :
    matrix_(matrix)
  {}

  template<class RowSizeInfo, class ColSizeInfo>
  void resize(const RowSizeInfo& rowSizeInfo, const ColSizeInfo& colSizeInfo)
  {
    indices_.resize(rowSizeInfo.size(), colSizeInfo.size());
  }

  template<class RowIndex, class ColIndex>
  void insertEntry(const RowIndex& rowIndex, const ColIndex& colIndex)
  {
    indices_.add(rowIndex, colIndex);
  }

  void setupMatrix()
  {
    size_type rows = indices_.rows();
    size_type cols = indices_.cols();
    matrix_.shape = {rows, cols};

    // Compute number of nonzeros
    std::size_t nnz = 0;
    for(auto row : Dune::range(rows))
      nnz += indices_.rowsize(row);

    // Allocate data
    matrix_.entries = pybind11::array_t<T>(nnz);
    matrix_.colIndices = pybind11::array_t<size_type>(nnz);
    matrix_.rowPtrs = pybind11::array_t<size_type>(rows+1);

    auto entries_mu = matrix_.entries.mutable_unchecked();
    auto colIndices_mu = matrix_.colIndices.mutable_unchecked();
    auto rowPtrs_mu = matrix_.rowPtrs.mutable_unchecked();

    // Write column indices
    size_type next = 0;
    for(auto row : Dune::range(rows))
    {
      rowPtrs_mu(row) = next;
      std::visit([&](const auto& colIndicesOfRow) {
        for(auto&& colIndex : colIndicesOfRow)
        {
          colIndices_mu(next) = colIndex;
          ++next;
        }
      }, indices_.columnIndices(row));
    }
    rowPtrs_mu(rows) = next;

    // Zero initialize entries
    for(auto i : Dune::range(nnz))
      entries_mu(i) = 0;
  }

private:
  Dune::MatrixIndexSet indices_;
  Matrix& matrix_;
};



/**
 * \brief Implementation of the MatrixBackend concept NumPyCSRMatrix
 *
 * This implements MatrixBackend concept from dune-fufem
 * for NumPyCSRMatrix objects.
 */
template<class T=double>
class NumPyCSRMatrixBackend
{
public:

  using Matrix = NumPyCSRMatrix<T>;
  using Entry = T;
  using value_type = Entry;

  NumPyCSRMatrixBackend(Matrix& matrix) :
    matrix_(&matrix)
  {}

  MatrixBuilder<Matrix> patternBuilder()
  {
    return {*matrix_};
  }

  template<class RowIndex, class ColIndex>
  auto nnzIndex(const RowIndex& row, const ColIndex& col) const
  {
    auto colIndices_u = matrix_->colIndices.unchecked();
    auto rowPtrs_u = matrix_->rowPtrs.unchecked();
    auto ptrRange = Dune::range(rowPtrs_u(row), rowPtrs_u(row+1));
    auto colIndicesOfRow = Dune::transformedRangeView(ptrRange, [&](auto i) -> decltype(auto) { return colIndices_u(i); });
    auto k = std::lower_bound(colIndicesOfRow.begin(), colIndicesOfRow.end(), col) - colIndicesOfRow.begin();
    return k+rowPtrs_u(row);
  }

  template<class RowIndex, class ColIndex>
  const Entry& operator()(const RowIndex& row, const ColIndex& col) const
  {
    auto entries_u = matrix_->entries.unchecked();
    return entries_u(nnzIndex(row, col));
  }

  template<class RowIndex, class ColIndex>
  Entry& operator()(const RowIndex& row, const ColIndex& col)
  {
    auto entries_mu = matrix_->entries.mutable_unchecked();
    return entries_mu(nnzIndex(row, col));
  }

  const Matrix& matrix() const
  {
    return *matrix_;
  }

  Matrix& matrix()
  {
    return *matrix_;
  }

  template<class Value>
  void assign(const Value& value)
  {
    auto nnz = matrix_->entries.shape(0);
    auto entries_mu = matrix_->entries.mutable_unchecked();
    for(auto&& i : Dune::range(nnz))
      entries_mu(i) = value;
  }

protected:

  Matrix* matrix_;
};



} // namespace Dune::Fufem



#endif // DUNE_FUFEM_BACKENDS_NUMPYBACKEND_HH
