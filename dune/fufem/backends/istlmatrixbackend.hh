// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_BACKENDS_ISTLMATRIXBACKEND_HH
#define DUNE_FUFEM_BACKENDS_ISTLMATRIXBACKEND_HH

#include <type_traits>
#include <utility>

#include <dune/common/indices.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/hybridutilities.hh>

#include <dune/functions/common/indexaccess.hh>

#include <dune/fufem/backends/singlerowcolmatrix.hh>
#include <dune/fufem/backends/matrixbuilder.hh>



namespace Dune::Fufem::Concept {

// Concept for a ConstMatrixBackend
template<class RowBasis, class ColBasis=RowBasis>
struct ConstMatrixBackend
{
  using RowMultiIndex = typename RowBasis::MultiIndex;
  using ColMultiIndex = typename ColBasis::MultiIndex;

  template<class V>
  auto require(const V& v) -> decltype(
    v(std::declval<RowMultiIndex>(), std::declval<ColMultiIndex>())
  );
};

// Concept for a MatrixBackend
template<class RowBasis, class ColBasis=RowBasis>
struct MatrixBackend : Dune::Concept::Refines< ConstMatrixBackend<RowBasis, ColBasis> >
{
  using RowMultiIndex = typename RowBasis::MultiIndex;
  using ColMultiIndex = typename ColBasis::MultiIndex;

  template<class V>
  auto require(const V& v) -> decltype(
    const_cast<V&>(v).patternBuilder(),
    const_cast<V&>(v)(std::declval<RowMultiIndex>(), std::declval<ColMultiIndex>()) = v(std::declval<RowMultiIndex>(), std::declval<ColMultiIndex>())
  );
};

} // namespace Dune::Fufem::Concept



namespace Dune::Fufem {



namespace Impl {



  // The following is essentially a copy of Dune::Functions::hybridIndexAccess
  // But since matrices do not provide size(), we have to use N() instead.
  template<class C, class I, class F,
    typename std::enable_if< Dune::models<Dune::Functions::Imp::Concept::HasDynamicIndexAccess<I>, C>(), int>::type = 0>
  static auto hybridRowIndexAccess(C&& c, const I& i, F&& f)
    -> decltype(f(c[i]))
  {
    return f(c[i]);
  }

  template<class C, class I, class F,
    typename std::enable_if< not Dune::models<Dune::Functions::Imp::Concept::HasDynamicIndexAccess<I>, C>(), int>::type = 0>
  static decltype(auto) hybridRowIndexAccess(C&& c, const I& i, F&& f)
  {
    return Hybrid::switchCases(std::make_index_sequence<std::decay_t<C>::N()>(), i,
        [&](const auto& ii) -> decltype(auto){
          return f(c[ii]);
        }, [&]() -> decltype(auto){
          return f(c[Dune::Indices::_0]);
        });
  }



  // Call f(matrix[i][j]) and return the result.
  // This works with dynamic indices i,j, even for a multi-type matrix.
  // However, it requires that f() has a unique return type.
  template<class Matrix, class RowIndex, class ColIndex, class F>
  decltype(auto) visitMatrixEntry(Matrix&& matrix, const RowIndex& i, const ColIndex& j, F&& f)
  {
    using namespace Dune::Functions;
    return hybridRowIndexAccess(matrix, i, [&](auto&& matrix_i) -> decltype(auto) {
      return hybridIndexAccess(matrix_i, j, f);
    });
  }



  // Call f(matrix[i0][j0]...[in][jm]), by recursively resolving row- and column-multi-indices.
  // Whenenver a SingleRowMatrix or SingleColumnMatrix is encountered, a zero row- or column-index
  // is inserted, respectively. The recursion ends, whenever f(matrixEntry) can be called.
  // This works with dynamic indices i,j, even for a multi-type matrix.
  // However, it requires that f() has a unique return type.
  template<class Matrix, class RowIndex, class ColIndex, class F>
  static decltype(auto) visitMatrixEntryRecursive(Matrix& matrix, const RowIndex& iii, const ColIndex& jjj, F&& f)
  {
    using namespace Dune::Indices;
    using namespace Dune::Functions::Imp;
    static constexpr bool isSingleRow = IsSingleRowMatrix<Matrix>::value;
    static constexpr bool isSingleCol = IsSingleColumnMatrix<Matrix>::value;
    auto splitIndex = [] (auto&& multiIndex) { return std::make_tuple(multiIndex[_0], shiftedDynamicMultiIndex<1>(multiIndex)) ;};
    if constexpr (std::is_invocable_v<F, Matrix&>)
    {
      return f(matrix);
    }
    else if constexpr (isSingleRow)
    {
      assert(jjj.size()>0);
      auto [j, jj] = splitIndex(jjj);
      // We have to capture jj explicitly by value, because capturing structured bindings
      // by reference is not allowed before C++20
      return visitMatrixEntry(matrix, _0, j, [&, jj=jj](auto&& matrix_0j) -> decltype(auto) {
        return visitMatrixEntryRecursive(matrix_0j, iii, jj, f);
      });
    }
    else if constexpr (isSingleCol)
    {
      assert(iii.size()>0);
      auto [i, ii] = splitIndex(iii);
      // We have to capture ii explicitly by value, because capturing structured bindings
      // by reference is not allowed before C++20
      return visitMatrixEntry(matrix, i, _0, [&, ii=ii](auto&& matrix_i0) -> decltype(auto) {
        return visitMatrixEntryRecursive(matrix_i0, ii, jjj, f);
      });
    }
    else
    {
      assert(iii.size()>0);
      assert(jjj.size()>0);
      auto [i, ii] = splitIndex(iii);
      auto [j, jj] = splitIndex(jjj);
      // We have to capture ii and jj explicitly by value, because capturing structured bindings
      // by reference is not allowed before C++20
      return visitMatrixEntry(matrix, i, j, [&, ii=ii, jj=jj](auto&& matrix_ij) -> decltype(auto) {
        return visitMatrixEntryRecursive(matrix_ij, ii, jj, f);
      });
    }
  }



} // namespace Impl



template<class M, class E=typename M::field_type>
class ISTLMatrixBackend
{

  template<class Result>
  struct ToScalar;

  template<class R>
  struct ToScalar<R&>
  {
    template<class Matrix,
      std::enable_if_t<std::is_convertible_v<Matrix&, R&>, int> = 0>
    R& operator()(Matrix& matrix) {
      return matrix;
    }

    R& operator()(Dune::FieldMatrix<R, 1, 1>& matrix) {
      return matrix[0][0];
    }
  };

  template<class R>
  struct ToScalar<const R&>
  {
    template<class Matrix,
      std::enable_if_t<std::is_convertible_v<Matrix&, const R&>, int> = 0>
    const R& operator()(Matrix& matrix) {
      return matrix;
    }

    const R& operator()(const Dune::FieldMatrix<R, 1, 1>& matrix) {
      return matrix[0][0];
    }
  };



public:

  using Matrix = M;
  using Entry = E;
  using value_type = Entry;

  ISTLMatrixBackend(Matrix& matrix) :
    matrix_(&matrix)
  {}

  MatrixBuilder<Matrix> patternBuilder()
  {
    return {*matrix_};
  }

  template<class RowMultiIndex, class ColMultiIndex>
  const Entry& operator()(const RowMultiIndex& row, const ColMultiIndex& col) const
  {
    return Impl::visitMatrixEntryRecursive(*matrix_, row, col, ToScalar<const Entry&>());
  }

  template<class RowMultiIndex, class ColMultiIndex>
  Entry& operator()(const RowMultiIndex& row, const ColMultiIndex& col)
  {
    return Impl::visitMatrixEntryRecursive(*matrix_, row, col, ToScalar<Entry&>());
  }

  /**
   * \brief Const access to wrapped matrix
   */
  const Matrix& matrix() const
  {
    return *matrix_;
  }

  /**
   * \brief Mutable access to wrapped matrix
   */
  Matrix& matrix()
  {
    return *matrix_;
  }

  /**
   * \brief Assign value to wrapped matrix
   */
  template<class Value>
  void assign(const Value& value)
  {
    matrix() = value;
  }

protected:

  Matrix* matrix_;
};



template<class Entry, class Matrix>
auto istlMatrixBackend(Matrix& matrix)
{
  return ISTLMatrixBackend<Matrix, Entry>(matrix);
}

template<class Matrix>
auto istlMatrixBackend(Matrix& matrix)
{
  return ISTLMatrixBackend<Matrix, typename Matrix::field_type>(matrix);
}



/**
 * \brief Return a `ConstMatrixBackend` for given matrix
 *
 * This returns a `ConstMatrixBackend` for the given matrix object
 * suitable to be accessed using the multi-indices of the provided
 * basis types.
 *
 * If the passed matrix object already satisfies the `ConstMatrixBackend`
 * concept, it is forwarded, otherwise it is wrapped using
 * `Dune::Fufem::istlMatrixBackend(...)`.
 * Thus the return type should always be captured using either
 * `const auto&` or `auto&&`.
 *
 * Warning: You must not pass temporaries to this method.
 *
 * \tparam RowBasis Basis type for row indices
 * \tparam ColBasis Basis type for column indices
 *
 * \param m Matrix to be accessed using basis indices.
 */
template<class RowBasis, class ColBasis, class M>
decltype(auto) toConstMatrixBackend(M& m) {
  if constexpr (Dune::models<Dune::Fufem::Concept::ConstMatrixBackend<RowBasis,ColBasis>, M>()) {
    return m;
  } else {
    return Dune::Fufem::istlMatrixBackend(m);
  }
}



/**
 * \brief Return a `MatrixBackend` for given matrix
 *
 * This returns a `MatrixBackend` for the given matrix object
 * suitable to be accessed using the multi-indices of the provided
 * basis types.
 *
 * If the passed matrix object already satisfies the `MatrixBackend`
 * concept, it is forwarded, otherwise it is wrapped using
 * `Dune::Fufem::istlMatrixBackend(...)`.
 * Thus the return type should always be captured using either
 * `const auto&` or `auto&&`.
 *
 * Warning: You must not pass temporaries to this method.
 *
 * \tparam RowBasis Basis type for row indices
 * \tparam ColBasis Basis type for column indices
 *
 * \param m Matrix to be accessed using basis indices.
 */
template<class RowBasis, class ColBasis, class M>
decltype(auto) toMatrixBackend(M& m) {
  if constexpr (Dune::models<Dune::Fufem::Concept::MatrixBackend<RowBasis, ColBasis>, M>()) {
    return m;
  } else {
    return Dune::Fufem::istlMatrixBackend(m);
  }
}



} // namespace Dune::Fufem



#endif // DUNE_FUFEM_BACKENDS_ISTLMATRIXBACKEND_HH
