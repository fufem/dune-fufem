#ifndef DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_LAPLACEASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_LAPLACEASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/version.hh>

#include <dune/istl/matrix.hh>

#include <dune/typetree/visitor.hh>
#include <dune/typetree/traversal.hh>

#include "dune/fufem/quadraturerules/quadraturerulecache.hh"



namespace Dune::Fufem
{

  namespace Impl
  {

    /** \brief Computes the inner product of the jacobians of the local basis functions
     *
     * It computes at each leaf node the inner product (hence, stiffness matrix) of the
     * jacobians of the local basis functions. It determines the local index and updates the
     * corresponding entry of the local matrix.
     */
    template <class LV, class M>
    class LeafNodeLaplaceAssembler
    {
    public:

      using LocalView = LV;
      using Matrix = M;
      using field_type = typename M::field_type;

      /** \brief Constructor with all necessary context information
       *
       * \param [in] lv (localView) element information and localBasis
       * \param [out] m (matrix) resulting local stiffness matrix, wrapped in the ISTLBackend

       */
      LeafNodeLaplaceAssembler(const LV& lv, M& m)
      : localView_(lv)
      , matrix_(m)
      {}

      template<class Node, class TreePath>
      void operator()(Node& node, [[maybe_unused]] TreePath treePath)
      {
        const auto& element = localView_.element();
        const auto& geometry = element.geometry();
        const auto& finiteElement = node.finiteElement();
        const auto& localBasis = finiteElement.localBasis();
        using JacobianType = typename std::decay_t<decltype(localBasis)>::Traits::JacobianType;
        constexpr int gridDim = LocalView::GridView::dimension;

        auto feQuadKey = QuadratureRuleKey(finiteElement);
        // we have a product of jacobians
        auto quadKey = feQuadKey.derivative().square();
        const auto& quad = QuadratureRuleCache<double, gridDim>::rule(quadKey);

        for( const auto& qp : quad )
        {
          const auto& quadPos = qp.position();
          const auto integrationElement = geometry.integrationElement(quadPos);

          std::vector<JacobianType> referenceJacobians;
          localBasis.evaluateJacobian(quadPos, referenceJacobians);

          const auto& jacobianInverse = geometry.jacobianInverse(quadPos);

          // transform jacobians
          std::vector<decltype(referenceJacobians[0] * jacobianInverse)> jacobians(referenceJacobians.size());
          for (size_t i=0; i<jacobians.size(); i++)
            jacobians[i] = referenceJacobians[i] * jacobianInverse;

          // compute matrix entries = inner products of jacobians
          auto z = qp.weight() * integrationElement;
          for (std::size_t i=0; i<localBasis.size(); ++i)
            for (std::size_t j=0; j<localBasis.size(); ++j)
              for ( std::size_t ii=0; ii<jacobians[i].N(); ii++ )
                for ( std::size_t jj=0; jj<jacobians[i].M(); jj++ )
                  matrix_[node.localIndex(i)][node.localIndex(j)] += z*jacobians[i][ii][jj]*jacobians[j][ii][jj];
        }
      }

    private:
      const LocalView& localView_;
      Matrix& matrix_;
    };

  } // namespace Impl

  /** \brief Local Laplace (stiffness) assembler for dune-functions basis
   *
   * We assume the stiffness matrix to be symmetric and hence ansatz and test space to be equal!
   * The implementation allows an arbitrary dune-functions compatible basis, as long as there
   * is an ISTLBackend available for the resulting matrix.
   */
  class LaplaceAssembler
  {
  public:

    template<class Element, class LocalMatrix, class LocalView>
    void operator()(const Element& element, LocalMatrix& localMatrix, const LocalView& testLocalView, const LocalView& ansatzLocalView) const
    {
      // the Laplace matrix operator assumes test == ansatz
      if (&testLocalView != &ansatzLocalView)
        DUNE_THROW(Dune::Exception,"The Laplace matrix operator assumes equal ansatz and test functions");

      // create a tree visitor and compute the inner products of the local functions at the leaf nodes
      Impl::LeafNodeLaplaceAssembler leadNodeLaplaceAssembler(testLocalView,localMatrix);
      TypeTree::forEachLeafNode(testLocalView.tree(),leadNodeLaplaceAssembler);
    }

  };

} // namespace Dune::Fufem



#endif  // DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_LAPLACEASSEMBLER_HH

