// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef DUNE_FUFEM_ASSEMBLERS_BASIS_INTERPOLATION_MATRIX_ASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBLERS_BASIS_INTERPOLATION_MATRIX_ASSEMBLER_HH

#include <type_traits>
#include <cmath>
#include <vector>

#include <dune/common/exceptions.hh>

#include <dune/fufem/backends/istlmatrixbackend.hh>

#include <dune/typetree/visitor.hh>
#include <dune/typetree/pairtraversal.hh>



namespace Impl{


/** \brief Visitor for interpolating the local basis of a leaf node by a fine local basis
 *
 * The LeafNodeInterpolationVisitor traverses the local tree of the coarse basis at an
 * element bound to the coarse localView. At every leaf node the local basis is extracted and
 * interpolated by the corresponding local basis of the fine element (bound to the fine localView).
 *
 * This works for all dune-functions compatible local basis trees.
 *
 * \todo In case of powerNodes we could recycle the interpolation for all child nodes to save
 *       some assembly time.
 */

template <class CLV, class FLV, class G, class M, class I>
class LeafNodeInterpolationVisitor
  : public Dune::TypeTree::TreePairVisitor
  , public Dune::TypeTree::DynamicTraversal
{
public:

  using CoarseLocalView = CLV;
  using FineLocalView = FLV;
  using Geometry = G;
  using Matrix = M;
  using IndexSet = I;

  /** \brief Constructor with all necessary context information
   *
   * \param [in] clv (coarseLocalView) coarse element information and localBasis
   * \param [in] flv (fineLocalView) fine element information and localBasis
   * \param [in] g (geometry) mapping fine -> coarse
   * \param [in] t (tolerance) threshold for considering an interpolated value as zero
   * \param [out] m (matrix) resulting tranferoperator matrix (coarse basis) -> (fine basis) wrapped in ISTLBackend
   * \param [out] i (indices) index set of the matrix
   * \param [in] e (exportIndices) boolean switch: true  = add indices to the index set
   *                                               false = increase the matrix entries
   */
  LeafNodeInterpolationVisitor(const CLV& clv, const FLV& flv, const G& g, double t, M& m, I& i, bool e)
  : coarseLocalView_(clv)
  , fineLocalView_(flv)
  , geometry_(g)
  , tolerance_(t)
  , matrix_(m)
  , indices_(i)
  , exportIndices_(e)
  {}

  template<class CoarseNode, class FineNode, class TreePath>
  void leaf(CoarseNode& coarseNode, FineNode& fineNode, TreePath treePath)
  {
    const auto& coarseFiniteElement = coarseNode.finiteElement();
    const auto& fineFiniteElement   = fineNode.finiteElement();

    const auto& coarseLocalBasis = coarseFiniteElement.localBasis();
    const auto& fineLocalBasis   = fineFiniteElement.localBasis();

    // Hack: The RangeFieldType is the best guess of a suitable type for coefficients we have here
    using CoefficientType = typename std::decay_t<decltype(coarseLocalBasis)>::Traits::RangeFieldType;

    // interpolated coefficients for the fine local basis
    std::vector<CoefficientType> coefficients(fineLocalBasis.size());

    using CoarseRange = typename std::decay_t<decltype(coarseLocalBasis)>::Traits::RangeType;

    // We first compute the values at all interpolation points.
    // To this end a dummy call to interpolate() is used to deduce
    // the interpolation points and evaluate the coarse bases.
    // Later the, we can simply return the pre-computed values
    // based on the evaluation order.
    // This requires that interpolate() does the evaluation at
    // a deterministic point set in a deterministic order and
    // thus rules out e.g. adaptive quadrature.
    // If the interpolation is given by simple point evaluations,
    // the cache computed here is essentially the local interpolation
    // matrix. However, it is also fine, if interpolate() does
    // additional computations based on the values, like, e.g.
    // a Piola-transformation.
    auto coarseValues = std::vector<std::vector<CoarseRange>>(fineLocalBasis.size());
    auto k = 0;
    auto trackingCoarseFunction = [&](const auto& x) {
      coarseValues.resize(k+1);
      coarseLocalBasis.evaluateFunction(geometry_(x), coarseValues[k]);
      return coarseValues[k++][0];
    };
    fineFiniteElement.localInterpolation().interpolate(trackingCoarseFunction, coefficients);

    for (size_t j=0; j<coarseLocalBasis.size(); j++)
    {
      // baseFctj behaves like the j-th coarse bases function if
      // interpolate() evaluates it at the same points in the same
      // order as above.
      auto k = 0;
      auto baseFctj = [&](const auto& x) {
        return coarseValues[k++][j];
      };

      // Interpolate j^th base function by the fine basis
      fineFiniteElement.localInterpolation().interpolate(baseFctj, coefficients);

      // get the matrix col index
      const auto& globalCoarseIndex = coarseLocalView_.index( coarseNode.localIndex( j ) );

      // set the matrix indices
      for (size_t i=0; i<fineLocalBasis.size(); i++)
      {
        // some values may be practically zero -- no need to store those
        if ( std::abs(coefficients[i]) < tolerance_ )
          continue;

        // get the matrix row index
        const auto& globalFineIndex = fineLocalView_.index( fineNode.localIndex( i ) );

        // Either insert the index or add the value.
        if (exportIndices_)
          indices_.insertEntry(globalFineIndex, globalCoarseIndex);
        else
          matrix_(globalFineIndex,globalCoarseIndex) = coefficients[i];
      }
    }
  }

private:

  const CoarseLocalView& coarseLocalView_;
  const FineLocalView& fineLocalView_;
  const Geometry& geometry_;
  double tolerance_;
  Matrix& matrix_;
  IndexSet& indices_;
  const bool exportIndices_;
};

} // namespace Impl


/** \brief Assemble the transfer matrix for multigrid methods
 *
 *  The coarse and fine basis has to be related: in a way that the fine grid is descendant of the
 *  coarse grid (not necessarily direct descandant).
 *  The two bases are assumed to implement the dune-functions basis interface.
 *
 * \param [out] matrix the block matrix corresponding to the basis transfer operator
 * \param [in] coarseBasis global basis on the coarse grid
 * \param [in] fineBasis global basis on the fine grid
 * \param [in] tolerance (optional) interpolation threshold. Default is 1e-12.
 */
template<class MatrixType, class CoarseBasis, class FineBasis>
static void assembleGlobalBasisTransferMatrix(MatrixType& matrix,
                                              const CoarseBasis& coarseBasis,
                                              const FineBasis& fineBasis,
                                              const double tolerance = 1e-12)
{
  const auto& coarseGridView = coarseBasis.gridView();
  const auto& fineGridView   = fineBasis.gridView();

  auto coarseLocalView = coarseBasis.localView();
  auto fineLocalView   = fineBasis.localView();

  // for multi index access wrap the matrix to the ISTLBackend
  auto&& matrixBackend = Dune::Fufem::toMatrixBackend<FineBasis, CoarseBasis>(matrix);

  // prepare index set with multi index access
  auto indices = matrixBackend.patternBuilder();
  indices.resize(fineBasis,coarseBasis);

  // ///////////////////////////////////////////
  // Determine the indices present in the matrix
  // ///////////////////////////////////////////

  // This map transforms coordinates to the ancestor element
  // by chaining the GeometryInFather entries in the vector.
  using GeometryInFather = std::decay_t<decltype(elements(fineGridView).begin()->geometryInFather())>;
  std::vector<GeometryInFather> geometryInFathersVector;
  const auto geometryInFathers = [&](auto&& x) {
    auto y = x;
    for (const auto& g : geometryInFathersVector)
      y = g.global(y);
    return y;
  };

  // loop over all fine grid elements
  for ( const auto& fineElement : elements(fineGridView) )
  {
    fineLocalView.bind(fineElement);

    // find a coarse element that is the parent of the fine element
    // start in the fine grid and track the geometry mapping
    auto fE = fineElement;
    // collect all geometryInFather's on the way
    geometryInFathersVector.clear();
    while ( not coarseGridView.contains(fE) and fE.hasFather() )
    {
      // add the geometry to the container
      geometryInFathersVector.push_back(fE.geometryInFather());
      // step up one level
      fE = fE.father();
    }

    // did we really end up in coarseElement?
    if ( not coarseGridView.contains(fE) )
      DUNE_THROW( Dune::Exception, "There is a fine element without a parent in the coarse grid!");

    const auto& coarseElement = fE;

    coarseLocalView.bind(coarseElement);

    // visit all children of the coarse node and interpolate with the fine basis functions
    ::Impl::LeafNodeInterpolationVisitor visitor( coarseLocalView, fineLocalView, geometryInFathers,
                                                tolerance, matrixBackend, indices,
                                                true /* = set indexSet */ );

    Dune::TypeTree::applyToTreePair(coarseLocalView.tree(),fineLocalView.tree(),visitor);
  }

  // apply the index set to the matrix
  indices.setupMatrix();
  matrix = 0;

  //////////////////////
  // Now set the values
  //////////////////////

  for ( const auto& fineElement : elements(fineGridView) )
  {
    fineLocalView.bind(fineElement);

    // find a coarse element that is the parent of the fine element
    // start in the fine grid and track the geometry mapping
    auto fE = fineElement;
    // collect all geometryInFather's on the way
    geometryInFathersVector.clear();
    while ( not coarseGridView.contains(fE) and fE.hasFather() )
    {
      // add the geometry to the container
      geometryInFathersVector.push_back(fE.geometryInFather());
      // step up one level
      fE = fE.father();
    }

    const auto& coarseElement = fE;

    coarseLocalView.bind(coarseElement);

    // visit all children of the coarse node and interpolate with the fine basis functions
    ::Impl::LeafNodeInterpolationVisitor visitor( coarseLocalView, fineLocalView, geometryInFathers,
                                                tolerance, matrixBackend, indices,
                                                false /* = increase matrix entries */ );

    Dune::TypeTree::applyToTreePair(coarseLocalView.tree(),fineLocalView.tree(),visitor);
  }
}


#endif
