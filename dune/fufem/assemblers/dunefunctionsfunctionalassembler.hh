// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_ASSEMBERS_DUNEFUNCTIONSFUNCTIONALASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBERS_DUNEFUNCTIONSFUNCTIONALASSEMBLER_HH

#include <dune/common/std/type_traits.hh>
#include <dune/common/concept.hh>

#include <dune/istl/bvector.hh>

#include <dune/grid/common/capabilities.hh>

#include <dune/functions/backends/concepts.hh>
#include <dune/functions/backends/istlvectorbackend.hh>

#include <dune/fufem/parallel/parallelalgorithm.hh>



namespace Dune {
namespace Fufem {


//! Generic global assembler for functionals on a gridview
template <class TestBasis>
class DuneFunctionsFunctionalAssembler
{
  using GridView = typename TestBasis::GridView;

  // SFINAE expression check for preprocess() method of local assembler
  template <class LocalAssembler>
  using LocalAssemblerPreprocess = decltype(std::declval<LocalAssembler>().preprocess(std::declval<typename TestBasis::LocalView>()));

public:
  //! create assembler for grid
  DuneFunctionsFunctionalAssembler(const TestBasis& tBasis) :
      testBasis_(tBasis)
  {}

  template<class V>
  static decltype(auto) toVectorBackend(V& v)
  {
    if constexpr (Dune::models<Dune::Functions::Concept::VectorBackend<TestBasis>, decltype(v)>())
      return v;
    else
      return Dune::Functions::istlVectorBackend(v);
  }

  template <class Vector, class LocalAssembler>
  void assembleBulkEntries(Vector&& vector, LocalAssembler&& localAssembler) const
  {
    auto&& vectorBackend = toVectorBackend(vector);

    auto testLocalView     = testBasis_.localView();

    if constexpr(Dune::Std::is_detected_v<LocalAssemblerPreprocess, LocalAssembler>)
      localAssembler.preprocess(testLocalView);

    using Field = std::decay_t<decltype(vectorBackend[testLocalView.index(0)])>;
    using LocalVector = Dune::BlockVector<Dune::FieldVector<Field,1>>;

    auto localVector = LocalVector(testLocalView.maxSize());

    for (const auto& element : elements(testBasis_.gridView()))
    {
      testLocalView.bind(element);

      for (size_t i=0; i<testLocalView.tree().size(); ++i)
      {
        auto localRow = testLocalView.tree().localIndex(i);
        localVector[localRow] = 0;
      }

      localAssembler(element, localVector, testLocalView);

      // Add element stiffness matrix onto the global stiffness matrix
      for (size_t i=0; i<testLocalView.tree().size(); ++i)
      {
        auto localRow = testLocalView.tree().localIndex(i);
        auto row = testLocalView.index(localRow);
        vectorBackend[row] += localVector[localRow];
      }
    }
  }

  template <class Vector, class LocalAssembler, class ElementPartition>
  void assembleBulkEntries(Vector&& vector, LocalAssembler&& localAssembler, const ElementPartition& elementPartition, std::size_t threadCount) const
  {
    static_assert(Dune::Capabilities::viewThreadSafe<typename GridView::Grid>::v, "Trying to use thread parallel assembler but grid is not viewThreadSafe.");

    auto&& vectorBackend = toVectorBackend(vector);

    parallelAlgorithm(threadCount, [&, localAssembler](auto parallel) mutable {
      auto testLocalView = testBasis_.localView();

      if constexpr(Dune::Std::is_detected_v<LocalAssemblerPreprocess, LocalAssembler>)
        localAssembler.preprocess(testLocalView);

      using Field = std::decay_t<decltype(vectorBackend[testLocalView.index(0)])>;
      using LocalVector = Dune::BlockVector<Dune::FieldVector<Field,1>>;

      auto localVector = LocalVector(testLocalView.maxSize());

      parallel.coloredForEach(elementPartition, [&](const auto& element) {
        testLocalView.bind(element);

        for (size_t i=0; i<testLocalView.tree().size(); ++i)
        {
          auto localRow = testLocalView.tree().localIndex(i);
          localVector[localRow] = 0;
        }

        localAssembler(element, localVector, testLocalView);

        // Add element stiffness matrix onto the global stiffness matrix
        for (size_t i=0; i<testLocalView.tree().size(); ++i)
        {
          auto localRow = testLocalView.tree().localIndex(i);
          auto row = testLocalView.index(localRow);
          vectorBackend[row] += localVector[localRow];
        }
      });
    });
  }


  template <class Vector, class LocalAssembler>
  void assembleBulk(Vector&& vector, LocalAssembler&& localAssembler) const
  {
    auto&& vectorBackend = toVectorBackend(vector);

    vectorBackend.resize(testBasis_);
    vectorBackend = 0.0;
    assembleBulkEntries(vectorBackend, std::forward<LocalAssembler>(localAssembler));
  }

  template <class Vector, class LocalAssembler, class ElementPartition>
  void assembleBulk(Vector&& vector, LocalAssembler&& localAssembler, const ElementPartition& elementPartition, std::size_t threadCount) const
  {
    auto&& vectorBackend = toVectorBackend(vector);

    vectorBackend.resize(testBasis_);
    vectorBackend = 0.0;
    assembleBulkEntries(vectorBackend, std::forward<LocalAssembler>(localAssembler), elementPartition, threadCount);
  }


protected:
  const TestBasis& testBasis_;
};


/**
 * \brief Create DuneFunctionsFunctionalAssembler
 */
template <class TestBasis>
auto duneFunctionsFunctionalAssembler(const TestBasis& testBasis)
{
  return DuneFunctionsFunctionalAssembler<TestBasis>(testBasis);
}



} // namespace Fufem
} // namespace Dune


#endif // DUNE_FUFEM_ASSEMBERS_DUNEFUNCTIONSFUNCTIONALASSEMBLER_HH

