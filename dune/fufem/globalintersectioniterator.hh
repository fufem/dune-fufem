#ifndef GLOBAL_INTERSECTION_ITERATOR_HH
#define GLOBAL_INTERSECTION_ITERATOR_HH

#include <dune/common/iteratorfacades.hh>
#include <dune/common/version.hh>

#include <dune/geometry/referenceelements.hh>

/** \brief Helper struct that statically either dereferences the iterator, if it returns a reference,
 *         or otherwise return the 'member' intersection.
 */
template <class IntersectionIterator, bool returnsReference>
struct ReturnReferenceHelper {

    typedef typename IntersectionIterator::Intersection Intersection;

    static const Intersection& returnReference(IntersectionIterator* nIt,
                                        const Intersection& intersection)
    {
        return intersection;
    }
};

template <class IntersectionIterator>
struct ReturnReferenceHelper<IntersectionIterator,true> {

    typedef typename IntersectionIterator::Intersection Intersection;

    static const Intersection& returnReference(IntersectionIterator* nIt,
                                        const Intersection& intersection)
    {
        return **nIt;
    }
};

/** \brief Statically either dereferences the iterator, if it returns a reference,
 *         or otherwise return the 'member' intersection.
 */
template <class IntersectionIterator, bool returnsReference>
static const typename IntersectionIterator::Intersection& returnReference(IntersectionIterator* nIt,
                            const typename IntersectionIterator::Intersection& intersection)
{
    return ReturnReferenceHelper<IntersectionIterator,returnsReference>::returnReference(nIt,intersection);
}


/** \brief Base class for iterators on intersections of a grid view
 *
 * \tparam GridView The grid view on which this boundary patch lives
 * \tparam Impl The Implementation derived from this class (CRTP).
 *
 * The derived class Impl has to provide two methods
 *
 * bool skipElement(const Element& e)
 * bool skipIntersection(const Intersection& e)
 *
 * that specify which elements and intersections should be skipped.
 * It must call intitialIncrement() in its constructor in order
 * to find the first valid intersection.
 */
template <class GridView, class Impl>
class GlobalIntersectionIterator
    : public Dune::ForwardIteratorFacade<GlobalIntersectionIterator<GridView, Impl>, const typename GridView::Intersection>
{
    enum {dim=GridView::dimension};

    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

public:
    /** \brief The type of objects we are iterating over
     */
    typedef typename IntersectionIterator::Intersection Intersection;

private:
    // If this value is true, then the implementation still returns the Intersection by const reference
    // thus in this case we don't need to store the intersections at all
    static const bool isReference = std::is_lvalue_reference<decltype(std::declval<IntersectionIterator>().operator*())>::value;

public:

    enum PositionFlag {begin, end};


    /** \brief Create begin or end iterator for given grid view
     *
     * The iterator does not necessarily point to a valid intersection
     * after creation! In order to guarantee this you must
     * call initialIncrement() from the constructor in the
     * derived class. This can not be done here since it
     * requires the skipXYZ() methods of the derived class
     * that is not yet initialized.
     *
     * \param gridView Iterate over the intersections of this grid view
     * \param flag Create begin or end iterator for PositionFlag = begin or end, respectively.
     */
    GlobalIntersectionIterator(const GridView& gridView, PositionFlag flag) :
        gridView_(gridView),
        eIt_(gridView.template end<0>()),
        endEIt_(gridView.template end<0>()),
        nIt_(0)
    {
        if (flag==begin)
        {
            eIt_ = gridView.template begin<0>();
            nIt_ = new IntersectionIterator(gridView.ibegin(*eIt_));
        }
    }

    /** \brief Create iterator for given grid view
     *
     * The iterator does not necessarily point to a valid intersection
     * after creation! In order to guarantee this you must
     * call initialIncrement() from the constructor in the
     * derived class. This can not be done here since it
     * requires the skipXYZ() methods of the derived class
     * that is not yet initialized.
     *
     * \param gridView Iterate over the intersections of this grid view
     * \param eIt Element iterator to the first element to consider
     * \param endEIt Element iterator after the last element to consider
     */
    GlobalIntersectionIterator(const GridView& gridView, const ElementIterator& eIt, const ElementIterator& endEIt) :
        gridView_(gridView),
        eIt_(eIt),
        endEIt_(endEIt),
        nIt_(0)
    {
        if (eIt_!=endEIt_)
            nIt_ = new IntersectionIterator(gridView.ibegin(*eIt_));
    }

    /** \brief Copy constructor
     *
     * \param other Copy from this iterator
     */
    GlobalIntersectionIterator(const GlobalIntersectionIterator& other) :
        gridView_(other.gridView_),
        eIt_(other.eIt_),
        endEIt_(other.endEIt_),
        nIt_(0)
    {
        if (other.nIt_)
            nIt_ = new IntersectionIterator(*other.nIt_);

        updateIntersection(other.intersection_);
    }

    ~GlobalIntersectionIterator() {
        delete(nIt_);
    }

    /** \brief Increment the pointer to the next intersection
     */
    void increment()
    {
        // Increment until a face in the patch is found
        do {
            ++(*nIt_);

            // if end intersection reached find next intersection
            if ((*nIt_) == gridView_.iend(*eIt_))
            {
                do {
                    ++eIt_;
                } while (eIt_ != endEIt_ and asImpl().skipElement(*eIt_));
                if (eIt_ != endEIt_)
                    (*nIt_) = gridView_.ibegin(*eIt_);
            }

            // if we reached the end then stop
            if (eIt_ == endEIt_)
                break;

            // check if we skip the intersection
            const Intersection& intersection = **nIt_;
            if (not asImpl().skipIntersection(intersection)) {
                updateIntersection(intersection);
                break;
            }

        } while (true);
    }

    bool equals(const GlobalIntersectionIterator& other) const
    {
        return (eIt_ == other.eIt_) and ((eIt_ == endEIt_) or ((*nIt_) == (*other.nIt_)));
    }

    const Intersection& dereference() const
    {
        return returnReference<IntersectionIterator,isReference>(nIt_,intersection_);
    }

    const GlobalIntersectionIterator& operator=(const GlobalIntersectionIterator& other)
    {
        eIt_ = other.eIt_;
        endEIt_ = other.endEIt_;

        if (nIt_)
            delete nIt_;

        if (other.nIt_)
            nIt_ = new IntersectionIterator(*other.nIt_);
        else
            nIt_ = 0;

        updateIntersection(other.intersection_);

        return *this;
    }

    bool containsInsideSubentity(int subEntity, int codim) const
    {
        auto re = Dune::referenceElement<double, dim>(eIt_->type());
        return re.subEntities((*nIt_)->indexInInside(), 1,codim).contains(subEntity);
    }

protected:

    //! Update the stored intersection if we have to
    void updateIntersection(const Intersection& intersection)
    {
        if (not isReference)
            intersection_ = std::move(intersection);
    }

    /** \brief Find first valid intersection
     *
     * Checks if current intersection is valid and iterates
     * to the first valid intersection if not.
     * You must call this method in the constructor of the
     * derived class!
     */
    void initialIncrement()
    {
        if (eIt_ == endEIt_)
            return;

        const Intersection& intersection = **nIt_;
        if (asImpl().skipIntersection(intersection))
            increment();
        else
            updateIntersection(intersection);
    }

    const Impl& asImpl() const
    {
        return static_cast<const Impl&>(*this);
    }

    Impl& asImpl()
    {
        return static_cast<Impl&>(*this);
    }

    GridView gridView_;

    ElementIterator eIt_;
    ElementIterator endEIt_;

    IntersectionIterator* nIt_;

    /** \brief The intersection corresponding to the actual global intersection iterator.
     *          It is only set if dereferencing the intersection iterator is returning a temporary.
     */
    Intersection intersection_;
};



#endif

