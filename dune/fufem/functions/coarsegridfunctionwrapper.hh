// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef DUNE_FUFEM_FUNCTIONS_COARSE_GRID_FUNCTION_WRAPPER_HH
#define DUNE_FUFEM_FUNCTIONS_COARSE_GRID_FUNCTION_WRAPPER_HH


#include <dune/fufem/functions/virtualgridfunction.hh>



/**
 * \brief A wrapper to allow evaluation of grid function on finer grid
 *
 * If the wrapped grid function can be evaluated on an element the wrapper
 * grid function can be evaluated on this element and all descendants.
 *
 * \tparam GF Type of wrapped coarse grid function
 */
template<class GF>
class CoarseGridFunctionWrapper :
    public VirtualGridFunction<typename GF::GridView::Grid, typename GF::Range>
{
        typedef VirtualGridFunction<typename GF::GridView::Grid, typename GF::Range> BaseType;

    public:
        typedef typename BaseType::LocalDomainType LocalDomainType;
        using RangeType = typename GF::Range;
        using DerivativeType = decltype(derivative(std::declval<GF>())(std::declval<typename GF::Domain>()));
        typedef typename GF::GridView::template Codim<0>::Entity Element;

        typedef GF CoarseGridFunction;


        /**
         * \brief Setup grid function
         *
         * \param f The coarse grid function
         */
        CoarseGridFunctionWrapper(const CoarseGridFunction& f) :
            BaseType(f.basis().gridView().grid()),
            f_(&f)
        {}

        /**
         * \brief Evaluate the function at a point in global coordinates
         *
         * \warning This involves a hierarchic search and can take a lot of time!
         */
        RangeType operator()(const typename GF::Domain x) const
        {
            RangeType y;
            this->evaluate(x,y);
            return y;
        }

        /**
         * \copydoc VirtualGridFunction::evaluateLocal
         */
        virtual void evaluateLocal(const Element& e, const LocalDomainType& x, RangeType& y) const
        {
            if (f_->basis().gridView().contains(e))
            {
                auto localF = localFunction(*f_);
                localF.bind(e);
                y = localF(x);
            }
            else
            {
                Element ancestor(e);
                LocalDomainType xCoarse;

                [[maybe_unused]] auto r = findAncestorInCoarseGridView(ancestor, x, xCoarse);
                assert(r);

                auto localF = localFunction(*f_);
                localF.bind(ancestor);
                y = localF(xCoarse);
            }
        }



        /**
         * \copydoc VirtualGridFunction::evaluateDerivativeLocal
         */
        virtual void evaluateDerivativeLocal(const Element& e, const LocalDomainType& x, DerivativeType& d) const
        {
            if (f_->basis().gridView().contains(e))
            {
                auto localDer = derivative(localFunction(*f_));
                localDer.bind(e);
                d = localDer(x);
            }
            else
            {
                Element ancestor(e);
                LocalDomainType xCoarse;

                [[maybe_unused]] auto r = findAncestorInCoarseGridView(ancestor, x, xCoarse);
                assert(r);

                auto localDer = derivative(localFunction(*f_));
                localDer.bind(ancestor);
                d = localDer(xCoarse);
            }
        }


        /**
         * \copydoc VirtualGridFunction::isDefinedOn
         */
        virtual bool isDefinedOn(const Element& e) const
        {
            if (e.isLeaf() or f_->basis().gridView().contains(e))
                return true;
            Element ep(e);
            return findAncestorInCoarseGridView(ep);
        }

    protected:

        bool findAncestorInCoarseGridView(Element& e) const
        {
            while (not(f_->basis().gridView().contains(e)))
            {
                if (not(e.hasFather()))
                    return false;
                e = e.father();
            }
            return true;
        }

        // Find the ancestor of e in the coarse grid view and transform
        // local coordinates to local coordinates w.r.t. the ancestor
        bool findAncestorInCoarseGridView(Element& e, LocalDomainType xFine, LocalDomainType& xCoarse) const
        {
            while (not(f_->basis().gridView().contains(e)))
            {
                if (not(e.hasFather()))
                    return false;
                xCoarse = e.geometryInFather().global(xFine);
                xFine = xCoarse;
                e = e.father();
            }
            return true;
        }

        const CoarseGridFunction* f_;
};


#endif

