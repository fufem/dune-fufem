// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FUNCTIONS_CACHEDFUNCTION_HH
#define DUNE_FUFEM_FUNCTIONS_CACHEDFUNCTION_HH

/**
   @file
   @brief Cache evaluations of a function
 */

#include <type_traits>
#include <utility>
#include <functional>
#include <map>

#include <dune/common/fvector.hh>
#include <dune/common/typelist.hh>



namespace Dune::Fufem {



struct Less : public std::less<>
{
  template<class T, int n>
  bool operator() (const Dune::FieldVector<T,n>& a, const Dune::FieldVector<T,n>& b) const
  {
    for (std::size_t i=0; i<a.size(); ++i)
    {
      if (a[i]<b[i])
        return true;
      if (a[i]>b[i])
        return false;
    }
    return false;
  }
};



/** \brief Cached evaluation of a function
 */
template <class F, class D, class Comp = Less>
class CachedFunction
{
  using Domain = D;
  using Range = std::decay_t<std::invoke_result_t<F,D>>;
  using EvaluationCache = typename std::map<Domain, Range, Comp>;

public:

  template<class FT>
  CachedFunction(FT&& f) :
    f_(std::forward<FT>(f))
  {}

  template<class FT>
  CachedFunction(FT&& f, Dune::MetaType<D>) :
    f_(std::forward<FT>(f))
  {}

  /** \brief Evaluate function
   */
  template<class X>
  Range operator()(const X& x) const
  {
    auto it = evalCache_.find(x);
    if (it != evalCache_.end())
      return it->second;
    else
      return (evalCache_[x] = f_(x));
  }

protected:
    F f_;
    mutable EvaluationCache evalCache_;
};

template<class FT, class D>
CachedFunction(FT&& f, Dune::MetaType<D> md) -> CachedFunction<std::decay_t<FT>,D>;



} // namespace Dune::Fufem



#endif // DUNE_FUFEM_FUNCTIONS_CACHEDFUNCTION_HH

