// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_LOGGER_HH
#define DUNE_FUFEM_LOGGER_HH

#include <chrono>
#include <mutex>
#include <string>

#include <dune/common/stringutility.hh>

namespace Dune::Fufem {

/**
 * \brief Create a simple logger callback
 *
 * \tparam Duration An instance of std::chrono::duration used for printing the
 *                  time stamps, defaults to std::chrono::duration<double>
 * \param stream The ostream this logger writes to
 * \param format The format string used for formatting the time stamp data
 *
 * This will create a logger object that can be
 * used to print strings to the given `stream`.
 * Each string is prepended with a time stamp
 * of the total time elapsed since the logger
 * was created and the time that evolved since
 * printing the last log message. Both are formatted
 * using the (printf-style) `format` string used
 * when creating the logger.
 *
 * The `Duration` parameter can be used for selecting the representation
 * of time stamps. The default is to represent seconds as `double`.
 * By passing `std::chrono::milliseconds` this can e.g. be changed
 * to represent milliseconds as integers. Notice that the format
 * string should match the representation type.
 */
template<class Duration=std::chrono::duration<double>>
auto makeLogger(std::ostream& stream, std::string format)
{
  auto startTimeStamp = std::chrono::high_resolution_clock::now();
  auto lastTimeStamp = std::chrono::high_resolution_clock::now();
  return [&, format, startTimeStamp, lastTimeStamp] (auto&& s) mutable {
    using namespace std::chrono;
    auto newTimeStamp = high_resolution_clock::now();
    stream
      << Dune::formatString( format,
        duration_cast<Duration>(newTimeStamp - startTimeStamp),
        duration_cast<Duration>(newTimeStamp - lastTimeStamp))
      << s << std::endl;
    lastTimeStamp = newTimeStamp;
  };
}



/**
 * \brief Create a simple logger callback
 *
 * \tparam Duration An instance of std::chrono::duration used for printing the
 *                  time stamps, defaults to std::chrono::duration<double>
 * \param stream The ostream this logger writes to
 * \param format The format string used for formatting the time stamp data
 * \param mutex A global mutex used for synchronization.
 *
 * This allows to create a logger similar to `makeLogger`
 * but synchronized using a `std::mutex` for writing from
 * concurrent streams. The `mutex` protects any access to the
 * `stream`, such that concurrent streams can either use the same logger
 * or different loggers using the same mutex.
 */
template<class Duration=std::chrono::duration<double>>
auto makeSynchronizedLogger(std::ostream& stream, std::string format, std::mutex& mutex)
{
  auto rawLogger=makeLogger<Duration>(stream, format);
  return [rawLogger,&mutex](auto&& s) mutable {
    auto lock = std::unique_lock(mutex);
    rawLogger(s);
  };
}

} // namespace Dune::Fufem

#endif // DUNE_FUFEM_LOGGER_HH

