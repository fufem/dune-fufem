// Create square cube with periodic boundary (left and right).
// Generate msh file with $ gmsh square.geo -1 -2 -o square.msh
// 0 for triangles, 1 for quads
tri_or_quad = 0;

Point(1) = {0, 0, 0};
Point(2) = {1, 0, 0};
Point(3) = {1, 1, 0};
Point(4) = {0, 1, 0};


Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Curve Loop(1) = {1, 2, 3, 4};
Plane Surface(1) = {1};
Transfinite Surface {1};

If (tri_or_quad == 1)
   Recombine Surface {1};
EndIf

Physical Surface(1) = {1};
Physical Curve(1) = {1};
Physical Curve(2) = {2};
Physical Curve(3) = {3};
Physical Curve(4) = {4};

Mesh.MshFileVersion = 2.2;
Mesh 2;
