#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/common/fvector.hh>

#include <dune/fufem/functiontools/boundarydofs.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/test/common.hh>

#include <dune/functions/functionspacebases/defaultglobalbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>

#include <dune/istl/bvector.hh>

using namespace Dune;

template<int order, class GW>
bool checkMarkBoundaryPatchDofs(const GW& gw)
{
    // create active vertices
    const auto dim = GW::dimension;
    const auto size = gw.size(dim);
    BitSetVector<dim> vertices(size);
    // set some values
    vertices[0].set();
    vertices[size / 2].set();
    vertices[size-1].set();

    // create BoundaryPatch
    BoundaryPatch bp(gw,vertices);

    // create scalar and powerBasis
    using namespace Functions::BasisFactory;
    auto lagrangeBasis = makeBasis(gw, lagrange<order>());
    auto powerBasis = makeBasis(gw, power<3>(lagrange<order>()));

    // create common bitsetvector
    BitSetVector<1> lagrangeDOFs;
    BitSetVector<3> powerDOFs;

    // ... and functions-compatible bitVectors
    Dune::BlockVector<Dune::FieldVector<char,1>> lagrangeBits;
    Dune::BlockVector<Dune::FieldVector<char,3>> powerBits;
    auto lagrangeBackend = Dune::Functions::istlVectorBackend(lagrangeBits);
    auto powerBackend = Dune::Functions::istlVectorBackend(powerBits);


    // check whether this compiles
    Dune::Fufem::markBoundaryPatchDofs(bp,lagrangeBasis,lagrangeDOFs);
    Dune::Fufem::markBoundaryPatchDofs(bp,powerBasis,powerDOFs);
    Dune::Fufem::markBoundaryPatchDofs(bp,lagrangeBasis,lagrangeBackend);
    Dune::Fufem::markBoundaryPatchDofs(bp,powerBasis,powerBackend);

    return true;
}


struct Suite
{
    template<class GridType>
    bool check(const GridType& grid)
    {
        auto maxLevel = grid.maxLevel();
        auto gridView = grid.levelGridView(maxLevel);

        bool passed = checkMarkBoundaryPatchDofs<0>(gridView);
        passed = passed and checkMarkBoundaryPatchDofs<1>(gridView);
        passed = passed and checkMarkBoundaryPatchDofs<2>(gridView);
        return passed;
    }
};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    Suite tests;

    bool passed = checkWithStructuredGrid<2>(tests, 3);
    passed = passed and checkWithStructuredGrid<3>(tests, 3);

    return passed ? 0 : 1;
}
