#include <config.h>

#include <dune/common/test/testsuite.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/indices.hh>
#include <dune/common/rangeutilities.hh>
#include <dune/common/classname.hh>
#include <dune/common/version.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/multitypeblockmatrix.hh>
#include <dune/istl/multitypeblockvector.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>

#include <dune/fufem/backends/istlmatrixbackend.hh>
#include <dune/fufem/test/common.hh>

// Due to a formerly unnoticed bug, activate bounds checking:
#define DUNE_CHECK_BOUNDS 1



// Build matrix that contains entries for all pairs (i,j)
// of basis functions that share an element.
template<class Matrix, class Basis>
Matrix buildMatrixForBasis(const Basis& basis) {
  Matrix matrix;

  auto backend = Dune::Fufem::istlMatrixBackend(matrix);
  auto pattern = backend.patternBuilder();

  pattern.resize(basis,basis);
  auto localView = basis.localView();
  for(const auto& e : elements(basis.gridView()))
  {
    localView.bind(e);
    for(auto i : Dune::range(localView.size()))
      for(auto j : Dune::range(localView.size()))
        pattern.insertEntry(localView.index(i), localView.index(j));
  }
  pattern.setupMatrix();
  return matrix;
}



// Test istlMatrixBackend() for existing matrix.
template<class Entry, class Matrix, class Basis>
Dune::TestSuite testISTLMatrixBackendForBasis(Matrix& matrix, const Basis& basis) {
  Dune::TestSuite suite(Dune::className(matrix));

// FieldTraits<MultiTypeBlockMatrix> are not implemented properly
// in dune-2.9. Hence determining the field type automatically
// in istlMatrixBackend fails and we have to provide the type
// manually.
#if DUNE_VERSION_GT(DUNE_COMMON, 2, 9)
  auto backend = Dune::Fufem::istlMatrixBackend(matrix);

  {
    auto&& backend2 = Dune::Fufem::toConstMatrixBackend<Basis, Basis>(matrix);
    suite.check(std::is_same_v<decltype(backend), std::decay_t<decltype(backend2)>>)
      << "istlMatrixBackend(matrix) and toConstMatrixBackend(matrix) yield different types";

    auto&& backend3 = Dune::Fufem::toConstMatrixBackend<Basis, Basis>(backend);
    suite.check(std::is_same_v<decltype(backend), std::decay_t<decltype(backend3)>>)
      << "istlMatrixBackend(matrix) and toConstMatrixBackend(backend) yield different types";
    suite.check((void*)(&backend) == (void*)(&backend3))
      << "toConstMatrixBackend(backend) does not forward existing backend";
  }
#else
  auto backend = Dune::Fufem::istlMatrixBackend<Entry>(matrix);
#endif

  backend.assign(1.0);

  auto localView = basis.localView();
  for(const auto& e : elements(basis.gridView()))
  {
    localView.bind(e);
    for(auto i : Dune::range(localView.size()))
    {
      auto rowIndex = localView.index(i);
      for(auto j : Dune::range(localView.size()))
      {
        auto colIndex = localView.index(j);
        suite.check(backend(rowIndex, colIndex) == 1.0, "Multi-index access")
          << "Unexpected entry (" << rowIndex << "," << colIndex << ")";
      }
    }
  }

  return suite;
}



// Build matrix for a basis and test istlMatrixBackend() for it.
template<class Entry, class Matrix, class Basis>
Dune::TestSuite testISTLMatrixBackendForBasis(const Basis& basis) {
  Matrix matrix = buildMatrixForBasis<Matrix>(basis);

  auto suite = testISTLMatrixBackendForBasis<Entry>(matrix, basis);

  auto backend = Dune::Fufem::istlMatrixBackend(matrix);
  auto&& backend2 = Dune::Fufem::toMatrixBackend<Basis, Basis>(matrix);
  auto&& backend3 = Dune::Fufem::toMatrixBackend<Basis, Basis>(backend);

  suite.check(std::is_same_v<decltype(backend), std::decay_t<decltype(backend2)>>)
    << "istlMatrixBackend(matrix) and toMatrixBackend(matrix) yield different types";
  suite.check(std::is_same_v<decltype(backend), std::decay_t<decltype(backend3)>>)
    << "istlMatrixBackend(matrix) and toMatrixBackend(backend) yield different types";
  suite.check((void*)(&backend) == (void*)(&backend3))
    << "toMatrixBackend(backend) does not forward existing backend";

  return suite;
}



template<class... V>
using MTBV = Dune::MultiTypeBlockVector<V...>;

template<class... V>
using MTBM = Dune::MultiTypeBlockMatrix<V...>;

template<class K, int n, int m>
using FM = Dune::FieldMatrix<K,n,m>;

template<class M>
using BCRSM = Dune::BCRSMatrix<M>;

template<class M>
using SRM = Dune::Fufem::SingleRowMatrix<M>;

template<class M>
using SCM = Dune::Fufem::SingleColumnMatrix<M>;



int main(int argc, char** argv) {
  Dune::MPIHelper::instance(argc, argv);

  Dune::TestSuite suite;

  using K = double;
  using namespace Dune::Functions::BasisFactory;

  // Construct grid for checking backend against bases
  auto grid = constructCoarseYaspGrid<3>();
  auto gridView = grid->leafGridView();

  //****************************************************************************
  // Test with dense matrix and scalar entries
  //****************************************************************************
  {
    using Matrix = Dune::Matrix<K>;
    const auto basis = makeBasis(
                          gridView,
                          lagrange<1>()
                        );
    // MatrixBuilder is not specialized for this type yet.
    Matrix matrix(basis.size(), basis.size());
    suite.subTest(testISTLMatrixBackendForBasis<K, Matrix>(matrix, basis));
  }

  //****************************************************************************
  // Test with dense matrix and scalar entries (as FM<K,1,1>
  //****************************************************************************
  {
    using Matrix = Dune::Matrix<FM<K,1,1>>;
    const auto basis = makeBasis(
                          gridView,
                          lagrange<1>()
                        );
    Matrix matrix(basis.size(), basis.size());
    suite.subTest(testISTLMatrixBackendForBasis<K, Matrix>(matrix, basis));
  }

  //****************************************************************************
  // Test with sparse matrix and scalar entries
  //****************************************************************************
  {
    using Matrix = BCRSM<K>;
    const auto basis = makeBasis(
                          gridView,
                          lagrange<1>()
                        );
    suite.subTest(testISTLMatrixBackendForBasis<K, Matrix>(basis));
  }

  //****************************************************************************
  // Test with sparse matrix and scalar entries (as FM<K,1,1>)
  //****************************************************************************
  {
    using Matrix = BCRSM<FM<K,1,1>>;
    const auto basis = makeBasis(
                          gridView,
                          lagrange<1>()
                        );
    suite.subTest(testISTLMatrixBackendForBasis<K, Matrix>(basis));
  }

  //****************************************************************************
  // Test with dense matrix and block entries
  //****************************************************************************
  {
    using Matrix = Dune::Matrix<FM<K,2,2>>;
    const auto basis = makeBasis(
                          gridView,
                          power<2>(
                            lagrange<1>()
                          )
                        );
    // MatrixBuilder is not specialized for this type yet.
    Matrix matrix(basis.size(), basis.size());
    suite.subTest(testISTLMatrixBackendForBasis<K, Matrix>(matrix, basis));
  }

  //****************************************************************************
  // Test with dense matrix and dynamic block entries
  //****************************************************************************
  {
    using Matrix = Dune::Matrix<Dune::Matrix<K>>;
    const auto basis = makeBasis(
                          gridView,
                          power<2>(
                            lagrange<1>()
                          )
                        );
    // MatrixBuilder is not specialized for this type yet.
    Matrix matrix(basis.size(), basis.size());
    for (auto i: Dune::range(basis.size()))
      for (auto j: Dune::range(basis.size()))
        matrix[i][j].setSize(basis.size({i}), basis.size({j}));
    suite.subTest(testISTLMatrixBackendForBasis<K, Matrix>(matrix, basis));
  }
  //****************************************************************************
  // Test with sparse matrix and block entries
  //****************************************************************************
  {
    using Matrix = BCRSM<FM<K,2,2>>;
    const auto basis = makeBasis(
                          gridView,
                          power<2>(
                            lagrange<1>()
                          )
                        );
    suite.subTest(testISTLMatrixBackendForBasis<K, Matrix>(basis));
  }

  //****************************************************************************
  // Test with MTBM and uniform indices
  //****************************************************************************
  {
    using Matrix =
      MTBM<
        MTBV<  BCRSM<FM<K,3,3>>, BCRSM<FM<K,3,5>>  >,
        MTBV<  BCRSM<FM<K,5,3>>, BCRSM<FM<K,5,5>>  >
      >;
    const auto basis = makeBasis(
                          gridView,
                          composite(
                            power<3>(
                              lagrange<1>()
                            ),
                            power<5>(
                              lagrange<0>()
                            )
                          )
                        );
    suite.subTest(testISTLMatrixBackendForBasis<K, Matrix>(basis));
  }

  //****************************************************************************
  // Test with MTBM and non-uniform indices
  //****************************************************************************
  {
    using Matrix =
      MTBM<
        MTBV<  BCRSM<FM<K,3,3>>,      BCRSM<SCM<FM<K,3,1>>>  >,
        MTBV<  BCRSM<SRM<FM<K,3,1>>>, BCRSM<FM<K,1,1>>       >
      >;
    const auto basis = makeBasis(
                          gridView,
                          composite(
                            power<3>(
                              lagrange<2>()
                            ),
                            lagrange<1>()
                          )
                        );
    suite.subTest(testISTLMatrixBackendForBasis<K, Matrix>(basis));
  }

  //****************************************************************************
  // Test with nested MTBM and non-uniform indices (scalar entries as FM<K,1,1>)
  //****************************************************************************
  {
    using Matrix =
      MTBM<
        MTBV<
          MTBM<
            MTBV<  Dune::Matrix<FM<K,3,3>>,      Dune::Matrix<SCM<FM<K,3,1>>>  >,
            MTBV<  Dune::Matrix<SRM<FM<K,3,1>>>, Dune::Matrix<FM<K,1,1>>       >
          >
        >
      >;
    const auto basis = makeBasis(
                          gridView,
                          composite(
                            composite(
                              power<3>(
                                lagrange<2>()
                              ),
                              lagrange<1>()
                            )
                          )
                        );
    // MatrixBuilder is not specialized for this type yet.
    using namespace Dune::Indices;
    Matrix matrix;
    matrix[_0][_0][_0][_0].setSize(basis.size({0,0}), basis.size({0,0}));
    matrix[_0][_0][_0][_1].setSize(basis.size({0,0}), basis.size({0,1}));
    matrix[_0][_0][_1][_0].setSize(basis.size({0,1}), basis.size({0,0}));
    matrix[_0][_0][_1][_1].setSize(basis.size({0,1}), basis.size({0,1}));
    suite.subTest(testISTLMatrixBackendForBasis<K, Matrix>(matrix, basis));
  }

  return suite.exit();
}
