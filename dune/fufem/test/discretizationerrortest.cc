// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#include <config.h>

#include <vector>

#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/common/version.hh>

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/uggrid.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/fufem/discretizationerror.hh>

using namespace Dune;


/** \brief The derivative of the test function
 */
template<class K, std::size_t d>
class TestDerivative
{
public:
    FieldVector<K,d> operator()(const FieldVector<K,d>& x) const
    {
        return x;
    }
};


/**
 * \brief A polynomial test function
 *
 * It is a second-order polynomial.  Therefore, its approximation by
 * second-order Lagrange finite elements is exact.
 *
 * \tparam K Scalar type. The test function will map K^d to K
 * \tparam d Number of independent variables
 */
template<class K, std::size_t d>
class TestPolynomial
{
public:
    //! Evaluate polynomial
    K operator() (const FieldVector<K,d>& x) const
    {
        auto y = K(0);
        for (size_t i=0; i<d; ++i)
            y += x[i]*x[i];
        return 0.5 * y;
    }

    /** \brief Obtain derivative of the test function
     */
    friend TestDerivative<K,d> derivative(const TestPolynomial& p)
    {
      return TestDerivative<K,d>();
    }
};


int main(int argc, char** argv)
{
    MPIHelper::instance(argc, argv);

    // Construct a 2d test grid
    const int dim = 2;

    const std::string path = std::string(DUNE_GRID_EXAMPLE_GRIDS_PATH) + "gmsh/";
    auto grid = GmshReader<UGGrid<dim>>::read(path + "curved2d.msh");
    auto gridView = grid->leafGridView();

    // Make a differentiable function with a closed-form expression
    TestPolynomial<double,dim> testFunction;

    // Make a finite element function
    using namespace Functions::BasisFactory;
    auto feBasis = makeBasis(gridView, lagrange<2>());

    std::vector<double> feCoefficients(feBasis.size());
    Functions::interpolate(feBasis, feCoefficients, testFunction);
    auto feFunction = Functions::makeDiscreteGlobalBasisFunction<double>(feBasis, feCoefficients);

    // Make a second finite element function that differs to the first one by a constant
    std::vector<double> shiftedFECoefficients = feCoefficients;
    for (double& coefficient : shiftedFECoefficients)
       coefficient += 1.0;
    auto shiftedFEFunction = Functions::makeDiscreteGlobalBasisFunction<double>(feBasis, shiftedFECoefficients);

    // Compute the total grid volume, which should equal the squared error in some tests
    double volume = 0.0;
    for (auto&& element : elements(gridView))
      volume += element.geometry().volume();

    // Here come the actual tests
    TestSuite testSuite;

    QuadratureRuleKey quadKey(dim,4);

    // The two functions differ by the constant function '1',
    // hence the squared L2 difference should equal the volume of the domain.
    auto l2Difference1 = Fufem::DiscretizationError::computeL2DifferenceSquared(shiftedFEFunction,
                                                                                testFunction,
                                                                                quadKey);
    testSuite.check(std::abs(l2Difference1 - volume) < 1e-8, "Testing the L2 error with a global function");

    auto l2Difference2 = Fufem::DiscretizationError::computeL2DifferenceSquared(shiftedFEFunction,
                                                                                feFunction,
                                                                                quadKey);
    testSuite.check(std::abs(l2Difference2 - volume) < 1e-8, "Testing the L2 error with a local function");

// Disabling the test for dune-functions 2.8 and older, because the derivative
// of a DiscreteGlobalBasisFunction only appeared in 2.9.
    // Test the difference of the derivatives
    auto h1Difference1 = Fufem::DiscretizationError::computeL2DifferenceSquared(derivative(shiftedFEFunction),
                                                                                derivative(testFunction),
                                                                                quadKey);
    testSuite.check(std::abs(h1Difference1) < 1e-12, "Testing the H1 half norm with a global function");

    auto h1Difference2 = Fufem::DiscretizationError::computeL2DifferenceSquared(derivative(shiftedFEFunction),
                                                                                derivative(feFunction),
                                                                                quadKey);
    testSuite.check(std::abs(h1Difference2) < 1e-12, "Testing the H1 half norm with a local function");

    return testSuite.exit();
}
