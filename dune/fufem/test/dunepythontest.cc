// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#include <config.h>

#ifdef HAVE_PYTHON
#include <Python.h>
#endif

#include <map>
#include <memory>
#include <vector>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/tuplevector.hh>
#include <dune/common/indices.hh>

#if HAVE_DUNE_UGGRID
#include <dune/grid/uggrid.hh>
#include <dune/grid/uggrid/uggridfactory.hh>
#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#endif

#include <dune/functions/common/differentiablefunctionfromcallables.hh>

#include <dune/fufem/dunepython.hh>


using namespace Dune::Indices;

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    // Use must start the python interpreter first
    Python::start();

    // You will normally want to execute code in the context of the main
    // module denoted __main__ inside of python.  For using other modules
    // see below.
    auto pyMain = Python::main();


    // In order to run python scripts from in the working
    // directory you have to add it to the search path.
    pyMain.runStream()
        << std::endl << "import sys"
        << std::endl << "sys.path.append('.')"
        << std::endl;

    {
        pyMain.runStream()
            << std::endl << "def f():"
            << std::endl << "    return 'f'"
            << std::endl << ""
            << std::endl << "def g(ff):"
            << std::endl << "    return ff()"
            << std::endl;
    }


    std::cout << std::endl;
    std::cout << "Example  1: Execute a single line of python code *******************************" << std::endl;
    {
        pyMain.run("print('Hello world!')");
    }



    std::cout << std::endl;
    std::cout << "Example  2: Executed multiple lines of python code using a stream **************" << std::endl;
    {
        // Create the class A in module/namespace __main__ referenced
        // by pyMain.
        pyMain.runStream()
            << std::endl << "class A:"
            << std::endl << "    def __init__(self, s):"
            << std::endl << "        self.s=s"
            << std::endl << "    def f(self):"
            << std::endl << "        print(self.s)"
            << std::endl << ""
            << std::endl << "a=A('Hello world!')"
            << std::endl << "a.f()";

        // You can retrieve objects in a module using get(for more see below).
        std::cout << pyMain.get("A").str() << std::endl;
    }



    std::cout << std::endl;
    std::cout << "Example  3: python references **************************************************" << std::endl;
    {
        // All python objects are represented by Python::Reference objects.
        // The objects encapsulate the manual reference counting needed by the python-C api.
        // You can always safely create, copy, destroy them.
        //
        // In principle they act like shared_ptr's in C++. A python object
        // will be destroyed if there is no reference left to it. Here
        // 'no reference' means no Python::Reference on the C++ side,
        // no reference by any other python object, and no reference
        // in the python interpreter.
        //
        // References of the last type are for example held by names of
        // objects. If you create an instance c of class C by c=C()
        // inside of some module (see below), then the module contains
        // the member 'c' storing a reference to the actual object.

        // Python objects are, e.g., obtained by evaluating expressions.
        Python::Reference e = pyMain.evaluate("1+2");

        // All python objects provide a string representation which
        // is accessible by the str() method.
        std::cout << "Result of the expression 1+2: " << e.str() << std::endl;
    }



    std::cout << std::endl;
    std::cout << "Example  4: Using python modules ***********************************************" << std::endl;
    {
        // Python modules are defined in python files named [module].py.
        // You can import them by name to make them visible inside
        // of another module.
        //
        // The first time you call the import() method the module
        // is actually imported and the contained initialization
        // code is executed. Later calls will only return
        // a Reference to the already imported module.

        // Import the module dunepythontest defined by the file
        // dunepythontest.py which must be contained in the search path.
        Python::Module module = pyMain.import("dunepythontest");

        // The Module class Reference and provides some special methods
        // for executing code in their context and importing other module
        // into them. Members of modules can be accessed using the get() method.

        // Get objects named 'add', 'foo', 'bar' from imported module.
        Python::Reference add = module.get("add");
        Python::Reference foo = module.get("foo");
        Python::Reference bar = module.get("bar");

        // Print string representations of the objects
        std::cout << add.str() << std::endl;
        std::cout << foo.str() << std::endl;
        std::cout << bar.str() << std::endl;

        // You can also import all members of a python module into __main__.
        pyMain.run("from dunepythontest import *");
        std::cout << pyMain.get("foo").str() << std::endl;
    }



    std::cout << std::endl;
    std::cout << "Example  5: Types derived from Reference ***************************************" << std::endl;
    {
        // There are various classes derived from Reference, e.g., for
        // modules, callables, ... providing corresponding special methods.
        // Depending on the a-priori knowledge methods will either
        // return a Reference (e.g. get()) or a derived type (e.g. import()).
        // However you can always try to convert a Reference
        // to such a derived type. This will automatically check
        // if the underlying python object is suitable and throw
        // an exception otherwise. In most cases it will be convenient
        // to use auto.

        // module1 is already a Module since import() has a-priori knowledge.
        auto module1 = pyMain.import("dunepythontest");

        // module2 is only a Reference since get() can return anything
        auto module2 = pyMain.get("dunepythontest");

        // You can obtain a Module by converting a Reference
        auto module3 = Python::Module(pyMain.get("dunepythontest"));

        // Converting a nonmatching object will fail.
        pyMain.run("someInt = 3");
        auto someInt = pyMain.get("someInt");
        std::cout << someInt << std::endl;
        try {
            Python::Module someIntAsModule(someInt);
        }
        catch (const Dune::Exception& e)
        {
            std::cout << "Trying to convert an integert to a Module object failed expectedly:" << std::endl;
            std::cout << e << std::endl;
        }
    }



    std::cout << std::endl;
    std::cout << "Example  6: Creating modules ***************************************************" << std::endl;
    {
        // You can also create modules manually without loading them from
        // a file. In order to execute code in an arbitrary module
        // you can hand it to the evaluate(), run(), runStream(), runFile()
        // as additional argument.

        // Until now there is no module 'mymodule' available.
        Python::runStream()
            << std::endl << "try:"
            << std::endl << "    import mymodule"
            << std::endl << "except:"
            << std::endl << "    print('mymodule was not found!')"
            << std::endl;

        // Create a new module not contained in a file
        auto myModule = Python::createModule("mymodule");

        // Populate the module by executing code inside
        myModule.run("a=1");
        myModule.run("b=1");
        myModule.runStream()
            << std::endl << "def f(x,y):"
            << std::endl << "    return x+y"
            << std::endl;

        // Now we can import the new module into other module, e.g., main
        // using its name.
        pyMain.import("mymodule");
        pyMain.run("print(mymodule.f(mymodule.a, mymodule.b))");

        // You can also import a Module by giving another name.
        pyMain.importAs(myModule, "mymodule2");
        pyMain.run("print(mymodule2.f(mymodule.a, mymodule.b))");
    }



    std::cout << std::endl;
    std::cout << "Example  7: Access any kind of member using get() ******************************" << std::endl;
    {
        // Almost all python objects act like dictionaries
        // and you can access their entries using the get()
        // method. This is for example true for members
        // of modules, classes, and objects.
        //
        // You can also store the result of the get()
        // for later usage.

        // Get the the class 'A'.
        auto A = pyMain.get("A");

        // Get the instance 'c' of class 'C' created above
        auto a = pyMain.get("a");
        std::cout << a.str() << std::endl;

        // Get the method 'f' of instance 'c'
        auto a_f = a.get("f");
    }



    std::cout << std::endl;
    std::cout << "Example  8: Setting python members using set() *********************************" << std::endl;
    {
        // Conversely to the get method you can set members to point
        // to another python object using the set() method.

        // Create a new python object storing the result of evaluation of an expression.
        auto e = pyMain.evaluate("'foo'");

        // If we would delete e there would be no reference left
        // to the result and hence it would be destroyed.

        // Give the result a name in the __main__ module
        // This is the same as doing run("result = 'foo'")
        pyMain.set("result", e);

        // Now the name 'result' in __main__ does also hold a reference to the result object.
        std::cout << pyMain.get("result").str() << std::endl;

        // Reassign by something different.
        pyMain.set("result", Python::evaluate("2+3"));

        // Now the name 'result' in __main__ holds a reference to the new result object.
        pyMain.run("print(result)");
    }



    std::cout << std::endl;
    std::cout << "Example  9: Calling things in python *******************************************" << std::endl;
    {
        // Python has a very broad notation of callable objects covering
        //
        //   free functions,
        //   lambda functions,
        //   classes,             (calling a class object means calling a constructor)
        //   methods of objects,  (you can use member functions like free functions g=c.f)
        //   functors             (instances of classes implementing the __call__ method)
        //
        // All of these can be accessed using the operator () on a
        // Python::Callable which refers to a callable python object.
        // To obtain a Callable convert an appropriate object.
        //
        // You can pass Callable objects and C++ objects
        // as arguments. In the latter case they will be converted
        // to python objects before the actual python call is
        // executed automatically (see below).
        //
        // Note: Calling operator() on a Python callable returns a Python object
        // (more specifically, a Python::Reference)!  If you want a callable
        // that returns a C++ object you need to wrap your Python::Callable
        // using Python::makeFunction or Python::makeDifferentiableFunction (see below).

        auto module = pyMain.import("dunepythontest");

        auto foo = module.get("foo");
        auto bar = module.get("bar");

        // Convert 'add' to Callable. This will only succeed for
        // callable python objects.
        auto add = Python::Callable(module.get("add"));

        // 'add' is a free function defined in the module taking two arguments.
        // We can call it using the () operator.
        auto result = add(foo, bar);

        // Finally we print a string representation of the result.
        std::cout << "Result of add(foo,bar)=" << result.str() << std::endl;

        // We can also pass C++ objects as arguments.
        std::cout << "Result of add(3.14159, 2.71828)=" << add(3.14159, 2.71828).str() << std::endl;

        // Create some more callable stuff
        pyMain.runStream()
            << std::endl << "class C:"
            << std::endl << "    def __init__(self, s):"
            << std::endl << "        self.s=s"
            << std::endl
            << std::endl << "    def printConstructorArg(self):"
            << std::endl << "        return self.s"
            << std::endl
            << std::endl << "    def f(self, x):"
            << std::endl << "        return x+self.s"
            << std::endl
            << std::endl << "    def add(self, x, y):"
            << std::endl << "        return x+y"
            << std::endl
            << std::endl << "    def sum(self, x):"
            << std::endl << "        return sum(x)"
            << std::endl
            << std::endl << "    def __call__(self, x):"
            << std::endl << "        return x+self.s+x"
            << std::endl;

        // Get the class 'C'. It is callable due to its constructor.
        auto C = Python::Callable(pyMain.get("C"));

        // Call a constructor to create an object of class C.
        // It is callable itself because it implements __call__()
        // which is the python analogue of operator().
        auto c = Python::Callable(C("c_foo"));

        // Call a method of an object.
        // This will print the stored argument of the constructor call for c.
        std::cout << Python::Callable(c.get("printConstructorArg"))().str() << std::endl;

        // Call the operator() of c (__call__ method of python object).
        std::cout << c("bar").str() << std::endl;

        // Call 'c' s method 'sum'.
        std::cout << Python::Callable(c.get("sum"))(Python::evaluate("[1,2,3]")).str() << std::endl;

        // Get the method 'add' of c.
        auto c_add = Python::Callable(c.get("add"));

        // Call 'c' s method 'add'.
        std::cout << "Result of c.add(foo,bar): " << c_add(foo, bar).str() << std::endl;

        // You can also call a lambda function
        auto lambda = Python::Callable(Python::evaluate("lambda x,y: (x+y)"));
        std::cout << "Result of call to lambda function: " << lambda(foo, bar).str() << std::endl;

        // Named arguments can also be passed and mixed with positional ones
        using namespace Python::Literals;
        using Python::arg;
        auto checkIncreasing = Python::Callable(module.get("checkIncreasing"));
        std::cout << "Result of checkIncreasing(0,1): " << checkIncreasing(0,1).str() << std::endl;
        std::cout << "Result of checkIncreasing(0,1,reverse=False): " << checkIncreasing(0, 1, arg("reverse", false)).str() << std::endl;
        std::cout << "Result of checkIncreasing(0,1,reverse=True): " << checkIncreasing(0, 1, "reverse"_a=true).str() << std::endl;
        std::cout << "Result of checkIncreasing(1,reverse=True,0): " << checkIncreasing(1, "reverse"_a=true,0).str() << std::endl;
    }



    std::cout << std::endl;
    std::cout << "Example 10: Converting from python to C ****************************************" << std::endl;
    {
        // Conversion of a python object px to a C object cx of type T
        // can be done using
        //
        //   px.toC(cx)
        //
        // The conversion is always done into an already existing C object.
        //
        // For the experts:
        // You can extend the conversion to a custom type T by
        // specializing the struct Conversion<T> and implementing
        // Conversion<T>::toC() using the raw python api.
        // If you do so be careful to do the reference counting
        // correctly and _not_ to steal a reference to the
        // converted python object.

        auto module = pyMain.import("dunepythontest");

        // Get objects named 'int_list', 'str_list', 'int_tuple' from imported module.
        auto int_list = module.get("int_list");
        auto str_list = module.get("str_list");
        auto int_tuple = module.get("int_tuple");
        auto mixed_tuple = module.get("mixed_tuple");

        // Modify first argument of int_list.
        // Similar to calling a Callable you can either hand
        // a Reference or some C++ type. In the latter case
        // it will be converted automatically using makeObject()
        Python::setItem(int_list, 0, 7);

        // Convert python integer list to vector and print results:
        std::vector<int> int_vec;
        int_list.toC(int_vec);
        std::cout << "python integer list (string representation):" << int_list.str() << std::endl;
        std::cout << "python integer list converted to vector<int>:" << std::endl;
        for(size_t i=0; i<int_vec.size(); ++i)
            std::cout << int_vec[i] << std::endl;

        // Convert python string list to vector and print results:
        std::vector<std::string> str_vec;
        str_list.toC(str_vec);
        std::cout << "python string list (string representation):" << str_list.str() << std::endl;
        std::cout << "python string list converted to vector<string>:" << std::endl;
        for(size_t i=0; i<str_vec.size(); ++i)
            std::cout << str_vec[i] << std::endl;

        // Convert python integer tuple to vector and print results:
        std::vector<int> int_vec2;
        int_tuple.toC(int_vec2);
        std::cout << "python integer tuple (string representation):" << int_tuple.str() << std::endl;
        std::cout << "python integer tuple converted to vector<int>:" << std::endl;
        for(size_t i=0; i<int_vec2.size(); ++i)
            std::cout << int_vec2[i] << std::endl;

        // Convert python mixed tuple to Dune::TupleVector and print results:
        Dune::TupleVector<std::vector<int>,std::string> mixed_tuple_c;
        mixed_tuple.toC(mixed_tuple_c);
        std::cout << "python mixed tuple (string representation):" << mixed_tuple.str() << std::endl;
        std::cout << "python mixed tuple converted to Dune::TupleVector:" << std::endl;
        for(size_t i=0; i<mixed_tuple_c[_0].size(); ++i)
            std::cout << mixed_tuple_c[_0][i] << std::endl;
        std::cout << mixed_tuple_c[_1] << std::endl;
    }



    std::cout << std::endl;
    std::cout << "Example 11: Converting from C to python ****************************************" << std::endl;
    {
        // Conversion of a C object cx of type T to a python object px
        // can be done using
        //
        //   Python::Reference px = Python::makeObject(cx);
        //
        // The conversion will always create a new python object.
        //
        // For the experts:
        // You can extend the conversion to a custom type T by
        // specializing the struct Conversion<T> and implementing
        // Conversion<T>::toPy() using the raw python api.
        // If you do so be careful to do the reference counting
        // correctly and to return a _new_ reference to the
        // created python object.

        // Create a C++ vector.
        std::vector<int> v(3);
        v[0] = 10;
        v[1] = 11;
        v[2] = 12;

        // Create a python list with the same content.
        // Similar to calling a Callable you can either hand
        // a Reference or some C++ type. In the latter case
        // it will be converted automatically using makeObject()
        pyMain.set("v", v);

        // Let python print the content of the list.
        pyMain.run("print(v)");
    }



    std::cout << std::endl;
    std::cout << "Example 12: Using python to define std::functions's *********************" << std::endl;
    {
        // There are various ways to wrap callable objects from python into a
        // std::function.  The basic requirement is that there is a C->python conversion
        // for the domain type and a python->C conversion for the range type.

        int x=2;

        // We can construct a std::function from a callable object in python.

        // This can be a function, ...
        pyMain.runStream()
            << std::endl << "def f(x):"
            << std::endl << "    return x + 3.141593 + 1"
            << std::endl;

        auto f = pyMain.get("f").toC<std::function<double(int)>>();
        std::cout << "Result of evaluating the wrapped function f: " << f(x) << std::endl;

        // ... a lambda function, ...
        auto lambda = Python::evaluate("lambda x: (x+2)").toC<std::function<double(int)>>();
        std::cout << "Result of evaluating a wrapped lambda function: " << lambda(x) << std::endl;

        // ... a functor (instance with __call__ method), ...
        Python::run("c = C(1)");
        auto c = pyMain.get("c").toC<std::function<double(int)>>();
        std::cout << "Result of evaluating the wrapped functor c: " << c(x) << std::endl;

        // ... or a method of an instance.
        auto c_f = pyMain.get("c").get("f").toC<std::function<double(int)>>();
        std::cout << "Result of evaluating the wrapped method c.f: " << c_f(x) << std::endl;
    }



    std::cout << std::endl;
    std::cout << "Example 13: Conversion for dune types ******************************************" << std::endl;
    {
        // Conversion is implemented for some dune types

        // One can convert a nested python dictionary to a ParameterTree.
        // Be careful: a python key 'foo.bar' containing a dot will
        // result in two levels 'foo' followed by 'bar' in the ParameterTree.
        pyMain.runStream()
            << std::endl << "tree = {}"
            << std::endl
            << std::endl << "tree[1] = 'one'"
            << std::endl << "tree['two'] = 2e-0"
            << std::endl << "tree['3'] = {}"
            << std::endl << "tree['3']['a'] = 'aaa'"
            << std::endl << "tree['3']['function'] = (lambda x: x*x)"
            << std::endl << "tree['a.b.c'] = 'abc'"
            << std::endl << "tree['a'] = {'b' : {'d':'d'}}"
            << std::endl;

        Dune::ParameterTree param;
        pyMain.get("tree").toC(param);
        std::cout << "python dictionary as ParameterTree:" << std::endl;
        param.report();
    }



    std::cout << std::endl;
    std::cout << "Example 14: Functions acting on dune types *************************************" << std::endl;
    {
        // Conversion is also implementing for FieldVector so we can use this in std::function

        // We can use vector valued functions
        {
            typedef Dune::FieldVector<double, 3> DT;
            typedef Dune::FieldVector<double, 2> RT;
            using F = std::function<RT(DT)>;
            pyMain.runStream()
                << std::endl << "def h(x):"
                << std::endl << "    return (x[0] + x[1], x[1] + x[2])"
                << std::endl;
            std::function<RT(DT)> h = pyMain.get("h").toC<F>();
            std::cout << "Result of calling a std::function< FV<double,2>(FV<double,3>) > : " << h({1, 2, 3}) << std::endl;
        }

        // Python scalars are treated like sequences with size one
        {
            pyMain.runStream()
                << std::endl << "def h2(x):"
                << std::endl << "    return x[0] + x[1]"     // Result is a scalar
                << std::endl << "def h3(x):"
                << std::endl << "    return (x[0] + x[1],)"  // Result is a list with size one
                << std::endl;
        }

    }



    std::cout << std::endl;
    std::cout << "Example 15: Importing function dictionaries ************************************" << std::endl;
    {
        // Since Conversion is implemented for
        //
        //   std::map,
        //   shared_ptr<T>,
        //   std::function
        //
        // it is also possible to import dictionaries of callable
        // python objects into a map of functions.

        // Create callable python objects with proper signature.
        pyMain.runStream()
            << std::endl << "class G:"
            << std::endl << "    def __init__(self, z):"
            << std::endl << "        self.z=z"
            << std::endl
            << std::endl << "    def __call__(self, x):"
            << std::endl << "        return (x[0] + self.z, x[1] + self.z)"
            << std::endl
            << std::endl << "    def f(self, x):"
            << std::endl << "        return (x[1] + self.z, x[2] + self.z)"
            << std::endl
            << std::endl << "g = G(3)"
            << std::endl;

        // Create dictionary of callable python objects.
        pyMain.runStream()
            << std::endl << "functions = {}"
            << std::endl
            << std::endl << "functions['h'] = h"
            << std::endl << "functions['sumsum'] = (lambda x: (sum(x), sum(x)) )"
            << std::endl << "functions['one'] = (lambda x: (1,1))"
            << std::endl << "functions['g'] = g"
            << std::endl << "functions['g.f'] = g.f"
            << std::endl;

        typedef Dune::FieldVector<double, 3> DT;
        typedef Dune::FieldVector<double, 2> RT;
        typedef std::function<RT(DT)> Function;
        typedef std::map<std::string, Function> FunctionMap;

        // Import dictionaries of callables to map of functions.
        FunctionMap functions;
        pyMain.get("functions").toC(functions);

        DT x;
        x[0] = 1;
        x[1] = 2;
        x[2] = 3;

        for(auto&& [key,f] : functions)
            std::cout << key << "(x) = " << f(x) << std::endl;
    }

    std::cout << std::endl;
    std::cout << "Example 16: Creating Dune-functions functions **********************************" << std::endl;
    {
        // Functions implementing the interface in dune-functions can also be created
        // directly

        using Domain = Dune::FieldVector<double, 3>;
        using Range = Dune::FieldVector<double, 2>;

        pyMain.import("math");
        pyMain.runStream()
            << std::endl << "def f(x):"
            << std::endl << "    return (x[0]**2+x[1], x[1]**2+x[2])"     // Result is a list
            << std::endl << "def df(x):"
            << std::endl << "    return ((2*x[0], 1, 0),(0, 2*x[1], 1))"  // Nested tuple as matrix
            << std::endl << "fdf = (f, df)"
            << std::endl;

        // makeDifferentiableFunction automatically converts to Python::Callable
        auto f = Python::makeDifferentiableFunction<Range(Domain)>(pyMain.get("f"), pyMain.get("df"));

        std::cout << "Result of calling a function FV<double,2>(FV<double,3>) : " << f({1, 2, 3}) << std::endl;
        std::cout << "Result of calling the derivative of a function FV<double,2>(FV<double,3>) : " << derivative(f)({1, 2, 3}) << std::endl;

        // functions can also be created from lambda expressions
        auto pyG = Python::evaluate("lambda x: math.sin(x)");
        auto pyDG = Python::evaluate("lambda x: math.cos(x)");
        auto pyDDG = Python::evaluate("lambda x: -math.sin(x)");
        auto pyDDDG = Python::evaluate("lambda x: -math.cos(x)");

        auto g = Python::makeDifferentiableFunction<double(double)>(pyG, pyDG, pyDDG, pyDDDG, pyG);

        std::cout << "Result of calling a function double(double) : " << g(0) << std::endl;
        std::cout << "Result of calling the derivative of a function double(double) : " << derivative(g)(0) << std::endl;
        std::cout << "Result of calling the 2nd derivative of a function double(double) : " << derivative(derivative(g))(0) << std::endl;
        std::cout << "Result of calling the 3rd derivative of a function double(double) : " << derivative(derivative(derivative(g)))(0) << std::endl;
        std::cout << "Result of calling the 4th derivative of a function double(double) : " << derivative(derivative(derivative(derivative(g))))(0) << std::endl;

        // If you only need the function but no derivatives, you can also use makeFunction()
        auto h = Python::makeFunction<double(double)>(Python::evaluate("lambda x: x*x"));
        std::cout << "Result of calling a function double(double) : " << h(0.5) << std::endl;
    }


#if HAVE_DUNE_UGGRID
    std::cout << std::endl;
    std::cout << "Example 17: Filling a GridFactory from python **********************************" << std::endl;
    {
        // prepare types and grid factory
        using Grid = Dune::UGGrid<2>;
        using GridFactory = Dune::GridFactory<Grid>;
        GridFactory gridFactory;

        // Get grid description from python and convert it to GridFactory
        auto pyGrid = pyMain.get("grid");
        pyGrid.toC(gridFactory);

        // Let GridFactory create grid
        std::unique_ptr<Grid> gridPtr(gridFactory.createGrid());
        auto& grid = *gridPtr;

        // Refine and write grid to visualize parametrized boundary
        grid.globalRefine(1);
        grid.globalRefine(1);

        Dune::VTKWriter<Grid::LeafGridView> vtkWriter(grid.leafGridView());
        vtkWriter.write("python-generated-grid");

        std::cout << "number of elements in created grid : " << grid.leafGridView().indexSet().size(0) << std::endl;
    }
#endif

    std::cout << std::endl;
    std::cout << "Example 18: calling functions with Fieldvector multiple times ******************" << std::endl;
    {
        using Vec3D = Dune::FieldVector<double, 3>;
        using Vec2D = Dune::FieldVector<double, 2>;
        using Vec1D = Dune::FieldVector<double, 1>;

        Vec3D a{0.5,0.5,0.5};
        Vec2D b{0.5,0.5};
        Vec1D c{0.5};

        auto test3d = Python::make_function<Vec3D>(Python::evaluate("lambda x: (0.0, 0.0, 0.0)"));
        auto test2d = Python::make_function<Vec2D>(Python::evaluate("lambda x: (0.0, 0.0)"));
        auto test1d = Python::make_function<Vec1D>(Python::evaluate("lambda x: (0.0)"));

        int nloop=2;
        std::cout << "multiple calls with 3d Fieldvector... "<< std::endl;
        for (int i=0; i<nloop; ++i)
            std::cout << i << ". call "<< test3d(a) << " should be (0,0,0)" << std::endl;

        std::cout << "multiple calls with 2d Fieldvector... "<< std::endl;
        for (int i=0; i<nloop; ++i)
            std::cout << i << ". call "<< test2d(b) << " should be (0,0)" << std::endl;

        std::cout << "multiple calls with 1d Fieldvector... "<< std::endl;
        for (int i=0; i<nloop; ++i)
            std::cout << i << ". call "<< test1d(c) << " should be (0)" << std::endl;
    }

    // Before exiting you should stop the python interpreter.
    Python::stop();

    return 0;
}
