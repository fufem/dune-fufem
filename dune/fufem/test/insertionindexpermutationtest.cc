#include <config.h>

#include <vector>
#include <string>
#include <numeric>
#include <algorithm>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/rangeutilities.hh>
#include <dune/common/version.hh>
#include <dune/common/test/testsuite.hh>

#include <dune/grid/io/file/gmshreader.hh>

#if HAVE_DUNE_UGGRID
#include <dune/grid/uggrid.hh>
#endif

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif

#include <dune/fufem/insertionindexpermutation.hh>



Dune::TestSuite checkPermuteInplace()
{
  auto testSuite = Dune::TestSuite("Check permuteInplace()");

  // Create some test data to permute
  auto originalData = std::vector<std::string>{"a", "b", "c", "d", "e", "f"};
  auto data = originalData;

  // Create initial identity permutation
  auto permutation = std::vector<int>(originalData.size());
  std::iota(permutation.begin(), permutation.end(), 0);

  // Check with all permutations
  do
  {
    // Permute data in-place according to permutation
    data = originalData;
    Dune::Fufem::permuteInplace(permutation, data);

    // Check if permuted data are correct
    for(auto k : Dune::IntegralRange(data.size()))
      testSuite.check(data[k] == originalData[permutation[k]])
        << "Mismatch at index " << k << " after permuteInplace()";
  }
  while (std::next_permutation(permutation.begin(), permutation.end()));

  return testSuite;
}



template<class Grid>
Dune::TestSuite checkPermuteFromBoundarySegmentInsertionIndices()
{
  auto name = std::string("Check permuteFromBoundarySegmentInsertionIndices<") + Dune::className<Grid>() + ">()";
  auto testSuite = Dune::TestSuite(name);

  // Create grid and store segment data
  std::vector<int> segmentData;
  std::vector<int> elementData;
  auto gridFactory = Dune::GridFactory<Grid>();
  Dune::GmshReader<Grid>::read(gridFactory, "square.msh", segmentData, elementData);
  auto gridPtr = gridFactory.createGrid();
  auto& grid = *gridPtr;

  // Create check if permuted segment data match what is defined in the Gmsh file
  auto checkSegmentData = [&]()
  {
    auto gridView = grid.leafGridView();
    using GridView = decltype(gridView);
    using It = BoundaryIterator<GridView>;
    auto boundaryIntersections = Dune::IteratorRange(It(gridView, It::PositionFlag::begin), It(gridView, It::PositionFlag::end));
    for(auto&& intersection : boundaryIntersections)
    {
      auto expectedData = 0;
      if (std::fabs(intersection.geometry().center()[1]) < 1e-10)
        expectedData = 1;
      if (std::fabs(intersection.geometry().center()[0]-1.0) < 1e-10)
        expectedData = 2;
      if (std::fabs(intersection.geometry().center()[1]-1.0) < 1e-10)
        expectedData = 3;
      if (std::fabs(intersection.geometry().center()[0]) < 1e-10)
        expectedData = 4;
      testSuite.check(segmentData[intersection.boundarySegmentIndex()] == expectedData)
        << "Segment data is " << segmentData[intersection.boundarySegmentIndex()]
        << " but should be " << expectedData
        << " for boundarySegmentIndex " << intersection.boundarySegmentIndex();
    }
  };

  // Permute segment data
  auto originalSegmentData = segmentData;
  {
    auto segmentData = originalSegmentData;
    Dune::Fufem::permuteFromBoundarySegmentInsertionIndices(gridFactory, grid, segmentData);
    checkSegmentData();
  }
  {
    auto segmentData = originalSegmentData;
    auto permutation = Dune::Fufem::boundarySegmentInsertionIndices(gridFactory, grid);
    Dune::Fufem::permuteInplace(permutation, segmentData);
    checkSegmentData();
  }

  return testSuite;
}



int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);

  auto testSuite = Dune::TestSuite();
  testSuite.subTest(checkPermuteInplace());
#if HAVE_DUNE_UGGRID
  testSuite.subTest(checkPermuteFromBoundarySegmentInsertionIndices<Dune::UGGrid<2>>());
#endif
#if HAVE_DUNE_ALUGRID
  testSuite.subTest(checkPermuteFromBoundarySegmentInsertionIndices<Dune::ALUGrid<2, 2, Dune::simplex>>());
#endif

  return testSuite.exit();
}
