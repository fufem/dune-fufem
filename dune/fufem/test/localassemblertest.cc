// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#include <config.h>

#include <array>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/geometrygrid.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/istl/bcrsmatrix.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/raviartthomasbasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>

#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/localassemblers/massassembler.hh>
#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>
#include <dune/fufem/backends/istlmatrixbackend.hh>


using namespace Dune;

template <class Basis, class Matrix>
void test(const Basis& basis)
{
  using Assembler = Fufem::DuneFunctionsOperatorAssembler<Basis, Basis>;

  auto matrix = Matrix{};
  auto matrixBackend = Dune::Fufem::istlMatrixBackend(matrix);

  auto assembler = Assembler{basis, basis};

  auto patternBuilder = matrixBackend.patternBuilder();
  assembler.assembleBulkPattern(patternBuilder);

  patternBuilder.setupMatrix();

  // test mass matrix
  Fufem::MassAssembler localMassAssembler;
  assembler.assembleBulkEntries(matrixBackend, localMassAssembler);

  // test stiffness matrix
  Fufem::LaplaceAssembler laplaceAssembler;
  assembler.assembleBulkEntries(matrixBackend, laplaceAssembler);
}

int main (int argc, char *argv[])
{
  Dune::MPIHelper::instance(argc, argv);

  const int dim = 2;

  // Build a test grid
  using Grid = YaspGrid<dim>;

  Grid grid({1.0, 1.0}, {5, 5});

  auto gridView = grid.leafGridView();
  using namespace Dune::Functions::BasisFactory;

  // Test with a scalar bases
  {
    auto basis = makeBasis(
      gridView,
        lagrange<2>()
      );

    using Basis = decltype(basis);
    using Matrix = BCRSMatrix<double>;

    test<Basis,Matrix>(basis);
  }

  // Test with a scalar bases on a grid with positive codimension
  {
    struct Deformation
    : public AnalyticalCoordFunction<double, 2, 3, Deformation>
    {
      FieldVector<double,3> operator()( const FieldVector<double, 2>& x) const
      {
        return {(1+x[1])*std::cos(x[0]),
                1.0,
                (1+x[1])*std::sin(x[0])};
      }
    };

    Deformation deformation;
    GeometryGrid<Grid, Deformation> surfaceGrid(grid, deformation);

    auto basis = makeBasis(
      surfaceGrid.leafGridView(),
        lagrange<2>()
      );

    using Basis = decltype(basis);
    using Matrix = BCRSMatrix<double>;

    test<Basis,Matrix>(basis);
  }

  // Test with a basis of vector-valued functions
  {
    auto basis = makeBasis(
      gridView,
        raviartThomas<1>()
      );

    using Basis = decltype(basis);
    using Matrix = BCRSMatrix<double>;

    test<Basis,Matrix>(basis);
  }

  // Test with a power basis
  {
    auto basis = makeBasis(
      gridView,
      power<2>(
        lagrange<2>(),
        flatInterleaved()
      ));

    using Basis = decltype(basis);
    using Matrix = BCRSMatrix<double>;

    test<Basis,Matrix>(basis);
  }

  return 0;
}

