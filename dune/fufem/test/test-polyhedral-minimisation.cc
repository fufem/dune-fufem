#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/fvector.hh>

#include <dune/fufem/geometry/polyhedrondistance.hh>

int main() {
  bool passed = true;
  using LocalVector = Dune::FieldVector<double, 2>;

  auto const test =
      [&](ConvexPolyhedron<LocalVector> const &p1,
          ConvexPolyhedron<LocalVector> const &p2, double analyticalDistance) {
        {
          double const error =
              std::abs(analyticalDistance - distance(p1, p2, 1e-12));
          std::cout << "error: " << error << std::endl;
          passed = passed and error < 1e-12;
        }
      };
  {
    /*
     * Calculate the distance between two triangles, where it is attained at a
     * face-vertex pair
     *
     *    O
     *    |\
     *    | \
     * O  O--O
     * |\
     * | \
     * O--O
     */
    double const analyticalDistance = std::sqrt(2.0);
    ConvexPolyhedron<LocalVector> const bs1 = {
      { { 0, 0 }, { 0, 2 }, { 2, 0 } }
    };
    ConvexPolyhedron<LocalVector> const bs2 = {
      { { 2, 2 }, { 2, 4 }, { 4, 2 } }
    };
    test(bs1, bs2, analyticalDistance);
  }

  {
    /*
     * Calculate the distance between two triangles, where it is
     * attained in a face-face pair
     *
     *    O--O
     *     \ |
     *      \|
     * O     O
     * |\
     * | \
     * O--O
     */
    double const analyticalDistance = 2.0 * std::sqrt(2.0);
    ConvexPolyhedron<LocalVector> const bs1 = {
      { { 0, 0 }, { 0, 2 }, { 2, 0 } }
    };
    ConvexPolyhedron<LocalVector> const bs2 = {
      { { 4, 4 }, { 2, 4 }, { 4, 2 } }
    };
    test(bs1, bs2, analyticalDistance);
  }

  {
    /*
     * Calculate the distance between two triangles, where it is
     * attained in a vertex-vertex pair
     *
     *       O
     *       |\
     *       | \
     * O--O  O--O
     *  \ |
     *   \|
     *    O
     */
    double const analyticalDistance = 2.0;
    ConvexPolyhedron<LocalVector> const bs1 = {
      { { 2, 2 }, { 0, 2 }, { 2, 0 } }
    };
    ConvexPolyhedron<LocalVector> const bs2 = {
      { { 4, 2 }, { 4, 4 }, { 6, 2 } }
    };
    test(bs1, bs2, analyticalDistance);
  }
  return passed ? 0 : 1;
}
