// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#include <config.h>

#include <array>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/common/fmatrix.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/istl/io.hh>

#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/backends/istlmatrixbackend.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>

#include <dune/fufem/assemblers/localassemblers/interiorpenaltydgassembler.hh>


#include <dune/functions/functionspacebases/lagrangedgbasis.hh>

/** Test whether the Dune::Fufem::DuneFunctionsOperatorAssembler and the classical Fufem assemblers
 *  assemble indeed the same matrix for the IPDG terms
 */
int main (int argc, char *argv[])
{
  Dune::MPIHelper::instance(argc, argv);

//  const int dim = 2;
//  const int refine = 9;

//  const int dim = 2;
//  const int refine = 4;

  const int dim = 2;
  const int refine = 1;
  constexpr int p = 2; // piecewise quadratic basis functions

  // Build a test grid
  typedef Dune::YaspGrid<dim> GridType;

  Dune::FieldVector<double,dim> h(1);
  std::array<int,dim> n;
  n.fill(2);
  n[0] = 3;

  GridType grid(h,n);
  grid.globalRefine(refine);

//  static const std::size_t N = 4;
  static const std::size_t N = 1;

  using Basis = Dune::Functions::LagrangeDGBasis<GridType::LeafGridView, p>;
  Basis basis(grid.leafGridView());

  using Field = double;
  using Basis = decltype(basis);

  using LocalMatrix =  Dune::FieldMatrix<Field,N,N>;
  using Matrix = Dune::BCRSMatrix<LocalMatrix>;

  using Assembler = Dune::Fufem::DuneFunctionsOperatorAssembler<Basis, Basis>;

  auto matrix = Matrix{};

  auto matrixBackend = Dune::Fufem::istlMatrixBackend(matrix);
  auto patternBuilder = matrixBackend.patternBuilder();

  auto assembler = Assembler{basis, basis};

  assembler.assembleSkeletonPattern(patternBuilder);

  patternBuilder.setupMatrix();

  auto vintageIPDGAssembler = InteriorPenaltyDGAssembler<GridType>();
  auto localBlockAssembler = [&](const auto& edge, auto& matrixContainer,
      auto&& insideTrialLocalView, auto&& insideAnsatzLocalView, auto&& outsideTrialLocalView, auto&& outsideAnsatzLocalView)
  {
      vintageIPDGAssembler.assembleBlockwise(edge, matrixContainer, insideTrialLocalView.tree().finiteElement(),
                                             insideAnsatzLocalView.tree().finiteElement(),
                                             outsideTrialLocalView.tree().finiteElement(),
                                             outsideAnsatzLocalView.tree().finiteElement());
  };
  auto localBoundaryAssembler = [&](const auto& edge, auto& localMatrix, auto&& insideTrialLocalView, auto&& insideAnsatzLocalView)
  {
      vintageIPDGAssembler.assemble(edge, localMatrix, insideTrialLocalView.tree().finiteElement(), insideAnsatzLocalView.tree().finiteElement());
  };

  assembler.assembleSkeletonEntries(matrixBackend, localBlockAssembler, localBoundaryAssembler); // IPDG terms

  // Errors should (if at all) only accrue from small numerical instabilities, hence the very strict threshold
  return 0;
}

