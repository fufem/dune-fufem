#ifndef DUNE_FUFEM_HDF5_FILE_HH
#define DUNE_FUFEM_HDF5_FILE_HH

#include <iostream>
#include <sys/stat.h>
#include <string>

#include <hdf5.h>

#include <dune/common/exceptions.hh>

namespace HDF5 {
class Grouplike {
public:
  bool hasDataset(std::string name) {
    return H5Lexists(c_obj(), name.c_str(), H5P_DEFAULT);
  }

  hid_t openDataset(std::string name) {
    return H5Dopen(c_obj(), name.c_str(), H5P_DEFAULT);
  }

  hid_t createDataset(std::string name, hid_t file_space, hid_t plist,
                      hid_t datatype) {
    return H5Dcreate(c_obj(), name.c_str(), datatype, file_space, H5P_DEFAULT,
                     plist, H5P_DEFAULT);
  }

  virtual hid_t c_obj() = 0;
  virtual bool isReadOnly() const = 0;
};

enum class Access { READONLY, READWRITE };

class File : public Grouplike {
public:
  File(std::string filename, Access access = Access::READWRITE)
      : readOnly_(access == Access::READONLY) {
    struct stat buffer;

    /* Use the latest HDF5 format (as opposed to: the latest format that supports
                                   the set of features we use: H5F_LIBVER_EARLIEST)
       Upside  : Improvements in speed, file size
       Downside: Output cannot be read by old tools/libraries
       Remedy:   h5repack and h5format_convert can restore compatibility. */
    H5F_libver_t const minimumVersion = H5F_LIBVER_LATEST;
    hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_libver_bounds(fapl, minimumVersion, H5F_LIBVER_LATEST);

    bool const fileExists = stat(filename.c_str(), &buffer) == 0;
    bool const createFile = !fileExists;
    if (createFile && readOnly_)
      DUNE_THROW(
          Dune::Exception,
          "Tried to open non-existing file in read-only mode: " << filename);

    // If createFile is true: We already know the file does not exist,
    // so H5F_ACC_EXCL and H5F_ACC_TRUNC are essentially equivalent
    unsigned int flags =
        createFile ? H5F_ACC_EXCL : (readOnly_ ? H5F_ACC_RDONLY : H5F_ACC_RDWR);
#if H5_VERSION_GE(1, 10, 0)
    // Enabling SWMR reading by default on HDF-1.10 does not hurt
    // (except maybe performance). In contrast, enabling SWMR writing
    // produces a file that is not readable by HDF5-1.8. The user can
    // still enable SWMR writing later through swmrEnable().
    if (readOnly_)
      flags |= H5F_ACC_SWMR_READ;
#endif

    file_ = createFile ? H5Fcreate(filename.c_str(), flags, H5P_DEFAULT, fapl)
                       : H5Fopen(filename.c_str(), flags, fapl);
    if (file_ < 0)
      DUNE_THROW(Dune::Exception, "Unable to open file: " << filename);
  }

  void flush() { H5Fflush(file_, H5F_SCOPE_LOCAL); }

  ~File() { H5Fclose(file_); }

  hid_t c_obj() override { return file_; }
  bool isReadOnly() const override { return readOnly_; }
  bool swmrEnabled() { return swmrEnabled_; }
  void swmrEnable() {
    if (swmrEnabled_)
      return;

#if H5_VERSION_GE(1, 10, 0)
    if (!readOnly_) {
      swmrEnabled_ = true;
      H5Fstart_swmr_write(file_);
    } else {
      std::cerr << "WARNING: SWMR writing requested for a read-only file"
                << std::endl;
    }
#else
    std::cerr
        << "WARNING: SWMR requested but not supported by this version of HDF5"
        << std::endl;
#endif
  }

private:
  hid_t file_;
  bool const readOnly_;
  bool swmrEnabled_ = false;
};

class Group : public Grouplike {
public:
  Group(Grouplike &parent, std::string groupname) : parent_(parent) {
    group_ = H5Lexists(parent_.c_obj(), groupname.c_str(), H5P_DEFAULT)
                 ? H5Gopen(parent_.c_obj(), groupname.c_str(), H5P_DEFAULT)
                 : H5Gcreate(parent_.c_obj(), groupname.c_str(), H5P_DEFAULT,
                             H5P_DEFAULT, H5P_DEFAULT);
  }

  ~Group() { H5Gclose(group_); }

  hid_t c_obj() override { return group_; }
  bool isReadOnly() const override { return parent_.isReadOnly(); }

private:
  Grouplike &parent_;
  hid_t group_;
};
}
#endif
