#ifndef DUNE_FUFEM_HDF5_SINGLETONWRITER_HH
#define DUNE_FUFEM_HDF5_SINGLETONWRITER_HH

#include <numeric>

#include <hdf5.h>

#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrix.hh>

#include "file.hh"
#include "tobuffer.hh"
#include "typetraits.hh"

namespace HDF5 {
template <int spatialDimensions, typename ctype = double, typename T = hsize_t>
class SingletonWriter {
private:
  int static const dimensions = spatialDimensions;

public:
  template <typename... Args>
  SingletonWriter(Grouplike &file, std::string datasetname, Args... args)
      : file_(file), capacity_{{T(args)...}} {
    static_assert(sizeof...(args) == spatialDimensions,
                  "wrong number of arguments");
    if (file_.isReadOnly())
        DUNE_THROW(Dune::Exception, "Cannot handle read-only file");

    if (file_.hasDataset(datasetname)) {
      dset_ = file_.openDataset(datasetname);
      checkDimensions();
      checkDatatype();
      checkLayout();
    } else {
      auto const initial_dims = maxExtent();
      auto const max_dims = maxExtent();
      hid_t file_space =
          H5Screate_simple(dimensions, initial_dims.data(), max_dims.data());

      hid_t plist = H5Pcreate(H5P_DATASET_CREATE);
      H5Pset_layout(plist, H5D_CONTIGUOUS);
      dset_ = file_.createDataset(datasetname, file_space, plist,
                                  TypeTraits<ctype>::getType());

      H5Pclose(plist);
      H5Sclose(file_space);
    }
  }

  ~SingletonWriter() { H5Dclose(dset_); }

  void set(std::vector<ctype> const &buffer) {
    if (buffer.size() != entrySize())
      DUNE_THROW(Dune::Exception, "Buffer size incorrect");

    hid_t file_space = H5Dget_space(dset_);
    auto const start = minExtent();
    auto const count = maxExtent();
    H5Sselect_hyperslab(file_space, H5S_SELECT_SET, start.data(), NULL,
                        count.data(), NULL);

    hid_t mem_space = H5Screate_simple(dimensions, count.data(), NULL);
    H5Dwrite(dset_, TypeTraits<ctype>::getType(), mem_space, file_space,
             H5P_DEFAULT, buffer.data());
    H5Sclose(mem_space);
    H5Sclose(file_space);
  }

private:
  size_t entrySize() {
    return std::accumulate(capacity_.begin(), capacity_.end(), 1,
                           std::multiplies<T>());
  }

  void checkLayout() {
    hid_t plist = H5Dget_create_plist(dset_);
    H5D_layout_t layout = H5Pget_layout(plist);
    if (layout != H5D_CONTIGUOUS)
      DUNE_THROW(Dune::Exception, "Layout not contiguous");

    H5Pclose(plist);
  }

  void checkDimensions() {
    hid_t file_space = H5Dget_space(dset_);
    std::array<T, dimensions> dims;
    int const rank = H5Sget_simple_extent_dims(file_space, dims.data(), NULL);

    if (rank != dimensions)
      DUNE_THROW(Dune::Exception, "Unexpected dataset rank");
    if (capacity_ != dims)
      DUNE_THROW(Dune::Exception, "unexpected dataset dimensions");

    H5Sclose(file_space);
  }

  void checkDatatype() {
    if (!H5Tequal(H5Dget_type(dset_), TypeTraits<ctype>::getType()))
      DUNE_THROW(Dune::Exception, "Unexpected data type");
  }

  std::array<T, dimensions> minExtent() {
    std::array<T, dimensions> ret;
    std::fill(ret.begin(), ret.end(), 0);
    return ret;
  }

  std::array<T, dimensions> maxExtent() { return capacity_; }

  Grouplike &file_;
  std::array<T, spatialDimensions> const capacity_;

  hid_t dset_;
};

template <int spatialDimensions, typename ctype, typename T, class Data>
void setEntry(SingletonWriter<spatialDimensions, ctype, T> &writer,
              Data const &data) {
  std::vector<ctype> buffer;
  toBuffer(data, buffer);
  writer.set(buffer);
}
}
#endif
