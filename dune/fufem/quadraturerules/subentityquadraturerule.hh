// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_QUADRATURERULES_SUBENTITYQUADRATURERULE_HH
#define DUNE_FUFEM_QUADRATURERULES_SUBENTITYQUADRATURERULE_HH

#include <dune/geometry/type.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/quadraturerules.hh>

namespace Dune::Fufem {

/**
 * \brief Quadrature rule for a subentity of some given geometry type.
 *
 * Wrap a quadrature rule for a subentity with smaller dimension
 * as quadrature rule on an entity/element, by mapping quadrature
 * points from local subentity coordinates to local entity coordinates.
 *
 * Although this rule looks like a quadrature rule on the entity it does
 * in fact integrate over the subentity. This e.g. has the advantage, that
 * integration over faces can use the same infrastructure as integration
 * over entities for caching.
 */
template <class ct, int dim, int codim>
class SubEntityQuadratureRule:
  public Dune::QuadratureRule<ct,dim>
{
  using Base = Dune::QuadratureRule<ct,dim>;

public:

  using Base::type;

  /**
   * \brief Create SubEntityQuadratureRule for entity of given GeometryType
   *
   * \param gt GeometryType of the entity the rule is defined on
   * \param subEntityIndex Index of codim subentity that the rule should integrate
   * \param rule A (dim-codim)-dimensional rule on the subEntityIndex-th subentity of codimension codim
   */
  SubEntityQuadratureRule(Dune::GeometryType gt, std::size_t subEntityIndex, const Dune::QuadratureRule<ct, dim-codim>& rule)
    : Base(gt),
      subEntityRule_(&rule)
  {
    [[maybe_unused]] auto subEntityGeometryType = Dune::referenceElement<ct,dim>(type()).type(subEntityIndex, codim);
    assert(subEntityGeometryType == subEntityRule().type());
    auto subEntityGeometry = Dune::referenceElement<ct,dim>(type()).template geometry<codim>(subEntityIndex);
    for(auto qp : subEntityRule())
      this->emplace_back(subEntityGeometry.global(qp.position()), qp.weight());
    this->delivered_order = subEntityRule().order();
  }

  const Dune::QuadratureRule<ct, dim-1>& subEntityRule() const
  {
    return *subEntityRule_;
  }

protected:
  const Dune::QuadratureRule<ct, dim-1>* subEntityRule_;
};


} // namespace Dune::Fufem

#endif // DUNE_FUFEM_QUADRATURERULES_SUBENTITYQUADRATURERULE_HH
