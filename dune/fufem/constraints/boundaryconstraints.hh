// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_CONSTRAINTS_BOUNDARYCONSTRAINTS_HH
#define DUNE_FUFEM_CONSTRAINTS_BOUNDARYCONSTRAINTS_HH

#include <variant>

#include <dune/functions/functionspacebases/interpolate.hh>

#include <dune/fufem/backends/traversal.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>
#include <dune/fufem/constraints/affineconstraints.hh>



namespace Dune::Fufem {

/**
 * \brief Compute constraints for essential boundary conditions
 * \ingroup Constraints
 *
 * \tparam Basis Type of the global basis
 * \tparam Function Type of the function to interpolate
 *
 * \param constraints An initialized AffineConstraints object
 * \param basis A global basis
 * \param f The Dirichlet values given as a function
 * \param boundaryPatch The BoundaryPath where the boundary condition should be applied
 *
 * This will interpolate the function `f` on the `boundaryPatch` and
 * add constraints for all corresponding DOFs to `constraints`.
 *
 * This method assumes that the `constraints` has already been initialized.
 * If `constraints` already contains constraint DOFs, then interpolation
 * and the newly added constraints are reduced to the previously unconstrained
 * DOFs.
 *
 * \warning This function is still experimental and the interface may change.
 */
template<class BV, class V, class MI, class C, class Basis, class Function>
auto computeBoundaryConstraints(AffineConstraints<BV, V, MI, C>& constraints, const Basis& basis, Function&& f, const BoundaryPatch<typename Basis::GridView>& boundaryPatch)
{
  auto&& isConstrained = Dune::Functions::istlVectorBackend(constraints.isConstrained());
  auto isNewConstrained = constraints.isConstrained();
  Dune::Fufem::markBoundaryPatchDofs(boundaryPatch, basis, isNewConstrained);
  Dune::Fufem::recursiveForEachVectorEntry(isNewConstrained, [&](auto&& isNewConstrained_i, const auto& i)
  {
    isNewConstrained_i = isNewConstrained_i and not isConstrained[i];
    isConstrained[i] =  isNewConstrained_i or isConstrained[i];
  });
  Dune::Functions::interpolate(basis, constraints.constantTerm(), f, isNewConstrained);
  constraints.resolveDependencies();
}


/**
 * \brief Compute constraints for essential boundary conditions
 * \ingroup Constraints
 *
 * \tparam Basis Type of the global basis
 * \tparam Function Type of the function to interpolate
 *
 * \param constraints An initialized AffineConstraints object
 * \param basis A global basis
 * \param f The Dirichlet values given as a function
 *
 * This will interpolate the function `f` on the boundary and
 * add constraints for all corresponding DOFs to `constraints`.
 *
 * This method assumes that the `constraints` has already been initialized.
 * If `constraints` already contains constraint DOFs, then interpolation
 * and the newly added constraints are reduced to the previously unconstrained
 * DOFs.
 *
 * \warning This function is still experimental and the interface may change.
 */
template<class BV, class V, class MI, class C, class Basis, class Function>
auto computeBoundaryConstraints(AffineConstraints<BV, V, MI, C>& constraints, const Basis& basis, Function&& f)
{
  auto&& isConstrained = Dune::Functions::istlVectorBackend(constraints.isConstrained());
  auto isNewConstrained = constraints.isConstrained();
  Dune::Fufem::markBoundaryDofs(basis, isNewConstrained);
  Dune::Fufem::recursiveForEachVectorEntry(isNewConstrained, [&](auto&& isNewConstrained_i, const auto& i)
  {
    isNewConstrained_i = isNewConstrained_i and not isConstrained[i];
    isConstrained[i] =  isNewConstrained_i or isConstrained[i];
  });
  Dune::Functions::interpolate(basis, constraints.constantTerm(), f, isNewConstrained);
  constraints.resolveDependencies();
}


/**
 * \brief Compute constraints for essential boundary conditions
 * \ingroup Constraints
 *
 * \tparam BitVector A bit vector type suitable for the global indices of the basis
 * \tparam Vector A coefficient vector type suitable for the global indices of the basis
 * \tparam Basis Type of the global basis
 * \tparam Function Type of the function to interpolate
 *
 * \param basis A global basis
 * \param f The Dirichlet values given as a function
 * \param boundaryPatch The BoundaryPath where the boundary condition should be applied
 *
 * This will interpolate the function `f` on the `boundaryPatch` and
 * return an `AffineConstraints` object constraining all corresponding DOFs.
 *
 * If the template default value `std::monostate` is used for the template
 * parameters `BitVector` or `Vector` then the actual container types are deduced
 * using `Dune::Functions::makeContainer(...)` and `Dune::Functions::makeISTLVector(...)`
 * from the container descriptor provided by `basis` (only with dune-function>=2.11).
 *
 * \warning This function is still experimental and the interface may change.
 */
template<class BitVector=std::monostate, class Vector=std::monostate, class Basis, class Function>
auto makeBoundaryConstraints(const Basis& basis, Function&& f, const BoundaryPatch<typename Basis::GridView>& boundaryPatch)
{
  auto constraints = makeAffineConstraints<BitVector, Vector>(basis);
  computeBoundaryConstraints(constraints, basis, f, boundaryPatch);
  return constraints;
}


/**
 * \brief Compute constraints for essential boundary conditions
 * \ingroup Constraints
 *
 * \tparam BitVector A bit vector type suitable for the global indices of the basis
 * \tparam Vector A coefficient vector type suitable for the global indices of the basis
 * \tparam Basis Type of the global basis
 * \tparam Function Type of the function to interpolate
 *
 * \param basis A global basis
 * \param f The Dirichlet values given as a function
 *
 * This will interpolate the function `f` on the boundary and
 * return an `AffineConstraints` object constraining all corresponding DOFs.
 *
 * If the template default value `std::monostate` is used for the template
 * parameters `BitVector` or `Vector` then the actual container types are deduced
 * using `Dune::Functions::makeContainer(...)` and `Dune::Functions::makeISTLVector(...)`
 * from the container descriptor provided by `basis` (only with dune-function>=2.11).
 *
 * \warning This function is still experimental and the interface may change.
 */
template<class BitVector=std::monostate, class Vector=std::monostate, class Basis, class Function>
auto makeBoundaryConstraints(const Basis& basis, Function&& f)
{
  auto constraints = makeAffineConstraints<BitVector, Vector>(basis);
  computeBoundaryConstraints(constraints, basis, f);
  return constraints;
}

} // namespace Dune::Fufem



#endif // DUNE_FUFEM_CONSTRAINTS_BOUNDARYCONSTRAINTS_HH
