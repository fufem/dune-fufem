// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_CONSTRAINTS_CONTINUITYCONSTRAINTS_HH
#define DUNE_FUFEM_CONSTRAINTS_CONTINUITYCONSTRAINTS_HH

#include <cmath>
#include <type_traits>
#include <variant>
#include <vector>

#include <dune/functions/functionspacebases/subentitydofs.hh>

#include <dune/fufem/constraints/affineconstraints.hh>



namespace Dune::Fufem {

/**
 * \brief Compute constraints for a conforming basis on a grid with hanging nodes
 * \ingroup Constraints
 *
 * \tparam Basis Type of the global basis
 * \param constraints An initialized AffineConstraints object
 * \param basis A global basis
 *
 * This will compute the constraints required to get a basis for the
 * subspace of continuous functions. This method is intended to be used
 * for getting the H^1-conforming subspace of a finite element space
 * on a grid with hanging nodes. It assumes that DOFs for noncontinuous
 * basis functions on an element can be interpolated from basis functions
 * of neighboring elements on coarser levels.
 *
 * This method assumes that the `AffineConstraints` object constraints
 * has already been initialized. It may also already contain constraints.
 *
 * \warning This function is still experimental and the interface may change.
 */
template<class BV, class V, class MI, class C, class Basis>
void computeContinuityConstraints(AffineConstraints<BV, V, MI, C>& constraints, const Basis& basis)
{
  using Constraints = AffineConstraints<BV, V, MI, C>;
  using Coefficient = typename Constraints::Coefficient;

  static_assert(std::is_same_v<typename Basis::MultiIndex, typename Constraints::MultiIndex>);

  // Interpolation weights smaller than the tolerance will be ignored
  const auto tol = 1e-5;

  auto&& isInterpolated = Dune::Functions::istlVectorBackend(constraints.isConstrained());
  auto&& interpolation = constraints.linearTerm();

  auto localIsConstrained = std::vector<bool>();
  auto interpolationValues = std::vector<Coefficient>();

  auto localIndices = [](const auto& tree) {
    return Dune::range(tree.localIndex(0), tree.localIndex(0)+tree.size());
  };

  auto localView = basis.localView();
  auto neighborLocalView = basis.localView();
  auto intersectionDOFs = Dune::Functions::subEntityDOFs(basis);

  for(const auto& element : elements(basis.gridView()))
  {
    localView.bind(element);
    auto level = element.level();

    bool allDofsConstrained = true;
    for(auto i : localIndices(localView.tree()))
      allDofsConstrained = allDofsConstrained and isInterpolated[localView.index(i)];
    if (allDofsConstrained)
        continue;

    for(const auto& intersection : intersections(basis.gridView(), element))
    {
      if (not(intersection.neighbor()))
        continue;

      const auto& neighborElement = intersection.outside();

      if (neighborElement.level()>=level)
        continue;

      intersectionDOFs.bind(localView, intersection);

      neighborLocalView.bind(neighborElement);

      Dune::TypeTree::forEachLeafNode(localView.tree(), [&](auto&& node, auto&& treePath) {
        auto&& neighborNode = Dune::TypeTree::child(neighborLocalView.tree(), treePath);

        const auto& neighborBasis = neighborNode.finiteElement().localBasis();
        using Range = typename std::decay_t<decltype(neighborBasis)>::Traits::RangeType;

        interpolationValues.resize(node.finiteElement().size());
        localIsConstrained.clear();
        localIsConstrained.resize(node.finiteElement().size(), false);

        // We first compute the neighbor basis values at all interpolation points.
        auto neighborBasisValues = std::vector<std::vector<Range>>(neighborBasis.size());
        auto k = 0;
        auto trackingBasisFunction = [&](const auto& x) {
          neighborBasisValues.resize(k+1);
          auto globalPosition = element.geometry().global(x);
          auto neighborLocalPosition = neighborElement.geometry().local(globalPosition);
          neighborBasis.evaluateFunction(neighborLocalPosition, neighborBasisValues[k]);
          return neighborBasisValues[k++][0];
        };
        node.finiteElement().localInterpolation().interpolate(trackingBasisFunction, interpolationValues);

        // Node we interpolate any neighbor basis function on the current element
        for(auto j : Dune::range(neighborBasis.size()))
        {
          auto j_global = neighborLocalView.index(neighborNode.localIndex(j));

          // neighborBasisFunction_j behaves like the j-th neighbor bases function if
          // interpolate() evaluates it at the same points in the same
          // order as above.
          auto k = 0;
          auto neighborBasisFunction_j = [&](const auto& x) {
            return neighborBasisValues[k++][j];
          };
          node.finiteElement().localInterpolation().interpolate(neighborBasisFunction_j, interpolationValues);

          for(auto i : Dune::range(node.finiteElement().size()))
          {
            // Only basis functions associated to this intersection or a subentity
            // are considered for being interpolated from the neighbor basis.
            if (not(intersectionDOFs.contains(node.localIndex(i))))
              continue;

            auto i_global = localView.index(node.localIndex(i));

            // We need to skip this if dof is already constrained from another element.
            // Otherwise we might get interpolation weights from multiple level neighbors
            // resulting in doubled interpolation weights after resolving the dependencies.
            if (isInterpolated[i_global])
              continue;

            if (i_global != j_global)
              if (std::abs(interpolationValues[i]) > tol)
              {
                interpolation[i_global][j_global] = interpolationValues[i];
                localIsConstrained[i] = true;
              }
          }
        }
        // Now mark all newly constrained nodes
        for(auto i : Dune::range(node.finiteElement().size()))
          if (localIsConstrained[i])
            isInterpolated[localView.index(node.localIndex(i))] = true;
      });
    }
  }
  constraints.resolveDependencies();
}



/**
 * \brief Compute constraints for a conforming basis on a grid with hanging nodes
 * \ingroup Constraints
 *
 * \tparam BitVector A bit vector type suitable for the global indices of the basis
 * \tparam Vector A coefficient vector type suitable for the global indices of the basis
 * \tparam Basis Type of the global basis
 *
 * \param basis A global basis
 *
 * This will compute the constraints required to get a basis for the
 * subspace of continuous functions. This method is intended to be used
 * for getting the H^1-conforming subspace of a finite element space
 * on a grid with hanging nodes. It assumes that DOFs for noncontinuous
 * basis functions on an element can be interpolated from basis functions
 * of neighboring elements on coarser levels.
 *
 * The constraints are returned as `AffineConstraints` object. The result
 * can be used to constrain a linear system to the conforming subspace.
 *
 * If the template default value `std::monostate` is used for the template
 * parameters `BitVector` or `Vector` then the actual container types are deduced
 * using `Dune::Functions::makeContainer(...)` and `Dune::Functions::makeISTLVector(...)`
 * from the container descriptor provided by `basis` (only with dune-function>=2.11).
 *
 * \warning This function is still experimental and the interface may change.
 */
template<class BitVector=std::monostate, class Vector=std::monostate, class Basis>
auto makeContinuityConstraints(const Basis& basis)
{
  auto constraints = makeAffineConstraints<BitVector, Vector>(basis);
  computeContinuityConstraints(constraints, basis);
  return constraints;
}

} // namespace Dune::Fufem



#endif // DUNE_FUFEM_CONSTRAINTS_CONTINUITYCONSTRAINTS_HH
