// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_CONSTRAINTS_AFFINECONSTRAINTS_HH
#define DUNE_FUFEM_CONSTRAINTS_AFFINECONSTRAINTS_HH

#include <iostream>
#include <map>
#include <unordered_map>
#include <utility>
#include <type_traits>

#include <dune/functions/backends/istlvectorbackend.hh>

#if DUNE_VERSION_GTE(DUNE_FUNCTIONS, 2, 11)
#include <dune/functions/backends/containerfactory.hh>
#include <dune/functions/backends/istlvectorfactory.hh>
#endif

#include <dune/fufem/backends/istlmatrixbackend.hh>
#include <dune/fufem/backends/traversal.hh>

namespace Dune::Fufem {



/**
 * \brief Class to handle affine constraints on a subset of DOFs
 * \ingroup Constraints
 *
 * This class handles affine constraints of the form
 *
 * \f[ x_i = \sum_{j\in \mathcal{I}} L_{ij} x_j + c_i \qquad i \in \mathcal{I}\f]
 *
 * or in short \f$ x = L x + c\f$ for a square matrix \f$ L \f$
 * where all indices are from the index set \f$\mathcal{I}\f$.
 * For exposition purposes we assume that \f$ \mathcal{I}=\{1,...,n\} \f$
 * while the implementation supports multi-indices as provided by the
 * global bases in dune-functions.
 * Under this assumption we have \f$L \in \mathbb{R}^{n \times n} \f$ and \f$c \in \mathbb{R}^n\f$.
 * The main purpose is to simplify the handling
 * of problems in the constrained affine subspace
 *
 * \f[ V_{L,c} = \{ x\,|\, x = L x+c\}.\f]
 *
 * We assume that the index set can be decomposed
 * according to \f$\mathcal{I}= \mathcal{I}_C \cup \mathcal{I}_N\f$ into disjoint subsets
 * \f$\mathcal{I}_C\f$ and \f$\mathcal{I}_N\f$ of constrained and non-constrained indices, respectively.
 * The constrained and non-constrained indices should satisfy the following two assumptions:
 * (a) For any non-constrained index \f$i\in \mathcal{I}_N\f$ we have (using the Kronecker delta)
 * \f$ L_{ij} = \delta_{ij} \forall j\in\mathcal{I} \f$ and \f$c_i=0\f$.
 * (b) For any constrained index \f$i\in \mathcal{I}_C\f$ we have
 * \f$L_{ii}=L_{ij}=0 \forall j\in\mathcal{I}_C\f$.
 * Then any constrained value \f$x_i\f$ with \f$i\in\mathcal{I}_C\f$
 * can be interpolated from \f$c_i\f$ and the non-constrained values \f$x_j\f$
 * according to
 *
 * \f[ x_i = \sum_{i\in \mathcal{I}} L_{ij} + c_i = \sum_{i\in \mathcal{I}_N} L_{ij} + c_i. \f]
 *
 * Under these assumptions we furthermore have \f$L^2 = L\f$ and \f$L c=0\f$,
 * i.e. \f$L\f$ is a linear projection and \f$c\f$ is in the kernel of \f$L\f$.
 * Then the affine map \f$ \Psi:\mathbb{R}^n \to V_{L,c}\f$
 * given by \f$v \mapsto \Psi(v) = L v +c\f$ is a surjective projection
 * into \f$V_{L,c}\f$. Hence the set \f$V_{L,c}\f$ of all \f$x\f$ satisfying
 * the constraint \f$x = Lx+c\f$ is equal to the set of all \f$x\f$ that
 * can be written as \f$x = Lv+c\f$ for some \f$v\f$, i.e.
 * \f$ V_{L,c} = \{ x\,|\, x = L x +c \} = \{Lv +c \,|\, v\}\f$.
 *
 * Now consider a linear variational problem
 *
 * \f[ u \in V_{L,c}: \qquad \langle A u, v \rangle = \langle b, v \rangle \qquad \forall v \in V_{L,0} \f]
 *
 * in the affine subspace \f$ V_{L,c}\f$.
 * Using the decomposition \f$u= u_0 + c\f$ with \f$ u_0 = L u_0 = L u \in V_{L,0} \f$
 * this can be written as
 *
 * \f[ u_0 \in V_{L,0}: \qquad \langle A u_0, v \rangle = \langle b - A c, v \rangle \qquad \forall v \in V_{L,0}. \f]
 *
 * Since \f$L:V_{L,0} \to V_{L,0}\f$ is a linear projection, we find that \f$u_0\f$ solves
 *
 * \f[ u_0 \in \mathbb{R}^n: \qquad \langle L^T A L u_0, v \rangle = \langle L^T(b - A c), v \rangle \qquad \forall v \in \mathbb{R}^n \f]
 *
 * The matrix \f$ L^T A L\f$ is obviously singular, since it is
 * zero on \f$ker(L) = (V_{L,0})^\perp\f$. Hence this
 * system does not have a unique solution.
 * Assuming that \f$A\f$ is regular on \f$V_{L,0}\f$, i.e.
 * \f$ker (A) \subset ker(L)\f$ we know
 * that the solution is at least unique in \f$V_{L,0}\f$.
 * To regularize the problem we consider the reduced identity matrix
 * \f$ \hat{I} \in \mathbb{R}^{n \times n}\f$
 * with \f$\hat{I}_{ij} = \delta_{ij}\f$ if \f$i,j \in \mathcal{I}_C\f$
 * and \f$\hat{I}_{ij} = 0\f$ if \f$i\in \mathcal{I}_N\f$ or \f$j\in \mathcal{I}_N\f$.
 * Since \f$L\hat{I} = L\f$ it is clear that the system
 *
 * \f[ \hat{u} \in \mathbb{R}^n: \qquad \langle \hat{A}, v \rangle = \langle \hat{b}, v \rangle \qquad \forall v \in \mathbb{R}^n \f]
 *
 * with \f$ \hat{A} = L^T A L + \hat{I} \f$ and \f$\hat{b} = L^T(b-A c) + c\f$
 * is equivalent to the constrained problem in the sense that \f$u_0 = L \hat{u}\f$.
 * Hence we can solve the constrained problem by solving
 *
 * \f[ \hat{A}\hat{u} = \hat{b} \f]
 *
 * and then setting \f$u = \Psi(\hat{u}) = L\hat{u} + c = L u_0 + c\f$.
 *
 * The present class supports this approach as follows:
 * The pair \f$(A,b)\f$ can be transformed to \f$(\hat{A}, \hat{b})\f$
 * using the method `constrainLinearSystem(A,b)`. Since this is an inplace operation
 * `extendMatrixPattern(...)` can be used before, to ensure that the pattern of the
 * matrix is large enough to also store \f$\hat{A}\f$. Once the system is solved,
 * \f$\hat{u}\f$ can be transformed to \f$u=\Psi(\hat{u})\f$ using
 * `interpolate(u)` which is again an inplace operation.
 *
 * \warning This class is still experimental and the interface may change.
 */
template<class BV, class V, class MI, class C = double>
class AffineConstraints
{

  template<class OtherMultiIndex>
  static decltype(auto) toMultiIndex(const OtherMultiIndex& omi)
  {
    if constexpr (std::is_same_v<OtherMultiIndex, MultiIndex>)
      return omi;
    else if constexpr (requires(MultiIndex mi){ mi.push_back(0); omi.size(); })
    {
      MultiIndex mi;
      for(auto i : omi)
        mi.push_back(i);
      return mi;
    }
  }

public:

  using BitVector = BV;
  using Vector = V;
  using MultiIndex = MI;
  using Coefficient = C;

  using LinearTermRow = std::map<MI, Coefficient>;
  using LinearTerm = std::unordered_map<MultiIndex, LinearTermRow>;
  using ConstantTerm = Vector;



  AffineConstraints() = default;
  AffineConstraints(const AffineConstraints&) = default;
  AffineConstraints(AffineConstraints&&) = default;

  /**
   * \brief Construct from bit vector and vector
   */
  AffineConstraints(BitVector&& isConstrained, Vector&& constantTerm)
    : isConstrained_(std::move(isConstrained))
    , constantTerm_(std::move(constantTerm))
  {}

  /**
   * \brief Construct from bit vector and vector
   */
  AffineConstraints(const BitVector& isConstrained, const Vector& constantTerm)
    : isConstrained_(isConstrained)
    , constantTerm_(constantTerm)
  {}

  /**
   * \brief Check if i-th DOF is constrained
   */
  template<class MultiIndexType>
  bool isConstrained(const MultiIndexType& i) const
  {
    return Dune::Functions::istlVectorBackend(isConstrained_)[toMultiIndex(i)];
  }

  /**
   * \brief Const access to bit-vector marking constrained DOFs
   */
  const BitVector& isConstrained() const
  {
    return isConstrained_;
  }

  /**
   * \brief Mutable access to bit-vector marking constrained DOFs
   */
  BitVector& isConstrained()
  {
    return isConstrained_;
  }

  /**
   * \brief Const access to linear part of affine map
   *
   * Assuming that the affine constraints are written as \f$ x = L x + c\f$
   * the result represents the sparse matrix \f$L\f$.
   */
  const LinearTerm& linearTerm() const
  {
    return interpolation_;
  }

  /**
   * \brief Mutable access to linear part of affine map
   *
   * Assuming that the affine constraints are written as \f$ x = L x + c\f$
   * the result represents the sparse matrix \f$L\f$.
   */
  LinearTerm& linearTerm()
  {
    return interpolation_;
  }

  /**
   * \brief Const access to i-th row of linear part of affine map
   *
   * Assuming that the affine constraints are written as \f$ x = L x + c\f$
   * the result represents the i-th row of the sparse matrix \f$L\f$.
   *
   * This method should only be used if the i-th DOF is constrained.
   * For non-constrained DOFs this returns an empty range instead
   * of of a range with one on the diagonal.
   */
  template<class MultiIndexType>
  const LinearTermRow& linearTerm(const MultiIndexType& i) const
  {
    auto it = interpolation_.find(toMultiIndex(i));
    if (it != interpolation_.end())
      return it->second;
    else
      return emptyInterpolationRow_;
  }

  /**
   * \brief Const access to constant part of affine map
   *
   * Assuming that the affine constraints are written as \f$ x = L x + c\f$
   * the result represents the vector \f$c\f$.
   */
  const ConstantTerm& constantTerm() const
  {
    return constantTerm_;
  }

  /**
   * \brief Mutable access to constant part of affine map
   *
   * Assuming that the affine constraints are written as \f$ x = L x + c\f$
   * the result represents the vector \f$c\f$.
   */
  ConstantTerm& constantTerm()
  {
    return constantTerm_;
  }

  /**
   * \brief Const access to i-th row of constant part of affine map
   *
   * Assuming that the affine constraints are written as \f$ x = L x + c\f$
   * the result represents the i-th row of the vector \f$c\f$.
   */
  template<class MultiIndexType>
  const auto& constantTerm(const MultiIndexType& i) const
  {
    return Dune::Functions::istlVectorBackend(constantTerm_)[toMultiIndex(i)];
  }

  /**
   * \brief Mutable access to i-th row of constant part of affine map
   *
   * Assuming that the affine constraints are written as \f$ x = L x + c\f$
   * the result represents the i-th row of the vector \f$c\f$.
   */
  template<class MultiIndexType>
  auto& constantTerm(const MultiIndexType& i)
  {
    return Dune::Functions::istlVectorBackend(constantTerm_)[i];
  }

  /**
   * \brief Interpolate vector according to affine constraints
   *
   * Assuming that the affine constraints are written as \f$ x = L x + c\f$
   * this overwrites the given vector \f$v\f$ by \f$L v+c\f$.
   *
   * This computed the values of constrained DOFs using the
   * interpolation weights while non-constrained DOFs remain
   * unchanged. After calling this methods, `v` satisfies the
   * constraints.
   */
  template<class Vector>
  void interpolate(Vector& v) const
  {
    auto vectorBackend = Dune::Functions::istlVectorBackend(v);
    Dune::Fufem::recursiveForEachVectorEntry(v, [&](auto&& vector_i, const auto& i)
    {
      if (isConstrained(i))
      {
        vector_i = constantTerm(i);
        for(const auto& [j, L_ij] : linearTerm(i))
          vector_i += L_ij * vectorBackend[j];
      }
    });
  }

  /**
   * \brief Extend matrix pattern for a constrained basis
   *
   * Assuming that the affine constraints are written as \f$ x = L x + c\f$
   * and the given pattern is able to store a sparse matrix \f$A\f$
   * this enlarges the pattern for storing \f$L^T A L\f$.
   */
  template<class MatrixPatternBackend, class Basis>
  void extendMatrixPattern(MatrixPatternBackend& patternBackend, const Basis& basis) const
  {
    auto localView = basis.localView();
    for(const auto& element : elements(basis.gridView()))
    {
      localView.bind(element);
      for(auto i_local : Dune::range(localView.tree().size()))
      {
        auto i = localView.index(localView.tree().localIndex(i_local));
        for(auto j_local : Dune::range(localView.tree().size()))
        {
          auto j = localView.index(localView.tree().localIndex(j_local));
          if (isConstrained(i))
          {
            if (isConstrained(j))
              for(const auto& [k, L_ik] : linearTerm(i))
                for(const auto& [l, L_jl] : linearTerm(j))
                  patternBackend.insertEntry(k, l);
            else
              for(const auto& [k, L_ik] : linearTerm(i))
                patternBackend.insertEntry(k, j);
          }
          else if (isConstrained(j))
          {
            for(const auto& [l, L_jl] : linearTerm(j))
              patternBackend.insertEntry(i, l);
          }
        }
      }
    }
  }

  /**
   * \brief Constrain matrix to a constrained basis
   *
   * Assuming that the affine constraints are written as \f$ x = L x + c\f$
   * this overwrites the given matrix \f$A\f$ by \f$L^T A L+\hat{I}\f$ where
   * \f$\hat{I}\f$ is the identity matrix constrained to the kernel of \f$L\f$
   * given by the constrained rows.
   */
  template<class Matrix>
  void constrainMatrix(Matrix& A) const
  {
    auto matrixBackend = Dune::Fufem::istlMatrixBackend(A);
    Dune::Fufem::recursiveForEachMatrixEntry(A, [&](auto&& matrix_ij, const auto& i, const auto& j)
    {
      auto equals = [](const auto& i, const auto& j) {
        if constexpr (std::is_same_v<decltype(i), decltype(j)>)
          return i==j;
        else
          return false;
      };
      if (isConstrained(i))
      {
        if (isConstrained(j))
          for(const auto& [k, L_ik] : linearTerm(i))
            for(const auto& [l, L_jl] : linearTerm(j))
              matrixBackend(k, l) += L_ik*L_jl * matrix_ij;
        else
          for(const auto& [k, L_ik] : linearTerm(i))
            matrixBackend(k, j) += L_ik * matrix_ij;
        matrix_ij = equals(i, j) ? 1 : 0;
      }
      else if (isConstrained(j))
      {
        for(const auto& [l, L_jl] : linearTerm(j))
          matrixBackend(i, l) += L_jl * matrix_ij;
        matrix_ij = equals(i, j) ? 1 : 0;
      }
    });
  }

  /**
   * \brief Constrain vector to a constrained basis
   *
   * Assuming that the affine constraints are written as \f$ x = L x + c\f$
   * this overwrites the given vector \f$b\f$ by \f$L^T b + c\f$.
   */
  template<class Vector>
  void constrainVector(Vector& b) const
  {
    auto vectorBackend = Dune::Functions::istlVectorBackend(b);
    Dune::Fufem::recursiveForEachVectorEntry(b, [&](auto&& vector_i, const auto& i)
    {
      if (isConstrained(i))
      {
        for(const auto& [k, L_ik] : linearTerm(i))
          vectorBackend[k] += L_ik * vector_i;
        vector_i = constantTerm(i);
      }
    });
  }

  /**
   * \brief Constrain linear system affine subspace satisfying the constraints
   *
   * Assuming that the affine constraints are written as \f$ x = L x + c\f$
   * this overwrites the given vector \f$b\f$ by \f$L^T(b-A c) + c\f$
   * and the matrix \f$A\f$ by \f$L^T A L + \hat{I}\f$ where
   * \f$\hat{I}\f$ is the identity matrix constrained to the kernel of \f$L\f$.
   */
  template<class Matrix, class Vector>
  void constrainLinearSystem(Matrix& A, Vector& b) const
  {
    A.mmv(constantTerm_, b);
    constrainMatrix(A);
    constrainVector(b);
  }

  /**
   * \brief Report the constraints to std::cout.
   */
  void report() const
  {
    for(const auto& [i, L_i] : interpolation_)
    {
      std::cout << "x_" << i << " = ";
      for(const auto& [j, L_ij] : L_i)
        std::cout << L_ij << " * x_" << j << " + " ;
      std::cout << constantTerm(i) << std::endl;
    }
    std::cout << "interpolated dofs : " << interpolation_.size() << std::endl;
  }

  /**
   * \brief Check constraints for consistency
   *
   * This will check if all constrained DOFs are interpolated from non-constrained ones only.
   */
  bool check() const
  {
    bool pass = true;
    for(const auto& [i, L_i] : interpolation_)
    {
      for(const auto& [j, L_ij] : L_i)
      {
        if (isConstrained(j))
        {
          std::cout << "Error: interpolation for constrained DOF " << i << " depends on constrained DOF " << j << std::endl;
          pass = false;
        }
      }
    }
    return pass;
  }

  /**
   * \brief Resolve dependencies of constrained DOFs on other constrained DOFs
   *
   * An `AffineConstraints` object can be setup by inserting interpolation
   * weights into the `linearTerm()` and `constantTerm()`. It may happen that some constrained
   * DOFs are interpolated from DOFs that are constrained themselves which
   * is not supported. To eliminate such cases the `resolveDependencies()`
   * method will recursively resolve such dependencies such that all constrained
   * DOFs are interpolated from non-constrained ones only. Notice that this
   * requires that there are no cyclic dependencies.
   */
  void resolveDependencies()
  {
    auto dependenciesResolved = isConstrained_;
    auto dependenciesResolvedBackend = Dune::Functions::istlVectorBackend(dependenciesResolved);
    dependenciesResolvedBackend = false;
    for(auto& [i, L_i] : interpolation_)
      if (not(dependenciesResolvedBackend[i]))
        resolveDOF(dependenciesResolved, i, L_i);
  }

private:

  void resolveDOF(BitVector& dependenciesResolved, const MultiIndex& i, LinearTermRow& L_i)
  {
    auto dependenciesResolvedBackend = Dune::Functions::istlVectorBackend(dependenciesResolved);
    auto c = Dune::Functions::istlVectorBackend(constantTerm_);
    auto it = L_i.begin();
    while (it != L_i.end())
    {
      auto& [j, L_ij] = *it;
      // If interpolation weight is itself constraint,
      // first resolve it recursively (if needed)
      // and then insert all indirect dependencies directly
      if (isConstrained(j))
      {
        c[i] += L_ij * c[j];
        auto L_j_it = interpolation_.find(j);
        if (L_j_it != interpolation_.end())
        {
          auto& L_j = L_j_it->second;
          if (not(dependenciesResolvedBackend[j]))
            resolveDOF(dependenciesResolved, j, L_j);
          for (const auto& [k, L_jk] : L_j)
            L_i[k] += L_ij * L_jk;
        }
        else
          dependenciesResolvedBackend[j] = true;
        it = L_i.erase(it);
      }
      else
        ++it;
    }
    dependenciesResolvedBackend[i] = true;
  }

  BitVector isConstrained_;
  LinearTerm interpolation_;
  LinearTermRow emptyInterpolationRow_;
  ConstantTerm constantTerm_;
};



/**
 * \brief Create and initialize an `AffineConstraints` object for a basis
 * \ingroup Constraints
 *
 * \tparam BitVector A bit vector type suitable for the global indices of the basis
 * \tparam Vector A coefficient vector type suitable for the global indices of the basis
 * \tparam Basis Type of the global basis
 *
 * \param basis A global basis
 *
 * If the template default value `std::monostate` is used for the template
 * parameters `BitVector` or `Vector` then the actual container types are deduced
 * using `Dune::Functions::makeContainer(...)` and `Dune::Functions::makeISTLVector(...)`
 * from the container descriptor provided by `basis` (only with dune-function>=2.11).
 */
template<class BitVector=std::monostate, class Vector=std::monostate, class Basis>
auto makeAffineConstraints(const Basis& basis)
{
  using MultiIndex = typename Basis::MultiIndex;
  using Coefficient = double;

  auto generateBitVector = [&] {
    if constexpr (std::is_same_v<BitVector, std::monostate>)
    {
#if DUNE_VERSION_GTE(DUNE_FUNCTIONS, 2, 11)
      return Dune::Functions::makeContainer<char>(basis.rootBasis().preBasis().containerDescriptor(), false);
#else
      static_assert(std::is_same_v<BitVector, std::monostate>, "Deducing a container-type in makeAffineConstraints requires dune-functions>=2.11.");
#endif
    }
    else
    {
      auto v = BitVector();
      Dune::Functions::istlVectorBackend(v).resize(basis);
      Dune::Functions::istlVectorBackend(v) = false;
      return v;
    }
  };
  auto generateVector = [&] {
    if constexpr (std::is_same_v<Vector, std::monostate>)
    {
#if DUNE_VERSION_GTE(DUNE_FUNCTIONS, 2, 11)
      return Dune::Functions::makeISTLVector<Coefficient>(basis.rootBasis().preBasis().containerDescriptor());
#else
      static_assert(std::is_same_v<Vector, std::monostate>, "Deducing a vector-type in makeAffineConstraints requires dune-functions>=2.11.");
#endif
    }
    else
    {
      auto v = Vector();
      Dune::Functions::istlVectorBackend(v).resize(basis);
      Dune::Functions::istlVectorBackend(v) = false;
      return v;
    }
  };
  using GeneratedBitVector = decltype(generateBitVector());
  using GeneratedVector = decltype(generateVector());
  return AffineConstraints<GeneratedBitVector, GeneratedVector, MultiIndex, Coefficient>(generateBitVector(), generateVector());
}



} // namespace Dune::Fufem



#endif // DUNE_FUFEM_CONSTRAINTS_AFFINECONSTRAINTS_HH
