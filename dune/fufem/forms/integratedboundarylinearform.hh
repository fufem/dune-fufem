// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_INTEGRATEDBOUNDARYLINEARFORM_HH
#define DUNE_FUFEM_FORMS_INTEGRATEDBOUNDARYLINEARFORM_HH

#include <cstddef>
#include <type_traits>
#include <utility>

#include <dune/common/rangeutilities.hh>

#include <dune/typetree/childextraction.hh>

#include <dune/fufem/boundarypatch.hh>

#include <dune/fufem/forms/shapefunctioncache.hh>
#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/localsumassembler.hh>



namespace Dune::Fufem::Forms {



  /**
   * \brief Local assembler corresponding to a boundary linear form
   *
   * \ingroup FormsUserInterfaceAssemblers
   *
   * \tparam LinearOperator A linear SumOperator to integrate
   * \tparam Patch The BoundaryPatch to integrate over
   *
   * This assembler computes boundary integrals
   * over all intersections contained in the BoundaryPatch.
   *
   * Local assemblers of the same arity can be chained using operator+.
   */
  template<class LinearOperator, class Patch>
  class IntegratedBoundaryLinearForm
  {
    using TestRootBasis = std::decay_t<decltype(std::get<0>(std::declval<LinearOperator>().basis()))>;
    using TestRootLocalView = typename TestRootBasis::LocalView;
    using TestRootTree = typename TestRootLocalView::Tree;

    using LocalOperator = decltype(localOperator(std::declval<LinearOperator>()));

    using FacetKey = typename MultipleQuadratureCacheManager<double, LinearOperator::Element::dimension>::FacetKey;

  public:

    using Element = typename LinearOperator::Element;
    using Intersection = typename Patch::GridView::Intersection;
    using IntersectionIterator = typename Patch::GridView::IntersectionIterator;

    IntegratedBoundaryLinearForm(const LinearOperator& sumOperator, const Patch& patch) :
      sumOperator_(sumOperator),
      sumLocalOperator_(localOperator(sumOperator_)),
      patch_(patch)
    {}

    /**
     * \brief Register local view
     *
     * This has to be called once, before using the assembler.
     * The passed local view must be the same that is used
     * when calling the assembler for on an element afterwards.
     */
    template<class TestLocalView>
    void preprocess(const TestLocalView& testLocalView)
    {
      cacheManager_.clear();
      sumLocalOperator_.registerLocalViews(testLocalView.rootLocalView());
      sumLocalOperator_.registerCaches(cacheManager_.prototype());
    }

    template<class LocalVector, class TestLocalView>
    void operator()(const Element& element, LocalVector& localVector, const TestLocalView& testSubspaceLocalView)
    {
      if (not patch_.containsFaceOf(element))
        return;
      for(const auto& intersection : intersections(patch_.gridView(), element))
        if (patch_.contains(intersection))
          this->operator()(intersection, localVector, testSubspaceLocalView);
    }

    template<class LocalVector, class TestLocalView>
    void operator()(const Intersection& intersection, LocalVector& localVector, const TestLocalView& testSubspaceLocalView)
    {
      auto facet = intersection.indexInInside();

      const auto& geometry = intersection.geometry();

      sumLocalOperator_.bind(intersection.inside());

      Impl::forEachTupleEntry(sumLocalOperator_.operators(), [&](auto& op) {
        cacheManager_[FacetKey(op.quadratureRuleKey(), facet)].invalidate();
      });

      Impl::forEachTupleEntry(sumLocalOperator_.operators(), [&](auto& op) {
        auto& cacheForRule = cacheManager_[FacetKey(op.quadratureRuleKey(), facet)];
        op.bindToCaches(cacheForRule);
        const auto& quadRule = cacheForRule.rule();
        for (auto k : Dune::range(quadRule.size()))
        {
          const auto& quadPoint = quadRule[k];
          auto quadPointPositionInFacet = intersection.geometryInInside().local(quadPoint.position());
          const auto integrationWeight = quadPoint.weight() * geometry.integrationElement(quadPointPositionInFacet);
          auto evaluatedOperator = op(IndexedReference(quadPoint.position(), k));
          axpy(integrationWeight, evaluatedOperator, localVector);
        }
      });
    }

    const LinearOperator& integrandOperator() const
    {
      return sumOperator_;
    }

  private:
    const LinearOperator sumOperator_;
    LocalOperator sumLocalOperator_;
    MultipleQuadratureCacheManager<double, Element::dimension> cacheManager_;
    const Patch& patch_;
  };



  template<class LinearOperator, class Patch>
  struct IsLocalAssembler<IntegratedBoundaryLinearForm<LinearOperator, Patch>> : public std::true_type {};



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_INTEGRATEDBOUNDARYLINEARFORM_HH
