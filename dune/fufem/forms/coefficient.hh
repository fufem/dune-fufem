// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_COEFFICIENT_HH
#define DUNE_FUFEM_FORMS_COEFFICIENT_HH

#include <type_traits>
#include <utility>

#include <dune/common/typetraits.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/fufem/forms/baseclass.hh>



namespace Dune::Fufem::Forms {



  /**
   * \brief Coefficient function
   *
   * \ingroup FormsUserInterfaceElementary
   *
   * \tparam F Type of the wrapped GridFunction
   *
   * This wraps a GridFunction in the sense of Dune::Functions
   * and provides the extended interface of a 0-linear operator
   * required to use the function as coefficient within Dune::Fufem::Forms.
   */
  template<class F>
  class Coefficient : public F, public MultilinearOperator<0>
  {
    using LocalFunction = std::decay_t<decltype(localFunction(std::declval<F&>()))>;
    using LocalDomain = typename F::EntitySet::Element::Geometry::LocalCoordinate;

  public:

    using Element = typename F::EntitySet::Element;
    using Range = decltype(std::declval<LocalFunction>()(std::declval<LocalDomain>()));

    Coefficient(const F& f) :
      F(f),
      order_(0)
    {}

    Coefficient(const F& f, std::size_t order) :
      F(f),
      order_(order)
    {}

    class LocalOperator : public LocalFunction
    {
    public:

      using Element = typename Coefficient::Element;
      using Range = typename Coefficient::Range;

      LocalOperator(LocalFunction&& fLocal, std::size_t order) :
        LocalFunction(std::move(fLocal)),
        order_(order)
      {}

      auto quadratureRuleKey() const
      {
        return QuadratureRuleKey(LocalFunction::localContext().type(), order_);
      }

      template<class... LV>
      void registerLocalViews(const LV&... lvs)
      {}

      template<class CacheManager>
      void registerCaches(CacheManager& cacheManager)
      {}

      template<class CacheManager>
      void bindToCaches(CacheManager& cacheManager)
      {}

    private:
      std::size_t order_;
    };

    friend LocalOperator localOperator(const Coefficient& coefficient)
    {
      return LocalOperator(localFunction(coefficient), coefficient.order_);
    }

    auto basis() const
    {
      return std::tuple<>();
    }

    auto treePath() const
    {
      return std::tuple<>();
    }

  private:
    std::size_t order_;
  };



} // namespace Dune::Fufem::Forms



#endif // DUNE_FUFEM_FORMS_COEFFICIENT_HH
