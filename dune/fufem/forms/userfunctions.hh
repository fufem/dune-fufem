// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_USERFUNCTIONS_HH
#define DUNE_FUFEM_FORMS_USERFUNCTIONS_HH

#include <cstddef>
#include <type_traits>
#include <utility>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/scaledidmatrix.hh>

#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/unaryoperators.hh>
#include <dune/fufem/forms/transformedoperator.hh>
#include <dune/fufem/forms/productoperator.hh>
#include <dune/fufem/forms/sumoperator.hh>
#include <dune/fufem/forms/boundunaryoperator.hh>
#include <dune/fufem/forms/localoperators.hh>



namespace Dune::Fufem::Forms::Impl {



  // ************************************************************
  // Implementation of compose(outerOp, innerOp)
  // ************************************************************

  template<class OuterTransform, class InnerTransform, class InnerOp,
    std::enable_if_t<isOperator_v<InnerOp>, int> = 0>
  auto composeImpl(const OuterTransform& outerTransform, const TransformedOperator<InnerTransform, InnerOp>& op)
  {
    return TransformedOperator(LocalOperators::localCompose(outerTransform, op.transformation()), op.baseOperator());
  }

  template<class OuterOp, class InnerOp,
    std::enable_if_t<isOperator_v<InnerOp>, int> = 0>
  auto composeImpl(const OuterOp& outerOp, const InnerOp& op)
  {
    return TransformedOperator(outerOp, op);
  }

  template<class OuterOp, class... InnerOps>
  auto composeImpl(const OuterOp& outerOp, const Dune::Fufem::Forms::SumOperator<InnerOps...>& innerOps)
  {
    if constexpr(SumOperator<InnerOps...>::arity==0)
      return TransformedOperator(outerOp, innerOps);
    else
      return std::apply([&](const auto&...op_i) {
          return Dune::Fufem::Forms::SumOperator(composeImpl(outerOp, op_i) ...);
      }, innerOps.operators());
  }



  // ************************************************************
  // Implementation of sum(l,r).
  // ************************************************************

  // This has four special cases to collapse nested sums
  // and two for the case that one argument is a constant.

  template<class L, class R,
    std::enable_if_t<isOperator_v<L> and isOperator_v<R>, int> = 0>
  auto sum(const L& l, const R& r)
  {
    return SumOperator<L, R>(l, r);
  }

  template<class L, class... Ri,
    std::enable_if_t<isOperator_v<L>, int> = 0>
  auto sum(const L& l, const SumOperator<Ri...>& r)
  {
    return std::apply([&](const auto&... ri) {
        return SumOperator<L, Ri...>(l, ri...);
    }, r.operators());
  }

  template<class... Li, class R,
    std::enable_if_t<isOperator_v<R>, int> = 0>
  auto sum(const SumOperator<Li...>& l, const R& r)
  {
    return std::apply([&](const auto&... li) {
        return SumOperator<Li..., R>(li..., r);
    }, l.operators());
  }

  template<class... Li, class... Rj>
  auto sum(const SumOperator<Li...>& l, const SumOperator<Rj...>& r)
  {
    return std::apply([&](const auto&...li) {
      return std::apply([&](const auto&...rj) {
          return SumOperator<Li..., Rj...>(li..., rj...);
      }, r.operators());
    }, l.operators());
  }

  // Overload for for the case that R is a constant
  template<class L, class R,
    std::enable_if_t<isOperatorOrSumOperator_v<L> and not isOperatorOrSumOperator_v<R>, int> = 0>
  auto sum(const L& l, const R& r)
  {
    return Impl::composeImpl([r](auto&& l_value) { return l_value+r; }, l);
  }

  // Overload for for the case that L is a constant
  template<class L, class R,
    std::enable_if_t<not isOperatorOrSumOperator_v<L> and isOperatorOrSumOperator_v<R>, int> = 0>
  auto sum(const L& l, const R& r)
  {
    return Impl::composeImpl([l](auto&& r_value) { return l+r_value; }, r);
  }



  // ************************************************************
  // Implementation of product(p, left, right)
  // ************************************************************

  // The following are the generic functions for producing
  // product operators from a binary operator and two other
  // terms. At least one of those terms should be a operator.

  // Overload for two plain operators
  template<class Op, class L, class R,
    std::enable_if_t<isOperator_v<L> and isOperator_v<R>, int> = 0,
    std::enable_if_t<Dune::IsCallable<Op(typename L::Range, typename R::Range)>::value, int> = 0>
  auto productImpl(const Op& op, const L& l, const R& r)
  {
    static_assert(L::arity+R::arity<=2, "Trying to construct multi-linear operator with arity>2");

    if constexpr((L::arity==1) and (L::arity==1))
    {
      if constexpr(L::argIndex==1)
        return ProductOperator(LocalOperators::TransposedBinaryOp(op), r, l);
      else
        return ProductOperator(op, l, r);
    }
    else
      return ProductOperator(op, l, r);
  }

  // Overload for one raw value and one operator
  // This will wrap the raw value into a operator.
  template<class Op, class L, class R,
    std::enable_if_t<(not isOperator_v<L>) and isOperator_v<R>, int> = 0,
    std::enable_if_t<Dune::IsCallable<Op(L, typename R::Range)>::value, int> = 0>
  auto productImpl(const Op& op, const L& l, const R& r)
  {
    using namespace std::placeholders;
    return composeImpl(std::bind(op, l, _1), r);
  }

  // Overload for one operator and one raw value
  // This will wrap the raw value into a operator.
  template<class Op, class L, class R,
    std::enable_if_t<isOperator_v<L> and (not isOperator_v<R>), int> = 0,
    std::enable_if_t<Dune::IsCallable<Op(typename L::Range, R)>::value, int> = 0>
  auto productImpl(const Op& op, const L& l, const R& r)
  {
    using namespace std::placeholders;
    return composeImpl(std::bind(op, _1, r), l);
  }

  // Overload for one sum-operator and one plain term
  // This will multiply out the product for the sum-operator.
  template<class Op, class... Li, class R,
    class = std::void_t<decltype((productImpl(std::declval<Op>(), std::declval<Li>(), std::declval<R>()),...))>>
  auto productImpl(const Op& op, const SumOperator<Li...>& l, const R& r)
  {
    return std::apply([&](const auto&... li) {
        return (productImpl(op, li, r) + ...);
    }, l.operators());
  }

  // Overload for one plain term and one sum-operator
  // This will multiply out the product for the sum-operator.
  template<class Op, class L, class... Ri,
    class = std::void_t<decltype((productImpl(std::declval<Op>(), std::declval<L>(), std::declval<Ri>()),...))>>
  auto productImpl(const Op& op, const L& l, const SumOperator<Ri...>& r)
  {
    return std::apply([&](const auto&...ri) {
        return (productImpl(op, l, ri) + ...);
    }, r.operators());
  }

  // Overload for two sum-operators
  // This will multiply out the product for both sum-operators.
  template<class Op, class... Li, class... Rj,
    class = std::void_t<decltype((productImpl(std::declval<Op>(), std::declval<Li>(), std::declval<SumOperator<Rj...>>()),...))>>
  auto productImpl(const Op& op, const SumOperator<Li...>& l, const SumOperator<Rj...>& r)
  {
    return std::apply([&](const auto&...li) {
      return (productImpl(op, li, r) + ...);
    }, l.operators());
  }



  // ************************************************************
  // Implementation of dot(l, r).
  // ************************************************************

  // These specializations only exist for optimization of special cases.

  // This is the default implementation.
  template<class L, class R,
    std::enable_if_t<isOperatorOrSumOperator_v<L> or isOperatorOrSumOperator_v<R>, int> = 0,
    class = std::void_t<decltype(productImpl(std::declval<LocalOperators::DotOp>(), std::declval<L>(), std::declval<R>()))>>
  auto dotImpl(const L& l, const R& r)
  {
    return productImpl(Dune::Fufem::Forms::LocalOperators::DotOp(), l, r);
  }

  template<class B, class TP, std::size_t argIndex_L, std::size_t argIndex_R>
  auto dotImpl(const FEFunctionOperator<B,TP,argIndex_L>& l, const FEFunctionOperator<B,TP,argIndex_R>& r);

  template<class B, class TP, std::size_t argIndex_L, std::size_t argIndex_R>
  auto dotImpl(const FEFunctionJacobianOperator<B,TP,argIndex_L>& l, const FEFunctionJacobianOperator<B,TP,argIndex_R>& r);

  template<class InnerOp>
  using TransposedOperator = TransformedOperator<Dune::Fufem::Forms::LocalOperators::TransposeOp, InnerOp>;

  template<class L, class R>
  auto dotImpl(const Impl::TransposedOperator<L>& l, const Impl::TransposedOperator<R>& r)
  {
    using namespace Dune::Indices;
    return Dune::Fufem::Forms::Impl::dotImpl(l.baseOperator(), r.baseOperator());
  }

  template<class B, class TP, std::size_t argIndex_L, std::size_t argIndex_R>
  auto dotImpl(const FEFunctionOperator<B,TP,argIndex_L>& l, const FEFunctionOperator<B,TP,argIndex_R>& r)
  {
    using Node = typename Dune::TypeTree::ChildForTreePath<typename B::LocalView::Tree, TP>;
    if constexpr (Node::isPower)
    {
      return unpackIntegerSequence(
        [&](auto... i) {
          return (Dune::Fufem::Forms::Impl::dotImpl(l.childOperator(i), r.childOperator(i)) + ...);
        },
        std::make_index_sequence<Node::degree()>{});
    }
    else
      return productImpl(Dune::Fufem::Forms::LocalOperators::DotOp(), l, r);
  }

  template<class B, class TP, std::size_t argIndex_L, std::size_t argIndex_R>
  auto dotImpl(const FEFunctionJacobianOperator<B,TP,argIndex_L>& l, const FEFunctionJacobianOperator<B,TP,argIndex_R>& r)
  {
    using Node = typename Dune::TypeTree::ChildForTreePath<typename B::LocalView::Tree, TP>;
    if constexpr (Node::isPower)
    {
      return unpackIntegerSequence(
        [&](auto... i) {
          return (Dune::Fufem::Forms::Impl::dotImpl(l.childOperator(i), r.childOperator(i)) + ...);
        },
        std::make_index_sequence<Node::degree()>{});
    }
    else
      return productImpl(Dune::Fufem::Forms::LocalOperators::DotOp(), l, r);
  }



} // namespace Dune::Fufem::Forms::Impl



namespace Dune::Fufem::Forms {



  // ************************************************************
  // Linear combination of operators of the same arity
  // ************************************************************

  /**
   * \brief Create the sum of two operators
   *
   * \ingroup FormsUserInterfaceArithmetic
   *
   * \param l First summand
   * \param r Second summand
   *
   * The actual implementation is constrained to a few special cases:
   * Either both summands have the same arity, or exactly one is nullary and
   * the other one is a constant value.
   * In the former case both operators will be combined into a SumOperator.
   * If l, r, or both are SumOperator's then all summands
   * are combined into a large SumOperator.
   */
#ifdef DOXYGEN
  template<class L, class R>
  auto operator+ (const L& l, const R& r);
#else
  // Overload for two operators of the same arity
  template<class L, class R,
    std::enable_if_t<isOperatorOrSumOperator_v<L> or isOperatorOrSumOperator_v<R>, int> = 0>
  auto operator+ (const L& l, const R& r)
  {
    if constexpr (isOperatorOrSumOperator_v<L> and isOperatorOrSumOperator_v<R>)
      static_assert(L::arity==R::arity, "The operators passed to operator+ do not have the same arity");
    else if constexpr (isOperatorOrSumOperator_v<R>)
      static_assert(R::arity==0, "Plain values can only be added to 0-linear operators");
    else if constexpr (isOperatorOrSumOperator_v<L>)
      static_assert(L::arity==0, "Plain values can only be added to 0-linear operators");
    return Impl::sum(l, r);
  }
#endif

  /**
   * \brief Negate an operator
   *
   * \ingroup FormsUserInterfaceArithmetic
   *
   * \param op Operator to negate (should be a MultilinearOperator or SumOperator)
   *
   * This transform the operator by pointwise negation.
   */
  template<class Op,
    std::enable_if_t<isOperatorOrSumOperator_v<Op>, int> = 0>
  auto operator- (const Op& op)
  {
    return Impl::composeImpl(std::negate(), op);
  }

  /**
   * \brief Create the difference of two operators
   *
   * \ingroup FormsUserInterfaceArithmetic
   *
   * \param l Minuend
   * \param r Subtrahend
   *
   * The actual implementation is constrained to a few special cases:
   * Either both arguments have the same arity, or exactly one is nullary and
   * the other one is a constant value.
   * This is a shortcut for l+(-r).
   */
#ifdef DOXYGEN
  template<class L, class R>
  auto operator- (const L& l, const R& r);
#else
  template<class L, class R,
    std::enable_if_t<isOperatorOrSumOperator_v<L> or isOperatorOrSumOperator_v<R>, int> = 0>
  auto operator- (const L& l, const R& r)
  {
    if constexpr (isOperatorOrSumOperator_v<L> and isOperatorOrSumOperator_v<R>)
      static_assert(L::arity==R::arity, "The operators passed to operator- do not have the same arity");
    else if constexpr (isOperatorOrSumOperator_v<R>)
      static_assert(R::arity==0, "Differences with plain values can only be used with 0-linear operators");
    else if constexpr (isOperatorOrSumOperator_v<L>)
      static_assert(L::arity==0, "Differences with plain values can only be used with 0-linear operators");
    return Impl::sum(l, -r);
  }
#endif



  // ************************************************************
  // Bilinear combination of operators
  // ************************************************************

  /**
   * \brief Generic exterior product of two multilinear operators
   *
   * \ingroup FormsUserInterfaceArithmetic
   *
   * \param op Local bilinear operator
   * \param l First factor (should be a constant, MultilinearOperator, or SumOperator)
   * \param r Second factor (should be a constant, MultilinearOperator, or SumOperator)
   *
   * This combines an m-linear and an n-linear operator to an (m+n)-linear operator.
   * At most one of the two factors can be a constant instead of a MultilinearOperator
   * or SumOperator. In this case it is treated like a 0-linear operator which is
   * constant in space.
   * The values in the associated function space range will be combined using the local
   * pointwise bilinear operator op.
   */
  template<class Op, class L, class R,
    std::enable_if_t<isOperatorOrSumOperator_v<L> or isOperatorOrSumOperator_v<R>, int> = 0,
    class = std::void_t<decltype(Impl::productImpl(std::declval<Op>(), std::declval<L>(), std::declval<R>()))>>
  auto product(const Op& op, const L& l, const R& r)
  {
    return Impl::productImpl(op, l, r);
  }

  /**
   * \brief Exterior product of two multilinear operators based on pointwise multiplication.
   *
   * \ingroup FormsUserInterfaceArithmetic
   *
   * \param l First factor (should be a constant, MultilinearOperator, or SumOperator)
   * \param r Second factor (should be a constant, MultilinearOperator, or SumOperator)
   *
   * This combines an m-linear and an n-linear operator to an (m+n)-linear operator.
   * At most one of the two factors can be a constant instead of a MultilinearOperator
   * or SumOperator. In this case it is treated like a 0-linear operator which is
   * constant in space.
   * The values in the associated function space range will be combined using the local
   * pointwise multiplication (supporting several combinations of matrix/vector/scalar products).
   */
  template<class L, class R,
    std::enable_if_t<isOperatorOrSumOperator_v<L> or isOperatorOrSumOperator_v<R>, int> = 0,
    class = std::void_t<decltype(product(std::declval<LocalOperators::MultOp>(), std::declval<L>(), std::declval<R>()))>>
  auto operator* (const L& l, const R& r)
  {
    // This overload is recursively constrained to cases where `operator*` makes sense.
    // Otherwise Dune::dot(a,b) may be activated for operators because it relies on availability
    // of a*b. This is a problem, because it leads to ambiguity with Forms::dot(a,b).
    return product(LocalOperators::MultOp{}, l, r);
  }

  /**
   * \brief Exterior product of two multilinear operators based on pointwise dot-product.
   *
   * \ingroup FormsUserInterfaceArithmetic
   *
   * \param l First factor (should be a constant, MultilinearOperator, or SumOperator)
   * \param r Second factor (should be a constant, MultilinearOperator, or SumOperator)
   *
   * This combines an m-linear and an n-linear operator to an (m+n)-linear operator.
   * At most one of the two factors can be a constant instead of a MultilinearOperator
   * or SumOperator. In this case it is treated like a 0-linear operator which is
   * constant in space.
   * The values in the associated function space range will be combined using the local
   * pointwise dot product.
   */
  template<class L, class R,
    std::enable_if_t<isOperatorOrSumOperator_v<L> or isOperatorOrSumOperator_v<R>, int> = 0,
    class = std::void_t<decltype(product(std::declval<LocalOperators::DotOp>(), std::declval<L>(), std::declval<R>()))>>
  auto dot(const L& l, const R& r)
  {
    return Impl::dotImpl(l, r);
  }



  // ************************************************************
  // Utilities for range transformations
  // ************************************************************

  /**
   * \brief Generic composition of a multilinear operators with a pointwise outer operator
   *
   * \ingroup FormsUserInterfaceLinearOperators
   *
   * \param outerOp Pointwise outer operator
   * \param innerOp Inner operator (should be a MultilinearOperator or SumOperator)
   *
   * Given an m-linear operator innerOp this returns another m-linear operator,
   * where outerOp is applied pointwise in the associated function space range.
   */
  template<class OuterOp, class InnerOp,
    std::enable_if_t<isOperatorOrSumOperator_v<InnerOp>, int> = 0,
    std::enable_if_t<Dune::IsCallable<OuterOp(typename InnerOp::Range)>::value, int> = 0>
  auto compose(const OuterOp& outerOp, const InnerOp& innerOp)
  {
    return Impl::composeImpl(outerOp, innerOp);
  }



  /**
   * \brief Adaptor for making a callback compatible with Dune::Fufem::Forms
   *
   * \ingroup FormsUserInterfaceLinearOperators
   *
   * \tparam OuterOp Pointwise outer operator
   *
   * When wrapping a simple callback f in a rf = RangeOperator(f), the latter
   * can be used flexibiliy in two ways: For simple argumemts x calling
   * rf(x) yields the same result as f(x). If the argument is a
   * MultilinearOperator or SumOperator, then calling rf(x) yields
   * a corresponding operator obtained by applying f pointwise on x.
   * While the latter can also be achieved using compose(f,x), using
   * a RangeOperator gives the freedom to apply the same function
   * regardless of the argument type.
   * It is also supported to pass additional arguments. The latter
   * will be forwarded to f and captured by value in the operator case.
   */
  template<class OuterOp>
  class RangeOperator
  {
  public:
    RangeOperator(const OuterOp& outerOp) : outerOp_(outerOp) {}

    template<class Op, class... Args,
      std::enable_if_t<isOperatorOrSumOperator_v<Op>, int> = 0>
    auto operator()(Op&& baseOperator, Args... args) const
    {
      using namespace std::placeholders;
      return compose(std::bind(outerOp_, _1, args...), std::forward<Op>(baseOperator));
    }

    template<class Op,
      std::enable_if_t<isOperatorOrSumOperator_v<Op>, int> = 0>
    auto operator()(Op&& baseOperator) const
    {
      return compose(outerOp_, std::forward<Op>(baseOperator));
    }

    template<class Arg0, class... Args,
      std::enable_if_t<not isOperatorOrSumOperator_v<Arg0>, int> = 0>
    auto operator()(Arg0&& arg0, Args&&... args) const
    {
      return outerOp_(std::forward<Arg0>(arg0), std::forward<Args>(args)...);
    }

  private:
    OuterOp outerOp_;
  };



  // ************************************************************
  // Predefined linear range transformations
  // ************************************************************

  /**
   * \brief Transform an operator by pointwise matrix transposition
   *
   * \ingroup FormsUserInterfaceLinearOperators
   *
   * \param op The operator to transform (should be a constant, MultilinearOperator, or SumOperator)
   *
   * Given an m-linear operator op this returns another m-linear operator,
   * where a pointwise transformation is applied in the associated function space range.
   * If op is a constant, the transformation is applied directly.
   * The pointwise transformation applies a matrix transposition.
   */
  template<class Op>
  auto transpose(const Op& op)
  {
    return RangeOperator(Dune::Fufem::Forms::LocalOperators::TransposeOp())(op);
  }

  /**
   * \brief Transform an operator by pointwise matrix symmetrization
   *
   * \ingroup FormsUserInterfaceLinearOperators
   *
   * \param op The operator to transform (should be a constant, MultilinearOperator, or SumOperator)
   *
   * Given an m-linear operator op this returns another m-linear operator,
   * where a pointwise transformation is applied in the associated function space range.
   * If op is a constant, the transformation is applied directly.
   * The pointwise transformation applies a matrix symmetrization.
   */
  template<class Op>
  auto symmetrize(const Op& op)
  {
    return RangeOperator(Dune::Fufem::Forms::LocalOperators::SymOp())(op);
  }

  /**
   * \brief Transform an operator by pointwise computation of the matrix trace
   *
   * \ingroup FormsUserInterfaceLinearOperators
   *
   * \param op The operator to transform (should be a constant, MultilinearOperator, or SumOperator)
   *
   * Given an m-linear operator op this returns another m-linear operator,
   * where a pointwise transformation is applied in the associated function space range.
   * If op is a constant, the transformation is applied directly.
   * The pointwise transformation computes the matrix trace.
   */
  template<class Op>
  auto trace(const Op& op)
  {
    return RangeOperator(Dune::Fufem::Forms::LocalOperators::TraceOp())(op);
  }



  // ************************************************************
  // Predefined non-linear range transformations
  // ************************************************************

  /**
   * \brief Transform an operator by pointwise computation of the matrix inverse
   *
   * \ingroup FormsUserInterfaceNonLinearOperators
   *
   * \param op The operator to invert poitwise (should be a constant, or 0-linear MultilinearOperator or SumOperator)
   *
   * Given an 0-linear operator op this returns another 0-linear operator,
   * where a pointwise transformation is applied in the associated function space range.
   * If op is a constant, the transformation is applied directly.
   * The pointwise transformation computes the matrix inverse.
   *
   * Since this is a nonlinear operation, it is only available if op is 0-linear.
   */
  template<class Op>
  auto inv(const Op& op)
  {
    if constexpr(isOperatorOrSumOperator_v<Op>)
      static_assert(Op::arity==0, "inv(op) can only be applied to 0-linear operators");
    return RangeOperator(Dune::Fufem::Forms::LocalOperators::InvertOp())(op);
  }

  /**
   * \brief Exterior product of a multilinear operator with the pointwise inverse of 0-linear operator
   *
   * \ingroup FormsUserInterfaceArithmetic
   *
   * \param l Nominator (should be a constant, MultilinearOperator, or SumOperator)
   * \param r Denominator (should be a constant, or 0-linear MultilinearOperator or SumOperator)
   *
   * This combines an m-linear and an 0-linear operator to an m-linear operator.
   * At most one of the two factors can be a constant instead of a MultilinearOperator
   * or SumOperator. In this case it is treated like a 0-linear operator which is
   * constant in space.
   * The values in the associated function space range will be combined using the local
   * pointwise multiplication of the first factor with the inverse of the second one.
   *
   * Since this is a nonlinear operation, it is only available if r is 0-linear.
   */
  template<class L, class R,
    std::enable_if_t<isOperatorOrSumOperator_v<L> or isOperatorOrSumOperator_v<R>, int> = 0,
    class = std::void_t<decltype(product(std::declval<LocalOperators::MultOp>(), std::declval<L>(), inv(std::declval<R>())))>>
  auto operator/ (const L& l, const R& r)
  {
    // Since inv() does not exist if R is a non-nullary operator,
    // we don't need to guard this further here.
    return product(LocalOperators::MultOp{}, l, inv(r));
  }



  // ************************************************************
  // Convenience functions for creating operators
  // ************************************************************

  /**
   * \brief Tag type to classify non-affine finite element families
   *
   * \ingroup FormsUserInterfaceElementary
   *
   * Pass this flag to trialFunction(...) or testFunction(...) if
   * the finite element family is not affine in the following sense:
   * A family is called affine, if the local finite elements behaves
   * identical on two elements whenever they have the same geometry type.
   *
   * This property allows to cache evaluation of shape functions on
   * quadrature rules across elements.
   *
   * If this flag is passed, any caching of shape function evaluations
   * across different elements is disabled.
   */
  class NonAffineFamiliy {};

  /**
   * \brief Create unary identity operator on test function space
   *
   * \ingroup FormsUserInterfaceElementary
   *
   * This creates an operator representing the identity on a discrete
   * function space.
   * The arguments of this operator are interpreted as test functions.
   *
   * Basis function evaluations will be cached across elements with
   * same geometry type.
   *
   * \param basis Global basis of the test function space
   */
  template<class Basis>
  auto testFunction(const Basis& basis)
  {
    using RootBasis = std::decay_t<decltype(basis.rootBasis())>;
    return FEFunctionOperator<RootBasis, typename Basis::PrefixPath, 0>(basis.rootBasis(), basis.prefixPath());
  }

  /**
   * \brief Create unary identity operator on test function space
   *
   * \ingroup FormsUserInterfaceElementary
   *
   * This creates an operator representing the identity on a discrete
   * function space.
   * The arguments of this operator are interpreted as test functions.
   *
   * By passing an additional NonAffineFamiliy tag, caching of
   * basis functions across elements with same geometry type
   * is disabled.
   *
   * \param basis Global basis of the test function space
   * \param tag Tag argument to disable cross-element caching
   */
  template<class Basis>
  auto testFunction(const Basis& basis, NonAffineFamiliy tag)
  {
    using RootBasis = std::decay_t<decltype(basis.rootBasis())>;
    return FEFunctionOperator<RootBasis, typename Basis::PrefixPath, 0>(basis.rootBasis(), basis.prefixPath(), false);
  }

  /**
   * \brief Create unary identity operator on test function space
   *
   * \ingroup FormsUserInterfaceElementary
   *
   * This creates an operator representing the identity on a discrete
   * function space.
   * The arguments of this operator are interpreted as test functions.
   *
   * The additional arguments are interpreted as tree path to identity
   * a subspace.
   * If the last argument is a NonAffineFamiliy tag, caching of
   * basis functions across elements with same geometry type
   * is disabled.
   *
   * \param basis Global basis of the test function space
   * \param args Tree path arguments for subspace basis, optionally followed by NonAffineFamiliy
   */
  template<class Basis, class... Args, std::enable_if_t<(sizeof...(Args)>0), int> = 0>
  auto testFunction(const Basis& basis, Args&&... args)
  {
    using LastArg = typename Dune::Functions::LastType<std::decay_t<Args>...>::type;
    if constexpr (std::is_same_v<LastArg, NonAffineFamiliy>)
    {
      return Dune::applyPartial([&](auto&&... treePathArgs){
          return testFunction(Dune::Functions::subspaceBasis(basis, treePathArgs...), NonAffineFamiliy{});
        },
        std::forward_as_tuple(std::forward<Args>(args)...),
        std::make_index_sequence<sizeof...(Args)-1>{}
      );
    }
    else
      return testFunction(Dune::Functions::subspaceBasis(basis, args...));
  }

  /**
   * \brief Create unary identity operator on trial function space
   *
   * \ingroup FormsUserInterfaceElementary
   *
   * This creates an operator representing the identity on a discrete
   * function space.
   * The arguments of this operator are interpreted as trial functions.
   *
   * Basis function evaluations will be cached across elements with
   * same geometry type.
   *
   * \param basis Global basis of the trial function space
   */
  template<class Basis>
  auto trialFunction(const Basis& basis)
  {
    using RootBasis = std::decay_t<decltype(basis.rootBasis())>;
    return FEFunctionOperator<RootBasis, typename Basis::PrefixPath, 1>(basis.rootBasis(), basis.prefixPath());
  }

  /**
   * \brief Create unary identity operator on trial function space
   *
   * \ingroup FormsUserInterfaceElementary
   *
   * This creates an operator representing the identity on a discrete
   * function space.
   * The arguments of this operator are interpreted as trial functions.
   *
   * By passing an additional NonAffineFamiliy tag, caching of
   * basis functions across elements with same geometry type
   * is disabled.
   *
   * \param basis Global basis of the trial function space
   * \param tag Tag argument to disable cross-element caching
   */
  template<class Basis>
  auto trialFunction(const Basis& basis, NonAffineFamiliy tag)
  {
    using RootBasis = std::decay_t<decltype(basis.rootBasis())>;
    return FEFunctionOperator<RootBasis, typename Basis::PrefixPath, 1>(basis.rootBasis(), basis.prefixPath(), false);
  }

  /**
   * \brief Create unary identity operator on trial function space
   *
   * \ingroup FormsUserInterfaceElementary
   *
   * This creates an operator representing the identity on a discrete
   * function space.
   * The arguments of this operator are interpreted as trial functions.
   *
   * The additional arguments are interpreted as tree path to identity
   * a subspace.
   * If the last argument is a NonAffineFamiliy tag, caching of
   * basis functions across elements with same geometry type
   * is disabled.
   *
   * \param basis Global basis of the trial function space
   * \param args Tree path arguments for subspace basis, optionally followed by NonAffineFamiliy
   */
  template<class Basis, class... Args, std::enable_if_t<(sizeof...(Args)>0), int> = 0>
  auto trialFunction(const Basis& basis, Args&&... args)
  {
    using LastArg = typename Dune::Functions::LastType<std::decay_t<Args>...>::type;
    if constexpr (std::is_same_v<LastArg, NonAffineFamiliy>)
    {
      return Dune::applyPartial([&](auto&&... treePathArgs){
          return trialFunction(Dune::Functions::subspaceBasis(basis, treePathArgs...), NonAffineFamiliy{});
        },
        std::forward_as_tuple(std::forward<Args>(args)...),
        std::make_index_sequence<sizeof...(Args)-1>{}
      );
    }
    else
      return trialFunction(Dune::Functions::subspaceBasis(basis, args...));
  }

  /**
   * \brief Bind a linear operator to a coefficient vector
   *
   * \ingroup FormsUserInterfaceElementary
   *
   * \param op The operator to bind (should be a 1-linear MultilinearOperator or SumOperator)
   * \param vector A suitable coefficient vector
   *
   * Given a 1-linear operator on discrete function space associated to a basis,
   * this returns the 0-linear operator obtained by application of the operator
   * to the element of the function space represented by the coefficient vector.
   * While a 1-linear operator is an abstract object with one free argument,
   * the latter is bound using this method resulting in a function.
   *
   * The return value implements both, the Dune::Fufem::Forms interface of a
   * 0-linear operator and the Dune::Functions interface of a GridFunction.
   * Thus it can be used as coefficient in composed Dune::Fufem::Form expressions
   * or in places requiring a GridFunction, e.g., for writing to vtk-files.
   */
  template<class Op, class V,
    std::enable_if_t<isOperatorOrSumOperator_v<Op>, int> = 0>
  auto bindToCoefficients(const Op& op, V&& vector)
  {
    using Basis = std::decay_t<decltype(std::get<0>(op.basis()))>;
    // Small helper functions to wrap vectors using istlVectorBackend
    // if they do not already satisfy the VectorBackend interface.
    auto toConstVectorBackend = [&](auto&& v) -> decltype(auto) {
      if constexpr (models<Dune::Functions::Concept::ConstVectorBackend<Basis>, decltype(v)>()) {
        return std::forward<decltype(v)>(v);
      } else {
        return Dune::Functions::istlVectorBackend(v);
      }
    };
    return BoundUnaryOperator(op, toConstVectorBackend(std::forward<V>(vector)));
  }



  // ************************************************************
  // Differential operators
  // ************************************************************

  /**
   * \brief Obtain the jacobian of an operator
   *
   * \ingroup FormsUserInterfaceDifferentialOperators
   *
   * \param op The operator to differentiate
   */
  template<class B, class TP, std::size_t argIndex>
  auto jacobian(const FEFunctionOperator<B, TP, argIndex>& op)
  {
    return FEFunctionJacobianOperator<B, TP, argIndex>(std::get<0>(op.basis()), std::get<0>(op.treePath()), op.isAffine());
  }

  /**
   * \brief Obtain the divergence of an operator
   *
   * \ingroup FormsUserInterfaceDifferentialOperators
   *
   * \param op The operator to differentiate
   */
  template<class B, class TP, std::size_t argIndex>
  auto divergence(const FEFunctionOperator<B, TP, argIndex>& op)
  {
    return FEFunctionDivergenceOperator<B, TP, argIndex>(std::get<0>(op.basis()), std::get<0>(op.treePath()), op.isAffine());
  }

  /**
   * \brief Obtain the divergence of an operator
   *
   * \ingroup FormsUserInterfaceDifferentialOperators
   *
   * \param op The operator to differentiate
   */
  template<class B, class TP, std::size_t argIndex>
  auto div(const FEFunctionOperator<B, TP, argIndex>& op)
  {
    return divergence(op);
  }

  /**
   * \brief Obtain the gradient of an operator
   *
   * \ingroup FormsUserInterfaceDifferentialOperators
   *
   * \param op The operator to differentiate
   *
   * This is a shortcut for transpose(jacobian(op)).
   *
   * Notice that for a scalar function space,
   * jacobian(op) uses single row matrices (row-vectors) as range type,
   * gradient(op) uses single column matrices (column-vectors) as range type.
   */
  template<class B, class TP, std::size_t argIndex>
  auto gradient(const FEFunctionOperator<B, TP, argIndex>& op)
  {
    return Fufem::Forms::transpose(jacobian(op));
  }

  /**
   * \brief Obtain the gradient of an operator
   *
   * \ingroup FormsUserInterfaceDifferentialOperators
   *
   * \param op The operator to differentiate
   *
   * This is a shortcut for transpose(jacobian(op)) and gradient(op).
   *
   * Notice that for a scalar function space,
   * jacobian(op) uses single row matrices (row-vectors) as range type,
   * grad(op) uses single column matrices (column-vectors) as range type.
   */
  template<class B, class TP, std::size_t argIndex>
  auto grad(const FEFunctionOperator<B, TP, argIndex>& op)
  {
    return Fufem::Forms::transpose(jacobian(op));
  }

  /**
   * \brief Obtain the i-th partial derivative of an operator
   *
   * \ingroup FormsUserInterfaceDifferentialOperators
   *
   * \param op The operator to differentiate (must support grad(op))
   * \param op The operator to differentiate (must support grad(op))
   *
   * This extracts the k-th component of the gradient, i.e.,
   * the k-th partial derivative.
   */
  template<class Op,
    std::enable_if_t<isOperator_v<Op>, int> = 0>
  auto D(const Op& op, std::size_t k)
  {
    return compose([k](auto&& g) { return g[k]; }, grad(op));
  }



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_USERFUNCTIONS_HH
