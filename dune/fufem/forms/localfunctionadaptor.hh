// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_LOCALFUNCTIONADAPTOR_HH
#define DUNE_FUFEM_FORMS_LOCALFUNCTIONADAPTOR_HH

#include <utility>

#include <dune/fufem/forms/shapefunctioncache.hh>



namespace Dune::Fufem::Forms {


  /**
   * \brief Adaptor for turning a Fufem::Forms LocalOperator into a LocalFunction
   *
   * \ingroup FormsDetail
   *
   * In Dune::Fufem::Forms a LocalOperator represents the localized version of
   * a multilinear operator from a tuple of finite element spaces to the integrable
   * functions. In this sense a 0-linear (or nullary) operator represents a fixed
   * integrable function and its LocalOperator is essentially a LocalFunction
   * in the sense of Dune::Functions. However, the interface of a nullary LocalOperator
   * and a nullary LocalFunction is slightly different: While the LocalFunction
   * can simply be evaluated at points given by local coordinates, a LocalOperator
   * implements cached evaluations an thus requires to first being bound to a cache
   * associated to a QuadratureRule and then being evaluated at a QuadraturePoint
   * identified by an index.
   *
   * The LocalFunctionAdaptor adapts a LocalOperator to the LocalFunction interface
   * by managing a local cache and binding passing it to the operator in the desired
   * form. This is meant to be used in nullary Dune::Fufem::Forms operators to
   * implement the GridFunction interface of Dune::Functions.
   */
  template<class LocalOperator>
  class LocalFunctionAdaptor
  {
  public:

    using Element = typename LocalOperator::Element;
    using Range = typename LocalOperator::Range;

  private:

    static constexpr int dimension = Element::dimension;
    using CacheManager = typename Dune::Fufem::Forms::CacheManager<double, dimension>;
    using QuadratureRule = typename CacheManager::QuadratureRule;
    using QuadraturePoint = typename QuadratureRule::value_type;
    using Domain = typename Element::Geometry::LocalCoordinate;

  public:

    LocalFunctionAdaptor(LocalOperator&& localOperator) :
      localOperator_(std::move(localOperator)),
      internalRule_(),
      internalCacheManager_()
    {
      localOperator_.registerCaches(internalCacheManager_);
      internalRule_.emplace_back(typename QuadraturePoint::Vector(),0.0);
    }

    LocalFunctionAdaptor(const LocalOperator& localOperator) :
      localOperator_(localOperator),
      internalRule_(),
      internalCacheManager_()
    {
      localOperator_.registerCaches(internalCacheManager_);
      internalRule_.emplace_back(typename QuadraturePoint::Vector(),0.0);
    }

    LocalFunctionAdaptor(const LocalFunctionAdaptor& other) :
      LocalFunctionAdaptor(other.localOperator_)
    {}

    LocalFunctionAdaptor(LocalFunctionAdaptor&& other) :
      LocalFunctionAdaptor(std::move(other.localOperator_))
    {}

    //! Bind the LocalFunction to an element.
    void bind(const Element& element)
    {
      localOperator_.bind(element);
    }

    //! Unbind the LocalFunction.
    void unbind()
    {
      localOperator_.unbind();
    }

    //! Check if LocalFunction is already bound to an element.
    bool bound() const
    {
      return localOperator_.bound();
    }

    //! Evaluate the local function
    auto operator()(const Domain& x) const
    {
      // The cache must always be fully invalidated,
      // because the quadrature rule was modified.
      // Since the internal cache was marked as non affine,
      // calling invalidate() will indeed remove all cached
      // entries.
      internalRule_[0] = QuadraturePoint{x, 0};
      internalCacheManager_.setRule(internalRule_);
      localOperator_.bindToCaches(internalCacheManager_);
      return localOperator_(IndexedReference(x, 0));
    }

    //! Return the element the local-function is bound to.
    const Element& localContext() const
    {
      return localOperator_.localContext();
    }

  private:
    mutable LocalOperator localOperator_;
    mutable QuadratureRule internalRule_;
    mutable CacheManager internalCacheManager_;
  };


} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_LOCALFUNCTIONADAPTOR_HH
