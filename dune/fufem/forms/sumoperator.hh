// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_SUMOPERATOR_HH
#define DUNE_FUFEM_FORMS_SUMOPERATOR_HH

#include <cstddef>
#include <type_traits>
#include <tuple>
#include <utility>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/localfunctionadaptor.hh>



namespace Dune::Fufem::Forms {



  /**
   * \brief Sum of several multilinear operators
   *
   * \ingroup FormsDetail
   *
   * \tparam Operator0 First summand
   * \tparam Operator Other summands
   */
  template<class Operator0, class... Operators>
  class SumOperator
  {
    template <class T, class... Ts>
    struct AreInteroperable : std::conjunction<Dune::IsInteroperable<T, Ts>...> {};

  public:
    static constexpr std::size_t arity = Operator0::arity;

    using Element = typename Operator0::Element;
    using Range = typename Operator0::Range;

    SumOperator(Operator0 operator0, Operators... operators) :
      operators_(operator0, operators...)
    {
      static_assert(AreInteroperable<typename Operator0::Range, typename Operators::Range...>::value, "Trying do build sum of operators with non-interoperable ranges.");
    }

    class LocalOperator
    {
    public:

      using Element = typename SumOperator::Element;
      using Range = typename SumOperator::Range;

      LocalOperator(const typename Operator0::LocalOperator& localOperator0, const typename Operators::LocalOperator&... localOperators) :
        localOperators_(localOperator0, localOperators...)
      {}

      auto quadratureRuleKey() const
      {
        auto  quadKey = QuadratureRuleKey();
        Impl::forEachTupleEntry(localOperators_, [&](const auto& localOperator) {
          quadKey = localOperator.quadratureRuleKey().sum(quadKey);
        });
        return quadKey;
      }

      void bind(const Element& element)
      {
        Impl::forEachTupleEntry(localOperators_, [&](auto& localOperator) {
          localOperator.bind(element);
        });
      }

      void unbind()
      {
        Impl::forEachTupleEntry(localOperators_, [&](auto& localOperator) {
          localOperator.unbind();
        });
      }

      template<class AugmentedLocalDomain>
      auto operator()(const AugmentedLocalDomain& x) const
      {
        Range y;
        y = 0;
        Impl::forEachTupleEntry(localOperators_, [&](auto& localOperator) {
           y += localOperator(x);
        });
        return y;
      }

      template<class... LocalViews>
      void registerLocalViews(const LocalViews&... localViews)
      {
        Impl::forEachTupleEntry(localOperators_, [&](auto& localOperator) {
          localOperator.registerLocalViews(localViews...);
        });
      }

      template<class CacheManager>
      void registerCaches(CacheManager& cacheManager)
      {
        Impl::forEachTupleEntry(localOperators_, [&](auto& localOperator) {
          localOperator.registerCaches(cacheManager);
        });
      }

      template<class CacheManager>
      void bindToCaches(CacheManager& cacheManager)
      {
        Impl::forEachTupleEntry(localOperators_, [&](auto& localOperator) {
          localOperator.bindToCaches(cacheManager);
        });
      }

      auto& operators()
      {
        return localOperators_;
      }

      const auto& operators() const
      {
        return localOperators_;
      }

    private:
      std::tuple<typename Operator0::LocalOperator, typename Operators::LocalOperator...> localOperators_;
    };

    friend LocalOperator localOperator(const SumOperator& sumOperator)
    {
      return std::apply([&](const auto&... op) {
        return LocalOperator(localOperator(op)...);
      }, sumOperator.operators());
    }

    template<bool dummy=true, std::enable_if_t<dummy and (SumOperator::arity==0), int> = 0>
    friend LocalFunctionAdaptor<LocalOperator> localFunction(const SumOperator& sumOperator)
    {
      return LocalFunctionAdaptor<LocalOperator>(localOperator(sumOperator));
    }

    auto& operators()
    {
      return operators_;
    }

    const auto& operators() const
    {
      return operators_;
    }

    auto basis() const
    {
      return std::get<0>(operators_).basis();
    }

  private:
    std::tuple<Operator0, Operators...> operators_;
  };



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_SUMOPERATOR_HH
