// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_SHAPEFUNCTIONCACHE_HH
#define DUNE_FUFEM_FORMS_SHAPEFUNCTIONCACHE_HH

#include <type_traits>
#include <utility>
#include <list>
#include <any>
#include <typeindex>

#include <dune/common/indices.hh>
#include <dune/common/tuplevector.hh>
#include <dune/common/version.hh>

#include <dune/typetree/treepath.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/quadraturerules/subentityquadraturerule.hh>



namespace Dune::Fufem::Forms::Impl {

  template<class C, class... T>
  static constexpr decltype(auto) accessByTreePath(C&& container, const Dune::TypeTree::HybridTreePath<T...>& path)
  {
    if constexpr (sizeof...(T)==0)
      return container;
    else
#if DUNE_VERSION_GT(DUNE_TYPETREE, 2, 9)
      return accessByTreePath(container[path.front()], pop_front(path));
#else
    {
      auto head = path[Dune::Indices::_0];
      auto tailPath = Dune::unpackIntegerSequence([&](auto... i){
                    return Dune::TypeTree::treePath(path[Dune::index_constant<i+1>{}]...);
                  }, std::make_index_sequence<sizeof...(T)-1>());
      return accessByTreePath(container[head], tailPath);
    }
#endif
  }


} // end namespace Dune::Fufem::Forms::Impl



namespace Dune::Fufem::Forms {



  /**
   * \brief A hierarchic cache for storing shape function evaluations for a tree
   *
   * \ingroup FormsDetail
   *
   * This caches evaluations for a whole local ansatz tree in the sense
   * of dune-functions and a single quadrature rule.
   * The cache is implemented as nested container. Given quadrature rule
   * and tree, a ShapeFunctionCache can be created using either
      \code
      auto cache = ShapeFunctionCache<Tree>(rule, tree);
      \endcode
   * or
      \code
      auto cache = ShapeFunctionCache<Tree>(rule);
      cache.setTree(tree);
      \endcode
   * Now, using a treePath to some node in the tree, the index k of
   * a quadrature point and the index j of a shape function, the cached value
   * can be obtained using
      \code
      const auto& values = cache[treePath].getValues()[k][j];
      const auto& jacobian = cache[treePath].getJacobians()[k][j];
      const auto& globalJacobian = cache[treePath].getGlobalJacobians()[k][j];
      \endcode
   * The hierarchic cache forms a tree, where each node allows access
   * to direct children using cache[index] or to descendents using
   * cache[treePath].
   * Cached values are only implemented for leaf nodes so far.
   * Notice that the associated ansatz tree and quadrature rule
   * are stored by reference. While construction of the ShapeFunctionCache
   * works with an unbound tree, the tree has to be bound, when requesting
   * cached values.
   */
  template<class Node, class CT=double, class Dummy=void>
  class ShapeFunctionCache;



  template<class Node, class CT>
  class ShapeFunctionCache<Node, CT, std::enable_if_t<Node::isLeaf>>
  {
  public:

    using QuadratureRule = Dune::QuadratureRule<CT, Node::Element::dimension>;

    // \Todo Use proper FEGlobalJacobian for surface meshes
    using FEValue = typename Node::FiniteElement::Traits::LocalBasisType::Traits::RangeType;
    using FEJacobian = typename Node::FiniteElement::Traits::LocalBasisType::Traits::JacobianType;
    using FEGlobalJacobian = typename Node::FiniteElement::Traits::LocalBasisType::Traits::JacobianType;

    using ValueCache = typename std::vector<std::vector<FEValue>>;
    using JacobianCache = typename std::vector<std::vector<FEJacobian>>;
    using GlobalJacobianCache = typename std::vector<std::vector<FEGlobalJacobian>>;

    ShapeFunctionCache(const QuadratureRule& rule, const Node& node)
    {
      setRule(rule);
      setTree(node);
    }

    ShapeFunctionCache(const QuadratureRule& rule) :
      node_(nullptr)
    {
      setRule(rule);
    }

    ShapeFunctionCache(const Node& node) :
      rule_(nullptr)
    {
      setTree(node);
    }

    ShapeFunctionCache(const ShapeFunctionCache& other) = default;

    ShapeFunctionCache() = default;

    void setRule(const QuadratureRule& rule)
    {
      rule_ = &rule;
      valueCache_.resize(rule_->size());
      jacobianCache_.resize(rule_->size());
      globalJacobianCache_.resize(rule_->size());
      valueCache_[0].clear();
      jacobianCache_[0].clear();
      globalJacobianCache_[0].clear();
    }

    void setTree(const Node& node)
    {
      node_ = &node;
    }

    void setNonAffine()
    {
      isNonAffine_ = true;
    }

    const QuadratureRule& rule() const
    {
      return *rule_;
    }

    void invalidate()
    {
      // We use the size at the first quadrature point to indicate
      // an invalidated cache. To this end we set it's size to zero.
      if (isNonAffine_)
      {
        valueCache_[0].clear();
        jacobianCache_[0].clear();
      }
      globalJacobianCache_[0].clear();
    }

    decltype(auto) operator[](const Dune::TypeTree::HybridTreePath<>& treePath) const
    {
      return *this;
    }

    decltype(auto) operator[](const Dune::TypeTree::HybridTreePath<>& treePath)
    {
      return *this;
    }

    const auto& getValues()
    {
      if (valueCache_[0].empty())
      {
        const auto& localBasis = node_->finiteElement().localBasis();
        for(std::size_t k=0; k<rule_->size(); ++k)
          localBasis.evaluateFunction((*rule_)[k].position(), valueCache_[k]);
      }
      return valueCache_;
    }

    const auto& getJacobians()
    {
      if (jacobianCache_[0].empty())
      {
        const auto& localBasis = node_->finiteElement().localBasis();
        for(std::size_t k=0; k<rule_->size(); ++k)
          localBasis.evaluateJacobian((*rule_)[k].position(), jacobianCache_[k]);
      }
      return jacobianCache_;
    }

    const auto& getGlobalJacobians()
    {
      if (globalJacobianCache_[0].empty())
      {
        const auto& geometry = node_->element().geometry();
        const auto& jacobians = getJacobians();
        for(std::size_t k=0; k<rule_->size(); ++k)
        {
          globalJacobianCache_[k].resize(jacobians[k].size());
          const auto& jacobianInverse = geometry.jacobianInverse((*rule_)[k].position());
          for(std::size_t i=0; i<jacobians[k].size(); ++i)
            globalJacobianCache_[k][i] = jacobians[k][i] * jacobianInverse;
        }
      }
      return globalJacobianCache_;
    }

  private:
    const QuadratureRule* rule_ = nullptr;
    const Node* node_ = nullptr;
    ValueCache valueCache_;
    JacobianCache jacobianCache_;
    GlobalJacobianCache globalJacobianCache_;
    bool isNonAffine_ = false;
  };



  template<class Node, class CT>
  class ShapeFunctionCache<Node, CT, std::enable_if_t<Node::isPower>>
  {
    using ChildNode = typename Node::ChildType;
    using ChildCache = ShapeFunctionCache<ChildNode, CT>;
  public:

    using QuadratureRule = Dune::QuadratureRule<CT, Node::Element::dimension>;

    ShapeFunctionCache(const QuadratureRule& rule, const Node& node) :
      childCache_(rule, node.child(0))
    {}

    ShapeFunctionCache(const QuadratureRule& rule) :
      childCache_(rule)
    {}

    ShapeFunctionCache(const Node& node) :
      childCache_(node.child(0))
    {}

    ShapeFunctionCache(const ShapeFunctionCache& other) = default;

    ShapeFunctionCache() = default;

    void setRule(const QuadratureRule& rule)
    {
      childCache_.setRule(rule);
    }

    void setTree(const Node& node)
    {
      childCache_.setTree(node.child(0));
    }

    void setNonAffine()
    {
      childCache_.setNonAffine();
    }

    const QuadratureRule& rule() const
    {
      return childCache_.rule();
    }

    void invalidate()
    {
      childCache_.invalidate();
    }

    const ChildCache& operator[](std::size_t i) const
    {
      assert(i<Node::degree());
      return childCache_;
    }

    ChildCache& operator[](std::size_t i)
    {
      assert(i<Node::degree());
      return childCache_;
    }

    template<class... T>
    decltype(auto) operator[](const Dune::TypeTree::HybridTreePath<T...>& treePath) const
    {
      return Impl::accessByTreePath(*this, treePath);
    }

    template<class... T>
    decltype(auto) operator[](const Dune::TypeTree::HybridTreePath<T...>& treePath)
    {
      return Impl::accessByTreePath(*this, treePath);
    }

    constexpr std::size_t size() const noexcept
    {
      return Node::degree();
    }

  private:
    ChildCache childCache_;
  };



  namespace Impl {

    template<class List, class CT>
    struct TupleVectorOfShapeFunctionCaches
    {};

    template<class CT, template<class...> class ListType, class... Args>
    struct TupleVectorOfShapeFunctionCaches<ListType<Args...>, CT>
    {
      using type = Dune::TupleVector<ShapeFunctionCache<Args, CT>...>;
    };

    template<class List, class CT>
    using TupleVectorOfShapeFunctionCaches_t = typename TupleVectorOfShapeFunctionCaches<List, CT>::type;

  } // end namespace Imp



  template<class Node, class CT>
  class ShapeFunctionCache<Node, CT, std::enable_if_t<Node::isComposite>>
    : public Impl::TupleVectorOfShapeFunctionCaches_t<typename Node::ChildTypes, CT>
  {
    using Base = Impl::TupleVectorOfShapeFunctionCaches_t<typename Node::ChildTypes, CT>;
  public:

    using QuadratureRule = Dune::QuadratureRule<CT, Node::Element::dimension>;

    ShapeFunctionCache(const QuadratureRule& rule, const Node& node)
    {
      setRule(rule);
      setTree(node);
    }

    ShapeFunctionCache(const QuadratureRule& rule)
    {
      setRule(rule);
    }

    ShapeFunctionCache(const Node& node)
    {
      setTree(node);
    }


    ShapeFunctionCache(const ShapeFunctionCache& other) = default;

    ShapeFunctionCache() = default;

    void setRule(const QuadratureRule& rule)
    {
      Hybrid::forEach(*this, [&](auto& childCache) {
        childCache.setRule(rule);
      });
    }

    void setTree(const Node& node)
    {
      Hybrid::forEach(Dune::range(Node::degree()), [&](const auto& i){
        (*this)[i].setTree(node.child(i));
      });
    }

    void setNonAffine()
    {
      Hybrid::forEach(Dune::range(Node::degree()), [&](const auto& i){
        (*this)[i].setNonAffine();
      });
    }

    const QuadratureRule& rule() const
    {
      return (*this)[Dune::Indices::_0].rule();
    }

    using Base::operator[];

    template<class... T>
    decltype(auto) operator[](const Dune::TypeTree::HybridTreePath<T...>& treePath) const
    {
      return Impl::accessByTreePath(*this, treePath);
    }

    template<class... T>
    decltype(auto) operator[](const Dune::TypeTree::HybridTreePath<T...>& treePath)
    {
      return Impl::accessByTreePath(*this, treePath);
    }

    void invalidate() {
      Hybrid::forEach(*this, [&](auto& childCache) {
        childCache.invalidate();
      });
    }

  };



  /**
   * \brief A cache providing multiple versions for different quadrature rules
   *
   * \ingroup FormsDetail
   *
   * \tparam C An underlying cache implementation for a single quadrature rule
   *
   * This provides multiple versions of C for different quadrature rules.
   * Currently element- and facet-quadrature rules are supported.
   * Element-quadrature rules are identified by a QuadratureRuleKey and
   * facet-quadrature rules are identified by a std::pair of QuadratureRuleKey and
   * and facet index.
   *
   * The usage pattern of this is as follows: The MultipleQuadratureCache stores
   * a default-constructed prototype instance of C which is available via the
   * multipleQuadratureCache.prototype() method. This prototype should initially
   * be setup with all the needed data. Later on, a cache for a specific QuadratureRule
   * can be obtained using multipleQuadratureCache[key] where the key identifies
   * either an element- or a facet rule. Internally this will copy the prototype
   * and set the QuadratureRule of the new cache to the desired one.
   */
  template<class C>
  class MultipleQuadratureCache
  {
    using Cache = C;
    using CT = typename Cache::QuadratureRule::CoordType;
    constexpr static int dimension = Cache::QuadratureRule::d;

    using QuadratureRule = Dune::QuadratureRule<CT, dimension>;
    using FacetQuadratureRule = Dune::Fufem::SubEntityQuadratureRule<CT , dimension, 1>;

  public:

    using ElementKey = QuadratureRuleKey;
    using FacetKey = std::pair<QuadratureRuleKey, std::size_t>;

  private:

    template<class Key>
    const QuadratureRule& getRule(const Key& key)
    {
      if constexpr (std::is_same_v<Key, ElementKey>)
        return QuadratureRuleCache<CT, dimension>::rule(key);
      else
      {
        auto facet = key.second;
        auto&& type = key.first.geometryType();
        auto facetGeometryType = Dune::referenceElement<CT,dimension>(type).type(facet, 1);
        auto facetQuadratureRuleKey = key.first;
        facetQuadratureRuleKey.setGeometryType(facetGeometryType);
        const auto& rule = QuadratureRuleCache<CT, dimension-1>::rule(facetQuadratureRuleKey);
        // We need to store the face quadrature rule.
        // By using an std::list, we can store pointers
        // to the stored rules, because they never get invalidated.
        facetQuadratureRules_.push_back(FacetQuadratureRule(type, facet, rule));
        return facetQuadratureRules_.back();
      }
    }

    template<class Key>
    Cache makeCache(const Key& key)
    {
      auto cache = Cache(prototype_);
      cache.setRule(getRule(key));
      return cache;
    }

  public:

    Cache& operator[](const QuadratureRuleKey& key) {
      auto it = elementCacheMap_.find(key);
      if (it == elementCacheMap_.end())
        return (elementCacheMap_.insert(std::make_pair(key, makeCache(key)))).first->second;
      else
        return it->second;
    }

    Cache& operator[](const FacetKey& facetKey) {
      auto it = facetCacheMap_.find(facetKey);
      if (it == facetCacheMap_.end())
        return (facetCacheMap_.insert(std::make_pair(facetKey, makeCache(facetKey)))).first->second;
      else
        return it->second;
    }

    Cache& prototype()
    {
      return prototype_;
    }

    void clear()
    {
      prototype_.clear();
      elementCacheMap_.clear();
      facetCacheMap_.clear();
      facetQuadratureRules_.clear();
    }

  private:
    std::map<ElementKey, Cache> elementCacheMap_;
    std::map<FacetKey, Cache> facetCacheMap_;
    Cache prototype_;
    std::list<FacetQuadratureRule> facetQuadratureRules_;
  };



  /**
   * \brief Objects of this class are used to uniquely identifies a cache
   *
   * \ingroup FormsDetail
   *
   * The UniqueCacheId is internally a `std::pair<void*, std::type_index>`
   * which allows to exactly identify objects of any type. E.g. a class
   * can use this to associate a cache to a tree object. Then other classes
   * sharing the same tree but not knowing each other can share an evaluation
   * cache  for the tree (e.g. a `ShapeFunctionCache<Tree>`) by identifying
   * the cache with a pointer to the tree.
   * To avoid that an class and its first member are identified, the UniqueCacheId
   * combines the `void*` with a `std::type_index` to also exactly fix the type.
   *
   * This class is meant to be used with a `CacheManager` which will store
   * the shared caches, identified via a `UniqueCacheId`.
   */
  class UniqueCacheId : public std::pair<const void*, std::type_index>
  {
    using Base = std::pair<const void*, std::type_index>;
  public:

    template<class T>
    UniqueCacheId(const T& t) :
      Base(&t, std::type_index(typeid(T)))
    {}
  };

  /**
   * \brief A class for managing caches of different types
   *
   * \ingroup FormsDetail
   *
   * This class can store a collection of caches of different type
   * using type erasure. Each stored cache is identified using
   * a UniqueCacheId. A cache object is stored in the CacheManager
   * by calling `cacheManager.registerCache(uniqueCacheId, cache)`.
   * Later calls with the same `uniqueCacheId` will not store
   * a new instance but reuse the existing one.
   *
   * While registering the caches is in general not critical,
   * requesting existing caches happens inside of local loops.
   * Thus this class is optimized for fast retrieval of stored
   * caches. To avoid a map lookup, all caches are stored in a plain
   * vector and accessed by an index. The index of the newly
   * stored or reused cache is returned by the registerCache()
   * method.
   *
   * A stored cache can be queried using `cacheManager.getCache<Cache>(index)`.
   * Notice that the template parameter is needed in order to cast
   * the type-erased stored cache to the actual implementation
   * class type.
   *
   * This class provides the interface of a cache itself and is designed
   * for being stored inside of a MultipleQuadratureCache. It is especially
   * copy-constructible. Copying a CacheManager will create copies of all
   * stored caches preserving the exact erased type and indices.
   * Thus the following is guaranteed to work (and actually the intended usage):
   * Register caches in one CacheManager A and store the obtained indices.
   * Create a copy B of A. Set a different QuadratureRule for B. Obtain
   * caches from B with the indices obtained while registering in A.
   */
  template<class CT, int dimension>
  class CacheManager
  {
  public:
    using QuadratureRule = Dune::QuadratureRule<CT, dimension>;
    using size_type = std::size_t;

  private:

    // Simple type erasure for stored caches.
    class TypeErasedCache
    {
      struct {
        std::any impl_;
        void*(*toRawPtr_)(std::any&);
        void(*invalidate_)(void*);
        void(*setRule_)(void*, const QuadratureRule&);
      } members_;
      void* rawPtr_;

      void* toRawPtr()
      {
        return members_.toRawPtr_(members_.impl_);
      }

    public:

      template<class Impl>
      TypeErasedCache(Impl&& impl)
        : members_{
            std::move(impl),
            [](std::any& impl) -> void* { return &std::any_cast<Impl&>(impl); },
            [](void* impl) { static_cast<Impl*>(impl)->invalidate(); },
            [](void* impl, const QuadratureRule& rule) { static_cast<Impl*>(impl)->setRule(rule); },
          }
        , rawPtr_(toRawPtr())
      {}

      TypeErasedCache(TypeErasedCache&& other)
        : members_(std::move(other.members_))
        , rawPtr_(toRawPtr())
      {}

      TypeErasedCache(const TypeErasedCache& other)
        : members_(other.members_)
        , rawPtr_(toRawPtr())
      {}

      void invalidate()
      {
        members_.invalidate_(rawPtr_);
      }

      void setRule(const QuadratureRule& rule)
      {
        members_.setRule_(rawPtr_, rule);
      }

      template<class Impl>
      Impl& get()
      {
        return *static_cast<Impl*>(rawPtr_);
      }
    };

    const QuadratureRule* rule_ = nullptr;
    std::map<UniqueCacheId, size_type> cacheIndex_;
    std::vector<TypeErasedCache> caches_;

  public:

    CacheManager() = default;
    CacheManager(CacheManager&& other) = default;
    CacheManager(const CacheManager& other) = default;

    CacheManager& operator= (const CacheManager&) = delete;
    CacheManager& operator= (CacheManager&&) = delete;

    /**
     * \brief Clear all stored data
     */
    void clear()
    {
      rule_ = nullptr;
      cacheIndex_.clear();
      caches_.clear();
    }

    /**
     * \brief Obtain the associated quadrature rule
     */
    const QuadratureRule& rule() const
    {
      return *rule_;
    }

    /**
     * \brief Set the associated quadrature rule for all stored caches
     */
    void setRule(const QuadratureRule& rule)
    {
      rule_ = &rule;
      for(auto index : Dune::range(caches_.size()))
        caches_[index].setRule(rule);
    }

    /**
     * \brief Invalidate all stores caches
     */
    void invalidate()
    {
      for(auto index : Dune::range(caches_.size()))
        caches_[index].invalidate();
    }

    // For diagnostics: Print number of stored caches
    void report() const
    {
      std::cout << "Number of managed caches " << caches_.size() << std::endl;
    }

    /**
     * \brief Register a new cache
     *
     * \param uniqueCacheId Unique identifier of the cache to be registered
     * \param cache Cache to be registered
     * \returns Index of the stored cache
     *
     * If there is no cache registered with the provided uniqueCacheId,
     * this will store a copy of the provided cache under the id. If there
     * Otherwise the already stored cache is reused and the provided
     * argument is ignored.
     * In any case the returned index can be used to obtain the cache later.
     */
    template<class Cache>
    size_type registerCache(UniqueCacheId uniqueCacheId, Cache&& cache)
    {
      auto it = cacheIndex_.find(uniqueCacheId);
      if (it == cacheIndex_.end())
      {
        caches_.push_back(std::forward<Cache>(cache));
        cacheIndex_[uniqueCacheId] = caches_.size()-1;
        return caches_.size()-1;
      }
      else
        return it->second;
    }

    /**
     * \brief Obtain a registered cache
     *
     * \tparam Cache Type of the stored cache
     * \param index Index of the stored cache as returned by registerCache()
     * \returns The cache stored under the given index casted to the provided type.
     *
     * Using a Cache type other than the one used when storing the cache
     * is undefined behaviour.
     */
    template<class Cache>
    auto& getCache(size_type index)
    {
      return caches_[index].template get<Cache>();
    }

  };



  /**
   * \brief Template alias for MultipleQuadratureCache<CacheManager<CT, dimension>>
   *
   * \ingroup FormsDetail
   */
  template<class CT, int dimension>
  using MultipleQuadratureCacheManager = MultipleQuadratureCache<CacheManager<CT, dimension>>;



  /**
   * \brief A simple cache implementation storing values
   *
   * \ingroup FormsDetail
   *
   * This can be used to register custom caches in a CacheManager.
   * The class is only parameterized with the stored value type.
   * There is no automatic mechanism for filling the cache.
   * This has to happen externally.
   */
  template<class CT, int dimension, class V>
  class SimpleCache
  {
  public:

    using QuadratureRule = Dune::QuadratureRule<CT, dimension>;

    using Value = V;
    using ValueCache = typename std::vector<Value>;

    SimpleCache(bool isNonAffine) :
      isNonAffine_(isNonAffine)
    {}

    SimpleCache(const SimpleCache& other) = default;

    void setRule(const QuadratureRule& rule)
    {
      rule_ = &rule;
      valueCache_.resize(rule_->size());
      isEmpty_ = true;
    }

    const QuadratureRule& rule() const
    {
      return *rule_;
    }

    void setNonAffine()
    {
      isNonAffine_ = true;
    }

    void invalidate()
    {
      if (isNonAffine_)
        isEmpty_ = true;
    }

    bool isEmpty()
    {
      return isEmpty_;
    }

    void setEmpty(bool isEmpty)
    {
      isEmpty_ = isEmpty;
    }

    auto& getValues()
    {
      return valueCache_;
    }

    const auto& getValues() const
    {
      return valueCache_;
    }

  private:
    const QuadratureRule* rule_ = nullptr;
    ValueCache valueCache_;
    bool isEmpty_ = true;
    bool isNonAffine_ = false;
  };



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_SHAPEFUNCTIONCACHE_HH

