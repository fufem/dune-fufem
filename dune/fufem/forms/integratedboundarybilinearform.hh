// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_INTEGRATEDBOUNDARYBILINEARFORM_HH
#define DUNE_FUFEM_FORMS_INTEGRATEDBOUNDARYBILINEARFORM_HH

#include <cstddef>
#include <type_traits>
#include <utility>

#include <dune/common/rangeutilities.hh>

#include <dune/typetree/childextraction.hh>

#include <dune/fufem/forms/shapefunctioncache.hh>
#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/localsumassembler.hh>



namespace Dune::Fufem::Forms {



  /**
   * \brief Local assembler corresponding to a boundary bilinear form
   *
   * \ingroup FormsUserInterfaceAssemblers
   *
   * \tparam BilinearOperator A bilinear SumOperator to integrate
   * \tparam Patch The BoundaryPatch to integrate over
   *
   * This assembler computes boundary integrals
   * over all intersections contained in the BoundaryPatch.
   *
   * Local assemblers of the same arity can be chained using operator+.
   */
  template<class BilinearOperator, class Patch>
  class IntegratedBoundaryBilinearForm
  {
    using TestRootBasis = std::decay_t<decltype(std::get<0>(std::declval<BilinearOperator>().basis()))>;
    using TestRootLocalView = typename TestRootBasis::LocalView;
    using TestRootTree = typename TestRootLocalView::Tree;

    using AnsatzRootBasis = std::decay_t<decltype(std::get<1>(std::declval<BilinearOperator>().basis()))>;
    using AnsatzRootLocalView = typename AnsatzRootBasis::LocalView;
    using AnsatzRootTree = typename AnsatzRootLocalView::Tree;

    using LocalOperator = decltype(localOperator(std::declval<BilinearOperator>()));

    using FacetKey = typename MultipleQuadratureCacheManager<double, BilinearOperator::Element::dimension>::FacetKey;

  public:
    using Element = typename BilinearOperator::Element;
    using Intersection = typename Patch::GridView::Intersection;
    using IntersectionIterator = typename Patch::GridView::IntersectionIterator;

    IntegratedBoundaryBilinearForm(const BilinearOperator& sumOperator, const Patch& patch) :
      sumOperator_(sumOperator),
      sumLocalOperator_(localOperator(sumOperator_)),
      patch_(patch)
    {}

    /**
     * \brief Register local views
     *
     * This has to be called once, before using the assembler.
     * The passed local views must be the same that are used
     * when calling the assembler for on an element afterwards.
     */
    template<class TestLocalView, class AnsatzLocalView>
    void preprocess(const TestLocalView& testLocalView, const AnsatzLocalView& ansatzLocalView)
    {
      cacheManager_.clear();
      sumLocalOperator_.registerLocalViews(testLocalView.rootLocalView(), ansatzLocalView.rootLocalView());
      sumLocalOperator_.registerCaches(cacheManager_.prototype());
    }

    template<class LocalMatrix, class TestLocalView, class AnsatzLocalView>
    void operator()(const Element& element, LocalMatrix& localMatrix, const TestLocalView& testSubspaceLocalView, const AnsatzLocalView& ansatzSubspaceLocalView)
    {
      if (not patch_.containsFaceOf(element))
        return;
      for(const auto& intersection : intersections(patch_.gridView(), element))
        if (patch_.contains(intersection))
          this->operator()(intersection, localMatrix, testSubspaceLocalView, ansatzSubspaceLocalView);
    }

    template<class LocalMatrix, class TestLocalView, class AnsatzLocalView>
    void operator()(const Intersection& intersection, LocalMatrix& localMatrix, const TestLocalView& testSubspaceLocalView, const AnsatzLocalView& ansatzSubspaceLocalView)
    {
      auto facet = intersection.indexInInside();

      const auto& geometry = intersection.geometry();

      sumLocalOperator_.bind(intersection.inside());

      Impl::forEachTupleEntry(sumLocalOperator_.operators(), [&](auto& op) {
        cacheManager_[FacetKey(op.quadratureRuleKey(), facet)].invalidate();
      });

      Impl::forEachTupleEntry(sumLocalOperator_.operators(), [&](auto& op) {
        auto& cacheForRule = cacheManager_[FacetKey(op.quadratureRuleKey(), facet)];
        op.bindToCaches(cacheForRule);
        const auto& quadRule = cacheForRule.rule();
        for (auto k : Dune::range(quadRule.size()))
        {
          const auto& quadPoint = quadRule[k];
          auto quadPointPositionInFacet = intersection.geometryInInside().local(quadPoint.position());
          const auto integrationWeight = quadPoint.weight() * geometry.integrationElement(quadPointPositionInFacet);
          auto evaluatedOperator = op(IndexedReference(quadPoint.position(), k));
          axpy(integrationWeight, evaluatedOperator, localMatrix);
        }
      });
    }

    const BilinearOperator& integrandOperator() const
    {
      return sumOperator_;
    }

  private:
    const BilinearOperator sumOperator_;
    mutable LocalOperator sumLocalOperator_;
    MultipleQuadratureCacheManager<double, Element::dimension> cacheManager_;
    const Patch& patch_;
  };



  template<class BilinearOperator, class Patch>
  struct IsLocalAssembler<IntegratedBoundaryBilinearForm<BilinearOperator, Patch>> : public std::true_type {};



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_INTEGRATEDBOUNDARYBILINEARFORM_HH
