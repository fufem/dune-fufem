// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_INTEGRATEDBILINEARFORM_HH
#define DUNE_FUFEM_FORMS_INTEGRATEDBILINEARFORM_HH

#include <cstddef>
#include <type_traits>
#include <utility>

#include <dune/common/rangeutilities.hh>

#include <dune/typetree/childextraction.hh>

#include <dune/fufem/forms/shapefunctioncache.hh>
#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/localsumassembler.hh>



namespace Dune::Fufem::Forms {



  /**
   * \brief Local assembler corresponding to a bulk bilinear form
   *
   * \ingroup FormsUserInterfaceAssemblers
   *
   * \tparam BilinearOperator A bilinear SumOperator to integrate
   *
   * This assembler computes bulk integrals over all elements
   * in the underlying GridView.
   *
   * Local assemblers of the same arity can be chained using operator+.
   */
  template<class BilinearOperator>
  class IntegratedBilinearForm
  {
    using TestRootBasis = std::decay_t<decltype(std::get<0>(std::declval<BilinearOperator>().basis()))>;
    using TestRootLocalView = typename TestRootBasis::LocalView;
    using TestRootTree = typename TestRootLocalView::Tree;

    using AnsatzRootBasis = std::decay_t<decltype(std::get<1>(std::declval<BilinearOperator>().basis()))>;
    using AnsatzRootLocalView = typename AnsatzRootBasis::LocalView;
    using AnsatzRootTree = typename AnsatzRootLocalView::Tree;

    using LocalOperator = decltype(localOperator(std::declval<BilinearOperator>()));

  public:
    using Element = typename BilinearOperator::Element;

    IntegratedBilinearForm(const BilinearOperator& sumOperator) :
      sumOperator_(sumOperator),
      sumLocalOperator_(localOperator(sumOperator_))
    {}

    /**
     * \brief Register local views
     *
     * This has to be called once, before using the assembler.
     * The passed local views must be the same that are used
     * when calling the assembler for on an element afterwards.
     */
    template<class TestLocalView, class AnsatzLocalView>
    void preprocess(const TestLocalView& testLocalView, const AnsatzLocalView& ansatzLocalView)
    {
      cacheManager_.clear();
      sumLocalOperator_.registerLocalViews(testLocalView.rootLocalView(), ansatzLocalView.rootLocalView());
      sumLocalOperator_.registerCaches(cacheManager_.prototype());
    }

    template<class LocalMatrix, class TestLocalView, class AnsatzLocalView>
    void operator()(const Element& element, LocalMatrix& localMatrix, const TestLocalView& testSubspaceLocalView, const AnsatzLocalView& ansatzSubspaceLocalView)
    {
      const auto& geometry = element.geometry();

      sumLocalOperator_.bind(element);

      Impl::forEachTupleEntry(sumLocalOperator_.operators(), [&](auto& op) {
        cacheManager_[op.quadratureRuleKey()].invalidate();
      });

      Impl::forEachTupleEntry(sumLocalOperator_.operators(), [&](auto& op) {
        auto& cacheForRule = cacheManager_[op.quadratureRuleKey()];
        op.bindToCaches(cacheForRule);
        const auto& quadRule = cacheForRule.rule();
        for (auto k : Dune::range(quadRule.size()))
        {
          const auto& quadPoint = quadRule[k];
          const auto integrationWeight = quadPoint.weight() * geometry.integrationElement(quadPoint.position());
          auto evaluatedOperator = op(IndexedReference(quadPoint.position(), k));
          axpy(integrationWeight, evaluatedOperator, localMatrix);
        }
      });
    }

    const BilinearOperator& integrandOperator() const
    {
      return sumOperator_;
    }

  private:
    const BilinearOperator sumOperator_;
    mutable LocalOperator sumLocalOperator_;
    MultipleQuadratureCacheManager<double, Element::dimension> cacheManager_;
  };



  template<class BilinearOperator>
  struct IsLocalAssembler<IntegratedBilinearForm<BilinearOperator>> : public std::true_type {};



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_INTEGRATEDBILINEARFORM_HH
