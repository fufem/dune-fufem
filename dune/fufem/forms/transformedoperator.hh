// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_TRANSFORMEDOPERATOR_HH
#define DUNE_FUFEM_FORMS_TRANSFORMEDOPERATOR_HH

#include <cstddef>
#include <type_traits>
#include <tuple>
#include <utility>
#include <functional>

#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/localfunctionadaptor.hh>



namespace Dune::Fufem::Forms::Impl {

  template<class Operator, int arity = Operator::arity>
  struct TransformedOperatorBaseTraits
  {
    using type = MultilinearOperator<arity>;
  };

  template<class Operator>
  struct TransformedOperatorBaseTraits<Operator, 1>
  {
    using type = UnaryOperator<Operator::argIndex>;
  };

} // namespace Dune::Fufem::Forms::Impl



namespace Dune::Fufem::Forms {



  /**
   * \brief Pointwise transformation of a multilinear operator
   *
   * \ingroup FormsDetail
   *
   * \tparam Op Outer transformation
   * \tparam BaseOperator The multilinear operator to transform
   */
  template<class Op, class BaseOperator>
  class TransformedOperator
    : public Impl::TransformedOperatorBaseTraits<BaseOperator>::type
  {
    using Transformation = Op;
    using BaseLocalOperator = decltype(localOperator(std::declval<BaseOperator>()));

  public:

    using Element = typename BaseOperator::Element;
    using Range = decltype(std::declval<Op>()(std::declval<typename BaseOperator::Range>()));

    TransformedOperator(const BaseOperator& baseOperator) :
      baseOperator_(baseOperator),
      transformation_()
    {}

    TransformedOperator(const Transformation& transformation, const BaseOperator& baseOperator) :
      baseOperator_(baseOperator),
      transformation_(transformation)
    {}

    class LocalOperator : public BaseLocalOperator
    {
    public:

      using Element = typename TransformedOperator::Element;
      using Range = typename TransformedOperator::Range;

      LocalOperator(const Transformation& transformation, BaseLocalOperator&& baseLocalOperator) :
        BaseLocalOperator(std::move(baseLocalOperator)),
        transformation_(transformation)
      {}

      template<class AugmentedLocalDomain>
      auto operator()(const AugmentedLocalDomain& x) const
      {
        return Impl::Tensor::compose(std::cref(transformation_), BaseLocalOperator::operator()(x));
      }

    private:
      Transformation transformation_;
    };

    friend LocalOperator localOperator(const TransformedOperator& transformedOperator)
    {
      return LocalOperator(transformedOperator.transformation(), localOperator(transformedOperator.baseOperator()));
    }

    template<bool dummy=true, std::enable_if_t<dummy and (TransformedOperator::arity==0), int> = 0>
    friend LocalFunctionAdaptor<LocalOperator> localFunction(const TransformedOperator& transformedOperator)
    {
      return LocalFunctionAdaptor<LocalOperator>(localOperator(transformedOperator));
    }

    auto basis() const
    {
      return baseOperator_.basis();
    }

    auto treePath() const
    {
      return baseOperator_.treePath();
    }

    const BaseOperator& baseOperator() const
    {
      return baseOperator_;
    }

    const Transformation& transformation() const
    {
      return transformation_;
    }

    friend auto gradient(const TransformedOperator& f)
    {
      static constexpr auto dimension = Element::Geometry::Coordinate::dimension;
      static_assert(dimension<4);

      auto gradientTransformation = [transformation = f.transformation()](const auto& g) {
        static_assert(dimension<=3, "The gradient of a transformed operator is not implemented for range dimensions > 3.");
        if constexpr (dimension==1)
          return std::array{transformation(g[0])};
        else if constexpr (dimension==2)
          return std::array{transformation(g[0]), transformation(g[1])};
        else if constexpr (dimension==3)
          return std::array{transformation(g[0]), transformation(g[1]), transformation(g[2])};
      };
      return compose(gradientTransformation, gradient(f.baseOperator()));
    }

    friend auto grad(const TransformedOperator& f)
    {
      return gradient(f);
    }

  private:
    BaseOperator baseOperator_;
    Transformation transformation_;
  };



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_TRANSFORMEDOPERATOR_HH
