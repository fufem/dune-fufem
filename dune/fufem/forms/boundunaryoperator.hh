// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_BOUNDUNARYOPERATOR_HH
#define DUNE_FUFEM_FORMS_BOUNDUNARYOPERATOR_HH

#include <cstddef>
#include <type_traits>
#include <tuple>
#include <utility>

#include <dune/common/version.hh>

#include <dune/typetree/treecontainer.hh>

#include <dune/functions/gridfunctions/gridviewentityset.hh>
#include <dune/functions/backends/concepts.hh>
#include <dune/functions/backends/istlvectorbackend.hh>

#if DUNE_VERSION_LTE(DUNE_TYPETREE, 2, 9)
#include <dune/functions/functionspacebases/subspacebasis.hh>
#endif

#include <dune/fufem/forms/shapefunctioncache.hh>
#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/localfunctionadaptor.hh>



namespace Dune::Fufem::Forms {


  /**
   * \brief Wrapper binding a linear operator to a coefficient vector
   *
   * \ingroup FormsDetail
   *
   * \tparam Op Type of unary operator to be bound
   * \tparam V Type of coefficient vector
   *
   * Given a 1-linear operator on discrete function space associated to a basis,
   * this returns the 0-linear operator obtained by application of the operator
   * to the element of the function space represented by the coefficient vector.
   *
   * The return value implements both, the Dune::Fufem::Forms interface of a
   * 0-linear operator and the Dune::Functions interface of a GridFunction.
   * Thus it can be used as coefficient in composed Dune::Fufem::Form expressions
   * or in places requiring a GridFunction, e.g., for writing to vtk-files.
   */
  template<class Op, class V>
  class BoundUnaryOperator : public MultilinearOperator<0>
  {
    using UnaryOperator = Op;
    using LocalUnaryOperator = decltype(localOperator(std::declval<UnaryOperator>()));
    using Tree = typename std::decay_t<decltype(std::get<0>(std::declval<UnaryOperator>().basis()))>::LocalView::Tree;
    using NodeFlags = typename TypeTree::UniformTreeContainer<char, Tree>;

    template<class OP, class Func>
    static void forEachOperator(OP&& op, Func&& f)
    {
      if constexpr (isOperator_v<UnaryOperator>)
        f(op);
      else if constexpr (isSumOperator_v<UnaryOperator>)
        Impl::forEachTupleEntry(op.operators(), f);
    }

  public:

    // Types present in DiscreteGlobalBasisFunctionBase
    using Basis = std::decay_t<decltype(std::get<0>(std::declval<UnaryOperator>().basis()))>;
    using Vector = V;
    using Coefficient = Dune::AutonomousValue<decltype(std::declval<Vector>()[std::declval<typename Basis::MultiIndex>()])>;
    using GridView = typename Basis::GridView;
    using EntitySet = typename Dune::Functions::GridViewEntitySet<GridView, 0>;
    using Domain = typename EntitySet::GlobalCoordinate;
    using LocalDomain = typename EntitySet::LocalCoordinate;

    // Types required by Operator interface
    using Element = typename EntitySet::Element;
    using Range = typename UnaryOperator::Range;

    BoundUnaryOperator(const UnaryOperator& unaryOperator, const Vector& coefficients) :
      cacheId_(*this),
      unaryOperator_(unaryOperator),
      coefficients_(Dune::stackobject_to_shared_ptr(coefficients))
    {
      static_assert(isOperatorOrSumOperator_v<UnaryOperator>, "The type passed to BoundUnaryOperator is not a operator.");
      static_assert(UnaryOperator::arity==1, "The operator passed to BoundUnaryOperator is not unary.");
    }

    BoundUnaryOperator(const UnaryOperator& unaryOperator, Vector&& coefficients) :
      cacheId_(*this),
      unaryOperator_(unaryOperator),
      coefficients_(std::make_shared<const Vector>(std::move(coefficients)))
    {
      static_assert(isOperatorOrSumOperator_v<UnaryOperator>, "The type passed to BoundUnaryOperator is not a operator.");
      static_assert(UnaryOperator::arity==1, "The operator passed to BoundUnaryOperator is not unary.");
    }

    BoundUnaryOperator(const UnaryOperator& unaryOperator, std::shared_ptr<const Vector> coefficients) :
      cacheId_(*this),
      unaryOperator_(unaryOperator),
      coefficients_(std::move(coefficients))
    {
      static_assert(isOperatorOrSumOperator_v<UnaryOperator>, "The type passed to BoundUnaryOperator is not a operator.");
      static_assert(UnaryOperator::arity==1, "The operator passed to BoundUnaryOperator is not unary.");
    }



    class LocalOperator
    {
    public:

      using Element = typename BoundUnaryOperator::Element;
      using Range = typename BoundUnaryOperator::Range;

    private:
      static constexpr int dimension = EntitySet::Element::dimension;

      using LocalView = typename Basis::LocalView;
      using Tree = typename LocalView::Tree;
      using size_type = typename Basis::LocalView::Tree::size_type;
      using CacheManager = typename Dune::Fufem::Forms::CacheManager<double, dimension>;
      using QuadratureRule = typename CacheManager::QuadratureRule;
      using QuadraturePoint = typename QuadratureRule::value_type;
      using ValueCache = SimpleCache<double, dimension, Range>;

      const LocalView& localView() const
      {
        if (externalLocalView_ == nullptr)
          return internalLocalView_;
        else
          return *externalLocalView_;
      }

      void loadLocalDOFs() const
      {
        const auto& lv = localView();
        localDoFs_.resize(lv.size());
        TypeTree::forEachLeafNode(lv.tree(), [&](auto&& node, auto&& treePath) {
          if (usedNodes_[treePath])
            for (auto i : Dune::range(node.size()))
            {
              auto localIndex = node.localIndex(i);
              localDoFs_[localIndex] = (*coefficients_)[lv.index(localIndex)];
            }
        });
      }

    public:

      LocalOperator(LocalUnaryOperator&& localUnaryOperator, NodeFlags usedNodes, const Basis& basis, std::shared_ptr<const Vector> coefficients, UniqueCacheId cacheId) :
        basis_(basis),
        localUnaryOperator_(std::move(localUnaryOperator)),
        usedNodes_(usedNodes),
        coefficients_(std::move(coefficients)),
        internalLocalView_(basis.localView()),
        cacheId_(cacheId)
      {
        localDoFs_.reserve(internalLocalView_.maxSize());
        localUnaryOperator_.registerLocalViews(internalLocalView_);
      }

      LocalOperator(const LocalOperator& other) :
        LocalOperator(LocalUnaryOperator(other.localUnaryOperator_), other.usedNodes_, other.basis_, other.coefficients_, other.cacheId_)
      {}

      LocalOperator(LocalOperator&& other) :
        LocalOperator(std::move(other.localUnaryOperator_), other.usedNodes_, other.basis_, other.coefficients_, other.cacheId_)
      {}

      void bind(const Element& element)
      {
        localDoFs_.resize(0);
        if (externalLocalView_ == nullptr)
          internalLocalView_.bind(element);
        localUnaryOperator_.bind(element);
      }

      //! Unbind the local-function.
      void unbind()
      {
        if (externalLocalView_ == nullptr)
          internalLocalView_.unbind();
      }

      // Additional interface of local nullary operator
      auto quadratureRuleKey() const
      {
        return localUnaryOperator_.quadratureRuleKey();
      }

      template<class T, class Index>
      auto operator()(const IndexedReference<T, Index>& x) const
      {
        auto& valueCache = cacheManager_->template getCache<ValueCache>(cacheIndex_);
        auto& values = valueCache.getValues();
        if (valueCache.isEmpty())
        {
          if (localDoFs_.size() == 0)
            loadLocalDOFs();
          const auto& rule = valueCache.rule();
          for (auto k : Dune::range(rule.size()))
          {
            auto& y = values[k];
            y = 0;
            forEachOperator(localUnaryOperator_, [&](auto& op) {
              auto evaluatedOperator = op(IndexedReference(rule[k].position(), k));
              auto offset = evaluatedOperator.offset(0);
              for (auto i : Dune::range(evaluatedOperator.extent(0)))
                y += localDoFs_[offset + i] * evaluatedOperator(i);
            });
          }
          valueCache.setEmpty(false);
        }
        return values[x.index()];
      }

      /**
       * \brief Register LocalViews managed outside.
       *
       */
      template<class... LV>
      void registerLocalViews(const LV&... lvs)
      {
        localUnaryOperator_.registerLocalViews(lvs...);
        Impl::visitMatchingLocalView([&](const auto& localView) {
          externalLocalView_ = &localView;
          localDoFs_.reserve(externalLocalView_->maxSize());
        }, basis_, lvs...);
      }

      void registerCaches(CacheManager& cacheManager)
      {
        localUnaryOperator_.registerCaches(cacheManager);
        cacheIndex_ = cacheManager.registerCache(cacheId_, ValueCache(true));
      }

      void bindToCaches(CacheManager& cacheManager)
      {
        localUnaryOperator_.bindToCaches(cacheManager);
        cacheManager_ = &cacheManager;
      }

    private:
      const Basis& basis_;
      mutable LocalUnaryOperator localUnaryOperator_;
      NodeFlags usedNodes_;
      std::shared_ptr<const Vector> coefficients_;
      mutable std::vector<Coefficient> localDoFs_;
      LocalView internalLocalView_;
      const LocalView* externalLocalView_ = nullptr;
      mutable CacheManager* cacheManager_ = nullptr;
      UniqueCacheId cacheId_;
      std::size_t cacheIndex_ = 0;
    };

    auto basis() const
    {
      return std::tuple<>();
    }

    auto treePath() const
    {
      return std::tuple<>();
    }

    friend LocalOperator localOperator(const BoundUnaryOperator& boundUnaryOperator)
    {
      const auto& rootBasis = std::get<0>(boundUnaryOperator.unaryOperator_.basis());
      // Mark all leaf nodes whose DOFs need to be loaded
      auto localView = rootBasis.localView();
      auto usedNodes = TypeTree::makeTreeContainer(localView.tree(), [](auto&&) { return char(false); });
      forEachOperator(boundUnaryOperator.unaryOperator_, [&](auto& op) {
        const auto& treePath = std::get<0>(op.treePath());
        const auto& node = Dune::TypeTree::child(localView.tree(), treePath);
        TypeTree::forEachLeafNode(node, [&](auto&& leafNode, auto&& leafTreePath) {
#if DUNE_VERSION_GT(DUNE_TYPETREE, 2, 9)
          usedNodes[TypeTree::join(treePath,leafTreePath)] = true;
#else
          usedNodes[Dune::Functions::Impl::joinTreePaths(treePath,leafTreePath)] = true;
#endif
        });
      });
      return LocalOperator(localOperator(boundUnaryOperator.unaryOperator_), usedNodes, rootBasis, boundUnaryOperator.coefficients_, boundUnaryOperator.cacheId_);
    }

    template<bool dummy=true, std::enable_if_t<dummy and (BoundUnaryOperator::arity==0), int> = 0>
    friend LocalFunctionAdaptor<LocalOperator> localFunction(const BoundUnaryOperator& boundUnaryOperator)
    {
      return LocalFunctionAdaptor<LocalOperator>(localOperator(boundUnaryOperator));
    }

    const auto& unaryOperator() const
    {
      return unaryOperator_;
    }

    const auto& coefficients() const
    {
      return coefficients_;
    }

    friend auto jacobian(const BoundUnaryOperator& f)
    {
      static_assert(isOperator_v<UnaryOperator>, "jacobian(BoundUnaryOperator<SumOperator<...>,V>(...)) is not implemented.");
      return Dune::Fufem::Forms::BoundUnaryOperator(jacobian(f.unaryOperator_), f.coefficients_);
    }

    friend auto gradient(const BoundUnaryOperator& f)
    {
      static_assert(isOperator_v<UnaryOperator>, "gradient(BoundUnaryOperator<SumOperator<...>,V>(...)) is not implemented.");
      return Dune::Fufem::Forms::BoundUnaryOperator(gradient(f.unaryOperator_), f.coefficients_);
    }

    friend auto grad(const BoundUnaryOperator& f)
    {
      static_assert(isOperator_v<UnaryOperator>, "grad(BoundUnaryOperator<SumOperator<...>,V>(...)) is not implemented.");
      return Dune::Fufem::Forms::BoundUnaryOperator(grad(f.unaryOperator_), f.coefficients_);
    }

    friend auto divergence(const BoundUnaryOperator& f)
    {
      static_assert(isOperator_v<UnaryOperator>, "divergence(BoundUnaryOperator<SumOperator<...>,V>(...)) is not implemented.");
      return Dune::Fufem::Forms::BoundUnaryOperator(divergence(f.unaryOperator_), f.coefficients_);
    }

    friend auto div(const BoundUnaryOperator& f)
    {
      static_assert(isOperator_v<UnaryOperator>, "div(BoundUnaryOperator<SumOperator<...>,V>(...)) is not implemented.");
      return Dune::Fufem::Forms::BoundUnaryOperator(div(f.unaryOperator_), f.coefficients_);
    }

  private:
    UniqueCacheId cacheId_;
    UnaryOperator unaryOperator_;
    std::shared_ptr<const Vector> coefficients_;
  };



//  template<class F, class V,
//    std::enable_if_t<isOperatorOrSumOperator_v<F>, int> = 0,
//    std::enable_if_t<not isOperatorOrSumOperator_v<std::decay_t<V>>, int> = 0>
//  auto operator,(const F& f, V&& vector)
//  {
//    return bindToCoefficients(f, std::forward<V>(vector));
//  }


} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_BOUNDUNARYOPERATOR_HH
