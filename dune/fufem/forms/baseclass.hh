// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_BASECLASS_HH
#define DUNE_FUFEM_FORMS_BASECLASS_HH

#include <cstddef>
#include <type_traits>
#include <tuple>
#include <utility>

#include <dune/common/typetraits.hh>
#include <dune/common/indices.hh>

#include <dune/typetree/childextraction.hh>
#include <dune/typetree/treepath.hh>



namespace Dune::Fufem::Forms::Impl {

  template<class Tuple, class F>
  void forEachTupleEntry(Tuple&& tuple, F&& f){
    std::apply([&](auto&&... entry){
      (f(std::forward<decltype(entry)>(entry)), ...);
    }, tuple);
  }

} // namespace Dune::Fufem::Forms::Impl



namespace Dune::Fufem::Forms::Impl::Tensor {

  // Types for defining extents and offsets of the tensors.
  // They are both thin wrappers around std::array, but
  // by defining separate types, we avoid errors when
  // passing extents and offsets in the wrong order.

  template<std::size_t k>
  struct Extents : public std::array<std::size_t, k>
  {
    template<class... II>
    Extents(II... ii) : std::array<std::size_t,k>{ii...}
    {}
  };

  template<class... II>
  Extents(II... ii) -> Extents<sizeof...(II)>;

  template<std::size_t k>
  struct Offsets : public std::array<std::size_t, k>
  {
    template<class... II>
    Offsets(II... ii) : std::array<std::size_t,k>{ii...}
    {}
  };

  template<class... II>
  Offsets(II... ii) -> Offsets<sizeof...(II)>;



  // This class represents a lazy dense block in a tensor of given rank R.
  // The block is identified via an offset array and its extents
  // both being of size R. The data in the block is represented by
  // a callback taking R index arguments i_k within i_k from [0, size[k])
  // for k from [0, R).
  //
  // One may later want to bring the interface closer to the one of std::mdspan.
  template<std::size_t rank, class F>
  class LazyTensorBlock
  {
  public:

    LazyTensorBlock(F f, Offsets<rank> offsets, Extents<rank> extents) :
      f_(std::move(f)),
      extents_(extents),
      offsets_(offsets)
    {}

    LazyTensorBlock(F f, Extents<rank> extents, Offsets<rank> offsets) :
      f_(std::move(f)),
      extents_(extents),
      offsets_(offsets)
    {}

    template<class... I>
    auto operator()(I... i) const
    {
      return f_(i...);
    }

    const auto& extents() const
    {
      return extents_;
    }

    auto extent(std::size_t k) const
    {
      return extents_[k];
    }

    const auto& offsets() const
    {
      return offsets_;
    }

    auto offset(std::size_t k) const
    {
      return offsets_[k];
    }

    auto callback() const
    {
      return f_;
    }

  private:
    F f_;
    Extents<rank> extents_;
    Offsets<rank> offsets_;
  };



  // This class represents a lazy dense block in a tensor of given rank 2.
  // In contrast to LazyTensorBlock<2,...> this class stores the tensor
  // as dyadic product of two rank-1 tensor factors. The entries of those
  // tensors are multiplied using agiven callback.
  template<class P, class F0, class F1>
  class LazyTensorBlockDyadicProduct
  {
  public:

    LazyTensorBlockDyadicProduct(P p, F0 f0, F1 f1, Offsets<2> offsets, Extents<2> extents) :
      p_(std::move(p)),
      f0_(std::move(f0)),
      f1_(std::move(f1)),
      extents_(extents),
      offsets_(offsets)
    {}

    LazyTensorBlockDyadicProduct(P p, F0 f0, F1 f1, Extents<2> extents, Offsets<2> offsets) :
      p_(std::move(p)),
      f0_(std::move(f0)),
      f1_(std::move(f1)),
      extents_(extents),
      offsets_(offsets)
    {}

    template<class I0, class I1>
    auto operator()(I0 i0, I1 i1) const
    {
      return p_(f0_(i0), f1_(i1));
    }

    const auto& extents() const
    {
      return extents_;
    }

    auto extent(std::size_t k) const
    {
      return extents_[k];
    }

    const auto& offsets() const
    {
      return offsets_;
    }

    auto offset(std::size_t k) const
    {
      return offsets_[k];
    }

    auto product() const
    {
      return p_;
    }

    template<std::size_t k>
    const auto& callback(Dune::index_constant<k>) const
    {
      if constexpr (k==0)
        return f0_;
      if constexpr (k==1)
        return f1_;
    }

  private:
    P p_;
    F0 f0_;
    F1 f1_;
    Extents<2> extents_;
    Offsets<2> offsets_;
  };



  template<class P, class X, class Y>
  auto product(P&& outer, const X& x, const Y& y)
  {
    return outer(x, y);
  }

  template<class P, class X, std::size_t rank_Y, class F_Y>
  auto product(P outer, const X& x, const LazyTensorBlock<rank_Y, F_Y>& y)
  {
    return Impl::Tensor::LazyTensorBlock(
      [outer=std::move(outer), x, y=y.callback()](const auto&... ii) { return outer(x, y(ii...)); },
      y.extents(),
      y.offsets()
    );
  }

  template<class P,  std::size_t rank_X, class F_X, class Y>
  auto product(P outer, const LazyTensorBlock<rank_X, F_X>& x, const Y& y)
  {
    return Impl::Tensor::LazyTensorBlock(
      [outer=std::move(outer), x=x.callback(), y](const auto&... ii) { return outer(x(ii...), y); },
      x.extents(),
      x.offsets()
    );
  }

  template<class P, class X, class Y_P, class Y_F0, class Y_F1>
  auto product(P outer, const X& x, const LazyTensorBlockDyadicProduct<Y_P, Y_F0, Y_F1>& y)
  {
    using namespace Dune::Indices;
    return Impl::Tensor::LazyTensorBlockDyadicProduct(
      [outer=std::move(outer), x, y=y.product()](const auto&... args) { return outer(x, y(args...)); },
      y.callback(_0),
      y.callback(_1),
      y.extents(),
      y.offsets()
    );
  }

  template<class P, class X_P, class X_F0, class X_F1, class Y>
  auto product(P outer, const LazyTensorBlockDyadicProduct<X_P, X_F0, X_F1>& x, const Y& y)
  {
    using namespace Dune::Indices;
    return Impl::Tensor::LazyTensorBlockDyadicProduct(
      [outer=std::move(outer), x=x.product(), y](const auto&... args) { return outer(x(args...), y); },
      x.callback(_0),
      x.callback(_1),
      x.extents(),
      x.offsets()
    );
  }

  template<class P, class F_X, class F_Y>
  auto product(P outer, const LazyTensorBlock<1, F_X>& x, const LazyTensorBlock<1, F_Y>& y)
  {
    return Impl::Tensor::LazyTensorBlockDyadicProduct(
      std::move(outer), x.callback(), y.callback(),
      Extents(x.extent(0), y.extent(0)),
      Offsets(x.offset(0), y.offset(0))
    );
  }



  template<class Outer, class T>
  auto compose(const Outer& outer, const T& tensor)
  {
    return outer(tensor);
  }

  template<class Outer, std::size_t rank, class F>
  auto compose(const Outer& outer, const LazyTensorBlock<rank, F>& tensor)
  {
    return Impl::Tensor::LazyTensorBlock(
      [outer, inner=tensor.callback()](const auto&... indices) { return outer(inner(indices...)); },
      tensor.extents(),
      tensor.offsets()
    );
  }

  template<class Outer, class P, class F0, class F1>
  auto compose(const Outer& outer, const LazyTensorBlockDyadicProduct<P, F0, F1>& tensor)
  {
    using namespace Dune::Indices;
    return Impl::Tensor::LazyTensorBlockDyadicProduct(
      [outer, inner=tensor.product()](const auto&... args) { return outer(inner(args...)); },
      tensor.callback(_0),
      tensor.callback(_1),
      tensor.extents(),
      tensor.offsets()
    );
  }



  // This axpy operation computes y += alpha*x
  // for a dense vector y, a scalar alpha, and a
  // LazyTensorBlock y of rank 1.
  template<class K, class F, class Vector>
  void axpy(const K& alpha, const LazyTensorBlock<1, F>& x, Vector& y)
  {
    for (auto i : Dune::range(x.extent(0)))
      y[x.offset(0)+i] += alpha*x(i);
  }

  // This axpy operation computes y += alpha*x
  // for a dense matrix y, a scalar alpha, and a
  // LazyTensorBlock y of rank 2.
  template<class K, class F, class Matrix>
  void axpy(const K& alpha, const LazyTensorBlock<2, F>& x, Matrix& y)
  {
    for (auto i : Dune::range(x.extent(0)))
      for (auto j : Dune::range(x.extent(1)))
        y[x.offset(0)+i][x.offset(1)+j] += alpha*x(i,j);
  }

  template<class K, class P, class F0, class F1, class Matrix>
  void axpy(const K& alpha, const LazyTensorBlockDyadicProduct<P, F0, F1>& x, Matrix& y)
  {
    using namespace Dune::Indices;
    // Since x.product() is a bilinear we can factor out
    // the multiplication with alpha and the evaluation
    // of the first factor x0. The second factor is
    // (partially) cached within the ProductOperator
    for (auto i : Dune::range(x.extent(0)))
    {
      auto x0_i = x.callback(_0)(i);
      x0_i *= alpha;
      for (auto j : Dune::range(x.extent(1)))
        y[x.offset(0)+i][x.offset(1)+j] += x.product()(x0_i, x.callback(_1)(j));
    }
  }

} // namespace Dune::Fufem::Forms::Impl::Tensor



namespace Dune::Fufem::Forms {



  /**
   * \brief Base class for multilinear operator implementations
   *
   * \ingroup FormsDetail
   *
   * This is motivated by UFL.
   * All classes deriving from this are multilinear
   * maps from finite element spaces into the space of functions
   * from the domain into some finite dimensional vector spaces.
   * Poinwise products (scalar-vector, matrix-vector, dot,...) of
   * multilinear maps induces multilinear maps of higher order.
   * The product of a k-linear map and an l-linear is a (k+l) linear
   * map into a suitable function space.
   *
   * Example:
   * Denote by {A->B} the set of all functions f:B->A. Now let
   * D a domain in R^d, V \subset {D->R} and W \subset {D->R}
   * two FE-spaces, and f \in {D->R^(d,d)} a fixed matrix valued
   * function.
   *
   * Then f is a 0-linear map into {D->R^(d,d)}.
   * The gradient operator \nabla:V -> {D->R^d} is a 1-linear map into {D->R^d}.
   * The gradient operator \nabla:W -> {D->R^d} is a 1-linear map into {D->R^d}.
   * The pointwise matrix-vector product f\nabla:V -> {D->R^d} is a 1-linear map into {D->R^d}.
   * The pointwise dot product dot(f\nabla(.), \nabla(.)):V\times W -> {D->R}
   * is a 2-linear map into {D->R}.
   *
   * Integrating a k-linear operator F:V_1 \times ... \times V_k -> {D->R} over D
   * results in a k-linear form \int_D F(...)dx : V_1 \times ... \times V_k -> D
   */
  template<std::size_t k>
  class MultilinearOperator {
  public:
    static constexpr std::size_t arity = k;
  };

  /**
   * \brief Base class for unary multilinear operator implementations
   *
   * \ingroup FormsDetail
   *
   * \tparam k Argument index (0 for test function, 1 for trial function)
   *
   * On top of MultilinearOperator<1> this adds a constant
   * fixing the index of the argument within composed multilinear
   * operators.
   */
  template<std::size_t k>
  struct UnaryOperator : public MultilinearOperator<1>
  {
    static constexpr std::size_t argIndex = k;
  };

  // Forward declaration
  template<class Operator, class... Operators>
  class SumOperator;



namespace Impl {


  std::false_type isOperatorHelper(const void*)
  {
    return {};
  }

  template<std::size_t k>
  std::true_type isOperatorHelper(const MultilinearOperator<k>*)
  {
    return {};
  }

  template<class T>
  struct IsSumOperatorHelper : std::false_type {};

  template<class... Op>
  struct IsSumOperatorHelper<SumOperator<Op...>> : std::true_type {};

}



  /**
   * \brief Traits class for checking if Op is derived from MultilinearOperator<k>
   *
   * \ingroup FormsDetail
   */
  template<class Op>
  using IsOperator = decltype(Impl::isOperatorHelper(std::declval<const std::decay_t<Op>*>()));

  /**
   * \brief Short cut for IsOperator<Op>::value
   *
   * \ingroup FormsDetail
   */
  template<class Op>
  inline constexpr bool isOperator_v = IsOperator<Op>::value;



  /**
   * \brief Traits class for checking if Op is a SumOperator
   *
   * \ingroup FormsDetail
   */
  template<class Op>
  using IsSumOperator = Impl::IsSumOperatorHelper<std::decay_t<Op>>;

  /**
   * \brief Short cut for IsSumOperator<Op>::value
   *
   * \ingroup FormsDetail
   */
  template<class Op>
  inline constexpr bool isSumOperator_v = IsSumOperator<Op>::value;



  /**
   * \brief Traits class for checking if Op is either a SumOperator or derived from MultilinearOperator<k>
   *
   * \ingroup FormsDetail
   */
  template<class Op>
  using IsOperatorOrSumOperator = std::disjunction<IsOperator<Op>, IsSumOperator<Op>>;

  /**
   * \brief Short cut for IsOperatorOrSumOperator<Op>::value
   *
   * \ingroup FormsDetail
   */
  template<class Op>
  inline constexpr bool isOperatorOrSumOperator_v = IsOperatorOrSumOperator<Op>::value;

} // namespace Dune::Fufem::Forms




namespace Dune::Fufem::Forms::Impl {

  // Execute callback on first LocalView in list, that belongs to the basis
  // and return true if any matching LocalView was found.
  template<class F, class Basis, class... LocalViews>
  bool visitMatchingLocalView(F&& f, const Basis& basis, LocalViews&&... localViews)
  {
    auto visitIfMatching = [&](const auto& localView) {
      if constexpr (std::is_same_v<Basis, typename std::decay_t<decltype(localView)>::GlobalBasis>)
        if (&localView.globalBasis() == &basis)
        {
          f(localView);
          return true;
        }
      return false;
    };
    return (false || ... || visitIfMatching(localViews));
  }

} // namespace Dune::Fufem::Forms::Impl




namespace Dune::Fufem::Forms {



  template<class T, class Index>
  class IndexedReference
  {
  public:
    IndexedReference(const T& ref, const Index& index) :
      ref_(&ref),
      index_(index)
    {}

    operator const T&() const
    {
      return *ref_;
    }

    const Index& index() const
    {
      return index_;
    }

  private:
    const T* ref_;
    const Index& index_;
  };



} // namespace Dune::Fufem::Forms



#endif // DUNE_FUFEM_FORMS_BASECLASS_HH
