// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_UNARYOPERATORS_HH
#define DUNE_FUFEM_FORMS_UNARYOPERATORS_HH

#include <cstddef>
#include <type_traits>
#include <vector>

#include <dune/common/typetraits.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/indices.hh>
#include <dune/common/tupleutility.hh>

#include <dune/typetree/childextraction.hh>
#include <dune/typetree/treepath.hh>

#include <dune/functions/functionspacebases/subspacebasis.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/shapefunctioncache.hh>



namespace Dune::Fufem::Forms {



  /**
   * \brief Base class of elementary differential operators on an FE-space
   *
   * \ingroup FormsDetail
   *
   * \tparam B Type of the dune-functions basis
   * \tparam TP Tree path within the basis
   * \tparam argIndex Index of arguments within composed multilinear operators
   *
   * This class contains all the generic management of bases,
   * trees, nodes, caches that is invariant under the linear
   * (differential) operator.
   */
  template<class B, class TP, std::size_t argIndex>
  class FEOperatorBase : public UnaryOperator<argIndex>
  {

  protected:
    using LocalView = typename B::LocalView;
    using Tree = typename LocalView::Tree;
    using Node = typename Dune::TypeTree::ChildForTreePath<Tree, TP>;
    using SubspaceBasis = typename Dune::Functions::SubspaceBasis<B, TP>;

    // Helper function to derive the leafTreePath associated
    // to a treePath. For treePaths referring to a leaf this is
    // the identity. For treePaths referring to the a power
    // node whose children are leaf, it's the first child.
    // This is used to implement vector valued ansatz functions
    // for power nodes.
    static auto leafTreePath(const TP& tp)
    {
      if constexpr(Node::isLeaf)
        return tp;
      else if constexpr(Node::isPower and Node::ChildType::isLeaf)
        return Dune::TypeTree::push_back(tp, Dune::Indices::_0);
    }

    using LeafTreePath = decltype(leafTreePath(std::declval<TP>()));
    using LeafNode = typename Dune::TypeTree::ChildForTreePath<Tree, LeafTreePath>;
    using Basis = B;
    using TreePath = TP;

  public:

    using Element = typename Basis::LocalView::Element;

    FEOperatorBase(const Basis& basis, const TreePath& treePath, bool isAffine = true) :
      subspaceBasis_(basis, treePath),
      isAffine_(isAffine)
    {}

    class LocalOperator
    {
    protected:

      using TreeCache = ShapeFunctionCache<Tree>;
      using LeafNodeCache = ShapeFunctionCache<LeafNode>;

    public:

      using Element = typename FEOperatorBase::Element;

      LocalOperator(const SubspaceBasis& subspaceBasis, bool isAffine) :
        subspaceBasis_(subspaceBasis),
        leafTreePath_(leafTreePath(subspaceBasis_.prefixPath())),
        leafNode_(nullptr),
        localView_(nullptr),
        isAffine_(isAffine)
      {}

      LocalOperator(const FEOperatorBase& op) :
        LocalOperator(op.subspaceBasis_, op.isAffine_)
      {}

      void unbind()
      {}

      auto quadratureRuleKey() const
      {
        return quadratureRuleKey_;
      }

      template<class... LV>
      void registerLocalViews(const LV&... lvs)
      {
        Impl::visitMatchingLocalView([&](const auto& localView) {
          localView_ = &localView;
          leafNode_ = & Dune::TypeTree::child(localView.tree(), leafTreePath_);
        }, subspaceBasis_.rootBasis(), lvs...);
      }

      template<class CacheManager>
      void registerCaches(CacheManager& cacheManager)
      {
        const auto& tree = localView_->tree();
        cacheIndex_ = cacheManager.registerCache(UniqueCacheId(tree), TreeCache(tree));
        if (not isAffine_)
          cacheManager.template getCache<TreeCache>(cacheIndex_)[subspaceBasis_.prefixPath()].setNonAffine();
      }

    protected:

      const LeafNode& leafNode() const
      {
        return *leafNode_;
      }

      template<class CacheManager>
      auto& leafNodeCache(CacheManager& cacheManager)
      {
        return cacheManager.template getCache<TreeCache>(cacheIndex_)[leafTreePath_];
      }

      SubspaceBasis subspaceBasis_;
      const LeafTreePath leafTreePath_;
      const LeafNode* leafNode_;
      const LocalView* localView_;
      QuadratureRuleKey quadratureRuleKey_;
      std::size_t cacheIndex_;
      bool isAffine_;
    };

    auto basis() const
    {
      return std::tie(subspaceBasis_.rootBasis());
    }

    auto treePath() const
    {
      return std::tie(subspaceBasis_.prefixPath());
    }

    bool isAffine() const
    {
      return isAffine_;
    }

  protected:
    SubspaceBasis subspaceBasis_;
    bool isAffine_;
  };



  /**
   * \brief Linear map representing the elements of an FE-space
   *
   * \ingroup FormsDetail
   *
   * \tparam B Type of the dune-functions basis
   * \tparam TP Tree path within the basis
   * \tparam argIndex Index of arguments within composed multilinear operators
   */
  template<class B, class TP, std::size_t argIndex>
  class FEFunctionOperator : public FEOperatorBase<B, TP, argIndex>
  {
    using Base = FEOperatorBase<B, TP, argIndex>;
    using Node = typename Base::Node;
    using LeafLBTraits = typename Base::LeafNode::FiniteElement::Traits::LocalBasisType::Traits;

  public:

    using Element = typename Base::Element;
    using Range = std::conditional_t<
      Node::isLeaf,
      typename LeafLBTraits::RangeType,
      Dune::FieldVector<typename LeafLBTraits::RangeFieldType, Node::degree()>>;

    using Base::Base;

    class LocalOperator : public Base::LocalOperator
    {
      using Base::LocalOperator::quadratureRuleKey_;
      using Base::LocalOperator::leafNode;
      using Base::LocalOperator::leafNodeCache;
      using Base::LocalOperator::LocalOperator;

    public:

      using Range = typename FEFunctionOperator::Range;

      void bind(const Element&)
      {
        quadratureRuleKey_ = QuadratureRuleKey(leafNode().finiteElement());
      }

      template<class CacheManager>
      void bindToCaches(CacheManager& cacheManager)
      {
        valueCache_ = &(leafNodeCache(cacheManager).getValues());
      }

      template<class AugmentedLocalDomain>
      auto operator()(const AugmentedLocalDomain& x) const
      {
        using namespace Impl::Tensor;
        const auto& values = (*valueCache_)[x.index()];
        if constexpr(Node::isLeaf)
        {
          return LazyTensorBlock(
            [&](const auto& i) {
              return values[i];
            },
            Extents(leafNode().size()),
            Offsets(leafNode().localIndex(0))
          );
        }
        else if constexpr(Node::isPower and Node::ChildType::isLeaf)
          return LazyTensorBlock(
            [&](const auto& i) {
              auto result = Range(0);
              auto componentIndex = i / leafNode().size();
              auto shapeFunctionIndex = i % leafNode().size();
              result[componentIndex] = values[shapeFunctionIndex];
              return result;
            },
            Extents(leafNode().size()*Node::degree()),
            Offsets(leafNode().localIndex(0))
          );
      }

    private:
      const typename Base::LocalOperator::LeafNodeCache::ValueCache* valueCache_ = nullptr;
    };

    friend LocalOperator localOperator(const FEFunctionOperator& op)
    {
      return LocalOperator(op);
    }

    template<std::size_t i>
    auto childOperator(Dune::index_constant<i> childIndex) const
    {
      using Basis = typename Base::Basis;
      auto childTP = Dune::TypeTree::push_back(Base::subspaceBasis_.prefixPath(), childIndex);
      return FEFunctionOperator<Basis, decltype(childTP), argIndex>(Base::subspaceBasis_.rootBasis(), childTP, Base::isAffine_);
    }

  };




  /**
   * \brief Linear map representing the jacobians of the elements of an FE-space
   *
   * \ingroup FormsDetail
   *
   * \tparam B Type of the dune-functions basis
   * \tparam TP Tree path within the basis
   * \tparam argIndex Index of arguments within composed multilinear operators
   */
  template<class B, class TP, std::size_t argIndex>
  class FEFunctionJacobianOperator : public FEOperatorBase<B, TP, argIndex>
  {
    using Base = FEOperatorBase<B, TP, argIndex>;
    using Node = typename Base::Node;
    using LeafLBTraits = typename Base::LeafNode::FiniteElement::Traits::LocalBasisType::Traits;

  public:

    using Element = typename Base::Element;
    using Range = std::conditional_t<
      Node::isLeaf,
      typename LeafLBTraits::JacobianType,
      Dune::FieldMatrix<typename LeafLBTraits::RangeFieldType, Node::degree(), LeafLBTraits::dimDomain>>;

    using Base::Base;

    class LocalOperator : public Base::LocalOperator
    {
      using Base::LocalOperator::quadratureRuleKey_;
      using Base::LocalOperator::leafNode;
      using Base::LocalOperator::leafNodeCache;
      using Base::LocalOperator::LocalOperator;

    public:

      using Range = typename FEFunctionJacobianOperator::Range;

      void bind(const Element&)
      {
        quadratureRuleKey_ = QuadratureRuleKey(leafNode().finiteElement()).derivative();
      }

      template<class CacheManager>
      void bindToCaches(CacheManager& cacheManager)
      {
        globalJacobianCache_ = &(leafNodeCache(cacheManager).getGlobalJacobians());
      }

      template<class AugmentedLocalDomain>
      auto operator()(const AugmentedLocalDomain& x) const
      {
        using namespace Impl::Tensor;
        const auto& globalJacobians = (*globalJacobianCache_)[x.index()];
        if constexpr(Node::isLeaf)
          return LazyTensorBlock(
            [&](const auto& i) {
              return globalJacobians[i];
            },
            Extents(leafNode().size()),
            Offsets(leafNode().localIndex(0))
          );
        else if constexpr(Node::isPower and Node::ChildType::isLeaf)
          return LazyTensorBlock(
            [&](const auto& i) {
              auto result = Range(0);
              auto componentIndex = i / leafNode().size();
              auto shapeFunctionIndex = i % leafNode().size();
              result[componentIndex] = globalJacobians[shapeFunctionIndex][0];
              return result;
            },
            Extents(leafNode().size()*Node::degree()),
            Offsets(leafNode().localIndex(0))
          );
      }

    private:
      const typename Base::LocalOperator::LeafNodeCache::GlobalJacobianCache* globalJacobianCache_ = nullptr;
    };

    friend LocalOperator localOperator(const FEFunctionJacobianOperator& op)
    {
      return LocalOperator(op);
    }

    template<std::size_t i>
    auto childOperator(Dune::index_constant<i> childIndex) const
    {
      using Basis = typename Base::Basis;
      auto childTP = Dune::TypeTree::push_back(Base::subspaceBasis_.prefixPath(), childIndex);
      return FEFunctionJacobianOperator<Basis, decltype(childTP), argIndex>(Base::subspaceBasis_.rootBasis(), childTP, Base::isAffine_);
    }

  };



  /**
   * \brief Linear map representing the divergenc of the elements of an FE-space
   *
   * \ingroup FormsDetail
   *
   * \tparam B Type of the dune-functions basis
   * \tparam TP Tree path within the basis
   * \tparam argIndex Index of arguments within composed multilinear operators
   */
  template<class B, class TP, std::size_t argIndex>
  class FEFunctionDivergenceOperator : public FEOperatorBase<B, TP, argIndex>
  {
    using Base = FEOperatorBase<B, TP, argIndex>;
    using Node = typename Base::Node;
    using LeafLBTraits = typename Base::LeafNode::FiniteElement::Traits::LocalBasisType::Traits;

  public:

    using Element = typename Base::Element;
    using Range = std::conditional_t<
      Node::isLeaf,
      typename LeafLBTraits::RangeFieldType,
      typename LeafLBTraits::JacobianType::block_type>;

    using Base::Base;

    class LocalOperator : public Base::LocalOperator
    {
      using Base::LocalOperator::quadratureRuleKey_;
      using Base::LocalOperator::leafNode;
      using Base::LocalOperator::leafNodeCache;
      using Base::LocalOperator::LocalOperator;

    public:

      using Range = typename FEFunctionDivergenceOperator::Range;

      void bind(const Element&)
      {
        quadratureRuleKey_ = QuadratureRuleKey(leafNode().finiteElement()).derivative();
      }

      template<class CacheManager>
      void bindToCaches(CacheManager& cacheManager)
      {
        globalJacobianCache_ = &(leafNodeCache(cacheManager).getGlobalJacobians());
      }

      template<class AugmentedLocalDomain>
      auto operator()(const AugmentedLocalDomain& x) const
      {
        using namespace Impl::Tensor;
        using LocalBasisTraits = typename Base::LeafNode::FiniteElement::Traits::LocalBasisType::Traits;
        const auto& globalJacobians = (*globalJacobianCache_)[x.index()];
        if constexpr(Node::isLeaf)
          return LazyTensorBlock(
            [&](const auto& i) {
              using Field = typename LocalBasisTraits::RangeFieldType;
              static constexpr auto dimension = LocalBasisTraits::dimDomain;
              auto div = Field{0};
              for(std::size_t j=0; j<dimension; ++j)
                div += globalJacobians[i][j][j];
              return div;
            },
            Extents(leafNode().size()),
            Offsets(leafNode().localIndex(0))
          );
        else if constexpr(Node::isPower and Node::ChildType::isLeaf)
          return LazyTensorBlock(
            [&](const auto& i) {
              // Here we should compute the trace of the global Jacobian J
              // that we would need to build first. But since only the
              // componentIndex'th row of J is nonzero, we can simply
              // return the diagonal entry of this row which coincides
              // with the componentIndex'th entry of the gradient of
              // the respective scalar basis function.
              auto componentIndex = i / leafNode().size();
              auto shapeFunctionIndex = i % leafNode().size();
              return globalJacobians[shapeFunctionIndex][0][componentIndex];
            },
            Extents(leafNode().size()*Node::degree()),
            Offsets(leafNode().localIndex(0))
          );
      }

    private:
      const typename Base::LocalOperator::LeafNodeCache::GlobalJacobianCache* globalJacobianCache_ = nullptr;
    };

    friend LocalOperator localOperator(const FEFunctionDivergenceOperator& op)
    {
      return LocalOperator(op);
    }

  };



} // namespace Dune::Fufem::Forms



#endif // DUNE_FUFEM_FORMS_UNARYOPERATORS_HH
