// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_LOCALSUMASSEMBLER_HH
#define DUNE_FUFEM_FORMS_LOCALSUMASSEMBLER_HH

#include <type_traits>
#include <tuple>
#include <utility>

#include <dune/common/std/type_traits.hh>

#include <dune/fufem/forms/baseclass.hh>



namespace Dune::Fufem::Forms {



  template<class T>
  struct IsLocalAssembler : public std::false_type {};



  /**
   * \brief Sum of local assemblers obtained using Dune::Fufem::Forms::integrate(...)
   *
   * \ingroup FormsUserInterfaceAssemblers
   *
   * \tparam LocalAssemblers The summand assemblers
   *
   * This assembler chains multiple local assemblers for a form
   * of the same arity. While the assemblers must either be all
   * linear or all bilinear, it is fine to mix bulk and boundary
   * assemblers.
   *
   * LocalSumAssembler's are obtained when chaining local assembles
   * using operator+. It is also possible to chain LocalSumAssembler's.
   */
  template<class... LocalAssemblers>
  class LocalSumAssembler
  {

    // SFINAE expression check for preprocess() method of local assembler
    template <class LocalAssembler, class TestLocalView, class AnsatzLocalView>
    using LocalAssemblerBinaryPreprocess = decltype(std::declval<LocalAssembler>().preprocess(std::declval<TestLocalView>(), std::declval<AnsatzLocalView>()));

    // SFINAE expression check for preprocess() method of local assembler
    template <class LocalAssembler, class TestLocalView>
    using LocalAssemblerUnaryPreprocess = decltype(std::declval<LocalAssembler>().preprocess(std::declval<TestLocalView>()));

  public:

    LocalSumAssembler(const LocalAssemblers&... localAssemblers) :
      localAssemblers_(localAssemblers...)
    {}

    template<class TestLocalView, class AnsatzLocalView>
    void preprocess(const TestLocalView& testLocalView, const AnsatzLocalView& ansatzLocalView)
    {
      Impl::forEachTupleEntry(localAssemblers_, [&](auto& localAssembler) {
        if constexpr(Dune::Std::is_detected_v<LocalAssemblerBinaryPreprocess, std::decay_t<decltype(localAssembler)>, TestLocalView, AnsatzLocalView>)
          localAssembler.preprocess(testLocalView, ansatzLocalView);
      });
    }

    template<class LocalContext, class LocalMatrix, class TestLocalView, class AnsatzLocalView>
    void operator()(const LocalContext& localContext, LocalMatrix& localMatrix, const TestLocalView& testSubspaceLocalView, const AnsatzLocalView& ansatzSubspaceLocalView)
    {
      Impl::forEachTupleEntry(localAssemblers_, [&](auto& localAssembler) {
        localAssembler(localContext, localMatrix, testSubspaceLocalView, ansatzSubspaceLocalView);
      });
    }

    template<class TestLocalView>
    void preprocess(const TestLocalView& testLocalView)
    {
      Impl::forEachTupleEntry(localAssemblers_, [&](auto& localAssembler) {
        if constexpr(Dune::Std::is_detected_v<LocalAssemblerUnaryPreprocess, std::decay_t<decltype(localAssembler)>, TestLocalView>)
          localAssembler.preprocess(testLocalView);
      });
    }

    template<class LocalContext, class LocalVector, class TestLocalView>
    void operator()(const LocalContext& localContext, LocalVector& localVector, const TestLocalView& testSubspaceLocalView)
    {
      Impl::forEachTupleEntry(localAssemblers_, [&](auto& localAssembler) {
        localAssembler(localContext, localVector, testSubspaceLocalView);
      });
    }

    const auto& assemblers() const
    {
      return localAssemblers_;
    }

  private:
    std::tuple<LocalAssemblers...> localAssemblers_;
  };

  template<class... LocalAssemblers>
  struct IsLocalAssembler<LocalSumAssembler<LocalAssemblers...>> : public std::true_type {};



} // namespace Dune::Fufem::Forms



namespace Dune::Fufem::Forms::Impl {



  // ************************************************************
  // Implementation of sum(l,r).
  // ************************************************************

  template<class L, class R,
    std::enable_if_t<Dune::Fufem::Forms::IsLocalAssembler<L>::value and Dune::Fufem::Forms::IsLocalAssembler<R>::value, int> = 0>
  auto sum(const L& l, const R& r)
  {
    return LocalSumAssembler(l, r);
  }

  template<class... L, class R,
    std::enable_if_t<Dune::Fufem::Forms::IsLocalAssembler<R>::value, int> = 0>
  auto sum(const LocalSumAssembler<L...>& l, const R& r)
  {
    return std::apply([&](const auto&... li) {
      return LocalSumAssembler(li..., r);
    }, l.assemblers());
  }

  template<class L, class... R,
    std::enable_if_t<Dune::Fufem::Forms::IsLocalAssembler<L>::value, int> = 0>
  auto sum(const L& l, const LocalSumAssembler<R...>& r)
  {
    return std::apply([&](const auto&... ri) {
      return LocalSumAssembler(l, ri...);
    }, r.assemblers());
  }

  template<class... L, class... R>
  auto sum(const LocalSumAssembler<L...>& l, const LocalSumAssembler<R...>& r)
  {
    return std::apply([&](const auto&... li) {
      return std::apply([&](const auto&... ri) {
        return LocalSumAssembler(li..., ri...);
      }, r.assemblers());
    }, l.assemblers());
  }



} // Dune::Fufem::Forms::Impl



namespace Dune::Fufem::Forms {



  // If we activate this doxygen documentation, we do not see the other operator+.

  /*
   * \brief Create the sum of two local assemblers
   *
   * \ingroup FormsUserInterfaceArithmetic
   */
  template<class L, class R,
    std::enable_if_t<Dune::Fufem::Forms::IsLocalAssembler<L>::value and Dune::Fufem::Forms::IsLocalAssembler<R>::value, int> = 0>
  auto operator+ (const L& l, const R& r)
  {
    return Impl::sum(l, r);
  }



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_LOCALSUMASSEMBLER_HH
