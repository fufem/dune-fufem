// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_INTEGRATE_HH
#define DUNE_FUFEM_FORMS_INTEGRATE_HH

#include <type_traits>
#include <utility>

#include <dune/fufem/boundarypatch.hh>

#include <dune/fufem/forms/baseclass.hh>

#include <dune/fufem/forms/integratedlinearform.hh>
#include <dune/fufem/forms/integratedbilinearform.hh>
#include <dune/fufem/forms/integratedboundarylinearform.hh>
#include <dune/fufem/forms/integratedboundarybilinearform.hh>



namespace Dune::Fufem::Forms {



  /**
   * \brief Integrate a k-linear operator to obtain a k-linear form
   *
   * \ingroup FormsUserInterfaceAssemblers
   *
   * \param op The operator to integrate (should be a MultilinearOperator or SumOperator)
   *
   * When passing a k-linear operator, the returned object is a
   * local assembler for assembling the corresponding k-linear form.
   *
   * This assembler created by this overload will compute bulk integrals
   * over all elements of the underlying GridView.
   *
   * Local assemblers of the same arity can be chained using operator+.
   */
  template<class MultilinearOperator,
    std::enable_if_t<isOperatorOrSumOperator_v<MultilinearOperator>, int> = 0>
  auto integrate(MultilinearOperator op)
  {
    auto sumOperator = SumOperator(op);
    if constexpr(MultilinearOperator::arity==1)
      return IntegratedLinearForm(sumOperator);
    else if constexpr(MultilinearOperator::arity==2)
      return IntegratedBilinearForm(sumOperator);
  }

  /**
   * \brief Integrate a k-linear operator to obtain a k-linear form
   *
   * \ingroup FormsUserInterfaceAssemblers
   *
   * \param op The operator to integrate (should be a MultilinearOperator or SumOperator)
   * \param patch The BoundaryPatch to integrate over
   *
   * When passing a k-linear operator, the returned object is a
   * local assembler for assembling the corresponding k-linear form.
   *
   * This assembler created by this overload will compute boundary integrals
   * over all intersections contained in the BoundaryPatch.
   *
   * Local assemblers of the same arity can be chained using operator+.
   */
  template<class MultilinearOperator, class GridView,
    std::enable_if_t<isOperatorOrSumOperator_v<MultilinearOperator>, int> = 0>
  auto integrate(MultilinearOperator op, const BoundaryPatch<GridView>& patch)
  {
    auto sumOperator = SumOperator(op);
    if constexpr(MultilinearOperator::arity==1)
      return IntegratedBoundaryLinearForm(sumOperator, patch);
    else if constexpr(MultilinearOperator::arity==2)
      return IntegratedBoundaryBilinearForm(sumOperator, patch);
  }



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_INTEGRATE_HH
