// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_FORMS_INTEGRATEDLINEARFORM_HH
#define DUNE_FUFEM_FORMS_INTEGRATEDLINEARFORM_HH

#include <cstddef>
#include <type_traits>
#include <utility>

#include <dune/common/rangeutilities.hh>

#include <dune/typetree/childextraction.hh>

#include <dune/fufem/forms/shapefunctioncache.hh>
#include <dune/fufem/forms/baseclass.hh>
#include <dune/fufem/forms/localsumassembler.hh>



namespace Dune::Fufem::Forms {



  /**
   * \brief Local assembler corresponding to a bulk linear form
   *
   * \ingroup FormsUserInterfaceAssemblers
   *
   * \tparam LinearOperator A linear SumOperator to integrate
   *
   * This assembler computes bulk integrals over all elements
   * in the underlying GridView.
   *
   * Local assemblers of the same arity can be chained using operator+.
   */
  template<class LinearOperator>
  class IntegratedLinearForm
  {
    using TestRootBasis = std::decay_t<decltype(std::get<0>(std::declval<LinearOperator>().basis()))>;
    using TestRootLocalView = typename TestRootBasis::LocalView;
    using TestRootTree = typename TestRootLocalView::Tree;

    using LocalOperator = decltype(localOperator(std::declval<LinearOperator>()));

  public:
    using Element = typename LinearOperator::Element;

    IntegratedLinearForm(const LinearOperator& sumOperator) :
      sumOperator_(sumOperator),
      sumLocalOperator_(localOperator(sumOperator_))
    {}

    /**
     * \brief Register local view
     *
     * This has to be called once, before using the assembler.
     * The passed local view must be the same that is used
     integrandOperator calling the assembler for on an element afterwards.
     */
    template<class TestLocalView>
    void preprocess(const TestLocalView& testLocalView)
    {
      cacheManager_.clear();
      sumLocalOperator_.registerLocalViews(testLocalView.rootLocalView());
      sumLocalOperator_.registerCaches(cacheManager_.prototype());
    }

    template<class LocalVector, class TestLocalView>
    void operator()(const Element& element, LocalVector& localVector, const TestLocalView& testSubspaceLocalView)
    {
      const auto& geometry = element.geometry();

      sumLocalOperator_.bind(element);

      Impl::forEachTupleEntry(sumLocalOperator_.operators(), [&](auto& op) {
        cacheManager_[op.quadratureRuleKey()].invalidate();
      });

      Impl::forEachTupleEntry(sumLocalOperator_.operators(), [&](auto& op) {
        auto& cacheForRule = cacheManager_[op.quadratureRuleKey()];
        op.bindToCaches(cacheForRule);
        const auto& quadRule = cacheForRule.rule();
        for (auto k : Dune::range(quadRule.size()))
        {
          const auto& quadPoint = quadRule[k];
          const auto integrationWeight = quadPoint.weight() * geometry.integrationElement(quadPoint.position());
          auto evaluatedOperator = op(IndexedReference(quadPoint.position(), k));
          axpy(integrationWeight, evaluatedOperator, localVector);
        }
      });
    }

    const LinearOperator& integrandOperator() const
    {
      return sumOperator_;
    }

  private:
    const LinearOperator sumOperator_;
    LocalOperator sumLocalOperator_;
    MultipleQuadratureCacheManager<double, Element::dimension> cacheManager_;
  };



  template<class LinearOperator>
  struct IsLocalAssembler<IntegratedLinearForm<LinearOperator>> : public std::true_type {};



} // namespace Dune::Fufem::Forms


#endif // DUNE_FUFEM_FORMS_INTEGRATEDLINEARFORM_HH
