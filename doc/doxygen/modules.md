<!-- -*- tab-width: 4; indent-tabs-mode: nil -*- -->
# Modules

@defgroup Fufem Fufem
@brief A discretization module based on dune-function

@defgroup FufemBases Function space bases
@ingroup Fufem
@brief Utilities for handling function space bases

@defgroup Constraints Constraints
@ingroup Fufem
@brief Utilities for handling affine constraints

@defgroup Forms Form language
@ingroup Fufem
@brief  A framework for the convenient definition of linear and bilinear forms

@defgroup FormsUserInterface Form language (user interface)
@ingroup Forms
@brief User interface for defining linear and bilinear forms

This contains the user interface classes and functions
for defining linear and bilinear forms.
A more detailed description of the user interface
is given in the Dune-Fufem manual together
with an overview of the underlying concepts
and some examples. The manual is contained in the
the `dune-fufem/doc/manual` subdirectory and can be build using `make doc`.
Further examples can be found in the `dune-fufem/examples` directory.

@defgroup FormsUserInterfaceElementary Form language (elementary operators)
@ingroup FormsUserInterface
@brief Elementary operators (test functions, trial functions, coefficients, ...)

@defgroup FormsUserInterfaceDifferentialOperators Form language (differential operators)
@ingroup FormsUserInterface
@brief Differential operators

@defgroup FormsUserInterfaceArithmetic Form language (arithmetic operations)
@ingroup FormsUserInterface
@brief Products, sums, differences, ... of operators and assemblers

@defgroup FormsUserInterfaceLinearOperators Form language (pointwise linear operators)
@ingroup FormsUserInterface
@brief Pointwise defined linear operators

@defgroup FormsUserInterfaceNonLinearOperators Form language (pointwise nonlinear operators)
@ingroup FormsUserInterface
@brief Pointwise defined nonlinear operators

@defgroup FormsUserInterfaceAssemblers Form language (integration and assembly)
@ingroup FormsUserInterface
@brief Definition of (bi)-linear form assemblers by integration

@defgroup FormsDetail Form language (low level)
@ingroup Forms
@brief Low level implementation classes for the form language

