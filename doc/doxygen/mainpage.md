<!-- vi: set ft=mkd ts=8 sw=2 et sts=2: -->
# The Dune-Fufem module

## Scope of the module
@todo Please doc me!

### Handling of boundary patches
@todo Please doc me!

### Global assemblers
@todo Please doc me!

### Form language for defining local assemblers
@todo Please doc me!

For more details refer to the @ref FormsUserInterface section.

## Documentation

### Class documentation
The module contains a class documentation which can be build using [doxygen].
After the module has been build, you can build the documentation using
`make doc`.

### Manual
There is also a work in progress document describing the module.
Like the class documentation this is build on `make doc`.

### Examples
Several example applications demonstrate how to use the module. These
example applications are contained in the `examples/` directory and
build when building the module. The definition of local assemblers
is also described in detail in the manual (see above)
for several examples.

## Using dune-fufem and licensing
@todo Please doc me!


## Building dune-fufem

### Dependencies
Dune-fufem depends on the dune [core modules][core] and the
extension modules
[dune-typetree][dune-typetree],
[dune-function][dune-functions],
[dune-uggrid][dune-uggrid],
[dune-vtk][dune-vtk], and
[dune-matrix-vector][dune-matrix-vector].
All of them are available using git:

* https://gitlab.dune-project.org/core/dune-common
* https://gitlab.dune-project.org/core/dune-geometry
* https://gitlab.dune-project.org/core/dune-grid
* https://gitlab.dune-project.org/core/dune-istl
* https://gitlab.dune-project.org/core/dune-localfunctions
* https://gitlab.dune-project.org/staging/dune-typetree
* https://gitlab.dune-project.org/staging/dune-functions
* https://gitlab.dune-project.org/staging/dune-uggrid
* https://gitlab.dune-project.org/extensions/dune-vtk
* https://gitlab.dune-project.org/fufem/dune-matrix-vector

The versioning of dune-fufem follows the scheme used in the core modules.
I.e. version x.y of dune-fufem will depend on version x.y of its dependency
modules. Analogously, the _master_ branch will depend on the _master_ branch of these modules.

Unless explicitly stated otherwise for a specific version,
dune-fufem supports/requires the same build tools (compilers, cmake)
as the corresponding version of the core modules.

### Building the module
Dune-fufem integrates into the cmake-based dune build system.
Hence it can be build (like any other module) using the `dunecontrol` script
provided by the core modules. For details on how to use this build system
and how to specify build options have a look at the documentation in the
dune-common module.


[core]: https://dune-project.org/groups/core
[dune-typetree]: https://gitlab.dune-project.org/staging/dune-typetree
[dune-functions]: https://gitlab.dune-project.org/staging/dune-functions
[dune-uggrid]: https://gitlab.dune-project.org/staging/dune-uggrid
[dune-vtk]: https://gitlab.dune-project.org/extensions/dune-vtk
[dune-matrix-vector]: https://gitlab.dune-project.org/fufem/dune-matrix-vector
[dune docs]: https://dune-project.org/doxygen
[doxygen]: http://www.stack.nl/~dimitri/doxygen/
