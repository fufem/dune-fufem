#!/bin/python3

import os
import yaml
import sys
import subprocess



# Traverse directory tree upwards until we find a dune.module file
modulePath = os.path.realpath(os.path.curdir)
while not os.path.exists(modulePath+"/dune.module"):
  parentPath = os.path.realpath(modulePath+"/..")
  if parentPath==modulePath:
    print("Error: No dune.module file found in ancestor directories")
    sys.exit(1)
  modulePath = parentPath
print("Module base directory is              : "+modulePath)

# Parse codespell command from .gitlab-ci.yml
try:
  with open(modulePath+"/.gitlab-ci.yml", "r") as f:
    tree = yaml.safe_load(f)
    command = tree["code-spelling-check"]["script"][0]
    command += " -S .git"
except:
  print("Error: Could not parse codespell command from .gitlab-ci.yml")
  sys.exit(1)
print("Codespell command from .gitlab-ci.yml : "+command)

if len(sys.argv[1:]) > 0:
  print("Additional options from command line  : "+(" ".join(sys.argv[1:])))

# Execute codespell command
try:
  print("Running codespell in module directory")
  subprocess.run(command.split(' ')+sys.argv[1:], cwd=modulePath)
except:
  print("Error: Failed running codespell")
  sys.exit(1)
